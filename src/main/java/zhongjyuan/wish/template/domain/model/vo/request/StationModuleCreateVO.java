package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName StationModuleCreateVO
 * @Description 岗位模块关联创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StationModuleCreateVO", description = "岗位模块关联创建对象")
public class StationModuleCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields stationId: 岗位主键
	 */
	// @NotNull(message = "岗位主键不能为空.")
	// @NotBlank(message = "岗位主键不能为空.")
	// @Size(message = "岗位主键最大长度为50.", max = 50)
	// @Min(message = "岗位主键最小为600.", value = 600)
	@ApiModelProperty(value = "岗位主键", name = "stationId", dataType = "Long")
    private Long stationId;

	/**
	 * @Fields stationCode: 岗位编码
	 */
	// @NotNull(message = "岗位编码不能为空.")
	// @NotBlank(message = "岗位编码不能为空.")
	// @Size(message = "岗位编码最大长度为50.", max = 50)
	// @Min(message = "岗位编码最小为600.", value = 600)
	@ApiModelProperty(value = "岗位编码", name = "stationCode", dataType = "String")
    private String stationCode;

	/**
	 * @Fields stationName: 岗位名称
	 */
	// @NotNull(message = "岗位名称不能为空.")
	// @NotBlank(message = "岗位名称不能为空.")
	// @Size(message = "岗位名称最大长度为50.", max = 50)
	// @Min(message = "岗位名称最小为600.", value = 600)
	@ApiModelProperty(value = "岗位名称", name = "stationName", dataType = "String")
    private String stationName;

	/**
	 * @Fields moduleId: 模块主键
	 */
	// @NotNull(message = "模块主键不能为空.")
	// @NotBlank(message = "模块主键不能为空.")
	// @Size(message = "模块主键最大长度为50.", max = 50)
	// @Min(message = "模块主键最小为600.", value = 600)
	@ApiModelProperty(value = "模块主键", name = "moduleId", dataType = "Long")
    private Long moduleId;

	/**
	 * @Fields moduleCode: 模块编码
	 */
	// @NotNull(message = "模块编码不能为空.")
	// @NotBlank(message = "模块编码不能为空.")
	// @Size(message = "模块编码最大长度为50.", max = 50)
	// @Min(message = "模块编码最小为600.", value = 600)
	@ApiModelProperty(value = "模块编码", name = "moduleCode", dataType = "String")
    private String moduleCode;

	/**
	 * @Fields moduleName: 模块名称
	 */
	// @NotNull(message = "模块名称不能为空.")
	// @NotBlank(message = "模块名称不能为空.")
	// @Size(message = "模块名称最大长度为50.", max = 50)
	// @Min(message = "模块名称最小为600.", value = 600)
	@ApiModelProperty(value = "模块名称", name = "moduleName", dataType = "String")
    private String moduleName;

	/**
	 * @Fields status: 状态
	 */
	// @NotNull(message = "状态不能为空.")
	// @NotBlank(message = "状态不能为空.")
	// @Size(message = "状态最大长度为50.", max = 50)
	// @Min(message = "状态最小为600.", value = 600)
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

}