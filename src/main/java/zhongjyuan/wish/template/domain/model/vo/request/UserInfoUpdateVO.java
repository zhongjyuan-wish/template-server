package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName UserInfoUpdateVO
 * @Description 用户信息更新对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserInfoUpdateVO", description = "用户信息更新对象")
public class UserInfoUpdateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	// @NotNull(message = "主键不能为空.")
	// @NotBlank(message = "主键不能为空.")
	// @Size(message = "主键最大长度为50.", max = 50)
	// @Min(message = "主键最小为600.", value = 600)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields name: 名称
	 */
	// @NotNull(message = "名称不能为空.")
	// @NotBlank(message = "名称不能为空.")
	// @Size(message = "名称最大长度为50.", max = 50)
	// @Min(message = "名称最小为600.", value = 600)
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields account: 账号
	 */
	// @NotNull(message = "账号不能为空.")
	// @NotBlank(message = "账号不能为空.")
	// @Size(message = "账号最大长度为50.", max = 50)
	// @Min(message = "账号最小为600.", value = 600)
	@ApiModelProperty(value = "账号", name = "account", dataType = "String")
    private String account;

	/**
	 * @Fields aliasName: 别称
	 */
	// @NotNull(message = "别称不能为空.")
	// @NotBlank(message = "别称不能为空.")
	// @Size(message = "别称最大长度为50.", max = 50)
	// @Min(message = "别称最小为600.", value = 600)
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields face: 头像
	 */
	// @NotNull(message = "头像不能为空.")
	// @NotBlank(message = "头像不能为空.")
	// @Size(message = "头像最大长度为50.", max = 50)
	// @Min(message = "头像最小为600.", value = 600)
	@ApiModelProperty(value = "头像", name = "face", dataType = "String")
    private String face;

	/**
	 * @Fields email: 电子邮箱
	 */
	// @NotNull(message = "电子邮箱不能为空.")
	// @NotBlank(message = "电子邮箱不能为空.")
	// @Size(message = "电子邮箱最大长度为50.", max = 50)
	// @Min(message = "电子邮箱最小为600.", value = 600)
	@ApiModelProperty(value = "电子邮箱", name = "email", dataType = "String")
    private String email;

	/**
	 * @Fields mobile: 移动号码
	 */
	// @NotNull(message = "移动号码不能为空.")
	// @NotBlank(message = "移动号码不能为空.")
	// @Size(message = "移动号码最大长度为50.", max = 50)
	// @Min(message = "移动号码最小为600.", value = 600)
	@ApiModelProperty(value = "移动号码", name = "mobile", dataType = "String")
    private String mobile;

	/**
	 * @Fields telephone: 固定号码
	 */
	// @NotNull(message = "固定号码不能为空.")
	// @NotBlank(message = "固定号码不能为空.")
	// @Size(message = "固定号码最大长度为50.", max = 50)
	// @Min(message = "固定号码最小为600.", value = 600)
	@ApiModelProperty(value = "固定号码", name = "telephone", dataType = "String")
    private String telephone;

	/**
	 * @Fields qq: QQ号码
	 */
	// @NotNull(message = "QQ号码不能为空.")
	// @NotBlank(message = "QQ号码不能为空.")
	// @Size(message = "QQ号码最大长度为50.", max = 50)
	// @Min(message = "QQ号码最小为600.", value = 600)
	@ApiModelProperty(value = "QQ号码", name = "qq", dataType = "String")
    private String qq;

	/**
	 * @Fields weibo: 新浪微博
	 */
	// @NotNull(message = "新浪微博不能为空.")
	// @NotBlank(message = "新浪微博不能为空.")
	// @Size(message = "新浪微博最大长度为50.", max = 50)
	// @Min(message = "新浪微博最小为600.", value = 600)
	@ApiModelProperty(value = "新浪微博", name = "weibo", dataType = "String")
    private String weibo;

	/**
	 * @Fields wechat: 微信号码
	 */
	// @NotNull(message = "微信号码不能为空.")
	// @NotBlank(message = "微信号码不能为空.")
	// @Size(message = "微信号码最大长度为50.", max = 50)
	// @Min(message = "微信号码最小为600.", value = 600)
	@ApiModelProperty(value = "微信号码", name = "wechat", dataType = "String")
    private String wechat;

	/**
	 * @Fields signature: 签名
	 */
	// @NotNull(message = "签名不能为空.")
	// @NotBlank(message = "签名不能为空.")
	// @Size(message = "签名最大长度为50.", max = 50)
	// @Min(message = "签名最小为600.", value = 600)
	@ApiModelProperty(value = "签名", name = "signature", dataType = "String")
    private String signature;

	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}