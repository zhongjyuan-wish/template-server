package zhongjyuan.wish.template.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zhongjyuan.wish.template.domain.model.entity.RoleModulePermissionDO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * @ClassName RoleModulePermissionMapper
 * @Description 角色模块权限关联表Mapper接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Mapper
public interface RoleModulePermissionMapper extends BaseMapper<RoleModulePermissionDO> {

	/**
	 * @Title selectMaxRowIndex
	 * @Author zhongjyuan
	 * @Description 查询最大行号
	 * @Param @return
	 * @Return Integer 最大行号
	 * @Throws
	 */
	@Select("SELECT IFNULL(MAX(ROW_INDEX),0) FROM sys_role_module_permission;")
	Integer selectMaxRowIndex();
	
}
