CREATE PROCEDURE `proc_${table.name}_children_nodes`(IN type INT,
	IN showLevel INT,
	IN showSiblings INT,
	IN showHitNodes INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点下所有子节点(包含路径上的枝干节点和叶子节点)'
BEGIN
		
	DECLARE childLevel INT;
	DECLARE whileIndex INT;
	<#noparse># 返回子节点结果集</#noparse>
	DECLARE childIdList LONGTEXT;
	<#noparse># 临时存放节点</#noparse>
	DECLARE tempIdList LONGTEXT;
	<#noparse># 临时存放子节点</#noparse>
	DECLARE tempChildIdList LONGTEXT;

	SET childIdList = '';
	SET childLevel = showLevel;
	SET tempChildIdList = hitNodeIds;

	SET whileIndex = 0;
	<#noparse># 循环，用于查询节点下所有的子节点</#noparse>
	WHILE tempChildIdList IS NOT NULL AND whileIndex <= showLevel DO

		<#noparse># 存入到返回结果中</#noparse>
		SET childIdList = CONCAT_WS(',', childIdList, tempChildIdList);

		<#noparse># 查询节点下所有子节点</#noparse>
		SELECT GROUP_CONCAT(id) INTO tempChildIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, tempChildIdList) > 0;

		SET whileIndex = whileIndex + 1;
	END WHILE;

	<#noparse># 是否加载对应兄弟节点(不包含命中节点的子级兄弟节点)</#noparse>
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND childIdList != CONCAT(',', hitNodeIds) THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, childIdList);

		SET childIdList = CONCAT_WS(',', childIdList, tempIdList);

	END IF;

	<#noparse># 是否包含命中节点</#noparse>
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND childIdList != CONCAT(',', hitNodeIds) THEN

		SET childIdList = REPLACE(REPLACE(childIdList, hitNodeIds, ''), ',,', ',');

	END IF;

	<#noparse># 返回结果[主键字符串]</#noparse>
	IF type IS NULL OR type = 0 THEN
		SELECT SUBSTRING(childIdList, 2) AS childIds;
	END IF;

	<#noparse># 返回结果[数据集]</#noparse>
	IF type IS NOT NULL AND type = 1 THEN
		SELECT * FROM ${table.name} A
		WHERE A.is_deleted = 0
		AND FIND_IN_SET(A.id, SUBSTRING(childIdList, 2))
		ORDER BY A.outline;
	END IF;
	<#noparse>
	# create by zhongjyuan
	</#noparse>
END