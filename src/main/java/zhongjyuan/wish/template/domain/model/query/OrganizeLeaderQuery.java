package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName OrganizeLeaderQuery
 * @Description 组织责任人关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizeLeaderQuery", description = "组织责任人关联查询条件对象")
public class OrganizeLeaderQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields organizeId: 组织主键
	 */
	@ApiModelProperty(value = "组织主键", name = "organizeId", dataType = "Long")
    private Long organizeId;

	/**
	 * @Fields organizeNo: 组织编号
	 */
	@ApiModelProperty(value = "组织编号", name = "organizeNo", dataType = "String")
    private String organizeNo;

	/**
	 * @Fields organizeName: 组织名称
	 */
	@ApiModelProperty(value = "组织名称", name = "organizeName", dataType = "String")
    private String organizeName;

	/**
	 * @Fields type: 类型[Leader:责任人;Header:分管领导;Functional:职能上级]
	 */
	@ApiModelProperty(value = "类型[Leader:责任人;Header:分管领导;Functional:职能上级]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields userId: 用户主键
	 */
	@ApiModelProperty(value = "用户主键", name = "userId", dataType = "Long")
    private Long userId;

	/**
	 * @Fields userName: 用户名称
	 */
	@ApiModelProperty(value = "用户名称", name = "userName", dataType = "String")
    private String userName;

	/**
	 * @Fields userAccount: 用户账号
	 */
	@ApiModelProperty(value = "用户账号", name = "userAccount", dataType = "String")
    private String userAccount;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}