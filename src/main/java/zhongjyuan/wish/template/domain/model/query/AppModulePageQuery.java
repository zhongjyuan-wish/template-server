package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.PageQuery;

/**
 * @ClassName AppModulePageQuery
 * @Description 应用模块关联分页查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppModulePageQuery", description = "应用模块关联分页查询条件对象")
public class AppModulePageQuery extends PageQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields appId: 应用主键
	 */
	@ApiModelProperty(value = "应用主键", name = "appId", dataType = "Long")
    private Long appId;

	/**
	 * @Fields appCode: 应用编码
	 */
	@ApiModelProperty(value = "应用编码", name = "appCode", dataType = "String")
    private String appCode;

	/**
	 * @Fields appName: 应用名称
	 */
	@ApiModelProperty(value = "应用名称", name = "appName", dataType = "String")
    private String appName;

	/**
	 * @Fields moduleId: 模块主键
	 */
	@ApiModelProperty(value = "模块主键", name = "moduleId", dataType = "Long")
    private Long moduleId;

	/**
	 * @Fields moduleCode: 模块编码
	 */
	@ApiModelProperty(value = "模块编码", name = "moduleCode", dataType = "String")
    private String moduleCode;

	/**
	 * @Fields moduleName: 模块名称
	 */
	@ApiModelProperty(value = "模块名称", name = "moduleName", dataType = "String")
    private String moduleName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}