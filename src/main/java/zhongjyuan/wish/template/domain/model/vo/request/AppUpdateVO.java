package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName AppUpdateVO
 * @Description 应用更新对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppUpdateVO", description = "应用更新对象")
public class AppUpdateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	// @NotNull(message = "主键不能为空.")
	// @NotBlank(message = "主键不能为空.")
	// @Size(message = "主键最大长度为50.", max = 50)
	// @Min(message = "主键最小为600.", value = 600)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
	// @NotNull(message = "编码不能为空.")
	// @NotBlank(message = "编码不能为空.")
	// @Size(message = "编码最大长度为50.", max = 50)
	// @Min(message = "编码最小为600.", value = 600)
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	// @NotNull(message = "名称不能为空.")
	// @NotBlank(message = "名称不能为空.")
	// @Size(message = "名称最大长度为50.", max = 50)
	// @Min(message = "名称最小为600.", value = 600)
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields group: 分组
	 */
	// @NotNull(message = "分组不能为空.")
	// @NotBlank(message = "分组不能为空.")
	// @Size(message = "分组最大长度为50.", max = 50)
	// @Min(message = "分组最小为600.", value = 600)
	@ApiModelProperty(value = "分组", name = "group", dataType = "Long")
    private Long group;

	/**
	 * @Fields type: 类型[PC;Mobile;Pad]
	 */
	// @NotNull(message = "类型[PC;Mobile;Pad]不能为空.")
	// @NotBlank(message = "类型[PC;Mobile;Pad]不能为空.")
	// @Size(message = "类型[PC;Mobile;Pad]最大长度为50.", max = 50)
	// @Min(message = "类型[PC;Mobile;Pad]最小为600.", value = 600)
	@ApiModelProperty(value = "类型[PC;Mobile;Pad]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
	// @NotNull(message = "图标不能为空.")
	// @NotBlank(message = "图标不能为空.")
	// @Size(message = "图标最大长度为50.", max = 50)
	// @Min(message = "图标最小为600.", value = 600)
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields blank: 打开方式
	 */
	// @NotNull(message = "打开方式不能为空.")
	// @NotBlank(message = "打开方式不能为空.")
	// @Size(message = "打开方式最大长度为50.", max = 50)
	// @Min(message = "打开方式最小为600.", value = 600)
	@ApiModelProperty(value = "打开方式", name = "blank", dataType = "String")
    private String blank;

	/**
	 * @Fields indexUrl: 首页地址
	 */
	// @NotNull(message = "首页地址不能为空.")
	// @NotBlank(message = "首页地址不能为空.")
	// @Size(message = "首页地址最大长度为50.", max = 50)
	// @Min(message = "首页地址最小为600.", value = 600)
	@ApiModelProperty(value = "首页地址", name = "indexUrl", dataType = "String")
    private String indexUrl;

	/**
	 * @Fields keyt: 密钥
	 */
	// @NotNull(message = "密钥不能为空.")
	// @NotBlank(message = "密钥不能为空.")
	// @Size(message = "密钥最大长度为50.", max = 50)
	// @Min(message = "密钥最小为600.", value = 600)
	@ApiModelProperty(value = "密钥", name = "keyt", dataType = "String")
    private String keyt;

	/**
	 * @Fields access: 授权码
	 */
	// @NotNull(message = "授权码不能为空.")
	// @NotBlank(message = "授权码不能为空.")
	// @Size(message = "授权码最大长度为50.", max = 50)
	// @Min(message = "授权码最小为600.", value = 600)
	@ApiModelProperty(value = "授权码", name = "access", dataType = "String")
    private String access;

	/**
	 * @Fields refresh: 令牌刷新时间
	 */
	// @NotNull(message = "令牌刷新时间不能为空.")
	// @NotBlank(message = "令牌刷新时间不能为空.")
	// @Size(message = "令牌刷新时间最大长度为50.", max = 50)
	// @Min(message = "令牌刷新时间最小为600.", value = 600)
	@ApiModelProperty(value = "令牌刷新时间", name = "refresh", dataType = "Integer")
    private Integer refresh;


	/**
	 * @Fields isBusiness: 是否业务
	 */
	// @NotNull(message = "是否业务不能为空.")
	// @NotBlank(message = "是否业务不能为空.")
	// @Size(message = "是否业务最大长度为50.", max = 50)
	// @Min(message = "是否业务最小为600.", value = 600)
	@ApiModelProperty(value = "是否业务", name = "isBusiness", dataType = "Boolean")
    private Boolean isBusiness;

	/**
	 * @Fields isExternal: 是否外部
	 */
	// @NotNull(message = "是否外部不能为空.")
	// @NotBlank(message = "是否外部不能为空.")
	// @Size(message = "是否外部最大长度为50.", max = 50)
	// @Min(message = "是否外部最小为600.", value = 600)
	@ApiModelProperty(value = "是否外部", name = "isExternal", dataType = "Boolean")
    private Boolean isExternal;



	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}