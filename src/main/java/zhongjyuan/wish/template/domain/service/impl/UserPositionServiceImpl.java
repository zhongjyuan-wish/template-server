package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserPositionMapper;
import zhongjyuan.wish.template.domain.service.IUserPositionService;

import zhongjyuan.wish.template.domain.model.entity.UserPositionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserPositionCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserPositionServiceImpl
 * @Description 用户职位关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserPositionServiceImpl implements IUserPositionService {

	@Autowired
	UserPositionMapper userPositionMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserPositionDO create(UserPositionDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userPositionMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userPositionMapper.insert(entity);

		log.info("======UserPositionServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserPositionDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserPositionDO update(UserPositionDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userPositionMapper.updateById(entity);

		log.info("======UserPositionServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserPositionDO 实体对象
	 * @Throws
	 */
	private UserPositionDO updateEnabled(UserPositionDO entity) {

		// 获取原始数据
		UserPositionDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserPositionCode.USERPOSITION_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserPositionDO delete(Long id) {

		// 获取原始数据
		UserPositionDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userPositionMapper.updateById(originEntity);

		log.info("======UserPositionServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserPositionDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserPositionDO> entities = new ArrayList<UserPositionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserPositionDO> queryWrapper = new QueryWrapper<UserPositionDO>();
		queryWrapper.lambda().in(UserPositionDO::getId, ids);
		entities = userPositionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserPositionDO> successEntities = new ArrayList<UserPositionDO>();
		
		// 根据主键更新
		for (UserPositionDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userPositionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserPositionServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserPositionDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserPositionCode.USERPOSITION_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserPositionDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userPositionMapper.selectById(id);
	}
	
	@Override
	public List<UserPositionDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userPositionMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserPositionVO> selectList(UserPositionQuery query) {
		List<UserPositionVO> vos = new ArrayList<UserPositionVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserPositionDO> queryWrapper = new QueryWrapper<UserPositionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserPositionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserPositionDO> entities = userPositionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserPositionConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserPositionBriefVO> selectBriefList(UserPositionBriefQuery query) {
		List<UserPositionBriefVO> briefVOs = new ArrayList<UserPositionBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserPositionDO> queryWrapper = new QueryWrapper<UserPositionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserPositionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserPositionDO> entities = userPositionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserPositionConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserPositionPageVO> selectPage(UserPositionPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserPositionDO> queryWrapper = new QueryWrapper<UserPositionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserPositionDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserPositionDO> page = userPositionMapper.selectPage(new CustomPage<UserPositionDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserPositionConverter.entityToPageVos(entities));
	}

}
