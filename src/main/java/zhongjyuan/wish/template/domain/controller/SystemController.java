package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.ISystemService;

import zhongjyuan.wish.template.domain.model.entity.SystemDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName SystemController
 * @Description 系统管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "系统管理", tags = "系统管理")
@RequestMapping("{version}/systems")
public class SystemController {

	@Autowired
	ISystemService systemService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> create(@RequestBody @Valid SystemCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		SystemDO entity = SystemConverter.createVoToEntity(createVO);
		entity = systemService.create(entity);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> update(@RequestBody @Valid SystemUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		SystemDO entity = SystemConverter.updateVoToEntity(updateVO);
		entity = systemService.update(entity);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<SystemDO> entities = systemService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(SystemConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<SystemDO> entities = systemService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(SystemConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> delete(@PathVariable("id") Long id) {
		SystemDO entity = systemService.delete(id);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> deleteByCode(@PathVariable("code") String code) {
		SystemDO entity = systemService.deleteByCode(code);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> enable(@PathVariable("id") Long id) {
		SystemDO entity = systemService.select(id);
		entity.setIsEnabled(true);
		entity = systemService.update(entity);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> enableByCode(@PathVariable("code") String code) {
		SystemDO entity = systemService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = systemService.update(entity);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> disable(@PathVariable("id") Long id) {
		SystemDO entity = systemService.select(id);
		entity.setIsEnabled(false);
		entity = systemService.update(entity);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "系统", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> disableByCode(@PathVariable("code") String code) {
		SystemDO entity = systemService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = systemService.update(entity);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<SystemDO> entities = systemService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(SystemConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<SystemDO> entities = systemService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(SystemConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> select(@PathVariable("id") Long id) {
		SystemDO entity = systemService.select(id);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemVO> selectByCode(@PathVariable("code") String code) {
		SystemDO entity = systemService.selectByCode(code);
		return ResponseResult.success(SystemConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemVO>> selectList(@Valid SystemQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(systemService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<SystemDO> entities = systemService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(SystemConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemBriefVO> selectBrief(@PathVariable("id") Long id) {
		SystemDO entity = systemService.select(id);
		return ResponseResult.success(SystemConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<SystemBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		SystemDO entity = systemService.selectByCode(code);
		return ResponseResult.success(SystemConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<SystemBriefVO>> selectBriefList(@Valid SystemBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(systemService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "系统", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页系统 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<SystemPageVO>> selectPage(@Valid SystemPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(systemService.selectPage(query));
	}
	
}
