package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName ModulePermissionBriefQuery
 * @Description 模块权限关联简要查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ModulePermissionBriefQuery", description = "模块权限关联简要查询条件对象")
public class ModulePermissionBriefQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields moduleId: 模块主键
	 */
	@ApiModelProperty(value = "模块主键", name = "moduleId", dataType = "Long")
    private Long moduleId;

	/**
	 * @Fields moduleCode: 模块编码
	 */
	@ApiModelProperty(value = "模块编码", name = "moduleCode", dataType = "String")
    private String moduleCode;

	/**
	 * @Fields moduleName: 模块名称
	 */
	@ApiModelProperty(value = "模块名称", name = "moduleName", dataType = "String")
    private String moduleName;

	/**
	 * @Fields permissionId: 权限主键
	 */
	@ApiModelProperty(value = "权限主键", name = "permissionId", dataType = "Long")
    private Long permissionId;

	/**
	 * @Fields permissionCode: 权限编码
	 */
	@ApiModelProperty(value = "权限编码", name = "permissionCode", dataType = "String")
    private String permissionCode;

	/**
	 * @Fields permissionName: 权限名称
	 */
	@ApiModelProperty(value = "权限名称", name = "permissionName", dataType = "String")
    private String permissionName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}