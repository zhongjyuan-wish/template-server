package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.OrganizeRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IOrganizeRoleService
 * @Description 组织角色关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IOrganizeRoleService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	OrganizeRoleDO create(OrganizeRoleDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	OrganizeRoleDO update(OrganizeRoleDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	OrganizeRoleDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<OrganizeRoleDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeRoleDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	OrganizeRoleDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<OrganizeRoleDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeRoleDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<OrganizeRoleVO> VO对象集合
	 * @Throws
	 */
	List<OrganizeRoleVO> selectList(OrganizeRoleQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<OrganizeRoleBriefVO> 简要对象集合
	 * @Throws
	 */
	List<OrganizeRoleBriefVO> selectBriefList(OrganizeRoleBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<OrganizeRolePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<OrganizeRolePageVO> selectPage(OrganizeRolePageQuery query);

}
