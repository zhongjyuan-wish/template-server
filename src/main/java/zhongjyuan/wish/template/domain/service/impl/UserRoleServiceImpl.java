package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserRoleMapper;
import zhongjyuan.wish.template.domain.service.IUserRoleService;

import zhongjyuan.wish.template.domain.model.entity.UserRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserRoleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserRoleServiceImpl
 * @Description 用户角色关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserRoleServiceImpl implements IUserRoleService {

	@Autowired
	UserRoleMapper userRoleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserRoleDO create(UserRoleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userRoleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userRoleMapper.insert(entity);

		log.info("======UserRoleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserRoleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserRoleDO update(UserRoleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userRoleMapper.updateById(entity);

		log.info("======UserRoleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	private UserRoleDO updateEnabled(UserRoleDO entity) {

		// 获取原始数据
		UserRoleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserRoleCode.USERROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserRoleDO delete(Long id) {

		// 获取原始数据
		UserRoleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userRoleMapper.updateById(originEntity);

		log.info("======UserRoleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserRoleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserRoleDO> entities = new ArrayList<UserRoleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserRoleDO> queryWrapper = new QueryWrapper<UserRoleDO>();
		queryWrapper.lambda().in(UserRoleDO::getId, ids);
		entities = userRoleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserRoleDO> successEntities = new ArrayList<UserRoleDO>();
		
		// 根据主键更新
		for (UserRoleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userRoleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserRoleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserRoleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserRoleCode.USERROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserRoleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userRoleMapper.selectById(id);
	}
	
	@Override
	public List<UserRoleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userRoleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserRoleVO> selectList(UserRoleQuery query) {
		List<UserRoleVO> vos = new ArrayList<UserRoleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserRoleDO> queryWrapper = new QueryWrapper<UserRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserRoleDO> entities = userRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserRoleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserRoleBriefVO> selectBriefList(UserRoleBriefQuery query) {
		List<UserRoleBriefVO> briefVOs = new ArrayList<UserRoleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserRoleDO> queryWrapper = new QueryWrapper<UserRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserRoleDO> entities = userRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserRoleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserRolePageVO> selectPage(UserRolePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserRoleDO> queryWrapper = new QueryWrapper<UserRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserRoleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserRoleDO> page = userRoleMapper.selectPage(new CustomPage<UserRoleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserRoleConverter.entityToPageVos(entities));
	}

}
