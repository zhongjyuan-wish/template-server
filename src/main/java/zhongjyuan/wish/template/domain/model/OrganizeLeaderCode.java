package zhongjyuan.wish.template.domain.model;

import zhongjyuan.domain.IResponseCode;

/**
 * @ClassName OrganizeLeaderCode
 * @Description 组织责任人关联响应码枚举对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public enum OrganizeLeaderCode implements IResponseCode {

	// region 组织责任人关联 => 1000 - 1099

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_CODE_EXISTS_WARN: [1000]创建组织责任人关联失败,已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_CODE_EXISTS_WARN(1000L, "创建组织责任人关联失败,已存在相同有效编码."),
	
	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_CODE_NOT_EXISTS_WARN: [1003]创建组织责任人关联失败,所属编码不存在.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_CODE_NOT_EXISTS_WARN(1003L, "创建组织责任人关联失败,所属编码不存在."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN: [1006]创建组织责任人关联失败,分组下已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN(1006L, "创建组织责任人关联失败,分组下已存在相同有效编码."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN: [1009]创建组织责任人关联失败,同级下已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN(1009L, "创建组织责任人关联失败,同级下已存在相同有效编码."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_CODE_AND_BUSINESS_EXISTS_WARN: [1012]创建组织责任人关联失败,业务下已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_CODE_AND_BUSINESS_EXISTS_WARN(1012L, "创建组织责任人关联失败,业务下已存在相同有效编码."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_NAME_EXISTS_WARN: [1015]创建组织责任人关联失败,已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_NAME_EXISTS_WARN(1015L, "创建组织责任人关联失败,已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_CODE_EXISTS_WARN: [1018]创建组织责任人关联失败,编码下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_CODE_EXISTS_WARN(1018L, "创建组织责任人关联失败,编码下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN: [1021]创建组织责任人关联失败,分组下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN(1021L, "创建组织责任人关联失败,分组下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN: [1024]创建组织责任人关联失败,同级下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN(1024L, "创建组织责任人关联失败,同级下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_BUSINESS_EXISTS_WARN: [1027]创建组织责任人关联失败,业务下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_CREATE_BY_NAME_AND_BUSINESS_EXISTS_WARN(1027L, "创建组织责任人关联失败,业务下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_CODE_EXISTS_WARN: [1030]更新组织责任人关联失败,已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_CODE_EXISTS_WARN(1030L, "更新组织责任人关联失败,已存在相同有效编码."),
	
	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_CODE_NOT_EXISTS_WARN: [1033]更新组织责任人关联失败,所属编码不存在.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_CODE_NOT_EXISTS_WARN(1033L, "更新组织责任人关联失败,所属编码不存在."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN: [1036]更新组织责任人关联失败,分组下已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN(1036L, "更新组织责任人关联失败,分组下已存在相同有效编码."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN: [1039]更新组织责任人关联失败,同级下已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN(1039L, "更新组织责任人关联失败,同级下已存在相同有效编码."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_CODE_AND_BUSINESS_EXISTS_WARN: [1042]更新组织责任人关联失败,业务下已存在相同有效编码.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_CODE_AND_BUSINESS_EXISTS_WARN(1042L, "更新组织责任人关联失败,业务下已存在相同有效编码."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_NAME_EXISTS_WARN: [1045]工更新组织责任人关联失败,已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_NAME_EXISTS_WARN(1045L, "工更新组织责任人关联失败,已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_CODE_EXISTS_WARN: [1048]更新组织责任人关联失败,编码下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_CODE_EXISTS_WARN(1048L, "更新组织责任人关联失败,编码下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN: [1051]更新组织责任人关联失败,分组下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN(1051L, "更新组织责任人关联失败,分组下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN: [1054]更新组织责任人关联失败,同级下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN(1054L, "更新组织责任人关联失败,同级下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_BUSINESS_EXISTS_WARN: [1057]更新组织责任人关联失败,业务下已存在相同有效名称.
	 */
	ORGANIZELEADER_FOR_UPDATE_BY_NAME_AND_BUSINESS_EXISTS_WARN(1057L, "更新组织责任人关联失败,业务下已存在相同有效名称."),

	/**
	 * @Fields ORGANIZELEADER_FOR_IS_LOCK_TURE_WARN: [1060]操作失败,该组织责任人关联已锁定.
	 */
	ORGANIZELEADER_FOR_IS_LOCK_TURE_WARN(1060L, "操作失败,该组织责任人关联已锁定."),

	/**
	 * @Fields ORGANIZELEADER_FOR_IS_SYSTEM_TURE_WARN: [1063]操作失败,该组织责任人关联为系统数据.
	 */
	ORGANIZELEADER_FOR_IS_SYSTEM_TURE_WARN(1063L, "操作失败,该组织责任人关联为系统数据."),

	/**
	 * @Fields ORGANIZELEADER_FOR_IS_DELETED_TURE_WARN: [1066]操作失败,该组织责任人关联已移除.
	 */
	ORGANIZELEADER_FOR_IS_DELETED_TURE_WARN(1066L, "操作失败,该组织责任人关联已移除."),

	/**
	 * @Fields ORGANIZELEADER_FOR_SELECT_NOT_EXISTS_WARN: [1069]操作失败,该组织责任人关联不存在.
	 */
	ORGANIZELEADER_FOR_SELECT_NOT_EXISTS_WARN(1069L, "操作失败,该组织责任人关联不存在."),

	// endregion

	;

	/**
	 * @Fields code: 响应消息码
	 */
	final Long code;

	/**
	 * @Fields message: 响应消息
	 */
	final String message;

	/**
	 * @Title OrganizeLeaderCode
	 * @Author zhongjyuan
	 * @Description 组织责任人关联响应码枚举对象
	 * @Param @param code 响应消息码
	 * @Param @param message 响应消息
	 * @Throws
	 */
	OrganizeLeaderCode(Long code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * @Title getCode 获取响应消息码
	 * @Description
	 * @return
	 * @see zhongjyuan.wish.model.IResponseCode#getCode()
	 */
	@Override
	public Long getCode() {
		return code;
	}

	/**
	 * @Title getMessage 获取响应 消息
	 * @Description
	 * @return
	 * @see zhongjyuan.wish.model.IResponseCode#getMessage()
	 */
	@Override
	public String getMessage() {
		return message;
	}
}