package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName RoleMutexDO
 * @Description 角色互斥关联表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_role_mutex")
@ApiModel(value = "RoleMutexDO", description = "角色互斥关联表实体对象")
public class RoleMutexDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields roleId: 角色主键
	 */
    @TableField("role_id")
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
    @TableField("role_code")
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
    @TableField("role_name")
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields mutexRoleId: 互斥角色主键
	 */
    @TableField("mutex_role_id")
	@ApiModelProperty(value = "互斥角色主键", name = "mutexRoleId", dataType = "Long")
    private Long mutexRoleId;

	/**
	 * @Fields mutexRoleCode: 互斥角色编码
	 */
    @TableField("mutex_role_code")
	@ApiModelProperty(value = "互斥角色编码", name = "mutexRoleCode", dataType = "String")
    private String mutexRoleCode;

	/**
	 * @Fields mutexRoleName: 互斥角色名称
	 */
    @TableField("mutex_role_name")
	@ApiModelProperty(value = "互斥角色名称", name = "mutexRoleName", dataType = "String")
    private String mutexRoleName;

	/**
	 * @Fields status: 状态
	 */
    @TableField("status")
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;


    public static final String ID = "id";

    public static final String ROLE_ID = "role_id";

    public static final String ROLE_CODE = "role_code";

    public static final String ROLE_NAME = "role_name";

    public static final String MUTEX_ROLE_ID = "mutex_role_id";

    public static final String MUTEX_ROLE_CODE = "mutex_role_code";

    public static final String MUTEX_ROLE_NAME = "mutex_role_name";

    public static final String STATUS = "status";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATE_TIME = "create_time";

}
