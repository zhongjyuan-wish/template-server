package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.UserInfoDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName UserInfoConverter
 * @Description 用户信息转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class UserInfoConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return UserInfoDO 实体对象
	 * @Throws
	 */
	public static UserInfoDO createVoToEntity(UserInfoCreateVO createVO) {
		UserInfoDO entity = new UserInfoDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return UserInfoDO 实体对象
	 * @Throws
	 */
	public static UserInfoDO updateVoToEntity(UserInfoUpdateVO updateVO) {
		UserInfoDO entity = new UserInfoDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserInfoVO VO对象
	 * @Throws
	 */
	public static UserInfoVO entityToVo(UserInfoDO entity) {

		UserInfoVO vo = new UserInfoVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserInfoBriefVO 简要对象
	 * @Throws
	 */
	public static UserInfoBriefVO entityToBriefVo(UserInfoDO entity) {

		UserInfoBriefVO briefVO = new UserInfoBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserInfoVO> VO对象集合
	 * @Throws
	 */
	public static List<UserInfoVO> entityToVos(List<UserInfoDO> entities) {

		List<UserInfoVO> vos = new ArrayList<UserInfoVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, UserInfoVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserInfoBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<UserInfoBriefVO> entityToBriefVos(List<UserInfoDO> entities) {

		List<UserInfoBriefVO> briefVOs = new ArrayList<UserInfoBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, UserInfoBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserInfoPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<UserInfoPageVO> entityToPageVos(List<UserInfoDO> entities) {

		List<UserInfoPageVO> pageVOs = new ArrayList<UserInfoPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, UserInfoPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}