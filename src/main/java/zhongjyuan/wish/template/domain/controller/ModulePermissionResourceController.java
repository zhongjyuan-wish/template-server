package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IModulePermissionResourceService;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionResourceDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName ModulePermissionResourceController
 * @Description 模块权限资源关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "模块权限资源关联管理", tags = "模块权限资源关联管理")
@RequestMapping("{version}/modulePermissionResources")
public class ModulePermissionResourceController {

	@Autowired
	IModulePermissionResourceService modulePermissionResourceService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "模块权限资源关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModulePermissionResourceVO> create(@RequestBody @Valid ModulePermissionResourceCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ModulePermissionResourceDO entity = ModulePermissionResourceConverter.createVoToEntity(createVO);
		entity = modulePermissionResourceService.create(entity);
		return ResponseResult.success(ModulePermissionResourceConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "模块权限资源关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModulePermissionResourceVO> update(@RequestBody @Valid ModulePermissionResourceUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ModulePermissionResourceDO entity = ModulePermissionResourceConverter.updateVoToEntity(updateVO);
		entity = modulePermissionResourceService.update(entity);
		return ResponseResult.success(ModulePermissionResourceConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "模块权限资源关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModulePermissionResourceVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModulePermissionResourceDO> entities = modulePermissionResourceService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModulePermissionResourceConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "模块权限资源关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModulePermissionResourceVO> delete(@PathVariable("id") Long id) {
		ModulePermissionResourceDO entity = modulePermissionResourceService.delete(id);
		return ResponseResult.success(ModulePermissionResourceConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModulePermissionResourceVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModulePermissionResourceDO> entities = modulePermissionResourceService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModulePermissionResourceConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModulePermissionResourceVO> select(@PathVariable("id") Long id) {
		ModulePermissionResourceDO entity = modulePermissionResourceService.select(id);
		return ResponseResult.success(ModulePermissionResourceConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModulePermissionResourceVO>> selectList(@Valid ModulePermissionResourceQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(modulePermissionResourceService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModulePermissionResourceBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModulePermissionResourceDO> entities = modulePermissionResourceService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModulePermissionResourceConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModulePermissionResourceBriefVO> selectBrief(@PathVariable("id") Long id) {
		ModulePermissionResourceDO entity = modulePermissionResourceService.select(id);
		return ResponseResult.success(ModulePermissionResourceConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModulePermissionResourceBriefVO>> selectBriefList(@Valid ModulePermissionResourceBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(modulePermissionResourceService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块权限资源关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页模块权限资源关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<ModulePermissionResourcePageVO>> selectPage(@Valid ModulePermissionResourcePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(modulePermissionResourceService.selectPage(query));
	}
	
}
