package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.RoleModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IRoleModulePermissionService
 * @Description 角色模块权限关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IRoleModulePermissionService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	RoleModulePermissionDO create(RoleModulePermissionDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	RoleModulePermissionDO update(RoleModulePermissionDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	RoleModulePermissionDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleModulePermissionDO> 实体对象集合
	 * @Throws
	 */
	List<RoleModulePermissionDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	RoleModulePermissionDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleModulePermissionDO> 实体对象集合
	 * @Throws
	 */
	List<RoleModulePermissionDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleModulePermissionVO> VO对象集合
	 * @Throws
	 */
	List<RoleModulePermissionVO> selectList(RoleModulePermissionQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	List<RoleModulePermissionBriefVO> selectBriefList(RoleModulePermissionBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<RoleModulePermissionPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<RoleModulePermissionPageVO> selectPage(RoleModulePermissionPageQuery query);

}
