package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.OrganizeRoleMapper;
import zhongjyuan.wish.template.domain.service.IOrganizeRoleService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.OrganizeRoleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName OrganizeRoleServiceImpl
 * @Description 组织角色关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class OrganizeRoleServiceImpl implements IOrganizeRoleService {

	@Autowired
	OrganizeRoleMapper organizeRoleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeRoleDO create(OrganizeRoleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = organizeRoleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		organizeRoleMapper.insert(entity);

		log.info("======OrganizeRoleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(OrganizeRoleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeRoleDO update(OrganizeRoleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		organizeRoleMapper.updateById(entity);

		log.info("======OrganizeRoleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	private OrganizeRoleDO updateEnabled(OrganizeRoleDO entity) {

		// 获取原始数据
		OrganizeRoleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(OrganizeRoleCode.ORGANIZEROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeRoleDO delete(Long id) {

		// 获取原始数据
		OrganizeRoleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		organizeRoleMapper.updateById(originEntity);

		log.info("======OrganizeRoleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<OrganizeRoleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<OrganizeRoleDO> entities = new ArrayList<OrganizeRoleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<OrganizeRoleDO> queryWrapper = new QueryWrapper<OrganizeRoleDO>();
		queryWrapper.lambda().in(OrganizeRoleDO::getId, ids);
		entities = organizeRoleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<OrganizeRoleDO> successEntities = new ArrayList<OrganizeRoleDO>();
		
		// 根据主键更新
		for (OrganizeRoleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			organizeRoleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======OrganizeRoleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(OrganizeRoleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(OrganizeRoleCode.ORGANIZEROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public OrganizeRoleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return organizeRoleMapper.selectById(id);
	}
	
	@Override
	public List<OrganizeRoleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return organizeRoleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<OrganizeRoleVO> selectList(OrganizeRoleQuery query) {
		List<OrganizeRoleVO> vos = new ArrayList<OrganizeRoleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeRoleDO> queryWrapper = new QueryWrapper<OrganizeRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeRoleDO> entities = organizeRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = OrganizeRoleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<OrganizeRoleBriefVO> selectBriefList(OrganizeRoleBriefQuery query) {
		List<OrganizeRoleBriefVO> briefVOs = new ArrayList<OrganizeRoleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeRoleDO> queryWrapper = new QueryWrapper<OrganizeRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeRoleDO> entities = organizeRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = OrganizeRoleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<OrganizeRolePageVO> selectPage(OrganizeRolePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeRoleDO> queryWrapper = new QueryWrapper<OrganizeRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeRoleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<OrganizeRoleDO> page = organizeRoleMapper.selectPage(new CustomPage<OrganizeRoleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> OrganizeRoleConverter.entityToPageVos(entities));
	}

}
