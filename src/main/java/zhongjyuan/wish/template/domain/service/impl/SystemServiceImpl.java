package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.SystemMapper;
import zhongjyuan.wish.template.domain.service.ISystemService;

import zhongjyuan.wish.template.domain.model.entity.SystemDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.SystemCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName SystemServiceImpl
 * @Description 系统表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class SystemServiceImpl implements ISystemService {

	@Autowired
	SystemMapper systemMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public SystemDO create(SystemDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = systemMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		systemMapper.insert(entity);

		log.info("======SystemServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(SystemDO entity) {

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);
		queryWrapper.lambda().eq(SystemDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(SystemDO::getGroup, entity.getGroup());
		}

		if (systemMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(SystemCode.SYSTEM_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(SystemCode.SYSTEM_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);
		queryWrapper.lambda().eq(SystemDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(SystemDO::getGroup, entity.getGroup());
		}

		if (systemMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(SystemCode.SYSTEM_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(SystemCode.SYSTEM_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public SystemDO update(SystemDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		systemMapper.updateById(entity);

		log.info("======SystemServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return SystemDO 实体对象
	 * @Throws
	 */
	private SystemDO updateEnabled(SystemDO entity) {

		// 获取原始数据
		SystemDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(SystemCode.SYSTEM_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(SystemCode.SYSTEM_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(SystemCode.SYSTEM_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);
		queryWrapper.lambda().eq(SystemDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(SystemDO::getGroup, entity.getGroup());
		}

		if (systemMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(SystemCode.SYSTEM_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(SystemCode.SYSTEM_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);
		queryWrapper.lambda().eq(SystemDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(SystemDO::getGroup, entity.getGroup());
		}

		if (systemMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(SystemCode.SYSTEM_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(SystemCode.SYSTEM_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public SystemDO delete(Long id) {

		// 获取原始数据
		SystemDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		systemMapper.updateById(originEntity);

		log.info("======SystemServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public SystemDO deleteByCode(String code) {

		// 获取原始数据
		SystemDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		systemMapper.updateById(originEntity);

		log.info("======SystemServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<SystemDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<SystemDO> entities = new ArrayList<SystemDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsSystem, false);
		queryWrapper.lambda().in(SystemDO::getId, ids);
		entities = systemMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<SystemDO> successEntities = new ArrayList<SystemDO>();
		
		// 根据主键更新
		for (SystemDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			systemMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======SystemServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<SystemDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<SystemDO> entities = new ArrayList<SystemDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsSystem, false);
		queryWrapper.lambda().in(SystemDO::getCode, codes);
		entities = systemMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<SystemDO> successEntities = new ArrayList<SystemDO>();
		
		// 根据主键更新
		for (SystemDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			systemMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======SystemServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(SystemDO entity) {
	
		if (entity == null) {
			throw new BusinessException(SystemCode.SYSTEM_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(SystemCode.SYSTEM_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(SystemCode.SYSTEM_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public SystemDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return systemMapper.selectById(id);
	}
	
	@Override
	public SystemDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);
		queryWrapper.lambda().eq(SystemDO::getCode, code);
		
		return systemMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<SystemDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return systemMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<SystemDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);
		queryWrapper.lambda().in(SystemDO::getCode, codes);
		
		return systemMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<SystemVO> selectList(SystemQuery query) {
		List<SystemVO> vos = new ArrayList<SystemVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(SystemDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(SystemDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(SystemDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(SystemDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<SystemDO> entities = systemMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = SystemConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<SystemBriefVO> selectBriefList(SystemBriefQuery query) {
		List<SystemBriefVO> briefVOs = new ArrayList<SystemBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(SystemDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(SystemDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(SystemDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(SystemDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<SystemDO> entities = systemMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = SystemConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<SystemPageVO> selectPage(SystemPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<SystemDO> queryWrapper = new QueryWrapper<SystemDO>();
		queryWrapper.lambda().eq(SystemDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(SystemDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(SystemDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(SystemDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(SystemDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<SystemDO> page = systemMapper.selectPage(new CustomPage<SystemDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> SystemConverter.entityToPageVos(entities));
	}

}
