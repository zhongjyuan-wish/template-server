package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName RoleModulePermissionQuery
 * @Description 角色模块权限关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleModulePermissionQuery", description = "角色模块权限关联查询条件对象")
public class RoleModulePermissionQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields roleModuleId: 角色模块主键
	 */
	@ApiModelProperty(value = "角色模块主键", name = "roleModuleId", dataType = "Long")
    private Long roleModuleId;

	/**
	 * @Fields roleModuleCode: 角色模块编码
	 */
	@ApiModelProperty(value = "角色模块编码", name = "roleModuleCode", dataType = "String")
    private String roleModuleCode;

	/**
	 * @Fields roleModuleName: 角色模块名称
	 */
	@ApiModelProperty(value = "角色模块名称", name = "roleModuleName", dataType = "String")
    private String roleModuleName;

	/**
	 * @Fields modulePermissionId: 模块权限主键
	 */
	@ApiModelProperty(value = "模块权限主键", name = "modulePermissionId", dataType = "Long")
    private Long modulePermissionId;

	/**
	 * @Fields modulePermissionCode: 模块权限编码
	 */
	@ApiModelProperty(value = "模块权限编码", name = "modulePermissionCode", dataType = "String")
    private String modulePermissionCode;

	/**
	 * @Fields modulePermissionName: 模块权限名称
	 */
	@ApiModelProperty(value = "模块权限名称", name = "modulePermissionName", dataType = "String")
    private String modulePermissionName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}