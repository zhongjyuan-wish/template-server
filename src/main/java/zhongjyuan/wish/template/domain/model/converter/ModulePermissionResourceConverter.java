package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionResourceDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName ModulePermissionResourceConverter
 * @Description 模块权限资源关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class ModulePermissionResourceConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	public static ModulePermissionResourceDO createVoToEntity(ModulePermissionResourceCreateVO createVO) {
		ModulePermissionResourceDO entity = new ModulePermissionResourceDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	public static ModulePermissionResourceDO updateVoToEntity(ModulePermissionResourceUpdateVO updateVO) {
		ModulePermissionResourceDO entity = new ModulePermissionResourceDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionResourceVO VO对象
	 * @Throws
	 */
	public static ModulePermissionResourceVO entityToVo(ModulePermissionResourceDO entity) {

		ModulePermissionResourceVO vo = new ModulePermissionResourceVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionResourceBriefVO 简要对象
	 * @Throws
	 */
	public static ModulePermissionResourceBriefVO entityToBriefVo(ModulePermissionResourceDO entity) {

		ModulePermissionResourceBriefVO briefVO = new ModulePermissionResourceBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePermissionResourceVO> VO对象集合
	 * @Throws
	 */
	public static List<ModulePermissionResourceVO> entityToVos(List<ModulePermissionResourceDO> entities) {

		List<ModulePermissionResourceVO> vos = new ArrayList<ModulePermissionResourceVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, ModulePermissionResourceVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePermissionResourceBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<ModulePermissionResourceBriefVO> entityToBriefVos(List<ModulePermissionResourceDO> entities) {

		List<ModulePermissionResourceBriefVO> briefVOs = new ArrayList<ModulePermissionResourceBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, ModulePermissionResourceBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePermissionResourcePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<ModulePermissionResourcePageVO> entityToPageVos(List<ModulePermissionResourceDO> entities) {

		List<ModulePermissionResourcePageVO> pageVOs = new ArrayList<ModulePermissionResourcePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, ModulePermissionResourcePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}