package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IRoleMutexService;

import zhongjyuan.wish.template.domain.model.entity.RoleMutexDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName RoleMutexController
 * @Description 角色互斥关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "角色互斥关联管理", tags = "角色互斥关联管理")
@RequestMapping("{version}/roleMutexs")
public class RoleMutexController {

	@Autowired
	IRoleMutexService roleMutexService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "角色互斥关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMutexVO> create(@RequestBody @Valid RoleMutexCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleMutexDO entity = RoleMutexConverter.createVoToEntity(createVO);
		entity = roleMutexService.create(entity);
		return ResponseResult.success(RoleMutexConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色互斥关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMutexVO> update(@RequestBody @Valid RoleMutexUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleMutexDO entity = RoleMutexConverter.updateVoToEntity(updateVO);
		entity = roleMutexService.update(entity);
		return ResponseResult.success(RoleMutexConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色互斥关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMutexVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleMutexDO> entities = roleMutexService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleMutexConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色互斥关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMutexVO> delete(@PathVariable("id") Long id) {
		RoleMutexDO entity = roleMutexService.delete(id);
		return ResponseResult.success(RoleMutexConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMutexVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleMutexDO> entities = roleMutexService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleMutexConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMutexVO> select(@PathVariable("id") Long id) {
		RoleMutexDO entity = roleMutexService.select(id);
		return ResponseResult.success(RoleMutexConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMutexVO>> selectList(@Valid RoleMutexQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleMutexService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMutexBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleMutexDO> entities = roleMutexService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleMutexConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMutexBriefVO> selectBrief(@PathVariable("id") Long id) {
		RoleMutexDO entity = roleMutexService.select(id);
		return ResponseResult.success(RoleMutexConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMutexBriefVO>> selectBriefList(@Valid RoleMutexBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleMutexService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色互斥关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页角色互斥关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<RoleMutexPageVO>> selectPage(@Valid RoleMutexPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleMutexService.selectPage(query));
	}
	
}
