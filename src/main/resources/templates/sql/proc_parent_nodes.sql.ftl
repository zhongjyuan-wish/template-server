CREATE PROCEDURE `proc_${table.name}_parent_nodes`(IN type INT,
	IN showSiblings INT,
	IN showHitNodes INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点上所有父节点'
BEGIN
		
	<#noparse># 返回父节点结果集</#noparse>
	DECLARE parentIdList LONGTEXT;
	<#noparse># 临时存放节点</#noparse>
	DECLARE tempIdList LONGTEXT;
	<#noparse># 临时存放父节点</#noparse>
	DECLARE tempParentIdList LONGTEXT;

	SET parentIdList = '';
	SET tempParentIdList = hitNodeIds;

	<#noparse># 循环，用于查询节点上所有的父节点</#noparse>
	WHILE tempParentIdList IS NOT NULL DO

		<#noparse># 存入到返回结果中</#noparse>
		SET parentIdList = CONCAT_WS(',', parentIdList, tempParentIdList);
		
		<#noparse># 查询节点上所有父节点</#noparse>
		SELECT GROUP_CONCAT(parent_id) INTO tempParentIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(id, tempParentIdList);-- id = tempParentIdList;
		
	END WHILE;

	<#noparse># 是否加载对应兄弟节点(不包含命中节点的子级兄弟节点)</#noparse>
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ','));

		SET parentIdList = CONCAT_WS(',', parentIdList, tempIdList);

	END IF;

	<#noparse># 是否包含命中节点</#noparse>
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET parentIdList = REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ',');

	END IF;

	<#noparse># 返回结果[主键字符串]</#noparse>
	IF type IS NULL OR type = 0 THEN
		SELECT SUBSTRING(parentIdList, 2) AS parentIds;
	END IF;

	<#noparse># 返回结果[主键字符串]</#noparse>
	IF type IS NOT NULL AND type = 1 THEN
		SELECT * FROM ${table.name} A
		WHERE A.is_deleted = 0
		AND FIND_IN_SET(A.id, SUBSTRING(parentIdList, 2))
		ORDER BY A.outline;
	END IF;
	<#noparse>
	# create by zhongjyuan
	</#noparse>
END