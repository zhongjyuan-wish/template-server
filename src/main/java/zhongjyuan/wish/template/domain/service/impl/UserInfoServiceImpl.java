package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserInfoMapper;
import zhongjyuan.wish.template.domain.service.IUserInfoService;

import zhongjyuan.wish.template.domain.model.entity.UserInfoDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserInfoCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserInfoServiceImpl
 * @Description 用户信息表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserInfoServiceImpl implements IUserInfoService {

	@Autowired
	UserInfoMapper userInfoMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserInfoDO create(UserInfoDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userInfoMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userInfoMapper.insert(entity);

		log.info("======UserInfoServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserInfoDO entity) {

		// 是否存在相同有效名称数据
		QueryWrapper<UserInfoDO> queryWrapper = new QueryWrapper<UserInfoDO>();
		queryWrapper.lambda().eq(UserInfoDO::getName, entity.getName());

		if (userInfoMapper.exists(queryWrapper)) {
			throw new BusinessException(UserInfoCode.USERINFO_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserInfoDO update(UserInfoDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userInfoMapper.updateById(entity);

		log.info("======UserInfoServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserInfoDO 实体对象
	 * @Throws
	 */
	private UserInfoDO updateEnabled(UserInfoDO entity) {

		// 获取原始数据
		UserInfoDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserInfoCode.USERINFO_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验

		// 是否存在相同有效名称数据
		QueryWrapper<UserInfoDO> queryWrapper = new QueryWrapper<UserInfoDO>();
		queryWrapper.lambda().eq(UserInfoDO::getName, entity.getName());

		if (userInfoMapper.exists(queryWrapper)) {
			throw new BusinessException(UserInfoCode.USERINFO_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserInfoDO delete(Long id) {

		// 获取原始数据
		UserInfoDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userInfoMapper.updateById(originEntity);

		log.info("======UserInfoServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserInfoDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserInfoDO> entities = new ArrayList<UserInfoDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserInfoDO> queryWrapper = new QueryWrapper<UserInfoDO>();
		queryWrapper.lambda().in(UserInfoDO::getId, ids);
		entities = userInfoMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserInfoDO> successEntities = new ArrayList<UserInfoDO>();
		
		// 根据主键更新
		for (UserInfoDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userInfoMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserInfoServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserInfoDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserInfoCode.USERINFO_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserInfoDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userInfoMapper.selectById(id);
	}
	
	@Override
	public List<UserInfoDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userInfoMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserInfoVO> selectList(UserInfoQuery query) {
		List<UserInfoVO> vos = new ArrayList<UserInfoVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserInfoDO> queryWrapper = new QueryWrapper<UserInfoDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserInfoDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(UserInfoDO::getName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserInfoDO> entities = userInfoMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserInfoConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserInfoBriefVO> selectBriefList(UserInfoBriefQuery query) {
		List<UserInfoBriefVO> briefVOs = new ArrayList<UserInfoBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserInfoDO> queryWrapper = new QueryWrapper<UserInfoDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserInfoDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(UserInfoDO::getName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserInfoDO> entities = userInfoMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserInfoConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserInfoPageVO> selectPage(UserInfoPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserInfoDO> queryWrapper = new QueryWrapper<UserInfoDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserInfoDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(UserInfoDO::getName, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserInfoDO> page = userInfoMapper.selectPage(new CustomPage<UserInfoDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserInfoConverter.entityToPageVos(entities));
	}

}
