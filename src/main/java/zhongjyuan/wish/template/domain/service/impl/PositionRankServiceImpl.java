package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.PositionRankMapper;
import zhongjyuan.wish.template.domain.service.IPositionRankService;

import zhongjyuan.wish.template.domain.model.entity.PositionRankDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.PositionRankCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName PositionRankServiceImpl
 * @Description 职业分级表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class PositionRankServiceImpl implements IPositionRankService {

	@Autowired
	PositionRankMapper positionRankMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionRankDO create(PositionRankDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = positionRankMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		positionRankMapper.insert(entity);

		log.info("======PositionRankServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(PositionRankDO entity) {

		// 是否存在相同有效编码数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionRankDO::getCode, entity.getCode());

		if (positionRankMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionRankDO::getName, entity.getName());

		if (positionRankMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionRankDO update(PositionRankDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		positionRankMapper.updateById(entity);

		log.info("======PositionRankServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return PositionRankDO 实体对象
	 * @Throws
	 */
	private PositionRankDO updateEnabled(PositionRankDO entity) {

		// 获取原始数据
		PositionRankDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在相同有效编码数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionRankDO::getCode, entity.getCode());

		if (positionRankMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionRankDO::getName, entity.getName());

		if (positionRankMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionRankDO delete(Long id) {

		// 获取原始数据
		PositionRankDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		positionRankMapper.updateById(originEntity);

		log.info("======PositionRankServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionRankDO deleteByCode(String code) {

		// 获取原始数据
		PositionRankDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		positionRankMapper.updateById(originEntity);

		log.info("======PositionRankServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<PositionRankDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<PositionRankDO> entities = new ArrayList<PositionRankDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsSystem, false);
		queryWrapper.lambda().in(PositionRankDO::getId, ids);
		entities = positionRankMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<PositionRankDO> successEntities = new ArrayList<PositionRankDO>();
		
		// 根据主键更新
		for (PositionRankDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			positionRankMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======PositionRankServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<PositionRankDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<PositionRankDO> entities = new ArrayList<PositionRankDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsSystem, false);
		queryWrapper.lambda().in(PositionRankDO::getCode, codes);
		entities = positionRankMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<PositionRankDO> successEntities = new ArrayList<PositionRankDO>();
		
		// 根据主键更新
		for (PositionRankDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			positionRankMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======PositionRankServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(PositionRankDO entity) {
	
		if (entity == null) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(PositionRankCode.POSITIONRANK_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public PositionRankDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return positionRankMapper.selectById(id);
	}
	
	@Override
	public PositionRankDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionRankDO::getCode, code);
		
		return positionRankMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<PositionRankDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return positionRankMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<PositionRankDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);
		queryWrapper.lambda().in(PositionRankDO::getCode, codes);
		
		return positionRankMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<PositionRankVO> selectList(PositionRankQuery query) {
		List<PositionRankVO> vos = new ArrayList<PositionRankVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PositionRankDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PositionRankDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PositionRankDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PositionRankDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<PositionRankDO> entities = positionRankMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = PositionRankConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<PositionRankBriefVO> selectBriefList(PositionRankBriefQuery query) {
		List<PositionRankBriefVO> briefVOs = new ArrayList<PositionRankBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PositionRankDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PositionRankDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PositionRankDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PositionRankDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<PositionRankDO> entities = positionRankMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = PositionRankConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<PositionRankPageVO> selectPage(PositionRankPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<PositionRankDO> queryWrapper = new QueryWrapper<PositionRankDO>();
		queryWrapper.lambda().eq(PositionRankDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PositionRankDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PositionRankDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PositionRankDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PositionRankDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<PositionRankDO> page = positionRankMapper.selectPage(new CustomPage<PositionRankDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> PositionRankConverter.entityToPageVos(entities));
	}

}
