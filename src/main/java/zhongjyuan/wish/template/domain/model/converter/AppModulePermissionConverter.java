package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.AppModulePermissionDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName AppModulePermissionConverter
 * @Description 应用模块权限关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class AppModulePermissionConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return AppModulePermissionDO 实体对象
	 * @Throws
	 */
	public static AppModulePermissionDO createVoToEntity(AppModulePermissionCreateVO createVO) {
		AppModulePermissionDO entity = new AppModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return AppModulePermissionDO 实体对象
	 * @Throws
	 */
	public static AppModulePermissionDO updateVoToEntity(AppModulePermissionUpdateVO updateVO) {
		AppModulePermissionDO entity = new AppModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModulePermissionVO VO对象
	 * @Throws
	 */
	public static AppModulePermissionVO entityToVo(AppModulePermissionDO entity) {

		AppModulePermissionVO vo = new AppModulePermissionVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModulePermissionBriefVO 简要对象
	 * @Throws
	 */
	public static AppModulePermissionBriefVO entityToBriefVo(AppModulePermissionDO entity) {

		AppModulePermissionBriefVO briefVO = new AppModulePermissionBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<AppModulePermissionVO> VO对象集合
	 * @Throws
	 */
	public static List<AppModulePermissionVO> entityToVos(List<AppModulePermissionDO> entities) {

		List<AppModulePermissionVO> vos = new ArrayList<AppModulePermissionVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, AppModulePermissionVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<AppModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<AppModulePermissionBriefVO> entityToBriefVos(List<AppModulePermissionDO> entities) {

		List<AppModulePermissionBriefVO> briefVOs = new ArrayList<AppModulePermissionBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, AppModulePermissionBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<AppModulePermissionPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<AppModulePermissionPageVO> entityToPageVos(List<AppModulePermissionDO> entities) {

		List<AppModulePermissionPageVO> pageVOs = new ArrayList<AppModulePermissionPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, AppModulePermissionPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}