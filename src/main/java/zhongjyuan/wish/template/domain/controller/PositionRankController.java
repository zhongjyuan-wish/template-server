package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IPositionRankService;

import zhongjyuan.wish.template.domain.model.entity.PositionRankDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName PositionRankController
 * @Description 职业分级管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "职业分级管理", tags = "职业分级管理")
@RequestMapping("{version}/positionRanks")
public class PositionRankController {

	@Autowired
	IPositionRankService positionRankService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> create(@RequestBody @Valid PositionRankCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		PositionRankDO entity = PositionRankConverter.createVoToEntity(createVO);
		entity = positionRankService.create(entity);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> update(@RequestBody @Valid PositionRankUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		PositionRankDO entity = PositionRankConverter.updateVoToEntity(updateVO);
		entity = positionRankService.update(entity);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionRankDO> entities = positionRankService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionRankDO> entities = positionRankService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> delete(@PathVariable("id") Long id) {
		PositionRankDO entity = positionRankService.delete(id);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> deleteByCode(@PathVariable("code") String code) {
		PositionRankDO entity = positionRankService.deleteByCode(code);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> enable(@PathVariable("id") Long id) {
		PositionRankDO entity = positionRankService.select(id);
		entity.setIsEnabled(true);
		entity = positionRankService.update(entity);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> enableByCode(@PathVariable("code") String code) {
		PositionRankDO entity = positionRankService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = positionRankService.update(entity);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> disable(@PathVariable("id") Long id) {
		PositionRankDO entity = positionRankService.select(id);
		entity.setIsEnabled(false);
		entity = positionRankService.update(entity);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业分级", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> disableByCode(@PathVariable("code") String code) {
		PositionRankDO entity = positionRankService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = positionRankService.update(entity);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionRankDO> entities = positionRankService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionRankDO> entities = positionRankService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> select(@PathVariable("id") Long id) {
		PositionRankDO entity = positionRankService.select(id);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankVO> selectByCode(@PathVariable("code") String code) {
		PositionRankDO entity = positionRankService.selectByCode(code);
		return ResponseResult.success(PositionRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankVO>> selectList(@Valid PositionRankQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(positionRankService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionRankDO> entities = positionRankService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionRankConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankBriefVO> selectBrief(@PathVariable("id") Long id) {
		PositionRankDO entity = positionRankService.select(id);
		return ResponseResult.success(PositionRankConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionRankBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		PositionRankDO entity = positionRankService.selectByCode(code);
		return ResponseResult.success(PositionRankConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionRankBriefVO>> selectBriefList(@Valid PositionRankBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(positionRankService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页职业分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<PositionRankPageVO>> selectPage(@Valid PositionRankPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(positionRankService.selectPage(query));
	}
	
}
