package ${package.ServiceImpl};

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import ${import_Batis_CustomPage};
import ${import_Batis_PageUtil};

import ${import_Commons_GlobalConfigurate};
import ${import_Commons_BusinessException};
import ${import_Commons_BeanUtil};
import ${import_Commons_EntityUtil};

<#if treeFlag>
import ${import_Manage_TreeManage};
</#if>
import ${import_Model};
import ${import_Model_Constant};
import ${import_Model_PageResult};
<#if treeFlag>
import ${import_Model_Query};
</#if>

import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};

import ${package.Entity}.${entity};
import ${package.Parent}.${package_Query}.*;
import ${package.Parent}.${package_Converter}.*;
import ${package.Parent}.${package_ResponseVO}.*;

import ${package.Parent}.${package_Model}.${entityNaming}Code;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName ${table.serviceImplName}
 * @Description ${table.comment!}服务实现类
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
@Slf4j
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} implements ${table.serviceName} {
<#assign mapperName="${table.mapperName?uncap_first}"/>

	@Autowired
	${table.mapperName} ${mapperName};

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ${entity} create(${entity} entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = ${table.mapperName?uncap_first}.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		${mapperName}.insert(entity);

		log.info("======${table.serviceImplName}:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author ${author}
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(${entity} entity) {
<#if groupFlag>

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;
</#if>
<#if treeFlag>

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		
</#if>
<#if codeFlag>

		// 是否存在相同有效编码数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>
		queryWrapper.lambda().eq(${entity}::getCode, entity.getCode());
<#if groupFlag>
		if (groupFlag) {
			queryWrapper.lambda().eq(${entity}::getGroup, entity.getGroup());
		}
</#if>
<#if treeFlag>
		if (parentFlag) {
			queryWrapper.lambda().eq(${entity}::getParentId, entity.getParentId());
		}
</#if>

		if (${mapperName}.exists(queryWrapper)) {
<#if groupFlag>
			if (groupFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
</#if>
<#if treeFlag>
			if (parentFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
</#if>
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}
</#if>

<#if nameFlag>
<#if codeFlag>
		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<${entity}>();
<#else>
		// 是否存在相同有效名称数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
</#if>
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>
		queryWrapper.lambda().eq(${entity}::getName, entity.getName());
<#if groupFlag>
		if (groupFlag) {
			queryWrapper.lambda().eq(${entity}::getGroup, entity.getGroup());
		}
</#if>

		if (${mapperName}.exists(queryWrapper)) {
<#if groupFlag>
			if (!groupFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
</#if>
<#if treeFlag>
			if (!parentFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
</#if>
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
</#if>
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ${entity} update(${entity} entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		${mapperName}.updateById(entity);

		log.info("======${table.serviceImplName}:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author ${author}
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	private ${entity} updateEnabled(${entity} entity) {

		// 获取原始数据
		${entity} originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_SELECT_NOT_EXISTS_WARN);
		}
<#if systemFlag>

		if (originEntity.getIsSystem()) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_IS_SYSTEM_TURE_WARN);
		}
</#if>
<#if lockFlag>

		if (originEntity.getIsLock()) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_IS_LOCK_TURE_WARN);
		}
</#if>
<#if deletedFlag>

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_IS_DELETED_TURE_WARN);
		}
</#if>
		
		// region 相关业务校验
<#if groupFlag>

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;
</#if>
<#if treeFlag>

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		
</#if>
<#if codeFlag>

		// 是否存在相同有效编码数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>
		queryWrapper.lambda().eq(${entity}::getCode, entity.getCode());
<#if groupFlag>
		if (groupFlag) {
			queryWrapper.lambda().eq(${entity}::getGroup, entity.getGroup());
		}
</#if>
<#if treeFlag>
		if (parentFlag) {
			queryWrapper.lambda().eq(${entity}::getParentId, entity.getParentId());
		}
</#if>

		if (${mapperName}.exists(queryWrapper)) {
<#if groupFlag>
			if (groupFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
</#if>
<#if treeFlag>
			if (parentFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
</#if>
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}
</#if>

<#if nameFlag>
<#if codeFlag>
		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<${entity}>();
<#else>
		// 是否存在相同有效名称数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
</#if>
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>
		queryWrapper.lambda().eq(${entity}::getName, entity.getName());
<#if groupFlag>
		if (groupFlag) {
			queryWrapper.lambda().eq(${entity}::getGroup, entity.getGroup());
		}
</#if>

		if (${mapperName}.exists(queryWrapper)) {
<#if groupFlag>
			if (!groupFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
</#if>
<#if treeFlag>
			if (!parentFlag) {
				throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
</#if>
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}
</#if>

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ${entity} delete(${keyPropertyType} id) {

		// 获取原始数据
		${entity} originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		${mapperName}.updateById(originEntity);

		log.info("======${table.serviceImplName}:delete({})======", id);
		return originEntity;
	}
	
<#if codeFlag>
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ${entity} deleteByCode(String code) {

		// 获取原始数据
		${entity} originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		${mapperName}.updateById(originEntity);

		log.info("======${table.serviceImplName}:deleteByCode({})======", code);
		return originEntity;
	}
	
</#if>
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<${entity}> delete(List<${keyPropertyType}> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<${entity}> entities = new ArrayList<${entity}>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if systemFlag>
		queryWrapper.lambda().eq(${entity}::getIsSystem, false);
</#if>
		queryWrapper.lambda().in(${entity}::getId, ids);
		entities = ${mapperName}.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<${entity}> successEntities = new ArrayList<${entity}>();
		
		// 根据主键更新
		for (${entity} entity : entities) {
<#if systemFlag>

			if (entity.getIsSystem()) {
				continue;
			}
</#if>
<#if lockFlag>

			if (entity.getIsLock()) {
				continue;
			}
</#if>
<#if deletedFlag>

			if (entity.getIsDeleted()) {
				continue;
			}
</#if>
			EntityUtil.setDeletion(entity);
			
			${mapperName}.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======${table.serviceImplName}:delete({})======", ids);
		return successEntities;
	}
	
<#if codeFlag>
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<${entity}> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<${entity}> entities = new ArrayList<${entity}>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
		queryWrapper.lambda().eq(${entity}::getIsSystem, false);
		queryWrapper.lambda().in(${entity}::getCode, codes);
		entities = ${mapperName}.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<${entity}> successEntities = new ArrayList<${entity}>();
		
		// 根据主键更新
		for (${entity} entity : entities) {
<#if systemFlag>

			if (entity.getIsSystem()) {
				continue;
			}
</#if>
<#if lockFlag>

			if (entity.getIsLock()) {
				continue;
			}
</#if>
<#if deletedFlag>

			if (entity.getIsDeleted()) {
				continue;
			}
</#if>
			EntityUtil.setDeletion(entity);
			
			${mapperName}.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======${table.serviceImplName}:deleteByCode({})======", codes);
		return successEntities;
	}
	
</#if>
	/**
	 * @Title deleteEnabled
	 * @Author ${author}
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(${entity} entity) {
	
		if (entity == null) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_SELECT_NOT_EXISTS_WARN);
		}
<#if systemFlag>

		if (entity.getIsSystem()) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_IS_SYSTEM_TURE_WARN);
		}
</#if>
<#if lockFlag>

		if (entity.getIsLock()) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_IS_LOCK_TURE_WARN);
		}
</#if>
<#if deletedFlag>

		if (entity.getIsDeleted()) {
			throw new BusinessException(${entityNaming}Code.${entityNaming?upper_case}_FOR_IS_DELETED_TURE_WARN);
		}
</#if>
	}

	@Override
	public ${entity} select(${keyPropertyType} id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return ${mapperName}.selectById(id);
	}
	
<#if codeFlag>
	@Override
	public ${entity} selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>
		queryWrapper.lambda().eq(${entity}::getCode, code);
		
		return ${mapperName}.selectOne(queryWrapper);
	}
	
</#if>
	@Override
	public List<${entity}> select(List<${keyPropertyType}> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return ${mapperName}.selectBatchIds(ids);
	}
	
<#if codeFlag>
	@Override
	public List<${entity}> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>
		queryWrapper.lambda().in(${entity}::getCode, codes);
		
		return ${mapperName}.selectList(queryWrapper);
	}
	
</#if>
	@Override
	public List<${entityNaming}VO> selectList(${entityNaming}Query query) {
		List<${entityNaming}VO> vos = new ArrayList<${entityNaming}VO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(${entity}::getDescription, query.getSearchValue())
<#if nameFlag>
							// 名称
							.or().like(${entity}::getName, query.getSearchValue())
</#if>
<#if treeFlag>
							// 父级名称
							.or().like(${entity}::getParentName, query.getSearchValue())
</#if>
;
				});
			}

<#if systemFlag>
			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(${entity}::getIsSystem, query.getIsSystem());
			}
			
</#if>
<#if enableFlag>
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(${entity}::getIsEnabled, query.getIsEnabled());
			}
			
</#if>
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = ${entityNaming}Converter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<${entityNaming}BriefVO> selectBriefList(${entityNaming}BriefQuery query) {
		List<${entityNaming}BriefVO> briefVOs = new ArrayList<${entityNaming}BriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(${entity}::getDescription, query.getSearchValue())
<#if nameFlag>
							// 名称
							.or().like(${entity}::getName, query.getSearchValue())
</#if>
<#if treeFlag>
							// 父级名称
							.or().like(${entity}::getParentName, query.getSearchValue())
</#if>
;
				});
			}

<#if systemFlag>
			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(${entity}::getIsSystem, query.getIsSystem());
			}
			
</#if>
<#if enableFlag>
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(${entity}::getIsEnabled, query.getIsEnabled());
			}
			
</#if>
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = ${entityNaming}Converter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<${entityNaming}PageVO> selectPage(${entityNaming}PageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
<#if deletedFlag>
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);
</#if>

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(${entity}::getDescription, query.getSearchValue())
<#if nameFlag>
							// 名称
							.or().like(${entity}::getName, query.getSearchValue())
</#if>
<#if treeFlag>
							// 父级名称
							.or().like(${entity}::getParentName, query.getSearchValue())
</#if>
;
				});
			}

<#if systemFlag>
			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(${entity}::getIsSystem, query.getIsSystem());
			}
			
</#if>
<#if enableFlag>
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(${entity}::getIsEnabled, query.getIsEnabled());
			}
			
</#if>
		}

		// 分页查询
		CustomPage<${entity}> page = ${mapperName}.selectPage(new CustomPage<${entity}>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> ${entityNaming}Converter.entityToPageVos(entities));
	}

<#if treeFlag>
	@Override
	public List<${entityNaming}TreeVO> selectLazy(TreeLazyQuery<${keyPropertyType}> query) {

		// 转成复杂查询条件对象
		TreeComplexQuery<${keyPropertyType}> complexQuery = new TreeComplexQuery<${keyPropertyType}>();
		BeanUtil.copyProperties(query, complexQuery);

		return selectChildrens(complexQuery);
	}

	@Override
	public List<${entityNaming}TreeVO> selectTree(TreeQuery<${keyPropertyType}> query) {
		List<${entityNaming}TreeVO> treeVOs = new ArrayList<${entityNaming}TreeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>();
		queryWrapper.lambda().eq(${entity}::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(${entity}::getDescription, query.getSearchValue())
<#if nameFlag>
							// 名称
							.or().like(${entity}::getName, query.getSearchValue())
</#if>
<#if treeFlag>
							// 父级名称
							.or().like(${entity}::getParentName, query.getSearchValue())
</#if>
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		${keyPropertyType} rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			treeVOs = ${entityNaming}Converter.entityToTreeVos(entities);
			treeVOs = (List<${entityNaming}TreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ${entityNaming}TreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<${entityNaming}TreeVO> selectParents(TreeComplexQuery<${keyPropertyType}> query) {
		List<${entityNaming}TreeVO> treeVOs = new ArrayList<${entityNaming}TreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		${keyPropertyType} rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectParents(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ${entityNaming}Converter.entityToTreeVos(entities);
			treeVOs = (List<${entityNaming}TreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ${entityNaming}TreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<${entityNaming}TreeVO> selectChildrens(TreeComplexQuery<${keyPropertyType}> query) {
		List<${entityNaming}TreeVO> treeVOs = new ArrayList<${entityNaming}TreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		${keyPropertyType} rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectChildrens(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ${entityNaming}Converter.entityToTreeVos(entities);
			treeVOs = (List<${entityNaming}TreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ${entityNaming}TreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<${entityNaming}TreeVO> selectLeafs(TreeComplexQuery<${keyPropertyType}> query) {
		List<${entityNaming}TreeVO> treeVOs = new ArrayList<${entityNaming}TreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		${keyPropertyType} rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectLeafs(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ${entityNaming}Converter.entityToTreeVos(entities);
			treeVOs = (List<${entityNaming}TreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ${entityNaming}TreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<${entityNaming}TreeVO> selectFulls(TreeComplexQuery<${keyPropertyType}> query) {
		List<${entityNaming}TreeVO> treeVOs = new ArrayList<${entityNaming}TreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		${keyPropertyType} rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<${entity}> entities = ${mapperName}.selectFulls(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ${entityNaming}Converter.entityToTreeVos(entities);
			treeVOs = (List<${entityNaming}TreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ${entityNaming}TreeVO.class);
		}

		return treeVOs;
	}
</#if>
}
</#if>
