CREATE PROCEDURE `proc_sys_module_leaf_nodes`(IN type INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点下所有叶子节点'
BEGIN

		# 计算节点下是否有节点，count = 0 表示为叶子节点
    DECLARE count INT;
		# 返回叶子节点结果集
    DECLARE leafChildIdList LONGTEXT;
		# 存放叶子节点
    DECLARE leafChild LONGTEXT;
		# 临时存放子节点
    DECLARE tempLeafChildId LONGTEXT;
		# 临时存放可能的叶子节点	
    DECLARE tempLeafChildIdList LONGTEXT;
		# 临时存放节点
    DECLARE tempIdList LONGTEXT;
    
    SET leafChildIdList = '';
    SET tempLeafChildId = hitNodeIds;

		# 外层循环，用于查询节点下所有的子节点
    WHILE tempLeafChildId IS NOT NULL DO
					 
			# 临时存放节点，用于内层循环判定是否为叶子节点，避免影响外层循环使用 tempLeafChildId
			SET tempLeafChildIdList = tempLeafChildId;
				
			# 内层循环，用于判断外层查询到的子节点是否为叶子节点
			WHILE LENGTH(tempLeafChildIdList) > 0 DO
				
				# 假定逗号分隔的第一个为叶子节点
				SET leafChild = SUBSTRING_INDEX(tempLeafChildIdList, ',', 1);
				
				# 查询该节点下是否有子节点
				select count(id) INTO count FROM sys_module WHERE is_deleted = 0 AND parent_id = leafChild;
				 
				# 如果该节点下没有子节点，则认为是叶子节点，存入到返回结果中
				IF count = 0 THEN
					SET leafChildIdList = CONCAT_WS(',', leafChildIdList, leafChild);
				END IF;
				
				# 将第一个节点截取掉，继续识别剩余的节点
				SET tempLeafChildIdList = SUBSTRING(tempLeafChildIdList, LENGTH(leafChild) + 2);
			END WHILE;

			# 查询节点下所有子节点
			SELECT GROUP_CONCAT(id) INTO tempLeafChildId FROM sys_module WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, tempLeafChildId) > 0;
				
    END WHILE;
		
		# 返回结果[主键字符串]
		IF type IS NULL OR type = 0 THEN
			SELECT SUBSTRING(leafChildIdList, 2) AS childLeafIds;
		END IF;
		
		# 返回结果[数据集]
		IF type IS NOT NULL AND type = 1 THEN
			SELECT * FROM sys_module menu
			WHERE menu.is_deleted = 0
			AND FIND_IN_SET(menu.id, SUBSTRING(leafChildIdList, 2))
			ORDER BY menu.outline;
		END IF;
	# create by zhongjyuan
END