package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IAppService;

import zhongjyuan.wish.template.domain.model.entity.AppDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName AppController
 * @Description 应用管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "应用管理", tags = "应用管理")
@RequestMapping("{version}/apps")
public class AppController {

	@Autowired
	IAppService appService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> create(@RequestBody @Valid AppCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppDO entity = AppConverter.createVoToEntity(createVO);
		entity = appService.create(entity);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> update(@RequestBody @Valid AppUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppDO entity = AppConverter.updateVoToEntity(updateVO);
		entity = appService.update(entity);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppDO> entities = appService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppDO> entities = appService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> delete(@PathVariable("id") Long id) {
		AppDO entity = appService.delete(id);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> deleteByCode(@PathVariable("code") String code) {
		AppDO entity = appService.deleteByCode(code);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> enable(@PathVariable("id") Long id) {
		AppDO entity = appService.select(id);
		entity.setIsEnabled(true);
		entity = appService.update(entity);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> enableByCode(@PathVariable("code") String code) {
		AppDO entity = appService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = appService.update(entity);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> disable(@PathVariable("id") Long id) {
		AppDO entity = appService.select(id);
		entity.setIsEnabled(false);
		entity = appService.update(entity);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> disableByCode(@PathVariable("code") String code) {
		AppDO entity = appService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = appService.update(entity);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppDO> entities = appService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppDO> entities = appService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> select(@PathVariable("id") Long id) {
		AppDO entity = appService.select(id);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppVO> selectByCode(@PathVariable("code") String code) {
		AppDO entity = appService.selectByCode(code);
		return ResponseResult.success(AppConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppVO>> selectList(@Valid AppQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppDO> entities = appService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppBriefVO> selectBrief(@PathVariable("id") Long id) {
		AppDO entity = appService.select(id);
		return ResponseResult.success(AppConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		AppDO entity = appService.selectByCode(code);
		return ResponseResult.success(AppConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppBriefVO>> selectBriefList(@Valid AppBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页应用 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<AppPageVO>> selectPage(@Valid AppPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appService.selectPage(query));
	}
	
}
