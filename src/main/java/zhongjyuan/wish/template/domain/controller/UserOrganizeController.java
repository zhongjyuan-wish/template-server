package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IUserOrganizeService;

import zhongjyuan.wish.template.domain.model.entity.UserOrganizeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName UserOrganizeController
 * @Description 用户组织关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "用户组织关联管理", tags = "用户组织关联管理")
@RequestMapping("{version}/userOrganizes")
public class UserOrganizeController {

	@Autowired
	IUserOrganizeService userOrganizeService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "用户组织关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserOrganizeVO> create(@RequestBody @Valid UserOrganizeCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserOrganizeDO entity = UserOrganizeConverter.createVoToEntity(createVO);
		entity = userOrganizeService.create(entity);
		return ResponseResult.success(UserOrganizeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "用户组织关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserOrganizeVO> update(@RequestBody @Valid UserOrganizeUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserOrganizeDO entity = UserOrganizeConverter.updateVoToEntity(updateVO);
		entity = userOrganizeService.update(entity);
		return ResponseResult.success(UserOrganizeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户组织关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserOrganizeVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserOrganizeDO> entities = userOrganizeService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserOrganizeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户组织关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserOrganizeVO> delete(@PathVariable("id") Long id) {
		UserOrganizeDO entity = userOrganizeService.delete(id);
		return ResponseResult.success(UserOrganizeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserOrganizeVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserOrganizeDO> entities = userOrganizeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserOrganizeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserOrganizeVO> select(@PathVariable("id") Long id) {
		UserOrganizeDO entity = userOrganizeService.select(id);
		return ResponseResult.success(UserOrganizeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserOrganizeVO>> selectList(@Valid UserOrganizeQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userOrganizeService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserOrganizeBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserOrganizeDO> entities = userOrganizeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserOrganizeConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserOrganizeBriefVO> selectBrief(@PathVariable("id") Long id) {
		UserOrganizeDO entity = userOrganizeService.select(id);
		return ResponseResult.success(UserOrganizeConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserOrganizeBriefVO>> selectBriefList(@Valid UserOrganizeBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userOrganizeService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户组织关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页用户组织关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<UserOrganizePageVO>> selectPage(@Valid UserOrganizePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userOrganizeService.selectPage(query));
	}
	
}
