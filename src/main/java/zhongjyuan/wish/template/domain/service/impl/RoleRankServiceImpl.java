package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.RoleRankMapper;
import zhongjyuan.wish.template.domain.service.IRoleRankService;

import zhongjyuan.wish.template.domain.model.entity.RoleRankDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.RoleRankCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName RoleRankServiceImpl
 * @Description 角色分级表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class RoleRankServiceImpl implements IRoleRankService {

	@Autowired
	RoleRankMapper roleRankMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleRankDO create(RoleRankDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = roleRankMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		roleRankMapper.insert(entity);

		log.info("======RoleRankServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(RoleRankDO entity) {

		// 是否存在相同有效编码数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleRankDO::getCode, entity.getCode());

		if (roleRankMapper.exists(queryWrapper)) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleRankDO::getName, entity.getName());

		if (roleRankMapper.exists(queryWrapper)) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleRankDO update(RoleRankDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		roleRankMapper.updateById(entity);

		log.info("======RoleRankServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	private RoleRankDO updateEnabled(RoleRankDO entity) {

		// 获取原始数据
		RoleRankDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在相同有效编码数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleRankDO::getCode, entity.getCode());

		if (roleRankMapper.exists(queryWrapper)) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleRankDO::getName, entity.getName());

		if (roleRankMapper.exists(queryWrapper)) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleRankDO delete(Long id) {

		// 获取原始数据
		RoleRankDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleRankMapper.updateById(originEntity);

		log.info("======RoleRankServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleRankDO deleteByCode(String code) {

		// 获取原始数据
		RoleRankDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleRankMapper.updateById(originEntity);

		log.info("======RoleRankServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleRankDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<RoleRankDO> entities = new ArrayList<RoleRankDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsSystem, false);
		queryWrapper.lambda().in(RoleRankDO::getId, ids);
		entities = roleRankMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleRankDO> successEntities = new ArrayList<RoleRankDO>();
		
		// 根据主键更新
		for (RoleRankDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			roleRankMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleRankServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleRankDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<RoleRankDO> entities = new ArrayList<RoleRankDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsSystem, false);
		queryWrapper.lambda().in(RoleRankDO::getCode, codes);
		entities = roleRankMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleRankDO> successEntities = new ArrayList<RoleRankDO>();
		
		// 根据主键更新
		for (RoleRankDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			roleRankMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleRankServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(RoleRankDO entity) {
	
		if (entity == null) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(RoleRankCode.ROLERANK_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public RoleRankDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return roleRankMapper.selectById(id);
	}
	
	@Override
	public RoleRankDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleRankDO::getCode, code);
		
		return roleRankMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<RoleRankDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return roleRankMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<RoleRankDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);
		queryWrapper.lambda().in(RoleRankDO::getCode, codes);
		
		return roleRankMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<RoleRankVO> selectList(RoleRankQuery query) {
		List<RoleRankVO> vos = new ArrayList<RoleRankVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleRankDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleRankDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(RoleRankDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(RoleRankDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleRankDO> entities = roleRankMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = RoleRankConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<RoleRankBriefVO> selectBriefList(RoleRankBriefQuery query) {
		List<RoleRankBriefVO> briefVOs = new ArrayList<RoleRankBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleRankDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleRankDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(RoleRankDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(RoleRankDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleRankDO> entities = roleRankMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = RoleRankConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<RoleRankPageVO> selectPage(RoleRankPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleRankDO> queryWrapper = new QueryWrapper<RoleRankDO>();
		queryWrapper.lambda().eq(RoleRankDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleRankDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleRankDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(RoleRankDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(RoleRankDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<RoleRankDO> page = roleRankMapper.selectPage(new CustomPage<RoleRankDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> RoleRankConverter.entityToPageVos(entities));
	}

}
