package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IRoleRankService;

import zhongjyuan.wish.template.domain.model.entity.RoleRankDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName RoleRankController
 * @Description 角色分级管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "角色分级管理", tags = "角色分级管理")
@RequestMapping("{version}/roleRanks")
public class RoleRankController {

	@Autowired
	IRoleRankService roleRankService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> create(@RequestBody @Valid RoleRankCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleRankDO entity = RoleRankConverter.createVoToEntity(createVO);
		entity = roleRankService.create(entity);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> update(@RequestBody @Valid RoleRankUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleRankDO entity = RoleRankConverter.updateVoToEntity(updateVO);
		entity = roleRankService.update(entity);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleRankDO> entities = roleRankService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleRankDO> entities = roleRankService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> delete(@PathVariable("id") Long id) {
		RoleRankDO entity = roleRankService.delete(id);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> deleteByCode(@PathVariable("code") String code) {
		RoleRankDO entity = roleRankService.deleteByCode(code);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> enable(@PathVariable("id") Long id) {
		RoleRankDO entity = roleRankService.select(id);
		entity.setIsEnabled(true);
		entity = roleRankService.update(entity);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> enableByCode(@PathVariable("code") String code) {
		RoleRankDO entity = roleRankService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = roleRankService.update(entity);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> disable(@PathVariable("id") Long id) {
		RoleRankDO entity = roleRankService.select(id);
		entity.setIsEnabled(false);
		entity = roleRankService.update(entity);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色分级", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> disableByCode(@PathVariable("code") String code) {
		RoleRankDO entity = roleRankService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = roleRankService.update(entity);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleRankDO> entities = roleRankService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleRankDO> entities = roleRankService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleRankConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> select(@PathVariable("id") Long id) {
		RoleRankDO entity = roleRankService.select(id);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankVO> selectByCode(@PathVariable("code") String code) {
		RoleRankDO entity = roleRankService.selectByCode(code);
		return ResponseResult.success(RoleRankConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankVO>> selectList(@Valid RoleRankQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleRankService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleRankDO> entities = roleRankService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleRankConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankBriefVO> selectBrief(@PathVariable("id") Long id) {
		RoleRankDO entity = roleRankService.select(id);
		return ResponseResult.success(RoleRankConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleRankBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		RoleRankDO entity = roleRankService.selectByCode(code);
		return ResponseResult.success(RoleRankConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleRankBriefVO>> selectBriefList(@Valid RoleRankBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleRankService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色分级", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页角色分级 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<RoleRankPageVO>> selectPage(@Valid RoleRankPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleRankService.selectPage(query));
	}
	
}
