package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.RoleMutexMapper;
import zhongjyuan.wish.template.domain.service.IRoleMutexService;

import zhongjyuan.wish.template.domain.model.entity.RoleMutexDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.RoleMutexCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName RoleMutexServiceImpl
 * @Description 角色互斥关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class RoleMutexServiceImpl implements IRoleMutexService {

	@Autowired
	RoleMutexMapper roleMutexMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleMutexDO create(RoleMutexDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = roleMutexMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		roleMutexMapper.insert(entity);

		log.info("======RoleMutexServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(RoleMutexDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleMutexDO update(RoleMutexDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		roleMutexMapper.updateById(entity);

		log.info("======RoleMutexServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	private RoleMutexDO updateEnabled(RoleMutexDO entity) {

		// 获取原始数据
		RoleMutexDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(RoleMutexCode.ROLEMUTEX_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleMutexDO delete(Long id) {

		// 获取原始数据
		RoleMutexDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleMutexMapper.updateById(originEntity);

		log.info("======RoleMutexServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleMutexDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<RoleMutexDO> entities = new ArrayList<RoleMutexDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleMutexDO> queryWrapper = new QueryWrapper<RoleMutexDO>();
		queryWrapper.lambda().in(RoleMutexDO::getId, ids);
		entities = roleMutexMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleMutexDO> successEntities = new ArrayList<RoleMutexDO>();
		
		// 根据主键更新
		for (RoleMutexDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			roleMutexMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleMutexServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(RoleMutexDO entity) {
	
		if (entity == null) {
			throw new BusinessException(RoleMutexCode.ROLEMUTEX_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public RoleMutexDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return roleMutexMapper.selectById(id);
	}
	
	@Override
	public List<RoleMutexDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return roleMutexMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<RoleMutexVO> selectList(RoleMutexQuery query) {
		List<RoleMutexVO> vos = new ArrayList<RoleMutexVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleMutexDO> queryWrapper = new QueryWrapper<RoleMutexDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleMutexDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleMutexDO> entities = roleMutexMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = RoleMutexConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<RoleMutexBriefVO> selectBriefList(RoleMutexBriefQuery query) {
		List<RoleMutexBriefVO> briefVOs = new ArrayList<RoleMutexBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleMutexDO> queryWrapper = new QueryWrapper<RoleMutexDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleMutexDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleMutexDO> entities = roleMutexMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = RoleMutexConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<RoleMutexPageVO> selectPage(RoleMutexPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleMutexDO> queryWrapper = new QueryWrapper<RoleMutexDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleMutexDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<RoleMutexDO> page = roleMutexMapper.selectPage(new CustomPage<RoleMutexDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> RoleMutexConverter.entityToPageVos(entities));
	}

}
