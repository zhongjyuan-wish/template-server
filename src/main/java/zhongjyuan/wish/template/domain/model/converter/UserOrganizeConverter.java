package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.UserOrganizeDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName UserOrganizeConverter
 * @Description 用户组织关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class UserOrganizeConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return UserOrganizeDO 实体对象
	 * @Throws
	 */
	public static UserOrganizeDO createVoToEntity(UserOrganizeCreateVO createVO) {
		UserOrganizeDO entity = new UserOrganizeDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return UserOrganizeDO 实体对象
	 * @Throws
	 */
	public static UserOrganizeDO updateVoToEntity(UserOrganizeUpdateVO updateVO) {
		UserOrganizeDO entity = new UserOrganizeDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserOrganizeVO VO对象
	 * @Throws
	 */
	public static UserOrganizeVO entityToVo(UserOrganizeDO entity) {

		UserOrganizeVO vo = new UserOrganizeVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserOrganizeBriefVO 简要对象
	 * @Throws
	 */
	public static UserOrganizeBriefVO entityToBriefVo(UserOrganizeDO entity) {

		UserOrganizeBriefVO briefVO = new UserOrganizeBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserOrganizeVO> VO对象集合
	 * @Throws
	 */
	public static List<UserOrganizeVO> entityToVos(List<UserOrganizeDO> entities) {

		List<UserOrganizeVO> vos = new ArrayList<UserOrganizeVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, UserOrganizeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserOrganizeBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<UserOrganizeBriefVO> entityToBriefVos(List<UserOrganizeDO> entities) {

		List<UserOrganizeBriefVO> briefVOs = new ArrayList<UserOrganizeBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, UserOrganizeBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserOrganizePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<UserOrganizePageVO> entityToPageVos(List<UserOrganizeDO> entities) {

		List<UserOrganizePageVO> pageVOs = new ArrayList<UserOrganizePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, UserOrganizePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}