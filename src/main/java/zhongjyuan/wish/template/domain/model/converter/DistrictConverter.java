package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.DistrictDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName DistrictConverter
 * @Description 行政区划转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class DistrictConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return DistrictDO 实体对象
	 * @Throws
	 */
	public static DistrictDO createVoToEntity(DistrictCreateVO createVO) {
		DistrictDO entity = new DistrictDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return DistrictDO 实体对象
	 * @Throws
	 */
	public static DistrictDO updateVoToEntity(DistrictUpdateVO updateVO) {
		DistrictDO entity = new DistrictDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return DistrictVO VO对象
	 * @Throws
	 */
	public static DistrictVO entityToVo(DistrictDO entity) {

		DistrictVO vo = new DistrictVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return DistrictBriefVO 简要对象
	 * @Throws
	 */
	public static DistrictBriefVO entityToBriefVo(DistrictDO entity) {

		DistrictBriefVO briefVO = new DistrictBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<DistrictVO> VO对象集合
	 * @Throws
	 */
	public static List<DistrictVO> entityToVos(List<DistrictDO> entities) {

		List<DistrictVO> vos = new ArrayList<DistrictVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, DistrictVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<DistrictBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<DistrictBriefVO> entityToBriefVos(List<DistrictDO> entities) {

		List<DistrictBriefVO> briefVOs = new ArrayList<DistrictBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, DistrictBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<DistrictPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<DistrictPageVO> entityToPageVos(List<DistrictDO> entities) {

		List<DistrictPageVO> pageVOs = new ArrayList<DistrictPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, DistrictPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
	/**
	 * @Title entityToTreeVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转树形对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<DistrictTreeVO> 树形对象集合
	 * @Throws
	 */
	public static List<DistrictTreeVO> entityToTreeVos(List<DistrictDO> entities) {

		List<DistrictTreeVO> treeVOs = new ArrayList<DistrictTreeVO>();

		/*
		 * 对象转换:
		 */
		treeVOs = BeanUtil.copyListProperties(entities, DistrictTreeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return treeVOs;
	}
}