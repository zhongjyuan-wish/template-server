package zhongjyuan.wish.template.domain.model.vo.response;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName UserStationPageVO
 * @Description 用户岗位关联分页对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserStationPageVO", description = "用户岗位关联分页对象")
public class UserStationPageVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields userId: 用户主键
	 */
	@ApiModelProperty(value = "用户主键", name = "userId", dataType = "Long")
    private Long userId;

	/**
	 * @Fields userName: 用户名称
	 */
	@ApiModelProperty(value = "用户名称", name = "userName", dataType = "String")
    private String userName;

	/**
	 * @Fields userAccount: 用户账号
	 */
	@ApiModelProperty(value = "用户账号", name = "userAccount", dataType = "String")
    private String userAccount;

	/**
	 * @Fields stationId: 岗位主键
	 */
	@ApiModelProperty(value = "岗位主键", name = "stationId", dataType = "Long")
    private Long stationId;

	/**
	 * @Fields stationCode: 岗位编码
	 */
	@ApiModelProperty(value = "岗位编码", name = "stationCode", dataType = "String")
    private String stationCode;

	/**
	 * @Fields stationName: 岗位名称
	 */
	@ApiModelProperty(value = "岗位名称", name = "stationName", dataType = "String")
    private String stationName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;
}