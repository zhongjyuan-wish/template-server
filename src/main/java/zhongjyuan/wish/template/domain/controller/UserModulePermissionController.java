package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IUserModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.UserModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName UserModulePermissionController
 * @Description 用户模块权限关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "用户模块权限关联管理", tags = "用户模块权限关联管理")
@RequestMapping("{version}/userModulePermissions")
public class UserModulePermissionController {

	@Autowired
	IUserModulePermissionService userModulePermissionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "用户模块权限关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserModulePermissionVO> create(@RequestBody @Valid UserModulePermissionCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserModulePermissionDO entity = UserModulePermissionConverter.createVoToEntity(createVO);
		entity = userModulePermissionService.create(entity);
		return ResponseResult.success(UserModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "用户模块权限关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserModulePermissionVO> update(@RequestBody @Valid UserModulePermissionUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserModulePermissionDO entity = UserModulePermissionConverter.updateVoToEntity(updateVO);
		entity = userModulePermissionService.update(entity);
		return ResponseResult.success(UserModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserModulePermissionVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserModulePermissionDO> entities = userModulePermissionService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserModulePermissionVO> delete(@PathVariable("id") Long id) {
		UserModulePermissionDO entity = userModulePermissionService.delete(id);
		return ResponseResult.success(UserModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserModulePermissionVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserModulePermissionDO> entities = userModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserModulePermissionVO> select(@PathVariable("id") Long id) {
		UserModulePermissionDO entity = userModulePermissionService.select(id);
		return ResponseResult.success(UserModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserModulePermissionVO>> selectList(@Valid UserModulePermissionQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userModulePermissionService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserModulePermissionBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserModulePermissionDO> entities = userModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserModulePermissionConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserModulePermissionBriefVO> selectBrief(@PathVariable("id") Long id) {
		UserModulePermissionDO entity = userModulePermissionService.select(id);
		return ResponseResult.success(UserModulePermissionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserModulePermissionBriefVO>> selectBriefList(@Valid UserModulePermissionBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userModulePermissionService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页用户模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<UserModulePermissionPageVO>> selectPage(@Valid UserModulePermissionPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userModulePermissionService.selectPage(query));
	}
	
}
