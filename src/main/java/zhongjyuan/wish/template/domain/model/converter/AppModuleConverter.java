package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.AppModuleDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName AppModuleConverter
 * @Description 应用模块关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class AppModuleConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	public static AppModuleDO createVoToEntity(AppModuleCreateVO createVO) {
		AppModuleDO entity = new AppModuleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	public static AppModuleDO updateVoToEntity(AppModuleUpdateVO updateVO) {
		AppModuleDO entity = new AppModuleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModuleVO VO对象
	 * @Throws
	 */
	public static AppModuleVO entityToVo(AppModuleDO entity) {

		AppModuleVO vo = new AppModuleVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModuleBriefVO 简要对象
	 * @Throws
	 */
	public static AppModuleBriefVO entityToBriefVo(AppModuleDO entity) {

		AppModuleBriefVO briefVO = new AppModuleBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<AppModuleVO> VO对象集合
	 * @Throws
	 */
	public static List<AppModuleVO> entityToVos(List<AppModuleDO> entities) {

		List<AppModuleVO> vos = new ArrayList<AppModuleVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, AppModuleVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<AppModuleBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<AppModuleBriefVO> entityToBriefVos(List<AppModuleDO> entities) {

		List<AppModuleBriefVO> briefVOs = new ArrayList<AppModuleBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, AppModuleBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<AppModulePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<AppModulePageVO> entityToPageVos(List<AppModuleDO> entities) {

		List<AppModulePageVO> pageVOs = new ArrayList<AppModulePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, AppModulePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}