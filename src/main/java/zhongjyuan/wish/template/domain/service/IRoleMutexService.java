package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.RoleMutexDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IRoleMutexService
 * @Description 角色互斥关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IRoleMutexService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	RoleMutexDO create(RoleMutexDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	RoleMutexDO update(RoleMutexDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	RoleMutexDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleMutexDO> 实体对象集合
	 * @Throws
	 */
	List<RoleMutexDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	RoleMutexDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleMutexDO> 实体对象集合
	 * @Throws
	 */
	List<RoleMutexDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleMutexVO> VO对象集合
	 * @Throws
	 */
	List<RoleMutexVO> selectList(RoleMutexQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleMutexBriefVO> 简要对象集合
	 * @Throws
	 */
	List<RoleMutexBriefVO> selectBriefList(RoleMutexBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<RoleMutexPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<RoleMutexPageVO> selectPage(RoleMutexPageQuery query);

}
