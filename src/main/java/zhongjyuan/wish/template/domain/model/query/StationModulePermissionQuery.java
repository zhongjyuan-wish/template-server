package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName StationModulePermissionQuery
 * @Description 岗位模块权限关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StationModulePermissionQuery", description = "岗位模块权限关联查询条件对象")
public class StationModulePermissionQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields stationModuleId: 岗位模块主键
	 */
	@ApiModelProperty(value = "岗位模块主键", name = "stationModuleId", dataType = "Long")
    private Long stationModuleId;

	/**
	 * @Fields stationModuleCode: 岗位模块编码
	 */
	@ApiModelProperty(value = "岗位模块编码", name = "stationModuleCode", dataType = "String")
    private String stationModuleCode;

	/**
	 * @Fields stationModuleName: 岗位模块名称
	 */
	@ApiModelProperty(value = "岗位模块名称", name = "stationModuleName", dataType = "String")
    private String stationModuleName;

	/**
	 * @Fields modulePermissionId: 模块权限主键
	 */
	@ApiModelProperty(value = "模块权限主键", name = "modulePermissionId", dataType = "Long")
    private Long modulePermissionId;

	/**
	 * @Fields modulePermissionCode: 模块权限编码
	 */
	@ApiModelProperty(value = "模块权限编码", name = "modulePermissionCode", dataType = "String")
    private String modulePermissionCode;

	/**
	 * @Fields modulePermissionName: 模块权限名称
	 */
	@ApiModelProperty(value = "模块权限名称", name = "modulePermissionName", dataType = "String")
    private String modulePermissionName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}