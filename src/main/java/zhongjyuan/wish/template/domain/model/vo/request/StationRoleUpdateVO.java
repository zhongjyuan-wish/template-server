package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName StationRoleUpdateVO
 * @Description 岗位角色关联更新对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StationRoleUpdateVO", description = "岗位角色关联更新对象")
public class StationRoleUpdateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	// @NotNull(message = "主键不能为空.")
	// @NotBlank(message = "主键不能为空.")
	// @Size(message = "主键最大长度为50.", max = 50)
	// @Min(message = "主键最小为600.", value = 600)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields stationId: 岗位主键
	 */
	// @NotNull(message = "岗位主键不能为空.")
	// @NotBlank(message = "岗位主键不能为空.")
	// @Size(message = "岗位主键最大长度为50.", max = 50)
	// @Min(message = "岗位主键最小为600.", value = 600)
	@ApiModelProperty(value = "岗位主键", name = "stationId", dataType = "Long")
    private Long stationId;

	/**
	 * @Fields stationNo: 岗位编号
	 */
	// @NotNull(message = "岗位编号不能为空.")
	// @NotBlank(message = "岗位编号不能为空.")
	// @Size(message = "岗位编号最大长度为50.", max = 50)
	// @Min(message = "岗位编号最小为600.", value = 600)
	@ApiModelProperty(value = "岗位编号", name = "stationNo", dataType = "String")
    private String stationNo;

	/**
	 * @Fields stationName: 岗位名称
	 */
	// @NotNull(message = "岗位名称不能为空.")
	// @NotBlank(message = "岗位名称不能为空.")
	// @Size(message = "岗位名称最大长度为50.", max = 50)
	// @Min(message = "岗位名称最小为600.", value = 600)
	@ApiModelProperty(value = "岗位名称", name = "stationName", dataType = "String")
    private String stationName;

	/**
	 * @Fields roleId: 角色主键
	 */
	// @NotNull(message = "角色主键不能为空.")
	// @NotBlank(message = "角色主键不能为空.")
	// @Size(message = "角色主键最大长度为50.", max = 50)
	// @Min(message = "角色主键最小为600.", value = 600)
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
	// @NotNull(message = "角色编码不能为空.")
	// @NotBlank(message = "角色编码不能为空.")
	// @Size(message = "角色编码最大长度为50.", max = 50)
	// @Min(message = "角色编码最小为600.", value = 600)
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
	// @NotNull(message = "角色名称不能为空.")
	// @NotBlank(message = "角色名称不能为空.")
	// @Size(message = "角色名称最大长度为50.", max = 50)
	// @Min(message = "角色名称最小为600.", value = 600)
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields status: 状态
	 */
	// @NotNull(message = "状态不能为空.")
	// @NotBlank(message = "状态不能为空.")
	// @Size(message = "状态最大长度为50.", max = 50)
	// @Min(message = "状态最小为600.", value = 600)
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

}