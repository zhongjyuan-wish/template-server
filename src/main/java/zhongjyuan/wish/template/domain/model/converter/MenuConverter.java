package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.MenuDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName MenuConverter
 * @Description 菜单转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class MenuConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return MenuDO 实体对象
	 * @Throws
	 */
	public static MenuDO createVoToEntity(MenuCreateVO createVO) {
		MenuDO entity = new MenuDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return MenuDO 实体对象
	 * @Throws
	 */
	public static MenuDO updateVoToEntity(MenuUpdateVO updateVO) {
		MenuDO entity = new MenuDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return MenuVO VO对象
	 * @Throws
	 */
	public static MenuVO entityToVo(MenuDO entity) {

		MenuVO vo = new MenuVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return MenuBriefVO 简要对象
	 * @Throws
	 */
	public static MenuBriefVO entityToBriefVo(MenuDO entity) {

		MenuBriefVO briefVO = new MenuBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<MenuVO> VO对象集合
	 * @Throws
	 */
	public static List<MenuVO> entityToVos(List<MenuDO> entities) {

		List<MenuVO> vos = new ArrayList<MenuVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, MenuVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<MenuBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<MenuBriefVO> entityToBriefVos(List<MenuDO> entities) {

		List<MenuBriefVO> briefVOs = new ArrayList<MenuBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, MenuBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<MenuPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<MenuPageVO> entityToPageVos(List<MenuDO> entities) {

		List<MenuPageVO> pageVOs = new ArrayList<MenuPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, MenuPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
	/**
	 * @Title entityToTreeVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转树形对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<MenuTreeVO> 树形对象集合
	 * @Throws
	 */
	public static List<MenuTreeVO> entityToTreeVos(List<MenuDO> entities) {

		List<MenuTreeVO> treeVOs = new ArrayList<MenuTreeVO>();

		/*
		 * 对象转换:
		 */
		treeVOs = BeanUtil.copyListProperties(entities, MenuTreeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return treeVOs;
	}
}