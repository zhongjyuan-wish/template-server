package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IStationRoleService;

import zhongjyuan.wish.template.domain.model.entity.StationRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName StationRoleController
 * @Description 岗位角色关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "岗位角色关联管理", tags = "岗位角色关联管理")
@RequestMapping("{version}/stationRoles")
public class StationRoleController {

	@Autowired
	IStationRoleService stationRoleService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "岗位角色关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationRoleVO> create(@RequestBody @Valid StationRoleCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		StationRoleDO entity = StationRoleConverter.createVoToEntity(createVO);
		entity = stationRoleService.create(entity);
		return ResponseResult.success(StationRoleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位角色关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationRoleVO> update(@RequestBody @Valid StationRoleUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		StationRoleDO entity = StationRoleConverter.updateVoToEntity(updateVO);
		entity = stationRoleService.update(entity);
		return ResponseResult.success(StationRoleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位角色关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationRoleVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationRoleDO> entities = stationRoleService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationRoleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位角色关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationRoleVO> delete(@PathVariable("id") Long id) {
		StationRoleDO entity = stationRoleService.delete(id);
		return ResponseResult.success(StationRoleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationRoleVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationRoleDO> entities = stationRoleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationRoleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationRoleVO> select(@PathVariable("id") Long id) {
		StationRoleDO entity = stationRoleService.select(id);
		return ResponseResult.success(StationRoleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationRoleVO>> selectList(@Valid StationRoleQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationRoleService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationRoleBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationRoleDO> entities = stationRoleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationRoleConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationRoleBriefVO> selectBrief(@PathVariable("id") Long id) {
		StationRoleDO entity = stationRoleService.select(id);
		return ResponseResult.success(StationRoleConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationRoleBriefVO>> selectBriefList(@Valid StationRoleBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationRoleService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页岗位角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<StationRolePageVO>> selectPage(@Valid StationRolePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationRoleService.selectPage(query));
	}
	
}
