package zhongjyuan.wish.template.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zhongjyuan.wish.template.domain.model.entity.RoleMutexDO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * @ClassName RoleMutexMapper
 * @Description 角色互斥关联表Mapper接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Mapper
public interface RoleMutexMapper extends BaseMapper<RoleMutexDO> {

	/**
	 * @Title selectMaxRowIndex
	 * @Author zhongjyuan
	 * @Description 查询最大行号
	 * @Param @return
	 * @Return Integer 最大行号
	 * @Throws
	 */
	@Select("SELECT IFNULL(MAX(ROW_INDEX),0) FROM sys_role_mutex;")
	Integer selectMaxRowIndex();
	
}
