package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IRoleModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.RoleModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName RoleModulePermissionController
 * @Description 角色模块权限关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "角色模块权限关联管理", tags = "角色模块权限关联管理")
@RequestMapping("{version}/roleModulePermissions")
public class RoleModulePermissionController {

	@Autowired
	IRoleModulePermissionService roleModulePermissionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "角色模块权限关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleModulePermissionVO> create(@RequestBody @Valid RoleModulePermissionCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleModulePermissionDO entity = RoleModulePermissionConverter.createVoToEntity(createVO);
		entity = roleModulePermissionService.create(entity);
		return ResponseResult.success(RoleModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色模块权限关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleModulePermissionVO> update(@RequestBody @Valid RoleModulePermissionUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleModulePermissionDO entity = RoleModulePermissionConverter.updateVoToEntity(updateVO);
		entity = roleModulePermissionService.update(entity);
		return ResponseResult.success(RoleModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleModulePermissionVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleModulePermissionDO> entities = roleModulePermissionService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleModulePermissionVO> delete(@PathVariable("id") Long id) {
		RoleModulePermissionDO entity = roleModulePermissionService.delete(id);
		return ResponseResult.success(RoleModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleModulePermissionVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleModulePermissionDO> entities = roleModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleModulePermissionVO> select(@PathVariable("id") Long id) {
		RoleModulePermissionDO entity = roleModulePermissionService.select(id);
		return ResponseResult.success(RoleModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleModulePermissionVO>> selectList(@Valid RoleModulePermissionQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleModulePermissionService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleModulePermissionBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleModulePermissionDO> entities = roleModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleModulePermissionConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleModulePermissionBriefVO> selectBrief(@PathVariable("id") Long id) {
		RoleModulePermissionDO entity = roleModulePermissionService.select(id);
		return ResponseResult.success(RoleModulePermissionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleModulePermissionBriefVO>> selectBriefList(@Valid RoleModulePermissionBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleModulePermissionService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页角色模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<RoleModulePermissionPageVO>> selectPage(@Valid RoleModulePermissionPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleModulePermissionService.selectPage(query));
	}
	
}
