package zhongjyuan.wish.template.domain.model.vo.response;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName AppPageVO
 * @Description 应用分页对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppPageVO", description = "应用分页对象")
public class AppPageVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields group: 分组
	 */
	@ApiModelProperty(value = "分组", name = "group", dataType = "Long")
    private Long group;

	/**
	 * @Fields type: 类型[PC;Mobile;Pad]
	 */
	@ApiModelProperty(value = "类型[PC;Mobile;Pad]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields blank: 打开方式
	 */
	@ApiModelProperty(value = "打开方式", name = "blank", dataType = "String")
    private String blank;

	/**
	 * @Fields indexUrl: 首页地址
	 */
	@ApiModelProperty(value = "首页地址", name = "indexUrl", dataType = "String")
    private String indexUrl;

	/**
	 * @Fields keyt: 密钥
	 */
	@ApiModelProperty(value = "密钥", name = "keyt", dataType = "String")
    private String keyt;

	/**
	 * @Fields access: 授权码
	 */
	@ApiModelProperty(value = "授权码", name = "access", dataType = "String")
    private String access;

	/**
	 * @Fields refresh: 令牌刷新时间
	 */
	@ApiModelProperty(value = "令牌刷新时间", name = "refresh", dataType = "Integer")
    private Integer refresh;

	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isBusiness: 是否业务
	 */
	@ApiModelProperty(value = "是否业务", name = "isBusiness", dataType = "Boolean")
    private Boolean isBusiness;

	/**
	 * @Fields isExternal: 是否外部
	 */
	@ApiModelProperty(value = "是否外部", name = "isExternal", dataType = "Boolean")
    private Boolean isExternal;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;


	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;
}