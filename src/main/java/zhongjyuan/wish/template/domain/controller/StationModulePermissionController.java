package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IStationModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.StationModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName StationModulePermissionController
 * @Description 岗位模块权限关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "岗位模块权限关联管理", tags = "岗位模块权限关联管理")
@RequestMapping("{version}/stationModulePermissions")
public class StationModulePermissionController {

	@Autowired
	IStationModulePermissionService stationModulePermissionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "岗位模块权限关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationModulePermissionVO> create(@RequestBody @Valid StationModulePermissionCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		StationModulePermissionDO entity = StationModulePermissionConverter.createVoToEntity(createVO);
		entity = stationModulePermissionService.create(entity);
		return ResponseResult.success(StationModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位模块权限关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationModulePermissionVO> update(@RequestBody @Valid StationModulePermissionUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		StationModulePermissionDO entity = StationModulePermissionConverter.updateVoToEntity(updateVO);
		entity = stationModulePermissionService.update(entity);
		return ResponseResult.success(StationModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationModulePermissionVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationModulePermissionDO> entities = stationModulePermissionService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationModulePermissionVO> delete(@PathVariable("id") Long id) {
		StationModulePermissionDO entity = stationModulePermissionService.delete(id);
		return ResponseResult.success(StationModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationModulePermissionVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationModulePermissionDO> entities = stationModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationModulePermissionVO> select(@PathVariable("id") Long id) {
		StationModulePermissionDO entity = stationModulePermissionService.select(id);
		return ResponseResult.success(StationModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationModulePermissionVO>> selectList(@Valid StationModulePermissionQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationModulePermissionService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationModulePermissionBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationModulePermissionDO> entities = stationModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationModulePermissionConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationModulePermissionBriefVO> selectBrief(@PathVariable("id") Long id) {
		StationModulePermissionDO entity = stationModulePermissionService.select(id);
		return ResponseResult.success(StationModulePermissionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationModulePermissionBriefVO>> selectBriefList(@Valid StationModulePermissionBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationModulePermissionService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页岗位模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<StationModulePermissionPageVO>> selectPage(@Valid StationModulePermissionPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationModulePermissionService.selectPage(query));
	}
	
}
