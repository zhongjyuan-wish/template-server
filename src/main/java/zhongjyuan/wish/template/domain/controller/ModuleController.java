package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IModuleService;

import zhongjyuan.wish.template.domain.model.entity.ModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName ModuleController
 * @Description 模块管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "模块管理", tags = "模块管理")
@RequestMapping("{version}/modules")
public class ModuleController {

	@Autowired
	IModuleService moduleService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> create(@RequestBody @Valid ModuleCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ModuleDO entity = ModuleConverter.createVoToEntity(createVO);
		entity = moduleService.create(entity);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> update(@RequestBody @Valid ModuleUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ModuleDO entity = ModuleConverter.updateVoToEntity(updateVO);
		entity = moduleService.update(entity);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleDO> entities = moduleService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModuleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleDO> entities = moduleService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModuleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> delete(@PathVariable("id") Long id) {
		ModuleDO entity = moduleService.delete(id);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> deleteByCode(@PathVariable("code") String code) {
		ModuleDO entity = moduleService.deleteByCode(code);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> enable(@PathVariable("id") Long id) {
		ModuleDO entity = moduleService.select(id);
		entity.setIsEnabled(true);
		entity = moduleService.update(entity);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> enableByCode(@PathVariable("code") String code) {
		ModuleDO entity = moduleService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = moduleService.update(entity);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> disable(@PathVariable("id") Long id) {
		ModuleDO entity = moduleService.select(id);
		entity.setIsEnabled(false);
		entity = moduleService.update(entity);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "模块", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> disableByCode(@PathVariable("code") String code) {
		ModuleDO entity = moduleService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = moduleService.update(entity);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleDO> entities = moduleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModuleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleDO> entities = moduleService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModuleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> select(@PathVariable("id") Long id) {
		ModuleDO entity = moduleService.select(id);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleVO> selectByCode(@PathVariable("code") String code) {
		ModuleDO entity = moduleService.selectByCode(code);
		return ResponseResult.success(ModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleVO>> selectList(@Valid ModuleQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleDO> entities = moduleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ModuleConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleBriefVO> selectBrief(@PathVariable("id") Long id) {
		ModuleDO entity = moduleService.select(id);
		return ResponseResult.success(ModuleConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ModuleBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		ModuleDO entity = moduleService.selectByCode(code);
		return ResponseResult.success(ModuleConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleBriefVO>> selectBriefList(@Valid ModuleBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<ModulePageVO>> selectPage(@Valid ModulePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectPage(query));
	}
	
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleTreeVO>> selectTree(@Valid TreeQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectTree(query));
	}
	
	@RequestMapping(value = "/tree/lazy", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(懒加载) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleTreeVO>> selectLazy(@Valid TreeLazyQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectLazy(query));
	}

	@RequestMapping(value = "/tree/parents", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleTreeVO>> selectParents(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectParents(query));
	}

	@RequestMapping(value = "/tree/parentIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectParentIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleTreeVO> treeVOs = moduleService.selectParents(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/childrens", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(所有子级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleTreeVO>> selectChildrens(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectChildrens(query));
	}

	@RequestMapping(value = "/tree/childrenIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectChildrenIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleTreeVO> treeVOs = moduleService.selectChildrens(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/leafs", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleTreeVO>> selectLeafs(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectLeafs(query));
	}

	@RequestMapping(value = "/tree/leafIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectLeafIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleTreeVO> treeVOs = moduleService.selectLeafs(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/fulls", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ModuleTreeVO>> selectFulls(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(moduleService.selectFulls(query));
	}

	@RequestMapping(value = "/tree/fullIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "模块", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形模块(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectFullIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ModuleTreeVO> treeVOs = moduleService.selectFulls(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}
}
