package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.RoleMutexDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName RoleMutexConverter
 * @Description 角色互斥关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class RoleMutexConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	public static RoleMutexDO createVoToEntity(RoleMutexCreateVO createVO) {
		RoleMutexDO entity = new RoleMutexDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return RoleMutexDO 实体对象
	 * @Throws
	 */
	public static RoleMutexDO updateVoToEntity(RoleMutexUpdateVO updateVO) {
		RoleMutexDO entity = new RoleMutexDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMutexVO VO对象
	 * @Throws
	 */
	public static RoleMutexVO entityToVo(RoleMutexDO entity) {

		RoleMutexVO vo = new RoleMutexVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMutexBriefVO 简要对象
	 * @Throws
	 */
	public static RoleMutexBriefVO entityToBriefVo(RoleMutexDO entity) {

		RoleMutexBriefVO briefVO = new RoleMutexBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleMutexVO> VO对象集合
	 * @Throws
	 */
	public static List<RoleMutexVO> entityToVos(List<RoleMutexDO> entities) {

		List<RoleMutexVO> vos = new ArrayList<RoleMutexVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, RoleMutexVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleMutexBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<RoleMutexBriefVO> entityToBriefVos(List<RoleMutexDO> entities) {

		List<RoleMutexBriefVO> briefVOs = new ArrayList<RoleMutexBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, RoleMutexBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleMutexPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<RoleMutexPageVO> entityToPageVos(List<RoleMutexDO> entities) {

		List<RoleMutexPageVO> pageVOs = new ArrayList<RoleMutexPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, RoleMutexPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}