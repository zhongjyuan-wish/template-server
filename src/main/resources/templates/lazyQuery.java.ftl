package ${package.Parent}.${package_Query};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import ${import_Model_TreeLazyQuery};

/**
 * @ClassName ${entityNaming}TreeLazyQuery
 * @Description ${table.comment?replace("表","")}懒加载查询条件对象
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${entityNaming}TreeLazyQuery", description = "${table.comment?replace("表","")}懒加载查询条件对象")
public class ${entityNaming}TreeLazyQuery extends TreeLazyQuery<${keyPropertyType}> {

	private static final long serialVersionUID = 1L;
}