package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.RoleRankDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IRoleRankService
 * @Description 角色分级表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IRoleRankService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	RoleRankDO create(RoleRankDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	RoleRankDO update(RoleRankDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	RoleRankDO delete(Long id);

	/**
	 * @Title deleteByCode
	 * @Author zhongjyuan
	 * @Description 根据编码删除
	 * @Param @param code 编码
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	RoleRankDO deleteByCode(String code);
	
	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleRankDO> 实体对象集合
	 * @Throws
	 */
	List<RoleRankDO> delete(List<Long> ids);

	/**
	 * @Title deleteByCode
	 * @Author zhongjyuan
	 * @Description 根据编码删除
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<RoleRankDO> 实体对象集合
	 * @Throws
	 */
	List<RoleRankDO> deleteByCode(List<String> codes);
	
	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	RoleRankDO select(Long id);

	/**
	 * @Title selectByCode
	 * @Author zhongjyuan
	 * @Description 根据编码查询
	 * @Param @param code 编码
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	RoleRankDO selectByCode(String code);
	
	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleRankDO> 实体对象集合
	 * @Throws
	 */
	List<RoleRankDO> select(List<Long> ids);

	/**
	 * @Title selectByCode
	 * @Author zhongjyuan
	 * @Description 根据编码查询
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<RoleRankDO> 实体对象集合
	 * @Throws
	 */
	List<RoleRankDO> selectByCode(List<String> codes);
	
	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleRankVO> VO对象集合
	 * @Throws
	 */
	List<RoleRankVO> selectList(RoleRankQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleRankBriefVO> 简要对象集合
	 * @Throws
	 */
	List<RoleRankBriefVO> selectBriefList(RoleRankBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<RoleRankPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<RoleRankPageVO> selectPage(RoleRankPageQuery query);

}
