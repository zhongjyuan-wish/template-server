package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName AppDO
 * @Description 应用表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_app")
@ApiModel(value = "AppDO", description = "应用表实体对象")
public class AppDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
    @TableField("code")
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
    @TableField("name")
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields group: 分组
	 */
    @TableField("group")
	@ApiModelProperty(value = "分组", name = "group", dataType = "Long")
    private Long group;

	/**
	 * @Fields type: 类型[PC;Mobile;Pad]
	 */
    @TableField("type")
	@ApiModelProperty(value = "类型[PC;Mobile;Pad]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
    @TableField("icon")
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields blank: 打开方式
	 */
    @TableField("blank")
	@ApiModelProperty(value = "打开方式", name = "blank", dataType = "String")
    private String blank;

	/**
	 * @Fields indexUrl: 首页地址
	 */
    @TableField("index_url")
	@ApiModelProperty(value = "首页地址", name = "indexUrl", dataType = "String")
    private String indexUrl;

	/**
	 * @Fields keyt: 密钥
	 */
    @TableField("keyt")
	@ApiModelProperty(value = "密钥", name = "keyt", dataType = "String")
    private String keyt;

	/**
	 * @Fields access: 授权码
	 */
    @TableField("access")
	@ApiModelProperty(value = "授权码", name = "access", dataType = "String")
    private String access;

	/**
	 * @Fields refresh: 令牌刷新时间
	 */
    @TableField("refresh")
	@ApiModelProperty(value = "令牌刷新时间", name = "refresh", dataType = "Integer")
    private Integer refresh;

	/**
	 * @Fields isSystem: 是否系统
	 */
    @TableField("is_system")
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isBusiness: 是否业务
	 */
    @TableField("is_business")
	@ApiModelProperty(value = "是否业务", name = "isBusiness", dataType = "Boolean")
    private Boolean isBusiness;

	/**
	 * @Fields isExternal: 是否外部
	 */
    @TableField("is_external")
	@ApiModelProperty(value = "是否外部", name = "isExternal", dataType = "Boolean")
    private Boolean isExternal;

	/**
	 * @Fields isEnabled: 是否有效
	 */
    @TableField("is_enabled")
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;

	/**
	 * @Fields isDeleted: 是否删除
	 */
    @TableField("is_deleted")
    @TableLogic
	@ApiModelProperty(value = "是否删除", name = "isDeleted", dataType = "Boolean")
    private Boolean isDeleted;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
    @TableField("creator_id")
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
    @TableField("creator_name")
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
    @TableField("updator_id")
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
    @TableField("updator_name")
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
    @TableField("update_time")
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;


    public static final String ID = "id";

    public static final String CODE = "code";

    public static final String NAME = "name";

    public static final String GROUP = "group";

    public static final String TYPE = "type";

    public static final String ICON = "icon";

    public static final String BLANK = "blank";

    public static final String INDEX_URL = "index_url";

    public static final String KEYT = "keyt";

    public static final String ACCESS = "access";

    public static final String REFRESH = "refresh";

    public static final String IS_SYSTEM = "is_system";

    public static final String IS_BUSINESS = "is_business";

    public static final String IS_EXTERNAL = "is_external";

    public static final String IS_ENABLED = "is_enabled";

    public static final String IS_DELETED = "is_deleted";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATOR_ID = "creator_id";

    public static final String CREATOR_NAME = "creator_name";

    public static final String CREATE_TIME = "create_time";

    public static final String UPDATOR_ID = "updator_id";

    public static final String UPDATOR_NAME = "updator_name";

    public static final String UPDATE_TIME = "update_time";

}
