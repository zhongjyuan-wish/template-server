package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.model.entity.ValueDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IValueService
 * @Description 码值表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IValueService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	ValueDO create(ValueDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	ValueDO update(ValueDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	ValueDO delete(Long id);

	/**
	 * @Title deleteByCode
	 * @Author zhongjyuan
	 * @Description 根据编码删除
	 * @Param @param code 编码
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	ValueDO deleteByCode(String code);
	
	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<ValueDO> 实体对象集合
	 * @Throws
	 */
	List<ValueDO> delete(List<Long> ids);

	/**
	 * @Title deleteByCode
	 * @Author zhongjyuan
	 * @Description 根据编码删除
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<ValueDO> 实体对象集合
	 * @Throws
	 */
	List<ValueDO> deleteByCode(List<String> codes);
	
	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	ValueDO select(Long id);

	/**
	 * @Title selectByCode
	 * @Author zhongjyuan
	 * @Description 根据编码查询
	 * @Param @param code 编码
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	ValueDO selectByCode(String code);
	
	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<ValueDO> 实体对象集合
	 * @Throws
	 */
	List<ValueDO> select(List<Long> ids);

	/**
	 * @Title selectByCode
	 * @Author zhongjyuan
	 * @Description 根据编码查询
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<ValueDO> 实体对象集合
	 * @Throws
	 */
	List<ValueDO> selectByCode(List<String> codes);
	
	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueVO> VO对象集合
	 * @Throws
	 */
	List<ValueVO> selectList(ValueQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueBriefVO> 简要对象集合
	 * @Throws
	 */
	List<ValueBriefVO> selectBriefList(ValueBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<ValuePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<ValuePageVO> selectPage(ValuePageQuery query);

	/**
	 * @Title selectLazy
	 * @Author zhongjyuan
	 * @Description 查询懒加载
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形结果对象
	 * @Throws
	 */
	List<ValueTreeVO> selectLazy(TreeLazyQuery<Long> query);

	/**
	 * @Title selectTree
	 * @Author zhongjyuan
	 * @Description 查询属性
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形结果对象
	 * @Throws
	 */
	List<ValueTreeVO> selectTree(TreeQuery<Long> query);

	/**
	 * @Title selectParents
	 * @Author zhongjyuan
	 * @Description 查询所有父级集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形结果对象
	 * @Throws
	 */
	List<ValueTreeVO> selectParents(TreeComplexQuery<Long> query);

	/**
	 * @Title selectChildrens
	 * @Author zhongjyuan
	 * @Description 查询所有子级集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形结果对象
	 * @Throws
	 */
	List<ValueTreeVO> selectChildrens(TreeComplexQuery<Long> query);

	/**
	 * @Title selectLeafs
	 * @Author zhongjyuan
	 * @Description 查询所有叶子节点集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形结果对象
	 * @Throws
	 */
	List<ValueTreeVO> selectLeafs(TreeComplexQuery<Long> query);

	/**
	 * @Title selectFulls
	 * @Author zhongjyuan
	 * @Description 查询全路径节点集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形结果都想
	 * @Throws
	 */
	List<ValueTreeVO> selectFulls(TreeComplexQuery<Long> query);
}
