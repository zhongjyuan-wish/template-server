package zhongjyuan.wish.template.domain.model.vo.response;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName RoleMutexPageVO
 * @Description 角色互斥关联分页对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleMutexPageVO", description = "角色互斥关联分页对象")
public class RoleMutexPageVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields roleId: 角色主键
	 */
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields mutexRoleId: 互斥角色主键
	 */
	@ApiModelProperty(value = "互斥角色主键", name = "mutexRoleId", dataType = "Long")
    private Long mutexRoleId;

	/**
	 * @Fields mutexRoleCode: 互斥角色编码
	 */
	@ApiModelProperty(value = "互斥角色编码", name = "mutexRoleCode", dataType = "String")
    private String mutexRoleCode;

	/**
	 * @Fields mutexRoleName: 互斥角色名称
	 */
	@ApiModelProperty(value = "互斥角色名称", name = "mutexRoleName", dataType = "String")
    private String mutexRoleName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;
}