package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.StationModuleMapper;
import zhongjyuan.wish.template.domain.service.IStationModuleService;

import zhongjyuan.wish.template.domain.model.entity.StationModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.StationModuleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName StationModuleServiceImpl
 * @Description 岗位模块关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class StationModuleServiceImpl implements IStationModuleService {

	@Autowired
	StationModuleMapper stationModuleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationModuleDO create(StationModuleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = stationModuleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		stationModuleMapper.insert(entity);

		log.info("======StationModuleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(StationModuleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationModuleDO update(StationModuleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		stationModuleMapper.updateById(entity);

		log.info("======StationModuleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return StationModuleDO 实体对象
	 * @Throws
	 */
	private StationModuleDO updateEnabled(StationModuleDO entity) {

		// 获取原始数据
		StationModuleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(StationModuleCode.STATIONMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationModuleDO delete(Long id) {

		// 获取原始数据
		StationModuleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		stationModuleMapper.updateById(originEntity);

		log.info("======StationModuleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<StationModuleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<StationModuleDO> entities = new ArrayList<StationModuleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<StationModuleDO> queryWrapper = new QueryWrapper<StationModuleDO>();
		queryWrapper.lambda().in(StationModuleDO::getId, ids);
		entities = stationModuleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<StationModuleDO> successEntities = new ArrayList<StationModuleDO>();
		
		// 根据主键更新
		for (StationModuleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			stationModuleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======StationModuleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(StationModuleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(StationModuleCode.STATIONMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public StationModuleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return stationModuleMapper.selectById(id);
	}
	
	@Override
	public List<StationModuleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return stationModuleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<StationModuleVO> selectList(StationModuleQuery query) {
		List<StationModuleVO> vos = new ArrayList<StationModuleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationModuleDO> queryWrapper = new QueryWrapper<StationModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<StationModuleDO> entities = stationModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = StationModuleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<StationModuleBriefVO> selectBriefList(StationModuleBriefQuery query) {
		List<StationModuleBriefVO> briefVOs = new ArrayList<StationModuleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationModuleDO> queryWrapper = new QueryWrapper<StationModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<StationModuleDO> entities = stationModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = StationModuleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<StationModulePageVO> selectPage(StationModulePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationModuleDO> queryWrapper = new QueryWrapper<StationModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationModuleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<StationModuleDO> page = stationModuleMapper.selectPage(new CustomPage<StationModuleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> StationModuleConverter.entityToPageVos(entities));
	}

}
