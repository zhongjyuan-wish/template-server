CREATE DEFINER=`idea_dev`@`172.56.8.26` PROCEDURE `proc_${table.name}_full_nodes`(IN type INT,
	IN showSiblings INT,
	IN showHitNodes INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点所在全路径所有节点'
BEGIN

	<#noparse>################## 临时存放节点 ###################</#noparse>
	DECLARE tempIdList LONGTEXT;

	<#noparse>################## 声明父序列变量 ###################</#noparse>
	<#noparse># 返回父节点结果集</#noparse>
	DECLARE parentIdList LONGTEXT;
	<#noparse># 临时存放父节点</#noparse>
	DECLARE tempParentIdList LONGTEXT;

	<#noparse>################## 声明子序列变量 ###################</#noparse>
	<#noparse># 返回叶子节点结果集</#noparse>
	DECLARE childIdList LONGTEXT;
	<#noparse># 临时存放子节点</#noparse>
	DECLARE tempChildIdList LONGTEXT;


	<#noparse>################## 查询父节点序列 ###################</#noparse>
	SET parentIdList = '';
	SET tempParentIdList = hitNodeIds;

	<#noparse># 循环，用于查询节点上所有的父节点</#noparse>
	WHILE tempParentIdList IS NOT NULL DO

		<#noparse># 存入到返回结果中</#noparse>
		SET parentIdList = CONCAT_WS(',', parentIdList, tempParentIdList);

		<#noparse># 查询节点上所有父节点</#noparse>
		SELECT GROUP_CONCAT(parent_id) INTO tempParentIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(id, tempParentIdList);-- id = tempParentIdList;
		
	END WHILE;

	<#noparse># 是否显示兄弟节点</#noparse>
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ','));

		SET parentIdList = CONCAT_WS(',', parentIdList, tempIdList);

	END IF;

	SET parentIdList = SUBSTRING(parentIdList, LENGTH(SUBSTRING_INDEX(parentIdList, ',', 2)) + 2);

	<#noparse># 是否包含命中节点</#noparse>
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET parentIdList = REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ',');

	END IF;    

	<#noparse>################## 查询子节点序列 ###################</#noparse>
	SET childIdList = '';
	SET tempChildIdList = hitNodeIds;

	<#noparse># 循环，用于查询节点下所有的子节点</#noparse>
	WHILE tempChildIdList IS NOT NULL DO

		<#noparse># 存入到返回结果中</#noparse>
		SET childIdList = CONCAT_WS(',', childIdList, tempChildIdList);

		<#noparse># 查询节点下所有子节点</#noparse>
		SELECT GROUP_CONCAT(id) INTO tempChildIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, tempChildIdList) > 0;
		
	END WHILE;

	<#noparse># 是否显示兄弟节点</#noparse>
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND childIdList != hitNodeIds THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, childIdList);

		SET childIdList = CONCAT_WS(',', childIdList, tempIdList);

	END IF;

	SET childIdList = SUBSTRING(childIdList, 2);

	<#noparse># 是否包含命中节点</#noparse>
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND childIdList != CONCAT(',', hitNodeIds) THEN

		SET childIdList = REPLACE(REPLACE(childIdList, hitNodeIds, ''), ',,', ',');

	END IF;


	<#noparse># 返回结果[主键字符串]</#noparse>
	IF type IS NULL OR type = 0 THEN
		SELECT CONCAT_WS(',' , parentIdList, childIdList) AS fullPathIds;
	END IF;

	<#noparse># 返回结果[数据集]</#noparse>
	IF type IS NOT NULL AND type = 1 THEN
		SELECT * FROM ${table.name} A
		WHERE A.is_deleted = 0
		AND FIND_IN_SET(A.id, CONCAT_WS(',' , parentIdList, childIdList))
		ORDER BY A.outline;
	END IF;
	<#noparse>
	# create by zhongjyuan
	</#noparse>
END