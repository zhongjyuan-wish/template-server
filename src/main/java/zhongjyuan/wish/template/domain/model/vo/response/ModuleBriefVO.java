package zhongjyuan.wish.template.domain.model.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName ModuleBriefVO
 * @Description 模块简要对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ModuleBriefVO", description = "模块简要对象")
public class ModuleBriefVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields group: 分组
	 */
	@ApiModelProperty(value = "分组", name = "group", dataType = "String")
    private String group;

	/**
	 * @Fields systemId: 系统主键
	 */
	@ApiModelProperty(value = "系统主键", name = "systemId", dataType = "Long")
    private Long systemId;

	/**
	 * @Fields systemCode: 系统编码
	 */
	@ApiModelProperty(value = "系统编码", name = "systemCode", dataType = "String")
    private String systemCode;

	/**
	 * @Fields systemName: 系统名称
	 */
	@ApiModelProperty(value = "系统名称", name = "systemName", dataType = "String")
    private String systemName;

	/**
	 * @Fields parentId: 父级主键
	 */
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;








	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}