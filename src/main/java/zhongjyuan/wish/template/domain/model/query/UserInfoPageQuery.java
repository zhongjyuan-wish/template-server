package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.PageQuery;

/**
 * @ClassName UserInfoPageQuery
 * @Description 用户信息分页查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserInfoPageQuery", description = "用户信息分页查询条件对象")
public class UserInfoPageQuery extends PageQuery {

	private static final long serialVersionUID = 1L;



	/**
	 * @Fields account: 账号
	 */
	@ApiModelProperty(value = "账号", name = "account", dataType = "String")
    private String account;

	/**
	 * @Fields aliasName: 别称
	 */
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields face: 头像
	 */
	@ApiModelProperty(value = "头像", name = "face", dataType = "String")
    private String face;

	/**
	 * @Fields email: 电子邮箱
	 */
	@ApiModelProperty(value = "电子邮箱", name = "email", dataType = "String")
    private String email;

	/**
	 * @Fields mobile: 移动号码
	 */
	@ApiModelProperty(value = "移动号码", name = "mobile", dataType = "String")
    private String mobile;

	/**
	 * @Fields telephone: 固定号码
	 */
	@ApiModelProperty(value = "固定号码", name = "telephone", dataType = "String")
    private String telephone;

	/**
	 * @Fields qq: QQ号码
	 */
	@ApiModelProperty(value = "QQ号码", name = "qq", dataType = "String")
    private String qq;

	/**
	 * @Fields weibo: 新浪微博
	 */
	@ApiModelProperty(value = "新浪微博", name = "weibo", dataType = "String")
    private String weibo;

	/**
	 * @Fields wechat: 微信号码
	 */
	@ApiModelProperty(value = "微信号码", name = "wechat", dataType = "String")
    private String wechat;

	/**
	 * @Fields signature: 签名
	 */
	@ApiModelProperty(value = "签名", name = "signature", dataType = "String")
    private String signature;








}