package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.PositionMapper;
import zhongjyuan.wish.template.domain.service.IPositionService;

import zhongjyuan.wish.template.domain.model.entity.PositionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.PositionCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName PositionServiceImpl
 * @Description 职业表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class PositionServiceImpl implements IPositionService {

	@Autowired
	PositionMapper positionMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionDO create(PositionDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = positionMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		positionMapper.insert(entity);

		log.info("======PositionServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(PositionDO entity) {

		// 是否存在相同有效编码数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionDO::getCode, entity.getCode());

		if (positionMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionCode.POSITION_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionDO::getName, entity.getName());

		if (positionMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionCode.POSITION_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionDO update(PositionDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		positionMapper.updateById(entity);

		log.info("======PositionServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return PositionDO 实体对象
	 * @Throws
	 */
	private PositionDO updateEnabled(PositionDO entity) {

		// 获取原始数据
		PositionDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(PositionCode.POSITION_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(PositionCode.POSITION_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(PositionCode.POSITION_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在相同有效编码数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionDO::getCode, entity.getCode());

		if (positionMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionCode.POSITION_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionDO::getName, entity.getName());

		if (positionMapper.exists(queryWrapper)) {
			throw new BusinessException(PositionCode.POSITION_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionDO delete(Long id) {

		// 获取原始数据
		PositionDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		positionMapper.updateById(originEntity);

		log.info("======PositionServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PositionDO deleteByCode(String code) {

		// 获取原始数据
		PositionDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		positionMapper.updateById(originEntity);

		log.info("======PositionServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<PositionDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<PositionDO> entities = new ArrayList<PositionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsSystem, false);
		queryWrapper.lambda().in(PositionDO::getId, ids);
		entities = positionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<PositionDO> successEntities = new ArrayList<PositionDO>();
		
		// 根据主键更新
		for (PositionDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			positionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======PositionServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<PositionDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<PositionDO> entities = new ArrayList<PositionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsSystem, false);
		queryWrapper.lambda().in(PositionDO::getCode, codes);
		entities = positionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<PositionDO> successEntities = new ArrayList<PositionDO>();
		
		// 根据主键更新
		for (PositionDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			positionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======PositionServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(PositionDO entity) {
	
		if (entity == null) {
			throw new BusinessException(PositionCode.POSITION_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(PositionCode.POSITION_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(PositionCode.POSITION_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public PositionDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return positionMapper.selectById(id);
	}
	
	@Override
	public PositionDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PositionDO::getCode, code);
		
		return positionMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<PositionDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return positionMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<PositionDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);
		queryWrapper.lambda().in(PositionDO::getCode, codes);
		
		return positionMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<PositionVO> selectList(PositionQuery query) {
		List<PositionVO> vos = new ArrayList<PositionVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PositionDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PositionDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PositionDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PositionDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<PositionDO> entities = positionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = PositionConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<PositionBriefVO> selectBriefList(PositionBriefQuery query) {
		List<PositionBriefVO> briefVOs = new ArrayList<PositionBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PositionDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PositionDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PositionDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PositionDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<PositionDO> entities = positionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = PositionConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<PositionPageVO> selectPage(PositionPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<PositionDO> queryWrapper = new QueryWrapper<PositionDO>();
		queryWrapper.lambda().eq(PositionDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PositionDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PositionDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PositionDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PositionDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<PositionDO> page = positionMapper.selectPage(new CustomPage<PositionDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> PositionConverter.entityToPageVos(entities));
	}

}
