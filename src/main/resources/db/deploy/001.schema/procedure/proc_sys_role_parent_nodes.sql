CREATE PROCEDURE `proc_sys_role_parent_nodes`(IN type INT,
	IN showSiblings INT,
	IN showHitNodes INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点上所有父节点'
BEGIN
		
	# 返回父节点结果集
	DECLARE parentIdList LONGTEXT;
	# 临时存放节点
	DECLARE tempIdList LONGTEXT;
	# 临时存放父节点
	DECLARE tempParentIdList LONGTEXT;

	SET parentIdList = '';
	SET tempParentIdList = hitNodeIds;

	# 循环，用于查询节点上所有的父节点
	WHILE tempParentIdList IS NOT NULL DO

		# 存入到返回结果中
		SET parentIdList = CONCAT_WS(',', parentIdList, tempParentIdList);
		
		# 查询节点上所有父节点
		SELECT GROUP_CONCAT(parent_id) INTO tempParentIdList FROM sys_role WHERE is_deleted = 0 AND FIND_IN_SET(id, tempParentIdList);-- id = tempParentIdList;
		
	END WHILE;

	# 是否加载对应兄弟节点(不包含命中节点的子级兄弟节点)
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM sys_role WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ','));

		SET parentIdList = CONCAT_WS(',', parentIdList, tempIdList);

	END IF;

	# 是否包含命中节点
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET parentIdList = REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ',');

	END IF;

	# 返回结果[主键字符串]
	IF type IS NULL OR type = 0 THEN
		SELECT SUBSTRING(parentIdList, 2) AS parentIds;
	END IF;

	# 返回结果[主键字符串]
	IF type IS NOT NULL AND type = 1 THEN
		SELECT * FROM sys_role A
		WHERE A.is_deleted = 0
		AND FIND_IN_SET(A.id, SUBSTRING(parentIdList, 2))
		ORDER BY A.outline;
	END IF;
	# create by zhongjyuan
END