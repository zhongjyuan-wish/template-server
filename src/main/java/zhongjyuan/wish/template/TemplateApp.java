package zhongjyuan.wish.template;

import zhongjyuan.wish.bootstrap.WishApplication;
import zhongjyuan.wish.cloud.bootstrap.annotation.WishCloudBootstrap;

/**
 * @ClassName TemplateApp
 * @Description 模板程序
 * @Author zhongjyuan
 * @Date 2023年2月8日 下午6:37:01
 * @Copyright zhongjyuan.com
 */
@WishCloudBootstrap
public class TemplateApp {

	public static void main(String[] args) {
		WishApplication.run(TemplateApp.class, args);
	}
}
