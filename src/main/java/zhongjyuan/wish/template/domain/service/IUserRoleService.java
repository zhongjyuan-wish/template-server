package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.UserRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IUserRoleService
 * @Description 用户角色关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IUserRoleService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	UserRoleDO create(UserRoleDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	UserRoleDO update(UserRoleDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	UserRoleDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<UserRoleDO> 实体对象集合
	 * @Throws
	 */
	List<UserRoleDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	UserRoleDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<UserRoleDO> 实体对象集合
	 * @Throws
	 */
	List<UserRoleDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<UserRoleVO> VO对象集合
	 * @Throws
	 */
	List<UserRoleVO> selectList(UserRoleQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<UserRoleBriefVO> 简要对象集合
	 * @Throws
	 */
	List<UserRoleBriefVO> selectBriefList(UserRoleBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<UserRolePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<UserRolePageVO> selectPage(UserRolePageQuery query);

}
