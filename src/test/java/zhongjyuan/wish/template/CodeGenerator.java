package zhongjyuan.wish.template;

import java.util.Arrays;

import org.junit.Test;

/**
 * @ClassName CodeGenerator
 * @Description 代码生成器
 * @Author zhongjyuan
 * @Date 2023年2月8日 下午3:53:36
 * @Copyright zhongjyuan.com
 */
public class CodeGenerator {

	@Test
	public void excute() {
		// 实例化生成器
		ServerGenerator generator = new ServerGenerator();

		// 执行
		generator.excute(Arrays.asList());
	}
}
