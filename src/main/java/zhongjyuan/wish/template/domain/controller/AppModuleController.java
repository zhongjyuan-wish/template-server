package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IAppModuleService;

import zhongjyuan.wish.template.domain.model.entity.AppModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName AppModuleController
 * @Description 应用模块关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "应用模块关联管理", tags = "应用模块关联管理")
@RequestMapping("{version}/appModules")
public class AppModuleController {

	@Autowired
	IAppModuleService appModuleService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "应用模块关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModuleVO> create(@RequestBody @Valid AppModuleCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppModuleDO entity = AppModuleConverter.createVoToEntity(createVO);
		entity = appModuleService.create(entity);
		return ResponseResult.success(AppModuleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用模块关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModuleVO> update(@RequestBody @Valid AppModuleUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppModuleDO entity = AppModuleConverter.updateVoToEntity(updateVO);
		entity = appModuleService.update(entity);
		return ResponseResult.success(AppModuleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用模块关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModuleVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppModuleDO> entities = appModuleService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppModuleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用模块关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModuleVO> delete(@PathVariable("id") Long id) {
		AppModuleDO entity = appModuleService.delete(id);
		return ResponseResult.success(AppModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModuleVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppModuleDO> entities = appModuleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppModuleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModuleVO> select(@PathVariable("id") Long id) {
		AppModuleDO entity = appModuleService.select(id);
		return ResponseResult.success(AppModuleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModuleVO>> selectList(@Valid AppModuleQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appModuleService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModuleBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppModuleDO> entities = appModuleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppModuleConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModuleBriefVO> selectBrief(@PathVariable("id") Long id) {
		AppModuleDO entity = appModuleService.select(id);
		return ResponseResult.success(AppModuleConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModuleBriefVO>> selectBriefList(@Valid AppModuleBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appModuleService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页应用模块关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<AppModulePageVO>> selectPage(@Valid AppModulePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appModuleService.selectPage(query));
	}
	
}
