package ${package.Mapper};

import ${superMapperClassPackage};
import ${package.Entity}.${entity};

<#if mapperAnnotation>
import org.apache.ibatis.annotations.Mapper;
</#if>
import org.apache.ibatis.annotations.Select;

<#if treeFlag>
import java.util.List;

import ${import_Model_TreeComplexQuery};
</#if>

/**
 * @ClassName ${table.mapperName}
 * @Description ${table.comment!}Mapper接口
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
<#if mapperAnnotation>
@Mapper
</#if>
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {

	/**
	 * @Title selectMaxRowIndex
	 * @Author ${author}
	 * @Description 查询最大行号
	 * @Param @return
	 * @Return Integer 最大行号
	 * @Throws
	 */
	@Select("SELECT IFNULL(MAX(ROW_INDEX),0) FROM ${table.name};")
	Integer selectMaxRowIndex();
	
<#if treeFlag>
	/**
	 * @Title updateOutline
	 * @Author ${author}
	 * @Description 更新层级码
	 * @Param
	 * @Return void
	 * @Throws
	 */
	void updateOutline();

	/**
	 * @Title selectParents
	 * @Author ${author}
	 * @Description 查询所有父级
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> selectParents(TreeComplexQuery<${keyPropertyType}> complexQuery);

	/**
	 * @Title selectChildrens
	 * @Author ${author}
	 * @Description 查询所有子级
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> selectChildrens(TreeComplexQuery<${keyPropertyType}> complexQuery);

	/**
	 * @Title selectLeafs
	 * @Author ${author}
	 * @Description 查询所有叶子节点
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> selectLeafs(TreeComplexQuery<${keyPropertyType}> complexQuery);

	/**
	 * @Title selectFulls
	 * @Author ${author}
	 * @Description 查询全路径节点
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> selectFulls(TreeComplexQuery<${keyPropertyType}> complexQuery);
</#if>
}
</#if>
