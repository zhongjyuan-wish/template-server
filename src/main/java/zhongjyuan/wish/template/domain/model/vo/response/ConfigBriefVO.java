package zhongjyuan.wish.template.domain.model.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName ConfigBriefVO
 * @Description 配置简要对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ConfigBriefVO", description = "配置简要对象")
public class ConfigBriefVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields value: 值
	 */
	@ApiModelProperty(value = "值", name = "value", dataType = "String")
    private String value;

	/**
	 * @Fields businessId: 业务主键
	 */
	@ApiModelProperty(value = "业务主键", name = "businessId", dataType = "Long")
    private Long businessId;

	/**
	 * @Fields businessCode: 业务编码
	 */
	@ApiModelProperty(value = "业务编码", name = "businessCode", dataType = "String")
    private String businessCode;

	/**
	 * @Fields businessName: 业务名称
	 */
	@ApiModelProperty(value = "业务名称", name = "businessName", dataType = "String")
    private String businessName;




	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}