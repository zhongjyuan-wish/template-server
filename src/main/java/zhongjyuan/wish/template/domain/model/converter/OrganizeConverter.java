package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.OrganizeDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName OrganizeConverter
 * @Description 组织转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class OrganizeConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return OrganizeDO 实体对象
	 * @Throws
	 */
	public static OrganizeDO createVoToEntity(OrganizeCreateVO createVO) {
		OrganizeDO entity = new OrganizeDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return OrganizeDO 实体对象
	 * @Throws
	 */
	public static OrganizeDO updateVoToEntity(OrganizeUpdateVO updateVO) {
		OrganizeDO entity = new OrganizeDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeVO VO对象
	 * @Throws
	 */
	public static OrganizeVO entityToVo(OrganizeDO entity) {

		OrganizeVO vo = new OrganizeVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeBriefVO 简要对象
	 * @Throws
	 */
	public static OrganizeBriefVO entityToBriefVo(OrganizeDO entity) {

		OrganizeBriefVO briefVO = new OrganizeBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizeVO> VO对象集合
	 * @Throws
	 */
	public static List<OrganizeVO> entityToVos(List<OrganizeDO> entities) {

		List<OrganizeVO> vos = new ArrayList<OrganizeVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, OrganizeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizeBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<OrganizeBriefVO> entityToBriefVos(List<OrganizeDO> entities) {

		List<OrganizeBriefVO> briefVOs = new ArrayList<OrganizeBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, OrganizeBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<OrganizePageVO> entityToPageVos(List<OrganizeDO> entities) {

		List<OrganizePageVO> pageVOs = new ArrayList<OrganizePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, OrganizePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
	/**
	 * @Title entityToTreeVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转树形对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizeTreeVO> 树形对象集合
	 * @Throws
	 */
	public static List<OrganizeTreeVO> entityToTreeVos(List<OrganizeDO> entities) {

		List<OrganizeTreeVO> treeVOs = new ArrayList<OrganizeTreeVO>();

		/*
		 * 对象转换:
		 */
		treeVOs = BeanUtil.copyListProperties(entities, OrganizeTreeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return treeVOs;
	}
}