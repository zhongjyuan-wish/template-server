package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName AppQuery
 * @Description 应用查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppQuery", description = "应用查询条件对象")
public class AppQuery extends SortQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields group: 分组
	 */
	@ApiModelProperty(value = "分组", name = "group", dataType = "Long")
    private Long group;

	/**
	 * @Fields type: 类型[PC;Mobile;Pad]
	 */
	@ApiModelProperty(value = "类型[PC;Mobile;Pad]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields blank: 打开方式
	 */
	@ApiModelProperty(value = "打开方式", name = "blank", dataType = "String")
    private String blank;

	/**
	 * @Fields indexUrl: 首页地址
	 */
	@ApiModelProperty(value = "首页地址", name = "indexUrl", dataType = "String")
    private String indexUrl;

	/**
	 * @Fields keyt: 密钥
	 */
	@ApiModelProperty(value = "密钥", name = "keyt", dataType = "String")
    private String keyt;

	/**
	 * @Fields access: 授权码
	 */
	@ApiModelProperty(value = "授权码", name = "access", dataType = "String")
    private String access;

	/**
	 * @Fields refresh: 令牌刷新时间
	 */
	@ApiModelProperty(value = "令牌刷新时间", name = "refresh", dataType = "Integer")
    private Integer refresh;

	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isBusiness: 是否业务
	 */
	@ApiModelProperty(value = "是否业务", name = "isBusiness", dataType = "Boolean")
    private Boolean isBusiness;

	/**
	 * @Fields isExternal: 是否外部
	 */
	@ApiModelProperty(value = "是否外部", name = "isExternal", dataType = "Boolean")
    private Boolean isExternal;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}