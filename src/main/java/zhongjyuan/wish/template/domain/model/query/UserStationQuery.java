package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName UserStationQuery
 * @Description 用户岗位关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserStationQuery", description = "用户岗位关联查询条件对象")
public class UserStationQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields userId: 用户主键
	 */
	@ApiModelProperty(value = "用户主键", name = "userId", dataType = "Long")
    private Long userId;

	/**
	 * @Fields userName: 用户名称
	 */
	@ApiModelProperty(value = "用户名称", name = "userName", dataType = "String")
    private String userName;

	/**
	 * @Fields userAccount: 用户账号
	 */
	@ApiModelProperty(value = "用户账号", name = "userAccount", dataType = "String")
    private String userAccount;

	/**
	 * @Fields stationId: 岗位主键
	 */
	@ApiModelProperty(value = "岗位主键", name = "stationId", dataType = "Long")
    private Long stationId;

	/**
	 * @Fields stationCode: 岗位编码
	 */
	@ApiModelProperty(value = "岗位编码", name = "stationCode", dataType = "String")
    private String stationCode;

	/**
	 * @Fields stationName: 岗位名称
	 */
	@ApiModelProperty(value = "岗位名称", name = "stationName", dataType = "String")
    private String stationName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}