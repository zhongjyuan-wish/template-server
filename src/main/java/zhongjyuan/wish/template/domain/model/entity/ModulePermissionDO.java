package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName ModulePermissionDO
 * @Description 模块权限关联表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_module_permission")
@ApiModel(value = "ModulePermissionDO", description = "模块权限关联表实体对象")
public class ModulePermissionDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields moduleId: 模块主键
	 */
    @TableField("module_id")
	@ApiModelProperty(value = "模块主键", name = "moduleId", dataType = "Long")
    private Long moduleId;

	/**
	 * @Fields moduleCode: 模块编码
	 */
    @TableField("module_code")
	@ApiModelProperty(value = "模块编码", name = "moduleCode", dataType = "String")
    private String moduleCode;

	/**
	 * @Fields moduleName: 模块名称
	 */
    @TableField("module_name")
	@ApiModelProperty(value = "模块名称", name = "moduleName", dataType = "String")
    private String moduleName;

	/**
	 * @Fields permissionId: 权限主键
	 */
    @TableField("permission_id")
	@ApiModelProperty(value = "权限主键", name = "permissionId", dataType = "Long")
    private Long permissionId;

	/**
	 * @Fields permissionCode: 权限编码
	 */
    @TableField("permission_code")
	@ApiModelProperty(value = "权限编码", name = "permissionCode", dataType = "String")
    private String permissionCode;

	/**
	 * @Fields permissionName: 权限名称
	 */
    @TableField("permission_name")
	@ApiModelProperty(value = "权限名称", name = "permissionName", dataType = "String")
    private String permissionName;

	/**
	 * @Fields status: 状态
	 */
    @TableField("status")
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;


    public static final String ID = "id";

    public static final String MODULE_ID = "module_id";

    public static final String MODULE_CODE = "module_code";

    public static final String MODULE_NAME = "module_name";

    public static final String PERMISSION_ID = "permission_id";

    public static final String PERMISSION_CODE = "permission_code";

    public static final String PERMISSION_NAME = "permission_name";

    public static final String STATUS = "status";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATE_TIME = "create_time";

}
