package zhongjyuan.wish.template;

import java.util.Arrays;

import zhongjyuan.wish.batis.generate.CustomAutoGenerator;

/**
 * @ClassName ServerGenerator
 * @Description 服务生成器
 * @Author zhongjyuan
 * @Date 2023年2月8日 下午5:44:24
 * @Copyright zhongjyuan.com
 */
public class ServerGenerator extends CustomAutoGenerator {

	@Override
	public void initialize() {
		super.initialize();

		// 自定义属性
		datasource_url = "jdbc:mysql://10.10.3.84:2433/z_sys?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8";
		datasource_userName = "root";
		datasource_password = "7roaddba";

		package_moduleName = "template.domain";
		strategy_tablePrefix = Arrays.asList("sys_");
	}

	public static void main(String[] args) {

		// 实例化生成器
		ServerGenerator generator = new ServerGenerator();

		// 执行
		generator.excute(Arrays.asList());
	}
}
