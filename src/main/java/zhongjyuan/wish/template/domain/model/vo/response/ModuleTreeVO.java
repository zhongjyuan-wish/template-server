package zhongjyuan.wish.template.domain.model.vo.response;

import java.util.List;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName ModuleTreeVO
 * @Description 模块树形对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ModuleTreeVO", description = "模块树形对象")
public class ModuleTreeVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields group: 分组
	 */
	@ApiModelProperty(value = "分组", name = "group", dataType = "String")
    private String group;

	/**
	 * @Fields systemId: 系统主键
	 */
	@ApiModelProperty(value = "系统主键", name = "systemId", dataType = "Long")
    private Long systemId;

	/**
	 * @Fields systemCode: 系统编码
	 */
	@ApiModelProperty(value = "系统编码", name = "systemCode", dataType = "String")
    private String systemCode;

	/**
	 * @Fields systemName: 系统名称
	 */
	@ApiModelProperty(value = "系统名称", name = "systemName", dataType = "String")
    private String systemName;

	/**
	 * @Fields parentId: 父级主键
	 */
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;


	/**
	 * @Fields level: 层级
	 */
	@ApiModelProperty(value = "层级", name = "level", dataType = "Integer")
    private Integer level;



	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;


	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;

	/**
	 * @Fields isLeaf: 是否叶子节点
	 */
	@ApiModelProperty(value = "是否叶子节点", name = "isLeaf", dataType = "Boolean")
	private Boolean isLeaf;
	/**
	 * @Fields hasChildren: 是否有子级
	 */
	@ApiModelProperty(value = "是否有子级", name = "hasChildren", dataType = "Boolean")
	private Boolean hasChildren;
	
	/**
	 * @Fields children: 子级集合
	 */
	@ApiModelProperty(value = "子级集合", name = "childrens", dataType = "List<ModuleTreeVO>")
	private List<ModuleTreeVO> children;
}