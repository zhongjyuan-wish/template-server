package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.CodeMapper;
import zhongjyuan.wish.template.domain.service.ICodeService;

import zhongjyuan.wish.template.domain.model.entity.CodeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.CodeCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName CodeServiceImpl
 * @Description 码表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class CodeServiceImpl implements ICodeService {

	@Autowired
	CodeMapper codeMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public CodeDO create(CodeDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = codeMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		codeMapper.insert(entity);

		log.info("======CodeServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(CodeDO entity) {

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(CodeDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(CodeDO::getGroup, entity.getGroup());
		}

		if (codeMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(CodeCode.CODE_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(CodeCode.CODE_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(CodeDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(CodeDO::getGroup, entity.getGroup());
		}

		if (codeMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(CodeCode.CODE_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(CodeCode.CODE_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public CodeDO update(CodeDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		codeMapper.updateById(entity);

		log.info("======CodeServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	private CodeDO updateEnabled(CodeDO entity) {

		// 获取原始数据
		CodeDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(CodeCode.CODE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(CodeCode.CODE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(CodeCode.CODE_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(CodeDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(CodeDO::getGroup, entity.getGroup());
		}

		if (codeMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(CodeCode.CODE_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(CodeCode.CODE_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(CodeDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(CodeDO::getGroup, entity.getGroup());
		}

		if (codeMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(CodeCode.CODE_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(CodeCode.CODE_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public CodeDO delete(Long id) {

		// 获取原始数据
		CodeDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		codeMapper.updateById(originEntity);

		log.info("======CodeServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public CodeDO deleteByCode(String code) {

		// 获取原始数据
		CodeDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		codeMapper.updateById(originEntity);

		log.info("======CodeServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<CodeDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<CodeDO> entities = new ArrayList<CodeDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsSystem, false);
		queryWrapper.lambda().in(CodeDO::getId, ids);
		entities = codeMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<CodeDO> successEntities = new ArrayList<CodeDO>();
		
		// 根据主键更新
		for (CodeDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			codeMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======CodeServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<CodeDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<CodeDO> entities = new ArrayList<CodeDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsSystem, false);
		queryWrapper.lambda().in(CodeDO::getCode, codes);
		entities = codeMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<CodeDO> successEntities = new ArrayList<CodeDO>();
		
		// 根据主键更新
		for (CodeDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			codeMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======CodeServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(CodeDO entity) {
	
		if (entity == null) {
			throw new BusinessException(CodeCode.CODE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(CodeCode.CODE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(CodeCode.CODE_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public CodeDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return codeMapper.selectById(id);
	}
	
	@Override
	public CodeDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(CodeDO::getCode, code);
		
		return codeMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<CodeDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return codeMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<CodeDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);
		queryWrapper.lambda().in(CodeDO::getCode, codes);
		
		return codeMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<CodeVO> selectList(CodeQuery query) {
		List<CodeVO> vos = new ArrayList<CodeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(CodeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(CodeDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(CodeDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(CodeDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<CodeDO> entities = codeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = CodeConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<CodeBriefVO> selectBriefList(CodeBriefQuery query) {
		List<CodeBriefVO> briefVOs = new ArrayList<CodeBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(CodeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(CodeDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(CodeDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(CodeDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<CodeDO> entities = codeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = CodeConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<CodePageVO> selectPage(CodePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<CodeDO> queryWrapper = new QueryWrapper<CodeDO>();
		queryWrapper.lambda().eq(CodeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(CodeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(CodeDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(CodeDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(CodeDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<CodeDO> page = codeMapper.selectPage(new CustomPage<CodeDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> CodeConverter.entityToPageVos(entities));
	}

}
