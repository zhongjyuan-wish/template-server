package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.RoleMergeMapper;
import zhongjyuan.wish.template.domain.service.IRoleMergeService;

import zhongjyuan.wish.template.domain.model.entity.RoleMergeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.RoleMergeCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName RoleMergeServiceImpl
 * @Description 角色合并关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class RoleMergeServiceImpl implements IRoleMergeService {

	@Autowired
	RoleMergeMapper roleMergeMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleMergeDO create(RoleMergeDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = roleMergeMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		roleMergeMapper.insert(entity);

		log.info("======RoleMergeServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(RoleMergeDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleMergeDO update(RoleMergeDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		roleMergeMapper.updateById(entity);

		log.info("======RoleMergeServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMergeDO 实体对象
	 * @Throws
	 */
	private RoleMergeDO updateEnabled(RoleMergeDO entity) {

		// 获取原始数据
		RoleMergeDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(RoleMergeCode.ROLEMERGE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleMergeDO delete(Long id) {

		// 获取原始数据
		RoleMergeDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleMergeMapper.updateById(originEntity);

		log.info("======RoleMergeServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleMergeDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<RoleMergeDO> entities = new ArrayList<RoleMergeDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleMergeDO> queryWrapper = new QueryWrapper<RoleMergeDO>();
		queryWrapper.lambda().in(RoleMergeDO::getId, ids);
		entities = roleMergeMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleMergeDO> successEntities = new ArrayList<RoleMergeDO>();
		
		// 根据主键更新
		for (RoleMergeDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			roleMergeMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleMergeServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(RoleMergeDO entity) {
	
		if (entity == null) {
			throw new BusinessException(RoleMergeCode.ROLEMERGE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public RoleMergeDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return roleMergeMapper.selectById(id);
	}
	
	@Override
	public List<RoleMergeDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return roleMergeMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<RoleMergeVO> selectList(RoleMergeQuery query) {
		List<RoleMergeVO> vos = new ArrayList<RoleMergeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleMergeDO> queryWrapper = new QueryWrapper<RoleMergeDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleMergeDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleMergeDO> entities = roleMergeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = RoleMergeConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<RoleMergeBriefVO> selectBriefList(RoleMergeBriefQuery query) {
		List<RoleMergeBriefVO> briefVOs = new ArrayList<RoleMergeBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleMergeDO> queryWrapper = new QueryWrapper<RoleMergeDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleMergeDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleMergeDO> entities = roleMergeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = RoleMergeConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<RoleMergePageVO> selectPage(RoleMergePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleMergeDO> queryWrapper = new QueryWrapper<RoleMergeDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleMergeDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<RoleMergeDO> page = roleMergeMapper.selectPage(new CustomPage<RoleMergeDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> RoleMergeConverter.entityToPageVos(entities));
	}

}
