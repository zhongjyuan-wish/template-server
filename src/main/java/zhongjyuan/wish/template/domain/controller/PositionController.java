package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IPositionService;

import zhongjyuan.wish.template.domain.model.entity.PositionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName PositionController
 * @Description 职业管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "职业管理", tags = "职业管理")
@RequestMapping("{version}/positions")
public class PositionController {

	@Autowired
	IPositionService positionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> create(@RequestBody @Valid PositionCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		PositionDO entity = PositionConverter.createVoToEntity(createVO);
		entity = positionService.create(entity);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> update(@RequestBody @Valid PositionUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		PositionDO entity = PositionConverter.updateVoToEntity(updateVO);
		entity = positionService.update(entity);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionDO> entities = positionService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionDO> entities = positionService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> delete(@PathVariable("id") Long id) {
		PositionDO entity = positionService.delete(id);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> deleteByCode(@PathVariable("code") String code) {
		PositionDO entity = positionService.deleteByCode(code);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> enable(@PathVariable("id") Long id) {
		PositionDO entity = positionService.select(id);
		entity.setIsEnabled(true);
		entity = positionService.update(entity);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> enableByCode(@PathVariable("code") String code) {
		PositionDO entity = positionService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = positionService.update(entity);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> disable(@PathVariable("id") Long id) {
		PositionDO entity = positionService.select(id);
		entity.setIsEnabled(false);
		entity = positionService.update(entity);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "职业", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> disableByCode(@PathVariable("code") String code) {
		PositionDO entity = positionService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = positionService.update(entity);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionDO> entities = positionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionDO> entities = positionService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> select(@PathVariable("id") Long id) {
		PositionDO entity = positionService.select(id);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionVO> selectByCode(@PathVariable("code") String code) {
		PositionDO entity = positionService.selectByCode(code);
		return ResponseResult.success(PositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionVO>> selectList(@Valid PositionQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(positionService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<PositionDO> entities = positionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(PositionConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionBriefVO> selectBrief(@PathVariable("id") Long id) {
		PositionDO entity = positionService.select(id);
		return ResponseResult.success(PositionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PositionBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		PositionDO entity = positionService.selectByCode(code);
		return ResponseResult.success(PositionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<PositionBriefVO>> selectBriefList(@Valid PositionBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(positionService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "职业", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页职业 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<PositionPageVO>> selectPage(@Valid PositionPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(positionService.selectPage(query));
	}
	
}
