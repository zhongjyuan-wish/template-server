package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName UserInfoDO
 * @Description 用户信息表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_user_info")
@ApiModel(value = "UserInfoDO", description = "用户信息表实体对象")
public class UserInfoDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields name: 名称
	 */
    @TableField("name")
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields account: 账号
	 */
    @TableField("account")
	@ApiModelProperty(value = "账号", name = "account", dataType = "String")
    private String account;

	/**
	 * @Fields aliasName: 别称
	 */
    @TableField("alias_name")
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields face: 头像
	 */
    @TableField("face")
	@ApiModelProperty(value = "头像", name = "face", dataType = "String")
    private String face;

	/**
	 * @Fields email: 电子邮箱
	 */
    @TableField("email")
	@ApiModelProperty(value = "电子邮箱", name = "email", dataType = "String")
    private String email;

	/**
	 * @Fields mobile: 移动号码
	 */
    @TableField("mobile")
	@ApiModelProperty(value = "移动号码", name = "mobile", dataType = "String")
    private String mobile;

	/**
	 * @Fields telephone: 固定号码
	 */
    @TableField("telephone")
	@ApiModelProperty(value = "固定号码", name = "telephone", dataType = "String")
    private String telephone;

	/**
	 * @Fields qq: QQ号码
	 */
    @TableField("qq")
	@ApiModelProperty(value = "QQ号码", name = "qq", dataType = "String")
    private String qq;

	/**
	 * @Fields weibo: 新浪微博
	 */
    @TableField("weibo")
	@ApiModelProperty(value = "新浪微博", name = "weibo", dataType = "String")
    private String weibo;

	/**
	 * @Fields wechat: 微信号码
	 */
    @TableField("wechat")
	@ApiModelProperty(value = "微信号码", name = "wechat", dataType = "String")
    private String wechat;

	/**
	 * @Fields signature: 签名
	 */
    @TableField("signature")
	@ApiModelProperty(value = "签名", name = "signature", dataType = "String")
    private String signature;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
    @TableField("creator_id")
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
    @TableField("creator_name")
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
    @TableField("updator_id")
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
    @TableField("updator_name")
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
    @TableField("update_time")
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;


    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String ACCOUNT = "account";

    public static final String ALIAS_NAME = "alias_name";

    public static final String FACE = "face";

    public static final String EMAIL = "email";

    public static final String MOBILE = "mobile";

    public static final String TELEPHONE = "telephone";

    public static final String QQ = "qq";

    public static final String WEIBO = "weibo";

    public static final String WECHAT = "wechat";

    public static final String SIGNATURE = "signature";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATOR_ID = "creator_id";

    public static final String CREATOR_NAME = "creator_name";

    public static final String CREATE_TIME = "create_time";

    public static final String UPDATOR_ID = "updator_id";

    public static final String UPDATOR_NAME = "updator_name";

    public static final String UPDATE_TIME = "update_time";

}
