package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.StationMapper;
import zhongjyuan.wish.template.domain.service.IStationService;

import zhongjyuan.wish.template.domain.model.entity.StationDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.StationCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName StationServiceImpl
 * @Description 岗位表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class StationServiceImpl implements IStationService {

	@Autowired
	StationMapper stationMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationDO create(StationDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = stationMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		stationMapper.insert(entity);

		log.info("======StationServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(StationDO entity) {

		// 是否存在相同有效编码数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);
		queryWrapper.lambda().eq(StationDO::getCode, entity.getCode());

		if (stationMapper.exists(queryWrapper)) {
			throw new BusinessException(StationCode.STATION_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);
		queryWrapper.lambda().eq(StationDO::getName, entity.getName());

		if (stationMapper.exists(queryWrapper)) {
			throw new BusinessException(StationCode.STATION_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationDO update(StationDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		stationMapper.updateById(entity);

		log.info("======StationServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return StationDO 实体对象
	 * @Throws
	 */
	private StationDO updateEnabled(StationDO entity) {

		// 获取原始数据
		StationDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(StationCode.STATION_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(StationCode.STATION_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(StationCode.STATION_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在相同有效编码数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);
		queryWrapper.lambda().eq(StationDO::getCode, entity.getCode());

		if (stationMapper.exists(queryWrapper)) {
			throw new BusinessException(StationCode.STATION_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);
		queryWrapper.lambda().eq(StationDO::getName, entity.getName());

		if (stationMapper.exists(queryWrapper)) {
			throw new BusinessException(StationCode.STATION_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationDO delete(Long id) {

		// 获取原始数据
		StationDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		stationMapper.updateById(originEntity);

		log.info("======StationServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationDO deleteByCode(String code) {

		// 获取原始数据
		StationDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		stationMapper.updateById(originEntity);

		log.info("======StationServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<StationDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<StationDO> entities = new ArrayList<StationDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsSystem, false);
		queryWrapper.lambda().in(StationDO::getId, ids);
		entities = stationMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<StationDO> successEntities = new ArrayList<StationDO>();
		
		// 根据主键更新
		for (StationDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			stationMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======StationServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<StationDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<StationDO> entities = new ArrayList<StationDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsSystem, false);
		queryWrapper.lambda().in(StationDO::getCode, codes);
		entities = stationMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<StationDO> successEntities = new ArrayList<StationDO>();
		
		// 根据主键更新
		for (StationDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			stationMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======StationServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(StationDO entity) {
	
		if (entity == null) {
			throw new BusinessException(StationCode.STATION_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(StationCode.STATION_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(StationCode.STATION_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public StationDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return stationMapper.selectById(id);
	}
	
	@Override
	public StationDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);
		queryWrapper.lambda().eq(StationDO::getCode, code);
		
		return stationMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<StationDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return stationMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<StationDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);
		queryWrapper.lambda().in(StationDO::getCode, codes);
		
		return stationMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<StationVO> selectList(StationQuery query) {
		List<StationVO> vos = new ArrayList<StationVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(StationDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(StationDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(StationDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<StationDO> entities = stationMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = StationConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<StationBriefVO> selectBriefList(StationBriefQuery query) {
		List<StationBriefVO> briefVOs = new ArrayList<StationBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(StationDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(StationDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(StationDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<StationDO> entities = stationMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = StationConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<StationPageVO> selectPage(StationPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationDO> queryWrapper = new QueryWrapper<StationDO>();
		queryWrapper.lambda().eq(StationDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(StationDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(StationDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(StationDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<StationDO> page = stationMapper.selectPage(new CustomPage<StationDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> StationConverter.entityToPageVos(entities));
	}

}
