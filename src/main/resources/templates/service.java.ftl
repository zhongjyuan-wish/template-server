package ${package.Service};

import java.util.List;

import ${import_Model_PageResult};
<#if treeFlag>
import ${import_Model_Query};
</#if>

import ${package.Entity}.${entity};
import ${package.Parent}.${package_Query}.*;
import ${package.Parent}.${package_ResponseVO}.*;

/**
 * @ClassName ${table.serviceName}
 * @Description ${table.comment!}服务接口
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} {

	/**
	 * @Title create
	 * @Author ${author}
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	${entity} create(${entity} entity);

	/**
	 * @Title update
	 * @Author ${author}
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	${entity} update(${entity} entity);

	/**
	 * @Title delete
	 * @Author ${author}
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	${entity} delete(${keyPropertyType} id);

<#if codeFlag>
	/**
	 * @Title deleteByCode
	 * @Author ${author}
	 * @Description 根据编码删除
	 * @Param @param code 编码
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	${entity} deleteByCode(String code);
	
</#if>
	/**
	 * @Title delete
	 * @Author ${author}
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> delete(List<${keyPropertyType}> ids);

<#if codeFlag>
	/**
	 * @Title deleteByCode
	 * @Author ${author}
	 * @Description 根据编码删除
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> deleteByCode(List<String> codes);
	
</#if>
	/**
	 * @Title select
	 * @Author ${author}
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	${entity} select(${keyPropertyType} id);

<#if codeFlag>
	/**
	 * @Title selectByCode
	 * @Author ${author}
	 * @Description 根据编码查询
	 * @Param @param code 编码
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	${entity} selectByCode(String code);
	
</#if>
	/**
	 * @Title select
	 * @Author ${author}
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> select(List<${keyPropertyType}> ids);

<#if codeFlag>
	/**
	 * @Title selectByCode
	 * @Author ${author}
	 * @Description 根据编码查询
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<${entity}> 实体对象集合
	 * @Throws
	 */
	List<${entity}> selectByCode(List<String> codes);
	
</#if>
	/**
	 * @Title selectList
	 * @Author ${author}
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}VO> VO对象集合
	 * @Throws
	 */
	List<${entityNaming}VO> selectList(${entityNaming}Query query);

	/**
	 * @Title selectBriefList
	 * @Author ${author}
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}BriefVO> 简要对象集合
	 * @Throws
	 */
	List<${entityNaming}BriefVO> selectBriefList(${entityNaming}BriefQuery query);

	/**
	 * @Title selectPage
	 * @Author ${author}
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<${entityNaming}PageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<${entityNaming}PageVO> selectPage(${entityNaming}PageQuery query);

<#if treeFlag>
	/**
	 * @Title selectLazy
	 * @Author ${author}
	 * @Description 查询懒加载
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形结果对象
	 * @Throws
	 */
	List<${entityNaming}TreeVO> selectLazy(TreeLazyQuery<${keyPropertyType}> query);

	/**
	 * @Title selectTree
	 * @Author ${author}
	 * @Description 查询属性
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形结果对象
	 * @Throws
	 */
	List<${entityNaming}TreeVO> selectTree(TreeQuery<${keyPropertyType}> query);

	/**
	 * @Title selectParents
	 * @Author ${author}
	 * @Description 查询所有父级集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形结果对象
	 * @Throws
	 */
	List<${entityNaming}TreeVO> selectParents(TreeComplexQuery<${keyPropertyType}> query);

	/**
	 * @Title selectChildrens
	 * @Author ${author}
	 * @Description 查询所有子级集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形结果对象
	 * @Throws
	 */
	List<${entityNaming}TreeVO> selectChildrens(TreeComplexQuery<${keyPropertyType}> query);

	/**
	 * @Title selectLeafs
	 * @Author ${author}
	 * @Description 查询所有叶子节点集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形结果对象
	 * @Throws
	 */
	List<${entityNaming}TreeVO> selectLeafs(TreeComplexQuery<${keyPropertyType}> query);

	/**
	 * @Title selectFulls
	 * @Author ${author}
	 * @Description 查询全路径节点集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形结果都想
	 * @Throws
	 */
	List<${entityNaming}TreeVO> selectFulls(TreeComplexQuery<${keyPropertyType}> query);
</#if>
}
</#if>
