package ${package.Parent}.${package_RequestVO};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import ${import_Model_AbstractModel};

/**
 * @ClassName ${entityNaming}CreateVO
 * @Description ${table.comment?replace("表","")}创建对象
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${entityNaming}CreateVO", description = "${table.comment?replace("表","")}创建对象")
public class ${entityNaming}CreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>

    <#-- 主键 -->
    <#if field.keyFlag>
    <#-- 普通字段 -->
    <#elseif field.fill??>
    <#elseif field.convert>
    <#if !createvo_ignorePropertyName?seq_contains(field.propertyName)>
    <#if field.comment!?length gt 0>
	/**
	 * @Fields ${field.propertyName}: ${field.comment}
	 */
	// @NotNull(message = "${field.comment}不能为空.")
	// @NotBlank(message = "${field.comment}不能为空.")
	// @Size(message = "${field.comment}最大长度为50.", max = 50)
	// @Min(message = "${field.comment}最小为600.", value = 600)
        <#if swagger>
	@ApiModelProperty(value = "${field.comment}", name = "${field.propertyName}", dataType = "${field.propertyType}")
        </#if>
    </#if>
    private ${field.propertyType} ${field.propertyName};
    </#if>
    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->
}