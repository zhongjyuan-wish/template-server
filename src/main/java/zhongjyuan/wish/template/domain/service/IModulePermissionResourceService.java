package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionResourceDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IModulePermissionResourceService
 * @Description 模块权限资源关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IModulePermissionResourceService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	ModulePermissionResourceDO create(ModulePermissionResourceDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	ModulePermissionResourceDO update(ModulePermissionResourceDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	ModulePermissionResourceDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<ModulePermissionResourceDO> 实体对象集合
	 * @Throws
	 */
	List<ModulePermissionResourceDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	ModulePermissionResourceDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<ModulePermissionResourceDO> 实体对象集合
	 * @Throws
	 */
	List<ModulePermissionResourceDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ModulePermissionResourceVO> VO对象集合
	 * @Throws
	 */
	List<ModulePermissionResourceVO> selectList(ModulePermissionResourceQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ModulePermissionResourceBriefVO> 简要对象集合
	 * @Throws
	 */
	List<ModulePermissionResourceBriefVO> selectBriefList(ModulePermissionResourceBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<ModulePermissionResourcePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<ModulePermissionResourcePageVO> selectPage(ModulePermissionResourcePageQuery query);

}
