package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IModulePermissionService
 * @Description 模块权限关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IModulePermissionService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	ModulePermissionDO create(ModulePermissionDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	ModulePermissionDO update(ModulePermissionDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	ModulePermissionDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<ModulePermissionDO> 实体对象集合
	 * @Throws
	 */
	List<ModulePermissionDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	ModulePermissionDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<ModulePermissionDO> 实体对象集合
	 * @Throws
	 */
	List<ModulePermissionDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ModulePermissionVO> VO对象集合
	 * @Throws
	 */
	List<ModulePermissionVO> selectList(ModulePermissionQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<ModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	List<ModulePermissionBriefVO> selectBriefList(ModulePermissionBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<ModulePermissionPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<ModulePermissionPageVO> selectPage(ModulePermissionPageQuery query);

}
