package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.RoleModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IRoleModuleService
 * @Description 角色模块关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IRoleModuleService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModuleDO 实体对象
	 * @Throws
	 */
	RoleModuleDO create(RoleModuleDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModuleDO 实体对象
	 * @Throws
	 */
	RoleModuleDO update(RoleModuleDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleModuleDO 实体对象
	 * @Throws
	 */
	RoleModuleDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleModuleDO> 实体对象集合
	 * @Throws
	 */
	List<RoleModuleDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return RoleModuleDO 实体对象
	 * @Throws
	 */
	RoleModuleDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<RoleModuleDO> 实体对象集合
	 * @Throws
	 */
	List<RoleModuleDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleModuleVO> VO对象集合
	 * @Throws
	 */
	List<RoleModuleVO> selectList(RoleModuleQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<RoleModuleBriefVO> 简要对象集合
	 * @Throws
	 */
	List<RoleModuleBriefVO> selectBriefList(RoleModuleBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<RoleModulePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<RoleModulePageVO> selectPage(RoleModulePageQuery query);

}
