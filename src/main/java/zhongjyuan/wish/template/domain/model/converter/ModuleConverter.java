package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.ModuleDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName ModuleConverter
 * @Description 模块转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class ModuleConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return ModuleDO 实体对象
	 * @Throws
	 */
	public static ModuleDO createVoToEntity(ModuleCreateVO createVO) {
		ModuleDO entity = new ModuleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return ModuleDO 实体对象
	 * @Throws
	 */
	public static ModuleDO updateVoToEntity(ModuleUpdateVO updateVO) {
		ModuleDO entity = new ModuleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModuleVO VO对象
	 * @Throws
	 */
	public static ModuleVO entityToVo(ModuleDO entity) {

		ModuleVO vo = new ModuleVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModuleBriefVO 简要对象
	 * @Throws
	 */
	public static ModuleBriefVO entityToBriefVo(ModuleDO entity) {

		ModuleBriefVO briefVO = new ModuleBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModuleVO> VO对象集合
	 * @Throws
	 */
	public static List<ModuleVO> entityToVos(List<ModuleDO> entities) {

		List<ModuleVO> vos = new ArrayList<ModuleVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, ModuleVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModuleBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<ModuleBriefVO> entityToBriefVos(List<ModuleDO> entities) {

		List<ModuleBriefVO> briefVOs = new ArrayList<ModuleBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, ModuleBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<ModulePageVO> entityToPageVos(List<ModuleDO> entities) {

		List<ModulePageVO> pageVOs = new ArrayList<ModulePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, ModulePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
	/**
	 * @Title entityToTreeVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转树形对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModuleTreeVO> 树形对象集合
	 * @Throws
	 */
	public static List<ModuleTreeVO> entityToTreeVos(List<ModuleDO> entities) {

		List<ModuleTreeVO> treeVOs = new ArrayList<ModuleTreeVO>();

		/*
		 * 对象转换:
		 */
		treeVOs = BeanUtil.copyListProperties(entities, ModuleTreeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return treeVOs;
	}
}