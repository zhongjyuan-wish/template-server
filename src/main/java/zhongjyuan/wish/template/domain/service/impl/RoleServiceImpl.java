package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.plugin.framework.TreeManage;
import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.mapper.RoleMapper;
import zhongjyuan.wish.template.domain.service.IRoleService;

import zhongjyuan.wish.template.domain.model.entity.RoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.RoleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName RoleServiceImpl
 * @Description 角色表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class RoleServiceImpl implements IRoleService {

	@Autowired
	RoleMapper roleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleDO create(RoleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = roleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		roleMapper.insert(entity);

		log.info("======RoleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(RoleDO entity) {

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleDO::getCode, entity.getCode());
		if (parentFlag) {
			queryWrapper.lambda().eq(RoleDO::getParentId, entity.getParentId());
		}

		if (roleMapper.exists(queryWrapper)) {
			if (parentFlag) {
				throw new BusinessException(RoleCode.ROLE_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(RoleCode.ROLE_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleDO::getName, entity.getName());

		if (roleMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(RoleCode.ROLE_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(RoleCode.ROLE_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleDO update(RoleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		roleMapper.updateById(entity);

		log.info("======RoleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleDO 实体对象
	 * @Throws
	 */
	private RoleDO updateEnabled(RoleDO entity) {

		// 获取原始数据
		RoleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(RoleCode.ROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(RoleCode.ROLE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(RoleCode.ROLE_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleDO::getCode, entity.getCode());
		if (parentFlag) {
			queryWrapper.lambda().eq(RoleDO::getParentId, entity.getParentId());
		}

		if (roleMapper.exists(queryWrapper)) {
			if (parentFlag) {
				throw new BusinessException(RoleCode.ROLE_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(RoleCode.ROLE_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleDO::getName, entity.getName());

		if (roleMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(RoleCode.ROLE_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(RoleCode.ROLE_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleDO delete(Long id) {

		// 获取原始数据
		RoleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleMapper.updateById(originEntity);

		log.info("======RoleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleDO deleteByCode(String code) {

		// 获取原始数据
		RoleDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleMapper.updateById(originEntity);

		log.info("======RoleServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<RoleDO> entities = new ArrayList<RoleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsSystem, false);
		queryWrapper.lambda().in(RoleDO::getId, ids);
		entities = roleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleDO> successEntities = new ArrayList<RoleDO>();
		
		// 根据主键更新
		for (RoleDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			roleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<RoleDO> entities = new ArrayList<RoleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsSystem, false);
		queryWrapper.lambda().in(RoleDO::getCode, codes);
		entities = roleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleDO> successEntities = new ArrayList<RoleDO>();
		
		// 根据主键更新
		for (RoleDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			roleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(RoleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(RoleCode.ROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(RoleCode.ROLE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(RoleCode.ROLE_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public RoleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return roleMapper.selectById(id);
	}
	
	@Override
	public RoleDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(RoleDO::getCode, code);
		
		return roleMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<RoleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return roleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<RoleDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);
		queryWrapper.lambda().in(RoleDO::getCode, codes);
		
		return roleMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<RoleVO> selectList(RoleQuery query) {
		List<RoleVO> vos = new ArrayList<RoleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(RoleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(RoleDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(RoleDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleDO> entities = roleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = RoleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<RoleBriefVO> selectBriefList(RoleBriefQuery query) {
		List<RoleBriefVO> briefVOs = new ArrayList<RoleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(RoleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(RoleDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(RoleDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleDO> entities = roleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = RoleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<RolePageVO> selectPage(RolePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(RoleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(RoleDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(RoleDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<RoleDO> page = roleMapper.selectPage(new CustomPage<RoleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> RoleConverter.entityToPageVos(entities));
	}

	@Override
	public List<RoleTreeVO> selectLazy(TreeLazyQuery<Long> query) {

		// 转成复杂查询条件对象
		TreeComplexQuery<Long> complexQuery = new TreeComplexQuery<Long>();
		BeanUtil.copyProperties(query, complexQuery);

		return selectChildrens(complexQuery);
	}

	@Override
	public List<RoleTreeVO> selectTree(TreeQuery<Long> query) {
		List<RoleTreeVO> treeVOs = new ArrayList<RoleTreeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleDO> queryWrapper = new QueryWrapper<RoleDO>();
		queryWrapper.lambda().eq(RoleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(RoleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(RoleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<RoleDO> entities = roleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			treeVOs = RoleConverter.entityToTreeVos(entities);
			treeVOs = (List<RoleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, RoleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<RoleTreeVO> selectParents(TreeComplexQuery<Long> query) {
		List<RoleTreeVO> treeVOs = new ArrayList<RoleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<RoleDO> entities = roleMapper.selectParents(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = RoleConverter.entityToTreeVos(entities);
			treeVOs = (List<RoleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, RoleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<RoleTreeVO> selectChildrens(TreeComplexQuery<Long> query) {
		List<RoleTreeVO> treeVOs = new ArrayList<RoleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<RoleDO> entities = roleMapper.selectChildrens(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = RoleConverter.entityToTreeVos(entities);
			treeVOs = (List<RoleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, RoleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<RoleTreeVO> selectLeafs(TreeComplexQuery<Long> query) {
		List<RoleTreeVO> treeVOs = new ArrayList<RoleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<RoleDO> entities = roleMapper.selectLeafs(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = RoleConverter.entityToTreeVos(entities);
			treeVOs = (List<RoleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, RoleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<RoleTreeVO> selectFulls(TreeComplexQuery<Long> query) {
		List<RoleTreeVO> treeVOs = new ArrayList<RoleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<RoleDO> entities = roleMapper.selectFulls(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = RoleConverter.entityToTreeVos(entities);
			treeVOs = (List<RoleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, RoleTreeVO.class);
		}

		return treeVOs;
	}
}
