package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName ModuleBriefQuery
 * @Description 模块简要查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ModuleBriefQuery", description = "模块简要查询条件对象")
public class ModuleBriefQuery extends SortQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields group: 分组
	 */
	@ApiModelProperty(value = "分组", name = "group", dataType = "String")
    private String group;

	/**
	 * @Fields systemId: 系统主键
	 */
	@ApiModelProperty(value = "系统主键", name = "systemId", dataType = "Long")
    private Long systemId;

	/**
	 * @Fields systemCode: 系统编码
	 */
	@ApiModelProperty(value = "系统编码", name = "systemCode", dataType = "String")
    private String systemCode;

	/**
	 * @Fields systemName: 系统名称
	 */
	@ApiModelProperty(value = "系统名称", name = "systemName", dataType = "String")
    private String systemName;

	/**
	 * @Fields parentId: 父级主键
	 */
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;





	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}