package zhongjyuan.wish.template.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zhongjyuan.wish.template.domain.model.entity.StationRoleDO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * @ClassName StationRoleMapper
 * @Description 岗位角色关联表Mapper接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Mapper
public interface StationRoleMapper extends BaseMapper<StationRoleDO> {

	/**
	 * @Title selectMaxRowIndex
	 * @Author zhongjyuan
	 * @Description 查询最大行号
	 * @Param @return
	 * @Return Integer 最大行号
	 * @Throws
	 */
	@Select("SELECT IFNULL(MAX(ROW_INDEX),0) FROM sys_station_role;")
	Integer selectMaxRowIndex();
	
}
