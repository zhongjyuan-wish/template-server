package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName OrganizeStationBriefQuery
 * @Description 组织岗位关联简要查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizeStationBriefQuery", description = "组织岗位关联简要查询条件对象")
public class OrganizeStationBriefQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields organizeId: 组织主键
	 */
	@ApiModelProperty(value = "组织主键", name = "organizeId", dataType = "Long")
    private Long organizeId;

	/**
	 * @Fields organizeNo: 组织编号
	 */
	@ApiModelProperty(value = "组织编号", name = "organizeNo", dataType = "String")
    private String organizeNo;

	/**
	 * @Fields organizeName: 组织名称
	 */
	@ApiModelProperty(value = "组织名称", name = "organizeName", dataType = "String")
    private String organizeName;

	/**
	 * @Fields stationId: 岗位主键
	 */
	@ApiModelProperty(value = "岗位主键", name = "stationId", dataType = "Long")
    private Long stationId;

	/**
	 * @Fields stationCode: 岗位编码
	 */
	@ApiModelProperty(value = "岗位编码", name = "stationCode", dataType = "String")
    private String stationCode;

	/**
	 * @Fields stationName: 岗位名称
	 */
	@ApiModelProperty(value = "岗位名称", name = "stationName", dataType = "String")
    private String stationName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}