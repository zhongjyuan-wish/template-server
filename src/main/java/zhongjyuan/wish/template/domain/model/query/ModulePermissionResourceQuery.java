package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName ModulePermissionResourceQuery
 * @Description 模块权限资源关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ModulePermissionResourceQuery", description = "模块权限资源关联查询条件对象")
public class ModulePermissionResourceQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields modulePermissionId: 模块权限主键
	 */
	@ApiModelProperty(value = "模块权限主键", name = "modulePermissionId", dataType = "Long")
    private Long modulePermissionId;

	/**
	 * @Fields modulePermissionCode: 模块权限编码
	 */
	@ApiModelProperty(value = "模块权限编码", name = "modulePermissionCode", dataType = "String")
    private String modulePermissionCode;

	/**
	 * @Fields modulePermissionName: 模块权限名称
	 */
	@ApiModelProperty(value = "模块权限名称", name = "modulePermissionName", dataType = "String")
    private String modulePermissionName;


	/**
	 * @Fields pattern: 资源正则
	 */
	@ApiModelProperty(value = "资源正则", name = "pattern", dataType = "String")
    private String pattern;

	/**
	 * @Fields method: 资源方式
	 */
	@ApiModelProperty(value = "资源方式", name = "method", dataType = "String")
    private String method;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}