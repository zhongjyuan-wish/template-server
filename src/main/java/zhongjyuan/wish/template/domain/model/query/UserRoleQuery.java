package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName UserRoleQuery
 * @Description 用户角色关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserRoleQuery", description = "用户角色关联查询条件对象")
public class UserRoleQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields userId: 用户主键
	 */
	@ApiModelProperty(value = "用户主键", name = "userId", dataType = "Long")
    private Long userId;

	/**
	 * @Fields userName: 用户名称
	 */
	@ApiModelProperty(value = "用户名称", name = "userName", dataType = "String")
    private String userName;

	/**
	 * @Fields userAccount: 用户账号
	 */
	@ApiModelProperty(value = "用户账号", name = "userAccount", dataType = "String")
    private String userAccount;

	/**
	 * @Fields roleId: 角色主键
	 */
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}