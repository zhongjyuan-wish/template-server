package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IOrganizeLeaderService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeLeaderDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName OrganizeLeaderController
 * @Description 组织责任人关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "组织责任人关联管理", tags = "组织责任人关联管理")
@RequestMapping("{version}/organizeLeaders")
public class OrganizeLeaderController {

	@Autowired
	IOrganizeLeaderService organizeLeaderService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "组织责任人关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeLeaderVO> create(@RequestBody @Valid OrganizeLeaderCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		OrganizeLeaderDO entity = OrganizeLeaderConverter.createVoToEntity(createVO);
		entity = organizeLeaderService.create(entity);
		return ResponseResult.success(OrganizeLeaderConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "组织责任人关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeLeaderVO> update(@RequestBody @Valid OrganizeLeaderUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		OrganizeLeaderDO entity = OrganizeLeaderConverter.updateVoToEntity(updateVO);
		entity = organizeLeaderService.update(entity);
		return ResponseResult.success(OrganizeLeaderConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "组织责任人关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeLeaderVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeLeaderDO> entities = organizeLeaderService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeLeaderConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "组织责任人关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeLeaderVO> delete(@PathVariable("id") Long id) {
		OrganizeLeaderDO entity = organizeLeaderService.delete(id);
		return ResponseResult.success(OrganizeLeaderConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeLeaderVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeLeaderDO> entities = organizeLeaderService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeLeaderConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeLeaderVO> select(@PathVariable("id") Long id) {
		OrganizeLeaderDO entity = organizeLeaderService.select(id);
		return ResponseResult.success(OrganizeLeaderConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeLeaderVO>> selectList(@Valid OrganizeLeaderQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeLeaderService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeLeaderBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeLeaderDO> entities = organizeLeaderService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeLeaderConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeLeaderBriefVO> selectBrief(@PathVariable("id") Long id) {
		OrganizeLeaderDO entity = organizeLeaderService.select(id);
		return ResponseResult.success(OrganizeLeaderConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeLeaderBriefVO>> selectBriefList(@Valid OrganizeLeaderBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeLeaderService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织责任人关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页组织责任人关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<OrganizeLeaderPageVO>> selectPage(@Valid OrganizeLeaderPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeLeaderService.selectPage(query));
	}
	
}
