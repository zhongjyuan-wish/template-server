package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.AppModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IAppModulePermissionService
 * @Description 应用模块权限关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IAppModulePermissionService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModulePermissionDO 实体对象
	 * @Throws
	 */
	AppModulePermissionDO create(AppModulePermissionDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModulePermissionDO 实体对象
	 * @Throws
	 */
	AppModulePermissionDO update(AppModulePermissionDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return AppModulePermissionDO 实体对象
	 * @Throws
	 */
	AppModulePermissionDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<AppModulePermissionDO> 实体对象集合
	 * @Throws
	 */
	List<AppModulePermissionDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return AppModulePermissionDO 实体对象
	 * @Throws
	 */
	AppModulePermissionDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<AppModulePermissionDO> 实体对象集合
	 * @Throws
	 */
	List<AppModulePermissionDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<AppModulePermissionVO> VO对象集合
	 * @Throws
	 */
	List<AppModulePermissionVO> selectList(AppModulePermissionQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<AppModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	List<AppModulePermissionBriefVO> selectBriefList(AppModulePermissionBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<AppModulePermissionPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<AppModulePermissionPageVO> selectPage(AppModulePermissionPageQuery query);

}
