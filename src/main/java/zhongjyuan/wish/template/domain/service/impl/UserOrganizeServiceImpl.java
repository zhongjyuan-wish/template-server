package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserOrganizeMapper;
import zhongjyuan.wish.template.domain.service.IUserOrganizeService;

import zhongjyuan.wish.template.domain.model.entity.UserOrganizeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserOrganizeCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserOrganizeServiceImpl
 * @Description 用户组织关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserOrganizeServiceImpl implements IUserOrganizeService {

	@Autowired
	UserOrganizeMapper userOrganizeMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserOrganizeDO create(UserOrganizeDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userOrganizeMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userOrganizeMapper.insert(entity);

		log.info("======UserOrganizeServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserOrganizeDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserOrganizeDO update(UserOrganizeDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userOrganizeMapper.updateById(entity);

		log.info("======UserOrganizeServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserOrganizeDO 实体对象
	 * @Throws
	 */
	private UserOrganizeDO updateEnabled(UserOrganizeDO entity) {

		// 获取原始数据
		UserOrganizeDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserOrganizeCode.USERORGANIZE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserOrganizeDO delete(Long id) {

		// 获取原始数据
		UserOrganizeDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userOrganizeMapper.updateById(originEntity);

		log.info("======UserOrganizeServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserOrganizeDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserOrganizeDO> entities = new ArrayList<UserOrganizeDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserOrganizeDO> queryWrapper = new QueryWrapper<UserOrganizeDO>();
		queryWrapper.lambda().in(UserOrganizeDO::getId, ids);
		entities = userOrganizeMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserOrganizeDO> successEntities = new ArrayList<UserOrganizeDO>();
		
		// 根据主键更新
		for (UserOrganizeDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userOrganizeMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserOrganizeServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserOrganizeDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserOrganizeCode.USERORGANIZE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserOrganizeDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userOrganizeMapper.selectById(id);
	}
	
	@Override
	public List<UserOrganizeDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userOrganizeMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserOrganizeVO> selectList(UserOrganizeQuery query) {
		List<UserOrganizeVO> vos = new ArrayList<UserOrganizeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserOrganizeDO> queryWrapper = new QueryWrapper<UserOrganizeDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserOrganizeDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserOrganizeDO> entities = userOrganizeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserOrganizeConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserOrganizeBriefVO> selectBriefList(UserOrganizeBriefQuery query) {
		List<UserOrganizeBriefVO> briefVOs = new ArrayList<UserOrganizeBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserOrganizeDO> queryWrapper = new QueryWrapper<UserOrganizeDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserOrganizeDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserOrganizeDO> entities = userOrganizeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserOrganizeConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserOrganizePageVO> selectPage(UserOrganizePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserOrganizeDO> queryWrapper = new QueryWrapper<UserOrganizeDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserOrganizeDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserOrganizeDO> page = userOrganizeMapper.selectPage(new CustomPage<UserOrganizeDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserOrganizeConverter.entityToPageVos(entities));
	}

}
