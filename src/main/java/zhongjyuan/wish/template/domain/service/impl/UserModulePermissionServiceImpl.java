package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserModulePermissionMapper;
import zhongjyuan.wish.template.domain.service.IUserModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.UserModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserModulePermissionCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserModulePermissionServiceImpl
 * @Description 用户模块权限关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserModulePermissionServiceImpl implements IUserModulePermissionService {

	@Autowired
	UserModulePermissionMapper userModulePermissionMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserModulePermissionDO create(UserModulePermissionDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userModulePermissionMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userModulePermissionMapper.insert(entity);

		log.info("======UserModulePermissionServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserModulePermissionDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserModulePermissionDO update(UserModulePermissionDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userModulePermissionMapper.updateById(entity);

		log.info("======UserModulePermissionServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserModulePermissionDO 实体对象
	 * @Throws
	 */
	private UserModulePermissionDO updateEnabled(UserModulePermissionDO entity) {

		// 获取原始数据
		UserModulePermissionDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserModulePermissionCode.USERMODULEPERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserModulePermissionDO delete(Long id) {

		// 获取原始数据
		UserModulePermissionDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userModulePermissionMapper.updateById(originEntity);

		log.info("======UserModulePermissionServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserModulePermissionDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserModulePermissionDO> entities = new ArrayList<UserModulePermissionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserModulePermissionDO> queryWrapper = new QueryWrapper<UserModulePermissionDO>();
		queryWrapper.lambda().in(UserModulePermissionDO::getId, ids);
		entities = userModulePermissionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserModulePermissionDO> successEntities = new ArrayList<UserModulePermissionDO>();
		
		// 根据主键更新
		for (UserModulePermissionDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userModulePermissionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserModulePermissionServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserModulePermissionDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserModulePermissionCode.USERMODULEPERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserModulePermissionDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userModulePermissionMapper.selectById(id);
	}
	
	@Override
	public List<UserModulePermissionDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userModulePermissionMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserModulePermissionVO> selectList(UserModulePermissionQuery query) {
		List<UserModulePermissionVO> vos = new ArrayList<UserModulePermissionVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserModulePermissionDO> queryWrapper = new QueryWrapper<UserModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserModulePermissionDO> entities = userModulePermissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserModulePermissionConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserModulePermissionBriefVO> selectBriefList(UserModulePermissionBriefQuery query) {
		List<UserModulePermissionBriefVO> briefVOs = new ArrayList<UserModulePermissionBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserModulePermissionDO> queryWrapper = new QueryWrapper<UserModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserModulePermissionDO> entities = userModulePermissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserModulePermissionConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserModulePermissionPageVO> selectPage(UserModulePermissionPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserModulePermissionDO> queryWrapper = new QueryWrapper<UserModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserModulePermissionDO> page = userModulePermissionMapper.selectPage(new CustomPage<UserModulePermissionDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserModulePermissionConverter.entityToPageVos(entities));
	}

}
