package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.AppModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IAppModuleService
 * @Description 应用模块关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IAppModuleService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	AppModuleDO create(AppModuleDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	AppModuleDO update(AppModuleDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	AppModuleDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<AppModuleDO> 实体对象集合
	 * @Throws
	 */
	List<AppModuleDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	AppModuleDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<AppModuleDO> 实体对象集合
	 * @Throws
	 */
	List<AppModuleDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<AppModuleVO> VO对象集合
	 * @Throws
	 */
	List<AppModuleVO> selectList(AppModuleQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<AppModuleBriefVO> 简要对象集合
	 * @Throws
	 */
	List<AppModuleBriefVO> selectBriefList(AppModuleBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<AppModulePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<AppModulePageVO> selectPage(AppModulePageQuery query);

}
