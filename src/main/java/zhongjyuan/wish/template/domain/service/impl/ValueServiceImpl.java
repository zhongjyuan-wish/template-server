package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.plugin.framework.TreeManage;
import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.mapper.ValueMapper;
import zhongjyuan.wish.template.domain.service.IValueService;

import zhongjyuan.wish.template.domain.model.entity.ValueDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.ValueCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName ValueServiceImpl
 * @Description 码值表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class ValueServiceImpl implements IValueService {

	@Autowired
	ValueMapper valueMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ValueDO create(ValueDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = valueMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		valueMapper.insert(entity);

		log.info("======ValueServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(ValueDO entity) {

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ValueDO::getCode, entity.getCode());
		if (parentFlag) {
			queryWrapper.lambda().eq(ValueDO::getParentId, entity.getParentId());
		}

		if (valueMapper.exists(queryWrapper)) {
			if (parentFlag) {
				throw new BusinessException(ValueCode.VALUE_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ValueCode.VALUE_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ValueDO::getName, entity.getName());

		if (valueMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(ValueCode.VALUE_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ValueCode.VALUE_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ValueDO update(ValueDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		valueMapper.updateById(entity);

		log.info("======ValueServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	private ValueDO updateEnabled(ValueDO entity) {

		// 获取原始数据
		ValueDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(ValueCode.VALUE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(ValueCode.VALUE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(ValueCode.VALUE_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ValueDO::getCode, entity.getCode());
		if (parentFlag) {
			queryWrapper.lambda().eq(ValueDO::getParentId, entity.getParentId());
		}

		if (valueMapper.exists(queryWrapper)) {
			if (parentFlag) {
				throw new BusinessException(ValueCode.VALUE_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ValueCode.VALUE_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ValueDO::getName, entity.getName());

		if (valueMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(ValueCode.VALUE_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ValueCode.VALUE_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ValueDO delete(Long id) {

		// 获取原始数据
		ValueDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		valueMapper.updateById(originEntity);

		log.info("======ValueServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ValueDO deleteByCode(String code) {

		// 获取原始数据
		ValueDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		valueMapper.updateById(originEntity);

		log.info("======ValueServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<ValueDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<ValueDO> entities = new ArrayList<ValueDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsSystem, false);
		queryWrapper.lambda().in(ValueDO::getId, ids);
		entities = valueMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<ValueDO> successEntities = new ArrayList<ValueDO>();
		
		// 根据主键更新
		for (ValueDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			valueMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======ValueServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<ValueDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<ValueDO> entities = new ArrayList<ValueDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsSystem, false);
		queryWrapper.lambda().in(ValueDO::getCode, codes);
		entities = valueMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<ValueDO> successEntities = new ArrayList<ValueDO>();
		
		// 根据主键更新
		for (ValueDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			valueMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======ValueServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(ValueDO entity) {
	
		if (entity == null) {
			throw new BusinessException(ValueCode.VALUE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(ValueCode.VALUE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(ValueCode.VALUE_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public ValueDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return valueMapper.selectById(id);
	}
	
	@Override
	public ValueDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ValueDO::getCode, code);
		
		return valueMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<ValueDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return valueMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<ValueDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);
		queryWrapper.lambda().in(ValueDO::getCode, codes);
		
		return valueMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<ValueVO> selectList(ValueQuery query) {
		List<ValueVO> vos = new ArrayList<ValueVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ValueDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ValueDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ValueDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(ValueDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(ValueDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ValueDO> entities = valueMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = ValueConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<ValueBriefVO> selectBriefList(ValueBriefQuery query) {
		List<ValueBriefVO> briefVOs = new ArrayList<ValueBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ValueDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ValueDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ValueDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(ValueDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(ValueDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ValueDO> entities = valueMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = ValueConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<ValuePageVO> selectPage(ValuePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ValueDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ValueDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ValueDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(ValueDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(ValueDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<ValueDO> page = valueMapper.selectPage(new CustomPage<ValueDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> ValueConverter.entityToPageVos(entities));
	}

	@Override
	public List<ValueTreeVO> selectLazy(TreeLazyQuery<Long> query) {

		// 转成复杂查询条件对象
		TreeComplexQuery<Long> complexQuery = new TreeComplexQuery<Long>();
		BeanUtil.copyProperties(query, complexQuery);

		return selectChildrens(complexQuery);
	}

	@Override
	public List<ValueTreeVO> selectTree(TreeQuery<Long> query) {
		List<ValueTreeVO> treeVOs = new ArrayList<ValueTreeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ValueDO> queryWrapper = new QueryWrapper<ValueDO>();
		queryWrapper.lambda().eq(ValueDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ValueDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ValueDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ValueDO::getParentName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ValueDO> entities = valueMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			treeVOs = ValueConverter.entityToTreeVos(entities);
			treeVOs = (List<ValueTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ValueTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ValueTreeVO> selectParents(TreeComplexQuery<Long> query) {
		List<ValueTreeVO> treeVOs = new ArrayList<ValueTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ValueDO> entities = valueMapper.selectParents(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ValueConverter.entityToTreeVos(entities);
			treeVOs = (List<ValueTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ValueTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ValueTreeVO> selectChildrens(TreeComplexQuery<Long> query) {
		List<ValueTreeVO> treeVOs = new ArrayList<ValueTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ValueDO> entities = valueMapper.selectChildrens(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ValueConverter.entityToTreeVos(entities);
			treeVOs = (List<ValueTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ValueTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ValueTreeVO> selectLeafs(TreeComplexQuery<Long> query) {
		List<ValueTreeVO> treeVOs = new ArrayList<ValueTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ValueDO> entities = valueMapper.selectLeafs(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ValueConverter.entityToTreeVos(entities);
			treeVOs = (List<ValueTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ValueTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ValueTreeVO> selectFulls(TreeComplexQuery<Long> query) {
		List<ValueTreeVO> treeVOs = new ArrayList<ValueTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ValueDO> entities = valueMapper.selectFulls(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ValueConverter.entityToTreeVos(entities);
			treeVOs = (List<ValueTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ValueTreeVO.class);
		}

		return treeVOs;
	}
}
