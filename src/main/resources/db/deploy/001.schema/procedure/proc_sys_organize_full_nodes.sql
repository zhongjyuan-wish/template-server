CREATE DEFINER=`idea_dev`@`172.56.8.26` PROCEDURE `proc_sys_organize_full_nodes`(IN type INT,
	IN showSiblings INT,
	IN showHitNodes INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点所在全路径所有节点'
BEGIN

	################## 临时存放节点 ###################
	DECLARE tempIdList LONGTEXT;

	################## 声明父序列变量 ###################
	# 返回父节点结果集
	DECLARE parentIdList LONGTEXT;
	# 临时存放父节点
	DECLARE tempParentIdList LONGTEXT;

	################## 声明子序列变量 ###################
	# 返回叶子节点结果集
	DECLARE childIdList LONGTEXT;
	# 临时存放子节点
	DECLARE tempChildIdList LONGTEXT;


	################## 查询父节点序列 ###################
	SET parentIdList = '';
	SET tempParentIdList = hitNodeIds;

	# 循环，用于查询节点上所有的父节点
	WHILE tempParentIdList IS NOT NULL DO

		# 存入到返回结果中
		SET parentIdList = CONCAT_WS(',', parentIdList, tempParentIdList);

		# 查询节点上所有父节点
		SELECT GROUP_CONCAT(parent_id) INTO tempParentIdList FROM sys_organize WHERE is_deleted = 0 AND FIND_IN_SET(id, tempParentIdList);-- id = tempParentIdList;
		
	END WHILE;

	# 是否显示兄弟节点
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM sys_organize WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ','));

		SET parentIdList = CONCAT_WS(',', parentIdList, tempIdList);

	END IF;

	SET parentIdList = SUBSTRING(parentIdList, LENGTH(SUBSTRING_INDEX(parentIdList, ',', 2)) + 2);

	# 是否包含命中节点
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND parentIdList != CONCAT(',', hitNodeIds) THEN

		SET parentIdList = REPLACE(REPLACE(parentIdList, hitNodeIds, ''), ',,', ',');

	END IF;    

	################## 查询子节点序列 ###################
	SET childIdList = '';
	SET tempChildIdList = hitNodeIds;

	# 循环，用于查询节点下所有的子节点
	WHILE tempChildIdList IS NOT NULL DO

		# 存入到返回结果中
		SET childIdList = CONCAT_WS(',', childIdList, tempChildIdList);

		# 查询节点下所有子节点
		SELECT GROUP_CONCAT(id) INTO tempChildIdList FROM sys_organize WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, tempChildIdList) > 0;
		
	END WHILE;

	# 是否显示兄弟节点
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND childIdList != hitNodeIds THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM sys_organize WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, childIdList);

		SET childIdList = CONCAT_WS(',', childIdList, tempIdList);

	END IF;

	SET childIdList = SUBSTRING(childIdList, 2);

	# 是否包含命中节点
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND childIdList != CONCAT(',', hitNodeIds) THEN

		SET childIdList = REPLACE(REPLACE(childIdList, hitNodeIds, ''), ',,', ',');

	END IF;


	# 返回结果[主键字符串]
	IF type IS NULL OR type = 0 THEN
		SELECT CONCAT_WS(',' , parentIdList, childIdList) AS fullPathIds;
	END IF;

	# 返回结果[数据集]
	IF type IS NOT NULL AND type = 1 THEN
		SELECT * FROM sys_organize A
		WHERE A.is_deleted = 0
		AND FIND_IN_SET(A.id, CONCAT_WS(',' , parentIdList, childIdList))
		ORDER BY A.outline;
	END IF;
	# create by zhongjyuan
END