package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName RoleDO
 * @Description 角色表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_role")
@ApiModel(value = "RoleDO", description = "角色表实体对象")
public class RoleDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields code: 编码
	 */
    @TableField("code")
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
    @TableField("name")
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields rankId: 分级主键
	 */
    @TableField("rank_id")
	@ApiModelProperty(value = "分级主键", name = "rankId", dataType = "Long")
    private Long rankId;

	/**
	 * @Fields rankCode: 分级编码
	 */
    @TableField("rank_code")
	@ApiModelProperty(value = "分级编码", name = "rankCode", dataType = "String")
    private String rankCode;

	/**
	 * @Fields rankName: 分级名称
	 */
    @TableField("rank_name")
	@ApiModelProperty(value = "分级名称", name = "rankName", dataType = "String")
    private String rankName;

	/**
	 * @Fields type: 类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]
	 */
    @TableField("type")
	@ApiModelProperty(value = "类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields parentId: 父级主键
	 */
    @TableField("parent_id")
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
    @TableField("parent_code")
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
    @TableField("parent_name")
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;

	/**
	 * @Fields path: 全路径
	 */
    @TableField("path")
	@ApiModelProperty(value = "全路径", name = "path", dataType = "String")
    private String path;

	/**
	 * @Fields level: 层级
	 */
    @TableField("level")
	@ApiModelProperty(value = "层级", name = "level", dataType = "Integer")
    private Integer level;

	/**
	 * @Fields outline: 层级码
	 */
    @TableField("outline")
	@ApiModelProperty(value = "层级码", name = "outline", dataType = "String")
    private String outline;

	/**
	 * @Fields isLeaf: 是否叶子
	 */
    @TableField("is_leaf")
	@ApiModelProperty(value = "是否叶子", name = "isLeaf", dataType = "Boolean")
    private Boolean isLeaf;

	/**
	 * @Fields isSystem: 是否系统
	 */
    @TableField("is_system")
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
    @TableField("is_enabled")
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;

	/**
	 * @Fields isDeleted: 是否删除
	 */
    @TableField("is_deleted")
    @TableLogic
	@ApiModelProperty(value = "是否删除", name = "isDeleted", dataType = "Boolean")
    private Boolean isDeleted;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
    @TableField("creator_id")
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
    @TableField("creator_name")
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
    @TableField("updator_id")
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
    @TableField("updator_name")
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
    @TableField("update_time")
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;


    public static final String ID = "id";

    public static final String CODE = "code";

    public static final String NAME = "name";

    public static final String RANK_ID = "rank_id";

    public static final String RANK_CODE = "rank_code";

    public static final String RANK_NAME = "rank_name";

    public static final String TYPE = "type";

    public static final String PARENT_ID = "parent_id";

    public static final String PARENT_CODE = "parent_code";

    public static final String PARENT_NAME = "parent_name";

    public static final String PATH = "path";

    public static final String LEVEL = "level";

    public static final String OUTLINE = "outline";

    public static final String IS_LEAF = "is_leaf";

    public static final String IS_SYSTEM = "is_system";

    public static final String IS_ENABLED = "is_enabled";

    public static final String IS_DELETED = "is_deleted";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATOR_ID = "creator_id";

    public static final String CREATOR_NAME = "creator_name";

    public static final String CREATE_TIME = "create_time";

    public static final String UPDATOR_ID = "updator_id";

    public static final String UPDATOR_NAME = "updator_name";

    public static final String UPDATE_TIME = "update_time";

}
