package ${package.Controller};

import java.util.ArrayList;
import java.util.List;	
<#if treeFlag>
import java.util.stream.Collectors;
</#if>
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

import ${import_Commons_ResponseResult};
import ${import_Commons_ApiVersion};
import ${import_Commons_Log};
import ${import_Model_PageResult};
import ${import_Model_LogType};
import ${import_Model_Query};

import ${package.Service}.${table.serviceName};

import ${package.Entity}.${entity};
import ${package.Parent}.${package_Query}.*;
import ${package.Parent}.${package_Converter}.*;
import ${package.Parent}.${package_RequestVO}.*;
import ${package.Parent}.${package_ResponseVO}.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName ${table.controllerName}
 * @Description ${table.comment?replace("表","")}管理
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
@ApiVersion(1)
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@Api(value = "${table.comment?replace("表","")}管理", tags = "${table.comment?replace("表","")}管理")
@RequestMapping("{version}/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${entityNaming?uncap_first}s</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>
<#assign serviceName="${table.serviceName?substring(1)?uncap_first}"/>
<#assign businessName="${table.comment?replace('表','')}"/>

	@Autowired
	${table.serviceName} ${serviceName};

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> create(@RequestBody @Valid ${entity?replace("DO","")}CreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		${entity} entity = ${entity?replace("DO","")}Converter.createVoToEntity(createVO);
		entity = ${serviceName}.create(entity);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> update(@RequestBody @Valid ${entity?replace("DO","")}UpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		${entity} entity = ${entity?replace("DO","")}Converter.updateVoToEntity(updateVO);
		entity = ${serviceName}.update(entity);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}VO>> delete(@RequestBody @Valid BatchQuery<${keyPropertyType}> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entity}> entities = ${serviceName}.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(${entityNaming}Converter.entityToVos(entities));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}VO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entity}> entities = ${serviceName}.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(${entityNaming}Converter.entityToVos(entities));
	}
	
</#if>
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> delete(@PathVariable("id") ${keyPropertyType} id) {
		${entity} entity = ${serviceName}.delete(id);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> deleteByCode(@PathVariable("code") String code) {
		${entity} entity = ${serviceName}.deleteByCode(code);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
</#if>
<#if enableFlag>
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> enable(@PathVariable("id") Long id) {
		${entity} entity = ${serviceName}.select(id);
		entity.setIsEnabled(true);
		entity = ${serviceName}.update(entity);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> enableByCode(@PathVariable("code") String code) {
		${entity} entity = ${serviceName}.selectByCode(code);
		entity.setIsEnabled(true);
		entity = ${serviceName}.update(entity);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
</#if>
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> disable(@PathVariable("id") Long id) {
		${entity} entity = ${serviceName}.select(id);
		entity.setIsEnabled(false);
		entity = ${serviceName}.update(entity);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "${businessName}", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> disableByCode(@PathVariable("code") String code) {
		${entity} entity = ${serviceName}.selectByCode(code);
		entity.setIsEnabled(false);
		entity = ${serviceName}.update(entity);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
</#if>
</#if>
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}VO>> select(@Valid BatchQuery<${keyPropertyType}> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entity}> entities = ${serviceName}.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(${entityNaming}Converter.entityToVos(entities));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}VO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entity}> entities = ${serviceName}.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(${entityNaming}Converter.entityToVos(entities));
	}
	
</#if>
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> select(@PathVariable("id") ${keyPropertyType} id) {
		${entity} entity = ${serviceName}.select(id);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}VO> selectByCode(@PathVariable("code") String code) {
		${entity} entity = ${serviceName}.selectByCode(code);
		return ResponseResult.success(${entityNaming}Converter.entityToVo(entity));
	}
	
</#if>
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}VO>> selectList(@Valid ${entityNaming}Query query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}BriefVO>> selectBrief(@Valid BatchQuery<${keyPropertyType}> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entity}> entities = ${serviceName}.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(${entityNaming}Converter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}BriefVO> selectBrief(@PathVariable("id") ${keyPropertyType} id) {
		${entity} entity = ${serviceName}.select(id);
		return ResponseResult.success(${entityNaming}Converter.entityToBriefVo(entity));
	}
	
<#if codeFlag>
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<${entityNaming}BriefVO> selectBriefByCode(@PathVariable("code") String code) {
		${entity} entity = ${serviceName}.selectByCode(code);
		return ResponseResult.success(${entityNaming}Converter.entityToBriefVo(entity));
	}
	
</#if>
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}BriefVO>> selectBriefList(@Valid ${entityNaming}BriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<PageResult<${entityNaming}PageVO>> selectPage(@Valid ${entityNaming}PageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectPage(query));
	}
	
<#if treeFlag>
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName} 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}TreeVO>> selectTree(@Valid TreeQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectTree(query));
	}
	
	@RequestMapping(value = "/tree/lazy", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(懒加载) 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}TreeVO>> selectLazy(@Valid TreeLazyQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectLazy(query));
	}

	@RequestMapping(value = "/tree/parents", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(所有父级) 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}TreeVO>> selectParents(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectParents(query));
	}

	@RequestMapping(value = "/tree/parentIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(所有父级) 作者:${author} 時間:${date}")
	public ResponseResult<List<${keyPropertyType}>> selectParentIds(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entityNaming}TreeVO> treeVOs = ${serviceName}.selectParents(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/childrens", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(所有子级) 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}TreeVO>> selectChildrens(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectChildrens(query));
	}

	@RequestMapping(value = "/tree/childrenIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(所有父级) 作者:${author} 時間:${date}")
	public ResponseResult<List<${keyPropertyType}>> selectChildrenIds(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entityNaming}TreeVO> treeVOs = ${serviceName}.selectChildrens(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/leafs", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(所有叶子) 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}TreeVO>> selectLeafs(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectLeafs(query));
	}

	@RequestMapping(value = "/tree/leafIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(所有叶子) 作者:${author} 時間:${date}")
	public ResponseResult<List<${keyPropertyType}>> selectLeafIds(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entityNaming}TreeVO> treeVOs = ${serviceName}.selectLeafs(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/fulls", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(全路径) 作者:${author} 時間:${date}")
	public ResponseResult<List<${entityNaming}TreeVO>> selectFulls(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(${serviceName}.selectFulls(query));
	}

	@RequestMapping(value = "/tree/fullIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "${businessName}", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形${businessName}(全路径) 作者:${author} 時間:${date}")
	public ResponseResult<List<${keyPropertyType}>> selectFullIds(@Valid TreeComplexQuery<${keyPropertyType}> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<${entityNaming}TreeVO> treeVOs = ${serviceName}.selectFulls(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}
</#if>
}
</#if>
