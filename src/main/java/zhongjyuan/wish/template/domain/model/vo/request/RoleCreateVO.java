package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName RoleCreateVO
 * @Description 角色创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleCreateVO", description = "角色创建对象")
public class RoleCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields code: 编码
	 */
	// @NotNull(message = "编码不能为空.")
	// @NotBlank(message = "编码不能为空.")
	// @Size(message = "编码最大长度为50.", max = 50)
	// @Min(message = "编码最小为600.", value = 600)
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	// @NotNull(message = "名称不能为空.")
	// @NotBlank(message = "名称不能为空.")
	// @Size(message = "名称最大长度为50.", max = 50)
	// @Min(message = "名称最小为600.", value = 600)
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields rankId: 分级主键
	 */
	// @NotNull(message = "分级主键不能为空.")
	// @NotBlank(message = "分级主键不能为空.")
	// @Size(message = "分级主键最大长度为50.", max = 50)
	// @Min(message = "分级主键最小为600.", value = 600)
	@ApiModelProperty(value = "分级主键", name = "rankId", dataType = "Long")
    private Long rankId;

	/**
	 * @Fields rankCode: 分级编码
	 */
	// @NotNull(message = "分级编码不能为空.")
	// @NotBlank(message = "分级编码不能为空.")
	// @Size(message = "分级编码最大长度为50.", max = 50)
	// @Min(message = "分级编码最小为600.", value = 600)
	@ApiModelProperty(value = "分级编码", name = "rankCode", dataType = "String")
    private String rankCode;

	/**
	 * @Fields rankName: 分级名称
	 */
	// @NotNull(message = "分级名称不能为空.")
	// @NotBlank(message = "分级名称不能为空.")
	// @Size(message = "分级名称最大长度为50.", max = 50)
	// @Min(message = "分级名称最小为600.", value = 600)
	@ApiModelProperty(value = "分级名称", name = "rankName", dataType = "String")
    private String rankName;

	/**
	 * @Fields type: 类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]
	 */
	// @NotNull(message = "类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]不能为空.")
	// @NotBlank(message = "类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]不能为空.")
	// @Size(message = "类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]最大长度为50.", max = 50)
	// @Min(message = "类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]最小为600.", value = 600)
	@ApiModelProperty(value = "类型[Standard:标准角色;Merge:融合角色;Custom:自定义角色]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields parentId: 父级主键
	 */
	// @NotNull(message = "父级主键不能为空.")
	// @NotBlank(message = "父级主键不能为空.")
	// @Size(message = "父级主键最大长度为50.", max = 50)
	// @Min(message = "父级主键最小为600.", value = 600)
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
	// @NotNull(message = "父级编码不能为空.")
	// @NotBlank(message = "父级编码不能为空.")
	// @Size(message = "父级编码最大长度为50.", max = 50)
	// @Min(message = "父级编码最小为600.", value = 600)
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
	// @NotNull(message = "父级名称不能为空.")
	// @NotBlank(message = "父级名称不能为空.")
	// @Size(message = "父级名称最大长度为50.", max = 50)
	// @Min(message = "父级名称最小为600.", value = 600)
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;








	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}