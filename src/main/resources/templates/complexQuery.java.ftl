package ${package.Parent}.${package_Query};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import ${import_Model_TreeComplexQuery};

/**
 * @ClassName ${entityNaming}TreeComplexQuery
 * @Description ${table.comment?replace("表","")}复杂查询条件对象
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${entityNaming}TreeComplexQuery", description = "${table.comment?replace("表","")}复杂查询条件对象")
public class ${entityNaming}TreeComplexQuery extends TreeComplexQuery<${keyPropertyType}> {

	private static final long serialVersionUID = 1L;
}