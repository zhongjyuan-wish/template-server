package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.RoleRankDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName RoleRankConverter
 * @Description 角色分级转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class RoleRankConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	public static RoleRankDO createVoToEntity(RoleRankCreateVO createVO) {
		RoleRankDO entity = new RoleRankDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return RoleRankDO 实体对象
	 * @Throws
	 */
	public static RoleRankDO updateVoToEntity(RoleRankUpdateVO updateVO) {
		RoleRankDO entity = new RoleRankDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleRankVO VO对象
	 * @Throws
	 */
	public static RoleRankVO entityToVo(RoleRankDO entity) {

		RoleRankVO vo = new RoleRankVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleRankBriefVO 简要对象
	 * @Throws
	 */
	public static RoleRankBriefVO entityToBriefVo(RoleRankDO entity) {

		RoleRankBriefVO briefVO = new RoleRankBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleRankVO> VO对象集合
	 * @Throws
	 */
	public static List<RoleRankVO> entityToVos(List<RoleRankDO> entities) {

		List<RoleRankVO> vos = new ArrayList<RoleRankVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, RoleRankVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleRankBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<RoleRankBriefVO> entityToBriefVos(List<RoleRankDO> entities) {

		List<RoleRankBriefVO> briefVOs = new ArrayList<RoleRankBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, RoleRankBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleRankPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<RoleRankPageVO> entityToPageVos(List<RoleRankDO> entities) {

		List<RoleRankPageVO> pageVOs = new ArrayList<RoleRankPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, RoleRankPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}