package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IAppRoleService;

import zhongjyuan.wish.template.domain.model.entity.AppRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName AppRoleController
 * @Description 应用角色关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "应用角色关联管理", tags = "应用角色关联管理")
@RequestMapping("{version}/appRoles")
public class AppRoleController {

	@Autowired
	IAppRoleService appRoleService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "应用角色关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppRoleVO> create(@RequestBody @Valid AppRoleCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppRoleDO entity = AppRoleConverter.createVoToEntity(createVO);
		entity = appRoleService.create(entity);
		return ResponseResult.success(AppRoleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用角色关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppRoleVO> update(@RequestBody @Valid AppRoleUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppRoleDO entity = AppRoleConverter.updateVoToEntity(updateVO);
		entity = appRoleService.update(entity);
		return ResponseResult.success(AppRoleConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用角色关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppRoleVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppRoleDO> entities = appRoleService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppRoleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用角色关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppRoleVO> delete(@PathVariable("id") Long id) {
		AppRoleDO entity = appRoleService.delete(id);
		return ResponseResult.success(AppRoleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppRoleVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppRoleDO> entities = appRoleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppRoleConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppRoleVO> select(@PathVariable("id") Long id) {
		AppRoleDO entity = appRoleService.select(id);
		return ResponseResult.success(AppRoleConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppRoleVO>> selectList(@Valid AppRoleQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appRoleService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppRoleBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppRoleDO> entities = appRoleService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppRoleConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppRoleBriefVO> selectBrief(@PathVariable("id") Long id) {
		AppRoleDO entity = appRoleService.select(id);
		return ResponseResult.success(AppRoleConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppRoleBriefVO>> selectBriefList(@Valid AppRoleBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appRoleService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用角色关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页应用角色关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<AppRolePageVO>> selectPage(@Valid AppRolePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appRoleService.selectPage(query));
	}
	
}
