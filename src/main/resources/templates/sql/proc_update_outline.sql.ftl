CREATE PROCEDURE `proc_${table.name}_update_outline`()
    COMMENT '更新${table.name}结构码'
BEGIN 

	DECLARE LEVEL INT; 
	SET LEVEL = 1; 
	UPDATE ${table.name} SET OUTLINE = '00' WHERE IS_DELETED = 0; 

	SET @RowNum = 0; 
	UPDATE ${table.name} A 
	INNER JOIN ( 
		SELECT C.ID, 
					 C.PARENT_ID, 
					 LEVEL LEVEL, 
					 1 IS_LEAF, 
					 C.ID PATH, 
					 LPAD((@RowNum:=@RowNum + 1), 4, '0') OUTLINE 
		FROM ${table.name} C 
		WHERE C.IS_DELETED = 0 
		AND C.PARENT_ID IS NULL 
	) B ON A.ID = B.ID 
	SET A.OUTLINE = B.OUTLINE, 
			A.LEVEL = B.LEVEL, 
			A.IS_LEAF = B.IS_LEAF, 
			A.PATH = B.PATH; 

	WHILE LEVEL < 10 DO 

		SET @RowNum = 0; 
		SET LEVEL = LEVEL + 1; 

		UPDATE ${table.name} A 
		INNER JOIN ( 
			SELECT C.ID, 
						 C.PARENT_ID, 
						 LEVEL LEVEL, 
						 1 IS_LEAF, 
						 CONCAT(D.path, '/', C.id) PATH, 
						 CONCAT(D.OUTLINE, '.', LPAD((@RowNum:=@RowNum+1 ), 4, '0')) OUTLINE
			FROM ${table.name} C 
			INNER JOIN ${table.name} D ON C.PARENT_ID = D.ID AND D.OUTLINE <> '00' 
			WHERE C.IS_DELETED = 0 
			AND C.OUTLINE = '00' 
		)B ON A.ID = B.ID 
		SET A.OUTLINE = B.OUTLINE, 
				A.LEVEL = B.LEVEL, 
				A.IS_LEAF = B.IS_LEAF, 
				A.PATH = B.PATH; 

	END WHILE; 

	UPDATE ${table.name} A 
	INNER JOIN  ${table.name} B ON A.ID = B.PARENT_ID AND B.IS_DELETED = 0 
	SET A.IS_LEAF = 0 
	WHERE A.IS_DELETED = 0; 
	<#noparse>
	# create by zhongjyuan
	</#noparse>
 END