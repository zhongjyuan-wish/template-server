package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.RoleModulePermissionDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName RoleModulePermissionConverter
 * @Description 角色模块权限关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class RoleModulePermissionConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	public static RoleModulePermissionDO createVoToEntity(RoleModulePermissionCreateVO createVO) {
		RoleModulePermissionDO entity = new RoleModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	public static RoleModulePermissionDO updateVoToEntity(RoleModulePermissionUpdateVO updateVO) {
		RoleModulePermissionDO entity = new RoleModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModulePermissionVO VO对象
	 * @Throws
	 */
	public static RoleModulePermissionVO entityToVo(RoleModulePermissionDO entity) {

		RoleModulePermissionVO vo = new RoleModulePermissionVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModulePermissionBriefVO 简要对象
	 * @Throws
	 */
	public static RoleModulePermissionBriefVO entityToBriefVo(RoleModulePermissionDO entity) {

		RoleModulePermissionBriefVO briefVO = new RoleModulePermissionBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleModulePermissionVO> VO对象集合
	 * @Throws
	 */
	public static List<RoleModulePermissionVO> entityToVos(List<RoleModulePermissionDO> entities) {

		List<RoleModulePermissionVO> vos = new ArrayList<RoleModulePermissionVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, RoleModulePermissionVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<RoleModulePermissionBriefVO> entityToBriefVos(List<RoleModulePermissionDO> entities) {

		List<RoleModulePermissionBriefVO> briefVOs = new ArrayList<RoleModulePermissionBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, RoleModulePermissionBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleModulePermissionPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<RoleModulePermissionPageVO> entityToPageVos(List<RoleModulePermissionDO> entities) {

		List<RoleModulePermissionPageVO> pageVOs = new ArrayList<RoleModulePermissionPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, RoleModulePermissionPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}