package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.AppMapper;
import zhongjyuan.wish.template.domain.service.IAppService;

import zhongjyuan.wish.template.domain.model.entity.AppDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.AppCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName AppServiceImpl
 * @Description 应用表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class AppServiceImpl implements IAppService {

	@Autowired
	AppMapper appMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppDO create(AppDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = appMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		appMapper.insert(entity);

		log.info("======AppServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(AppDO entity) {

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);
		queryWrapper.lambda().eq(AppDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(AppDO::getGroup, entity.getGroup());
		}

		if (appMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(AppCode.APP_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(AppCode.APP_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);
		queryWrapper.lambda().eq(AppDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(AppDO::getGroup, entity.getGroup());
		}

		if (appMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(AppCode.APP_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(AppCode.APP_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppDO update(AppDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		appMapper.updateById(entity);

		log.info("======AppServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppDO 实体对象
	 * @Throws
	 */
	private AppDO updateEnabled(AppDO entity) {

		// 获取原始数据
		AppDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(AppCode.APP_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(AppCode.APP_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(AppCode.APP_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);
		queryWrapper.lambda().eq(AppDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(AppDO::getGroup, entity.getGroup());
		}

		if (appMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(AppCode.APP_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(AppCode.APP_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);
		queryWrapper.lambda().eq(AppDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(AppDO::getGroup, entity.getGroup());
		}

		if (appMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(AppCode.APP_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(AppCode.APP_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppDO delete(Long id) {

		// 获取原始数据
		AppDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		appMapper.updateById(originEntity);

		log.info("======AppServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppDO deleteByCode(String code) {

		// 获取原始数据
		AppDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		appMapper.updateById(originEntity);

		log.info("======AppServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<AppDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<AppDO> entities = new ArrayList<AppDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsSystem, false);
		queryWrapper.lambda().in(AppDO::getId, ids);
		entities = appMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<AppDO> successEntities = new ArrayList<AppDO>();
		
		// 根据主键更新
		for (AppDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			appMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======AppServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<AppDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<AppDO> entities = new ArrayList<AppDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsSystem, false);
		queryWrapper.lambda().in(AppDO::getCode, codes);
		entities = appMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<AppDO> successEntities = new ArrayList<AppDO>();
		
		// 根据主键更新
		for (AppDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			appMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======AppServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(AppDO entity) {
	
		if (entity == null) {
			throw new BusinessException(AppCode.APP_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(AppCode.APP_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(AppCode.APP_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public AppDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return appMapper.selectById(id);
	}
	
	@Override
	public AppDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);
		queryWrapper.lambda().eq(AppDO::getCode, code);
		
		return appMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<AppDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return appMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<AppDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);
		queryWrapper.lambda().in(AppDO::getCode, codes);
		
		return appMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<AppVO> selectList(AppQuery query) {
		List<AppVO> vos = new ArrayList<AppVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(AppDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(AppDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(AppDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<AppDO> entities = appMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = AppConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<AppBriefVO> selectBriefList(AppBriefQuery query) {
		List<AppBriefVO> briefVOs = new ArrayList<AppBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(AppDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(AppDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(AppDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<AppDO> entities = appMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = AppConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<AppPageVO> selectPage(AppPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppDO> queryWrapper = new QueryWrapper<AppDO>();
		queryWrapper.lambda().eq(AppDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(AppDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(AppDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(AppDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<AppDO> page = appMapper.selectPage(new CustomPage<AppDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> AppConverter.entityToPageVos(entities));
	}

}
