package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName PositionRankQuery
 * @Description 职业分级查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PositionRankQuery", description = "职业分级查询条件对象")
public class PositionRankQuery extends SortQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}