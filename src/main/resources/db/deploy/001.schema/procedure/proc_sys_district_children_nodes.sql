CREATE PROCEDURE `proc_sys_district_children_nodes`(IN type INT,
	IN showLevel INT,
	IN showSiblings INT,
	IN showHitNodes INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点下所有子节点(包含路径上的枝干节点和叶子节点)'
BEGIN
		
	DECLARE childLevel INT;
	DECLARE whileIndex INT;
	# 返回子节点结果集
	DECLARE childIdList LONGTEXT;
	# 临时存放节点
	DECLARE tempIdList LONGTEXT;
	# 临时存放子节点
	DECLARE tempChildIdList LONGTEXT;

	SET childIdList = '';
	SET childLevel = showLevel;
	SET tempChildIdList = hitNodeIds;

	SET whileIndex = 0;
	# 循环，用于查询节点下所有的子节点
	WHILE tempChildIdList IS NOT NULL AND whileIndex <= showLevel DO

		# 存入到返回结果中
		SET childIdList = CONCAT_WS(',', childIdList, tempChildIdList);

		# 查询节点下所有子节点
		SELECT GROUP_CONCAT(id) INTO tempChildIdList FROM sys_district WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, tempChildIdList) > 0;

		SET whileIndex = whileIndex + 1;
	END WHILE;

	# 是否加载对应兄弟节点(不包含命中节点的子级兄弟节点)
	IF (showSiblings IS NOT NULL AND showSiblings = 1) AND childIdList != CONCAT(',', hitNodeIds) THEN

		SET tempIdList = '';

		SELECT GROUP_CONCAT(id) INTO tempIdList FROM sys_district WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, childIdList);

		SET childIdList = CONCAT_WS(',', childIdList, tempIdList);

	END IF;

	# 是否包含命中节点
	IF (showHitNodes IS NOT NULL AND showHitNodes = 0) AND childIdList != CONCAT(',', hitNodeIds) THEN

		SET childIdList = REPLACE(REPLACE(childIdList, hitNodeIds, ''), ',,', ',');

	END IF;

	# 返回结果[主键字符串]
	IF type IS NULL OR type = 0 THEN
		SELECT SUBSTRING(childIdList, 2) AS childIds;
	END IF;

	# 返回结果[数据集]
	IF type IS NOT NULL AND type = 1 THEN
		SELECT * FROM sys_district A
		WHERE A.is_deleted = 0
		AND FIND_IN_SET(A.id, SUBSTRING(childIdList, 2))
		ORDER BY A.outline;
	END IF;
	# create by zhongjyuan
END