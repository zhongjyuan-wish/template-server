package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName OrganizeCreateVO
 * @Description 组织创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizeCreateVO", description = "组织创建对象")
public class OrganizeCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields no: 编号
	 */
	// @NotNull(message = "编号不能为空.")
	// @NotBlank(message = "编号不能为空.")
	// @Size(message = "编号最大长度为50.", max = 50)
	// @Min(message = "编号最小为600.", value = 600)
	@ApiModelProperty(value = "编号", name = "no", dataType = "String")
    private String no;

	/**
	 * @Fields name: 名称
	 */
	// @NotNull(message = "名称不能为空.")
	// @NotBlank(message = "名称不能为空.")
	// @Size(message = "名称最大长度为50.", max = 50)
	// @Min(message = "名称最小为600.", value = 600)
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields aliasName: 别称
	 */
	// @NotNull(message = "别称不能为空.")
	// @NotBlank(message = "别称不能为空.")
	// @Size(message = "别称最大长度为50.", max = 50)
	// @Min(message = "别称最小为600.", value = 600)
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields shortName: 简称
	 */
	// @NotNull(message = "简称不能为空.")
	// @NotBlank(message = "简称不能为空.")
	// @Size(message = "简称最大长度为50.", max = 50)
	// @Min(message = "简称最小为600.", value = 600)
	@ApiModelProperty(value = "简称", name = "shortName", dataType = "String")
    private String shortName;

	/**
	 * @Fields fullName: 全称
	 */
	// @NotNull(message = "全称不能为空.")
	// @NotBlank(message = "全称不能为空.")
	// @Size(message = "全称最大长度为50.", max = 50)
	// @Min(message = "全称最小为600.", value = 600)
	@ApiModelProperty(value = "全称", name = "fullName", dataType = "String")
    private String fullName;

	/**
	 * @Fields type: 类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]
	 */
	// @NotNull(message = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]不能为空.")
	// @NotBlank(message = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]不能为空.")
	// @Size(message = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]最大长度为50.", max = 50)
	// @Min(message = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]最小为600.", value = 600)
	@ApiModelProperty(value = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
	// @NotNull(message = "图标不能为空.")
	// @NotBlank(message = "图标不能为空.")
	// @Size(message = "图标最大长度为50.", max = 50)
	// @Min(message = "图标最小为600.", value = 600)
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields parentId: 父级主键
	 */
	// @NotNull(message = "父级主键不能为空.")
	// @NotBlank(message = "父级主键不能为空.")
	// @Size(message = "父级主键最大长度为50.", max = 50)
	// @Min(message = "父级主键最小为600.", value = 600)
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentNo: 父级编号
	 */
	// @NotNull(message = "父级编号不能为空.")
	// @NotBlank(message = "父级编号不能为空.")
	// @Size(message = "父级编号最大长度为50.", max = 50)
	// @Min(message = "父级编号最小为600.", value = 600)
	@ApiModelProperty(value = "父级编号", name = "parentNo", dataType = "String")
    private String parentNo;

	/**
	 * @Fields parentName: 父级名称
	 */
	// @NotNull(message = "父级名称不能为空.")
	// @NotBlank(message = "父级名称不能为空.")
	// @Size(message = "父级名称最大长度为50.", max = 50)
	// @Min(message = "父级名称最小为600.", value = 600)
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;

	/**
	 * @Fields directId: 直属主键
	 */
	// @NotNull(message = "直属主键不能为空.")
	// @NotBlank(message = "直属主键不能为空.")
	// @Size(message = "直属主键最大长度为50.", max = 50)
	// @Min(message = "直属主键最小为600.", value = 600)
	@ApiModelProperty(value = "直属主键", name = "directId", dataType = "Long")
    private Long directId;

	/**
	 * @Fields directNo: 直属编号
	 */
	// @NotNull(message = "直属编号不能为空.")
	// @NotBlank(message = "直属编号不能为空.")
	// @Size(message = "直属编号最大长度为50.", max = 50)
	// @Min(message = "直属编号最小为600.", value = 600)
	@ApiModelProperty(value = "直属编号", name = "directNo", dataType = "String")
    private String directNo;

	/**
	 * @Fields directName: 直属名称
	 */
	// @NotNull(message = "直属名称不能为空.")
	// @NotBlank(message = "直属名称不能为空.")
	// @Size(message = "直属名称最大长度为50.", max = 50)
	// @Min(message = "直属名称最小为600.", value = 600)
	@ApiModelProperty(value = "直属名称", name = "directName", dataType = "String")
    private String directName;








	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}