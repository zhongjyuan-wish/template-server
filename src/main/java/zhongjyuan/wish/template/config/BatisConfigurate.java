package zhongjyuan.wish.template.config;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName BatisConfigurate
 * @Description Batis配置对象
 * @Author zhongjyuan
 * @Date 2023年1月5日 下午6:02:46
 * @Copyright zhongjyuan.com
 */
@Slf4j
@Configuration
@MapperScan(basePackages = "zhongjyuan.wish.template.domain.mapper") /* [注意]basePackages对应服务数据表Mapper接口对象包路径 */
public class BatisConfigurate implements TransactionManagementConfigurer {

	@Autowired
	DataSource dataSource;

	@Override
	public TransactionManager annotationDrivenTransactionManager() {
		return new DataSourceTransactionManager(dataSource);
	}

	/**
	 * @Title mySqlSessionTemplate
	 * @Author zhongjyuan
	 * @Description 注入SqlSessionTemplate
	 * @Param @param sqlSessionFactory
	 * @Param @return
	 * @Param @throws Exception
	 * @Return SqlSessionTemplate
	 * @Throws
	 */
	@Primary
	@Bean(name = "mySqlSessionTemplate")
	public SqlSessionTemplate mySqlSessionTemplate(SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

	/**
	 * @Title sqlSessionFactory
	 * @Author zhongjyuan
	 * @Description 注入SqlSessionFactory
	 * @Param @return
	 * @Param @throws Exception
	 * @Return MybatisSqlSessionFactoryBean
	 * @Throws
	 */
	@Bean
	public MybatisSqlSessionFactoryBean sqlSessionFactory() throws Exception {
		log.info("==============初始化 sqlSessionFactory===============");

		MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();

		mybatisSqlSessionFactoryBean.setDataSource(dataSource);
		mybatisSqlSessionFactoryBean.setGlobalConfig(globalConfig());
		mybatisSqlSessionFactoryBean.setConfiguration(configuration());
		mybatisSqlSessionFactoryBean.setPlugins(new Interceptor[] { pageInterceptor() });
		mybatisSqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:com/wish/template/domain/mapper/xml/*.xml"));

		return mybatisSqlSessionFactoryBean;
	}

	/**
	 * @Title globalConfig
	 * @Author zhongjyuan
	 * @Description 全局配置
	 * @Param @return
	 * @Return GlobalConfig
	 * @Throws
	 */
	private GlobalConfig globalConfig() {
		log.info("==============初始化 globalConfig===============");

		GlobalConfig globalConfig = new GlobalConfig();

		// MyBatis-Plus 全局策略中的 DB 策略配置
		GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();

		// 字段验证策略之 update
		dbConfig.setUpdateStrategy(FieldStrategy.IGNORED);

		globalConfig.setDbConfig(dbConfig);

		return globalConfig;
	}

	/**
	 * @Title configuration
	 * @Author zhongjyuan
	 * @Description Mybatis配置对象
	 * @Param @return
	 * @Return MybatisConfiguration
	 * @Throws
	 */
	private MybatisConfiguration configuration() {
		log.info("==============初始化 configuration===============");

		MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();

		// 开启驼峰命名 此属性在 MyBatis 中原默认值为 false 如果您的数据库命名符合规则无需使用
		mybatisConfiguration.setMapUnderscoreToCamelCase(true);

		return mybatisConfiguration;
	}

	/**
	 * @Title pageInterceptor
	 * @Author zhongjyuan
	 * @Description Mybatis-plus 分页插件
	 * @Param @return
	 * @Return MybatisPlusInterceptor
	 * @Throws
	 */
	private MybatisPlusInterceptor pageInterceptor() {
		log.info("==============初始化 pageInterceptor===============");

		// 分页插件
		MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

		interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));

		return interceptor;
	}
}
