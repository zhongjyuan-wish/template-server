package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.AppModuleMapper;
import zhongjyuan.wish.template.domain.service.IAppModuleService;

import zhongjyuan.wish.template.domain.model.entity.AppModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.AppModuleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName AppModuleServiceImpl
 * @Description 应用模块关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class AppModuleServiceImpl implements IAppModuleService {

	@Autowired
	AppModuleMapper appModuleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppModuleDO create(AppModuleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = appModuleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		appModuleMapper.insert(entity);

		log.info("======AppModuleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(AppModuleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppModuleDO update(AppModuleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		appModuleMapper.updateById(entity);

		log.info("======AppModuleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppModuleDO 实体对象
	 * @Throws
	 */
	private AppModuleDO updateEnabled(AppModuleDO entity) {

		// 获取原始数据
		AppModuleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(AppModuleCode.APPMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppModuleDO delete(Long id) {

		// 获取原始数据
		AppModuleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		appModuleMapper.updateById(originEntity);

		log.info("======AppModuleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<AppModuleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<AppModuleDO> entities = new ArrayList<AppModuleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<AppModuleDO> queryWrapper = new QueryWrapper<AppModuleDO>();
		queryWrapper.lambda().in(AppModuleDO::getId, ids);
		entities = appModuleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<AppModuleDO> successEntities = new ArrayList<AppModuleDO>();
		
		// 根据主键更新
		for (AppModuleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			appModuleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======AppModuleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(AppModuleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(AppModuleCode.APPMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public AppModuleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return appModuleMapper.selectById(id);
	}
	
	@Override
	public List<AppModuleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return appModuleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<AppModuleVO> selectList(AppModuleQuery query) {
		List<AppModuleVO> vos = new ArrayList<AppModuleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppModuleDO> queryWrapper = new QueryWrapper<AppModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<AppModuleDO> entities = appModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = AppModuleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<AppModuleBriefVO> selectBriefList(AppModuleBriefQuery query) {
		List<AppModuleBriefVO> briefVOs = new ArrayList<AppModuleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppModuleDO> queryWrapper = new QueryWrapper<AppModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<AppModuleDO> entities = appModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = AppModuleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<AppModulePageVO> selectPage(AppModulePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppModuleDO> queryWrapper = new QueryWrapper<AppModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppModuleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<AppModuleDO> page = appModuleMapper.selectPage(new CustomPage<AppModuleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> AppModuleConverter.entityToPageVos(entities));
	}

}
