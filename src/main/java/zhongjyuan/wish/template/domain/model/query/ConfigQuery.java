package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName ConfigQuery
 * @Description 配置查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ConfigQuery", description = "配置查询条件对象")
public class ConfigQuery extends SortQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields value: 值
	 */
	@ApiModelProperty(value = "值", name = "value", dataType = "String")
    private String value;

	/**
	 * @Fields businessId: 业务主键
	 */
	@ApiModelProperty(value = "业务主键", name = "businessId", dataType = "Long")
    private Long businessId;

	/**
	 * @Fields businessCode: 业务编码
	 */
	@ApiModelProperty(value = "业务编码", name = "businessCode", dataType = "String")
    private String businessCode;

	/**
	 * @Fields businessName: 业务名称
	 */
	@ApiModelProperty(value = "业务名称", name = "businessName", dataType = "String")
    private String businessName;

	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}