<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">

<#if enableCache>
    <!-- 开启二级缓存 -->
    <cache type="${cacheClassName}"/>

</#if>
<#if baseResultMap>
    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
<#list table.fields as field>
<#if field.keyFlag><#--生成主键排在第一位-->
        <id column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
<#list table.commonFields as field><#--生成公共字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#list>
<#list table.fields as field>
<#if !field.keyFlag><#--生成普通字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
    </resultMap>

</#if>
<#if baseColumnList>
    <!-- 通用查询结果列 add ${author} by ${date} -->
    <sql id="Base_Column_List">
<#list table.commonFields as field>
        ${field.columnName},
</#list>
        ${table.fieldNames}
    </sql>

</#if>

<#if treeFlag>
    <!-- 更新层级码 add ${author} by ${date} -->
	<update id="updateOutline" statementType="CALLABLE">
		{ CALL PROC_${schemaName?upper_case}${table.name?upper_case}_UPDATE_OUTLINE() }
	</update>
	
    <!-- 获取所有父级 add ${author} by ${date} -->
	<select id = "selectParents" statementType="CALLABLE" resultType = "${package.Entity}.${entity}">
		{
			CALL PROC_${schemaName?upper_case}${table.name?upper_case}_PARENT_NODES
			(
				1,
				<#noparse>
				#{showSiblings, mode = IN, jdbcType = TINYINT},
				#{showHitNodes, mode = IN, jdbcType = TINYINT},
				#{hitNodeIdString, mode = IN, jdbcType = VARCHAR}
				</#noparse>
			)
		}
	</select> 
	
    <!-- 获取所有子级 add ${author} by ${date} -->
	<select id = "selectChildrens" statementType="CALLABLE" resultType = "${package.Entity}.${entity}">
		{
			CALL PROC_${schemaName?upper_case}${table.name?upper_case}_CHILDREN_NODES
			(
				1,
				<#noparse>
				#{showLevel, mode = IN, jdbcType = TINYINT},
				#{showSiblings, mode = IN, jdbcType = TINYINT},
				#{showHitNodes, mode = IN, jdbcType = TINYINT},
				#{hitNodeIdString, mode = IN, jdbcType = VARCHAR}
				</#noparse>
			)
		}
	</select> 
	
    <!-- 获取所有叶子 add ${author} by ${date} -->
	<select id = "selectLeafs" statementType="CALLABLE" resultType = "${package.Entity}.${entity}">
		{
			CALL PROC_${schemaName?upper_case}${table.name?upper_case}_LEAF_NODES
			(
				1,
				<#noparse>
				#{hitNodeIdString, mode = IN, jdbcType = VARCHAR}
				</#noparse>
			)
		}
	</select> 
	
    <!-- 获取全路径节点 add ${author} by ${date} -->
	<select id = "selectFulls" statementType="CALLABLE" resultType = "${package.Entity}.${entity}">
		{
			CALL PROC_${schemaName?upper_case}${table.name?upper_case}_FULL_NODES
			(
				1,
				<#noparse>
				#{showSiblings, mode = IN, jdbcType = TINYINT},
				#{showHitNodes, mode = IN, jdbcType = TINYINT},
				#{hitNodeIdString, mode = IN, jdbcType = VARCHAR}
				</#noparse>
			)
		}
	</select> 
</#if>
</mapper>
