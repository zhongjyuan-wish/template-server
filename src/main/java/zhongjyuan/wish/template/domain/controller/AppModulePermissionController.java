package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IAppModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.AppModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName AppModulePermissionController
 * @Description 应用模块权限关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "应用模块权限关联管理", tags = "应用模块权限关联管理")
@RequestMapping("{version}/appModulePermissions")
public class AppModulePermissionController {

	@Autowired
	IAppModulePermissionService appModulePermissionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "应用模块权限关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModulePermissionVO> create(@RequestBody @Valid AppModulePermissionCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppModulePermissionDO entity = AppModulePermissionConverter.createVoToEntity(createVO);
		entity = appModulePermissionService.create(entity);
		return ResponseResult.success(AppModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "应用模块权限关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModulePermissionVO> update(@RequestBody @Valid AppModulePermissionUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		AppModulePermissionDO entity = AppModulePermissionConverter.updateVoToEntity(updateVO);
		entity = appModulePermissionService.update(entity);
		return ResponseResult.success(AppModulePermissionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModulePermissionVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppModulePermissionDO> entities = appModulePermissionService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "应用模块权限关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModulePermissionVO> delete(@PathVariable("id") Long id) {
		AppModulePermissionDO entity = appModulePermissionService.delete(id);
		return ResponseResult.success(AppModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModulePermissionVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppModulePermissionDO> entities = appModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppModulePermissionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModulePermissionVO> select(@PathVariable("id") Long id) {
		AppModulePermissionDO entity = appModulePermissionService.select(id);
		return ResponseResult.success(AppModulePermissionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModulePermissionVO>> selectList(@Valid AppModulePermissionQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appModulePermissionService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModulePermissionBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<AppModulePermissionDO> entities = appModulePermissionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(AppModulePermissionConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<AppModulePermissionBriefVO> selectBrief(@PathVariable("id") Long id) {
		AppModulePermissionDO entity = appModulePermissionService.select(id);
		return ResponseResult.success(AppModulePermissionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<AppModulePermissionBriefVO>> selectBriefList(@Valid AppModulePermissionBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appModulePermissionService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "应用模块权限关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页应用模块权限关联 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<AppModulePermissionPageVO>> selectPage(@Valid AppModulePermissionPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(appModulePermissionService.selectPage(query));
	}
	
}
