package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IOrganizeService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName OrganizeController
 * @Description 组织管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "组织管理", tags = "组织管理")
@RequestMapping("{version}/organizes")
public class OrganizeController {

	@Autowired
	IOrganizeService organizeService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "组织", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeVO> create(@RequestBody @Valid OrganizeCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		OrganizeDO entity = OrganizeConverter.createVoToEntity(createVO);
		entity = organizeService.create(entity);
		return ResponseResult.success(OrganizeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "组织", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeVO> update(@RequestBody @Valid OrganizeUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		OrganizeDO entity = OrganizeConverter.updateVoToEntity(updateVO);
		entity = organizeService.update(entity);
		return ResponseResult.success(OrganizeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "组织", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeDO> entities = organizeService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "组织", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeVO> delete(@PathVariable("id") Long id) {
		OrganizeDO entity = organizeService.delete(id);
		return ResponseResult.success(OrganizeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "组织", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeVO> enable(@PathVariable("id") Long id) {
		OrganizeDO entity = organizeService.select(id);
		entity.setIsEnabled(true);
		entity = organizeService.update(entity);
		return ResponseResult.success(OrganizeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "组织", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeVO> disable(@PathVariable("id") Long id) {
		OrganizeDO entity = organizeService.select(id);
		entity.setIsEnabled(false);
		entity = organizeService.update(entity);
		return ResponseResult.success(OrganizeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeDO> entities = organizeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeVO> select(@PathVariable("id") Long id) {
		OrganizeDO entity = organizeService.select(id);
		return ResponseResult.success(OrganizeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeVO>> selectList(@Valid OrganizeQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeDO> entities = organizeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeBriefVO> selectBrief(@PathVariable("id") Long id) {
		OrganizeDO entity = organizeService.select(id);
		return ResponseResult.success(OrganizeConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeBriefVO>> selectBriefList(@Valid OrganizeBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<OrganizePageVO>> selectPage(@Valid OrganizePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectPage(query));
	}
	
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeTreeVO>> selectTree(@Valid TreeQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectTree(query));
	}
	
	@RequestMapping(value = "/tree/lazy", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(懒加载) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeTreeVO>> selectLazy(@Valid TreeLazyQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectLazy(query));
	}

	@RequestMapping(value = "/tree/parents", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeTreeVO>> selectParents(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectParents(query));
	}

	@RequestMapping(value = "/tree/parentIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectParentIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeTreeVO> treeVOs = organizeService.selectParents(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/childrens", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(所有子级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeTreeVO>> selectChildrens(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectChildrens(query));
	}

	@RequestMapping(value = "/tree/childrenIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectChildrenIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeTreeVO> treeVOs = organizeService.selectChildrens(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/leafs", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeTreeVO>> selectLeafs(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectLeafs(query));
	}

	@RequestMapping(value = "/tree/leafIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectLeafIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeTreeVO> treeVOs = organizeService.selectLeafs(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/fulls", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeTreeVO>> selectFulls(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeService.selectFulls(query));
	}

	@RequestMapping(value = "/tree/fullIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形组织(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectFullIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeTreeVO> treeVOs = organizeService.selectFulls(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}
}
