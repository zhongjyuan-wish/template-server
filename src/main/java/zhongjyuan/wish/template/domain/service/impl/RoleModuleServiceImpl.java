package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.RoleModuleMapper;
import zhongjyuan.wish.template.domain.service.IRoleModuleService;

import zhongjyuan.wish.template.domain.model.entity.RoleModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.RoleModuleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName RoleModuleServiceImpl
 * @Description 角色模块关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class RoleModuleServiceImpl implements IRoleModuleService {

	@Autowired
	RoleModuleMapper roleModuleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleModuleDO create(RoleModuleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = roleModuleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		roleModuleMapper.insert(entity);

		log.info("======RoleModuleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(RoleModuleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleModuleDO update(RoleModuleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		roleModuleMapper.updateById(entity);

		log.info("======RoleModuleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModuleDO 实体对象
	 * @Throws
	 */
	private RoleModuleDO updateEnabled(RoleModuleDO entity) {

		// 获取原始数据
		RoleModuleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(RoleModuleCode.ROLEMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleModuleDO delete(Long id) {

		// 获取原始数据
		RoleModuleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleModuleMapper.updateById(originEntity);

		log.info("======RoleModuleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleModuleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<RoleModuleDO> entities = new ArrayList<RoleModuleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleModuleDO> queryWrapper = new QueryWrapper<RoleModuleDO>();
		queryWrapper.lambda().in(RoleModuleDO::getId, ids);
		entities = roleModuleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleModuleDO> successEntities = new ArrayList<RoleModuleDO>();
		
		// 根据主键更新
		for (RoleModuleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			roleModuleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleModuleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(RoleModuleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(RoleModuleCode.ROLEMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public RoleModuleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return roleModuleMapper.selectById(id);
	}
	
	@Override
	public List<RoleModuleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return roleModuleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<RoleModuleVO> selectList(RoleModuleQuery query) {
		List<RoleModuleVO> vos = new ArrayList<RoleModuleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleModuleDO> queryWrapper = new QueryWrapper<RoleModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleModuleDO> entities = roleModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = RoleModuleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<RoleModuleBriefVO> selectBriefList(RoleModuleBriefQuery query) {
		List<RoleModuleBriefVO> briefVOs = new ArrayList<RoleModuleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleModuleDO> queryWrapper = new QueryWrapper<RoleModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleModuleDO> entities = roleModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = RoleModuleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<RoleModulePageVO> selectPage(RoleModulePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleModuleDO> queryWrapper = new QueryWrapper<RoleModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleModuleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<RoleModuleDO> page = roleModuleMapper.selectPage(new CustomPage<RoleModuleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> RoleModuleConverter.entityToPageVos(entities));
	}

}
