package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.StationRoleMapper;
import zhongjyuan.wish.template.domain.service.IStationRoleService;

import zhongjyuan.wish.template.domain.model.entity.StationRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.StationRoleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName StationRoleServiceImpl
 * @Description 岗位角色关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class StationRoleServiceImpl implements IStationRoleService {

	@Autowired
	StationRoleMapper stationRoleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationRoleDO create(StationRoleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = stationRoleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		stationRoleMapper.insert(entity);

		log.info("======StationRoleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(StationRoleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationRoleDO update(StationRoleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		stationRoleMapper.updateById(entity);

		log.info("======StationRoleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return StationRoleDO 实体对象
	 * @Throws
	 */
	private StationRoleDO updateEnabled(StationRoleDO entity) {

		// 获取原始数据
		StationRoleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(StationRoleCode.STATIONROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public StationRoleDO delete(Long id) {

		// 获取原始数据
		StationRoleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		stationRoleMapper.updateById(originEntity);

		log.info("======StationRoleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<StationRoleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<StationRoleDO> entities = new ArrayList<StationRoleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<StationRoleDO> queryWrapper = new QueryWrapper<StationRoleDO>();
		queryWrapper.lambda().in(StationRoleDO::getId, ids);
		entities = stationRoleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<StationRoleDO> successEntities = new ArrayList<StationRoleDO>();
		
		// 根据主键更新
		for (StationRoleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			stationRoleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======StationRoleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(StationRoleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(StationRoleCode.STATIONROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public StationRoleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return stationRoleMapper.selectById(id);
	}
	
	@Override
	public List<StationRoleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return stationRoleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<StationRoleVO> selectList(StationRoleQuery query) {
		List<StationRoleVO> vos = new ArrayList<StationRoleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationRoleDO> queryWrapper = new QueryWrapper<StationRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<StationRoleDO> entities = stationRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = StationRoleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<StationRoleBriefVO> selectBriefList(StationRoleBriefQuery query) {
		List<StationRoleBriefVO> briefVOs = new ArrayList<StationRoleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationRoleDO> queryWrapper = new QueryWrapper<StationRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<StationRoleDO> entities = stationRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = StationRoleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<StationRolePageVO> selectPage(StationRolePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<StationRoleDO> queryWrapper = new QueryWrapper<StationRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(StationRoleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<StationRoleDO> page = stationRoleMapper.selectPage(new CustomPage<StationRoleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> StationRoleConverter.entityToPageVos(entities));
	}

}
