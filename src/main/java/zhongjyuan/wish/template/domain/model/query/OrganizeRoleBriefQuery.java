package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName OrganizeRoleBriefQuery
 * @Description 组织角色关联简要查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizeRoleBriefQuery", description = "组织角色关联简要查询条件对象")
public class OrganizeRoleBriefQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields organizeId: 组织主键
	 */
	@ApiModelProperty(value = "组织主键", name = "organizeId", dataType = "Long")
    private Long organizeId;

	/**
	 * @Fields organizeNo: 组织编号
	 */
	@ApiModelProperty(value = "组织编号", name = "organizeNo", dataType = "String")
    private String organizeNo;

	/**
	 * @Fields organizeName: 组织名称
	 */
	@ApiModelProperty(value = "组织名称", name = "organizeName", dataType = "String")
    private String organizeName;

	/**
	 * @Fields roleId: 角色主键
	 */
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}