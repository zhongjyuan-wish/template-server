package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IDistrictService;

import zhongjyuan.wish.template.domain.model.entity.DistrictDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName DistrictController
 * @Description 行政区划管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "行政区划管理", tags = "行政区划管理")
@RequestMapping("{version}/districts")
public class DistrictController {

	@Autowired
	IDistrictService districtService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> create(@RequestBody @Valid DistrictCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		DistrictDO entity = DistrictConverter.createVoToEntity(createVO);
		entity = districtService.create(entity);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> update(@RequestBody @Valid DistrictUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		DistrictDO entity = DistrictConverter.updateVoToEntity(updateVO);
		entity = districtService.update(entity);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictDO> entities = districtService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(DistrictConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictDO> entities = districtService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(DistrictConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> delete(@PathVariable("id") Long id) {
		DistrictDO entity = districtService.delete(id);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> deleteByCode(@PathVariable("code") String code) {
		DistrictDO entity = districtService.deleteByCode(code);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> enable(@PathVariable("id") Long id) {
		DistrictDO entity = districtService.select(id);
		entity.setIsEnabled(true);
		entity = districtService.update(entity);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> enableByCode(@PathVariable("code") String code) {
		DistrictDO entity = districtService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = districtService.update(entity);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> disable(@PathVariable("id") Long id) {
		DistrictDO entity = districtService.select(id);
		entity.setIsEnabled(false);
		entity = districtService.update(entity);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "行政区划", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> disableByCode(@PathVariable("code") String code) {
		DistrictDO entity = districtService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = districtService.update(entity);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictDO> entities = districtService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(DistrictConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictDO> entities = districtService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(DistrictConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> select(@PathVariable("id") Long id) {
		DistrictDO entity = districtService.select(id);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictVO> selectByCode(@PathVariable("code") String code) {
		DistrictDO entity = districtService.selectByCode(code);
		return ResponseResult.success(DistrictConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictVO>> selectList(@Valid DistrictQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictDO> entities = districtService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(DistrictConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictBriefVO> selectBrief(@PathVariable("id") Long id) {
		DistrictDO entity = districtService.select(id);
		return ResponseResult.success(DistrictConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<DistrictBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		DistrictDO entity = districtService.selectByCode(code);
		return ResponseResult.success(DistrictConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictBriefVO>> selectBriefList(@Valid DistrictBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<DistrictPageVO>> selectPage(@Valid DistrictPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectPage(query));
	}
	
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictTreeVO>> selectTree(@Valid TreeQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectTree(query));
	}
	
	@RequestMapping(value = "/tree/lazy", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(懒加载) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictTreeVO>> selectLazy(@Valid TreeLazyQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectLazy(query));
	}

	@RequestMapping(value = "/tree/parents", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictTreeVO>> selectParents(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectParents(query));
	}

	@RequestMapping(value = "/tree/parentIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<Long>> selectParentIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictTreeVO> treeVOs = districtService.selectParents(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/childrens", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(所有子级) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictTreeVO>> selectChildrens(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectChildrens(query));
	}

	@RequestMapping(value = "/tree/childrenIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<Long>> selectChildrenIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictTreeVO> treeVOs = districtService.selectChildrens(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/leafs", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictTreeVO>> selectLeafs(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectLeafs(query));
	}

	@RequestMapping(value = "/tree/leafIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<Long>> selectLeafIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictTreeVO> treeVOs = districtService.selectLeafs(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/fulls", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<DistrictTreeVO>> selectFulls(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(districtService.selectFulls(query));
	}

	@RequestMapping(value = "/tree/fullIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "行政区划", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形行政区划(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<Long>> selectFullIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<DistrictTreeVO> treeVOs = districtService.selectFulls(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}
}
