package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.UserRoleDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName UserRoleConverter
 * @Description 用户角色关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class UserRoleConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	public static UserRoleDO createVoToEntity(UserRoleCreateVO createVO) {
		UserRoleDO entity = new UserRoleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return UserRoleDO 实体对象
	 * @Throws
	 */
	public static UserRoleDO updateVoToEntity(UserRoleUpdateVO updateVO) {
		UserRoleDO entity = new UserRoleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserRoleVO VO对象
	 * @Throws
	 */
	public static UserRoleVO entityToVo(UserRoleDO entity) {

		UserRoleVO vo = new UserRoleVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserRoleBriefVO 简要对象
	 * @Throws
	 */
	public static UserRoleBriefVO entityToBriefVo(UserRoleDO entity) {

		UserRoleBriefVO briefVO = new UserRoleBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserRoleVO> VO对象集合
	 * @Throws
	 */
	public static List<UserRoleVO> entityToVos(List<UserRoleDO> entities) {

		List<UserRoleVO> vos = new ArrayList<UserRoleVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, UserRoleVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserRoleBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<UserRoleBriefVO> entityToBriefVos(List<UserRoleDO> entities) {

		List<UserRoleBriefVO> briefVOs = new ArrayList<UserRoleBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, UserRoleBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserRolePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<UserRolePageVO> entityToPageVos(List<UserRoleDO> entities) {

		List<UserRolePageVO> pageVOs = new ArrayList<UserRolePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, UserRolePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}