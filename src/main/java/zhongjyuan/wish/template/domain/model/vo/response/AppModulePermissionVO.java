package zhongjyuan.wish.template.domain.model.vo.response;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName AppModulePermissionVO
 * @Description 应用模块权限关联对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppModulePermissionVO", description = "应用模块权限关联对象")
public class AppModulePermissionVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields appModuleId: 应用模块主键
	 */
	@ApiModelProperty(value = "应用模块主键", name = "appModuleId", dataType = "Long")
    private Long appModuleId;

	/**
	 * @Fields appModuleCode: 应用模块编码
	 */
	@ApiModelProperty(value = "应用模块编码", name = "appModuleCode", dataType = "String")
    private String appModuleCode;

	/**
	 * @Fields appModuleName: 应用模块名称
	 */
	@ApiModelProperty(value = "应用模块名称", name = "appModuleName", dataType = "String")
    private String appModuleName;

	/**
	 * @Fields modulePermissionId: 模块权限主键
	 */
	@ApiModelProperty(value = "模块权限主键", name = "modulePermissionId", dataType = "Long")
    private Long modulePermissionId;

	/**
	 * @Fields modulePermissionCode: 模块权限编码
	 */
	@ApiModelProperty(value = "模块权限编码", name = "modulePermissionCode", dataType = "String")
    private String modulePermissionCode;

	/**
	 * @Fields modulePermissionName: 模块权限名称
	 */
	@ApiModelProperty(value = "模块权限名称", name = "modulePermissionName", dataType = "String")
    private String modulePermissionName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;
}