package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.CodeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName ICodeService
 * @Description 码表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface ICodeService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	CodeDO create(CodeDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	CodeDO update(CodeDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	CodeDO delete(Long id);

	/**
	 * @Title deleteByCode
	 * @Author zhongjyuan
	 * @Description 根据编码删除
	 * @Param @param code 编码
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	CodeDO deleteByCode(String code);
	
	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<CodeDO> 实体对象集合
	 * @Throws
	 */
	List<CodeDO> delete(List<Long> ids);

	/**
	 * @Title deleteByCode
	 * @Author zhongjyuan
	 * @Description 根据编码删除
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<CodeDO> 实体对象集合
	 * @Throws
	 */
	List<CodeDO> deleteByCode(List<String> codes);
	
	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	CodeDO select(Long id);

	/**
	 * @Title selectByCode
	 * @Author zhongjyuan
	 * @Description 根据编码查询
	 * @Param @param code 编码
	 * @Param @return
	 * @Return CodeDO 实体对象
	 * @Throws
	 */
	CodeDO selectByCode(String code);
	
	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<CodeDO> 实体对象集合
	 * @Throws
	 */
	List<CodeDO> select(List<Long> ids);

	/**
	 * @Title selectByCode
	 * @Author zhongjyuan
	 * @Description 根据编码查询
	 * @Param @param codes 编码集合
	 * @Param @return
	 * @Return List<CodeDO> 实体对象集合
	 * @Throws
	 */
	List<CodeDO> selectByCode(List<String> codes);
	
	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<CodeVO> VO对象集合
	 * @Throws
	 */
	List<CodeVO> selectList(CodeQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<CodeBriefVO> 简要对象集合
	 * @Throws
	 */
	List<CodeBriefVO> selectBriefList(CodeBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<CodePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<CodePageVO> selectPage(CodePageQuery query);

}
