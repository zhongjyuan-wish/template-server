package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName RoleMutexQuery
 * @Description 角色互斥关联查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleMutexQuery", description = "角色互斥关联查询条件对象")
public class RoleMutexQuery extends SortQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields roleId: 角色主键
	 */
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields mutexRoleId: 互斥角色主键
	 */
	@ApiModelProperty(value = "互斥角色主键", name = "mutexRoleId", dataType = "Long")
    private Long mutexRoleId;

	/**
	 * @Fields mutexRoleCode: 互斥角色编码
	 */
	@ApiModelProperty(value = "互斥角色编码", name = "mutexRoleCode", dataType = "String")
    private String mutexRoleCode;

	/**
	 * @Fields mutexRoleName: 互斥角色名称
	 */
	@ApiModelProperty(value = "互斥角色名称", name = "mutexRoleName", dataType = "String")
    private String mutexRoleName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}