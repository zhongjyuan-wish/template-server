package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.OrganizeLeaderDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IOrganizeLeaderService
 * @Description 组织责任人关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IOrganizeLeaderService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeLeaderDO 实体对象
	 * @Throws
	 */
	OrganizeLeaderDO create(OrganizeLeaderDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeLeaderDO 实体对象
	 * @Throws
	 */
	OrganizeLeaderDO update(OrganizeLeaderDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return OrganizeLeaderDO 实体对象
	 * @Throws
	 */
	OrganizeLeaderDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<OrganizeLeaderDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeLeaderDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return OrganizeLeaderDO 实体对象
	 * @Throws
	 */
	OrganizeLeaderDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<OrganizeLeaderDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeLeaderDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<OrganizeLeaderVO> VO对象集合
	 * @Throws
	 */
	List<OrganizeLeaderVO> selectList(OrganizeLeaderQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<OrganizeLeaderBriefVO> 简要对象集合
	 * @Throws
	 */
	List<OrganizeLeaderBriefVO> selectBriefList(OrganizeLeaderBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<OrganizeLeaderPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<OrganizeLeaderPageVO> selectPage(OrganizeLeaderPageQuery query);

}
