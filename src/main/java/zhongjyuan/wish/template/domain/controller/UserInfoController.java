package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IUserInfoService;

import zhongjyuan.wish.template.domain.model.entity.UserInfoDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName UserInfoController
 * @Description 用户信息管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "用户信息管理", tags = "用户信息管理")
@RequestMapping("{version}/userInfos")
public class UserInfoController {

	@Autowired
	IUserInfoService userInfoService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "用户信息", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserInfoVO> create(@RequestBody @Valid UserInfoCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserInfoDO entity = UserInfoConverter.createVoToEntity(createVO);
		entity = userInfoService.create(entity);
		return ResponseResult.success(UserInfoConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "用户信息", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserInfoVO> update(@RequestBody @Valid UserInfoUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserInfoDO entity = UserInfoConverter.updateVoToEntity(updateVO);
		entity = userInfoService.update(entity);
		return ResponseResult.success(UserInfoConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户信息", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserInfoVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserInfoDO> entities = userInfoService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserInfoConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户信息", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserInfoVO> delete(@PathVariable("id") Long id) {
		UserInfoDO entity = userInfoService.delete(id);
		return ResponseResult.success(UserInfoConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserInfoVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserInfoDO> entities = userInfoService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserInfoConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserInfoVO> select(@PathVariable("id") Long id) {
		UserInfoDO entity = userInfoService.select(id);
		return ResponseResult.success(UserInfoConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserInfoVO>> selectList(@Valid UserInfoQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userInfoService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserInfoBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserInfoDO> entities = userInfoService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserInfoConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserInfoBriefVO> selectBrief(@PathVariable("id") Long id) {
		UserInfoDO entity = userInfoService.select(id);
		return ResponseResult.success(UserInfoConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserInfoBriefVO>> selectBriefList(@Valid UserInfoBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userInfoService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户信息", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页用户信息 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<UserInfoPageVO>> selectPage(@Valid UserInfoPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userInfoService.selectPage(query));
	}
	
}
