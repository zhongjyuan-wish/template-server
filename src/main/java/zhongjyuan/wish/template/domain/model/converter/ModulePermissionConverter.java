package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName ModulePermissionConverter
 * @Description 模块权限关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class ModulePermissionConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	public static ModulePermissionDO createVoToEntity(ModulePermissionCreateVO createVO) {
		ModulePermissionDO entity = new ModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	public static ModulePermissionDO updateVoToEntity(ModulePermissionUpdateVO updateVO) {
		ModulePermissionDO entity = new ModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionVO VO对象
	 * @Throws
	 */
	public static ModulePermissionVO entityToVo(ModulePermissionDO entity) {

		ModulePermissionVO vo = new ModulePermissionVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionBriefVO 简要对象
	 * @Throws
	 */
	public static ModulePermissionBriefVO entityToBriefVo(ModulePermissionDO entity) {

		ModulePermissionBriefVO briefVO = new ModulePermissionBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePermissionVO> VO对象集合
	 * @Throws
	 */
	public static List<ModulePermissionVO> entityToVos(List<ModulePermissionDO> entities) {

		List<ModulePermissionVO> vos = new ArrayList<ModulePermissionVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, ModulePermissionVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<ModulePermissionBriefVO> entityToBriefVos(List<ModulePermissionDO> entities) {

		List<ModulePermissionBriefVO> briefVOs = new ArrayList<ModulePermissionBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, ModulePermissionBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ModulePermissionPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<ModulePermissionPageVO> entityToPageVos(List<ModulePermissionDO> entities) {

		List<ModulePermissionPageVO> pageVOs = new ArrayList<ModulePermissionPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, ModulePermissionPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}