package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IOrganizeStationService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeStationDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName OrganizeStationController
 * @Description 组织岗位关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "组织岗位关联管理", tags = "组织岗位关联管理")
@RequestMapping("{version}/organizeStations")
public class OrganizeStationController {

	@Autowired
	IOrganizeStationService organizeStationService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "组织岗位关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeStationVO> create(@RequestBody @Valid OrganizeStationCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		OrganizeStationDO entity = OrganizeStationConverter.createVoToEntity(createVO);
		entity = organizeStationService.create(entity);
		return ResponseResult.success(OrganizeStationConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "组织岗位关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeStationVO> update(@RequestBody @Valid OrganizeStationUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		OrganizeStationDO entity = OrganizeStationConverter.updateVoToEntity(updateVO);
		entity = organizeStationService.update(entity);
		return ResponseResult.success(OrganizeStationConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "组织岗位关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeStationVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeStationDO> entities = organizeStationService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeStationConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "组织岗位关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeStationVO> delete(@PathVariable("id") Long id) {
		OrganizeStationDO entity = organizeStationService.delete(id);
		return ResponseResult.success(OrganizeStationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeStationVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeStationDO> entities = organizeStationService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeStationConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeStationVO> select(@PathVariable("id") Long id) {
		OrganizeStationDO entity = organizeStationService.select(id);
		return ResponseResult.success(OrganizeStationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeStationVO>> selectList(@Valid OrganizeStationQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeStationService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeStationBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<OrganizeStationDO> entities = organizeStationService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(OrganizeStationConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<OrganizeStationBriefVO> selectBrief(@PathVariable("id") Long id) {
		OrganizeStationDO entity = organizeStationService.select(id);
		return ResponseResult.success(OrganizeStationConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<OrganizeStationBriefVO>> selectBriefList(@Valid OrganizeStationBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeStationService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "组织岗位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页组织岗位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<OrganizeStationPageVO>> selectPage(@Valid OrganizeStationPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(organizeStationService.selectPage(query));
	}
	
}
