package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName OrganizeDO
 * @Description 组织表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_organize")
@ApiModel(value = "OrganizeDO", description = "组织表实体对象")
public class OrganizeDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields no: 编号
	 */
    @TableField("no")
	@ApiModelProperty(value = "编号", name = "no", dataType = "String")
    private String no;

	/**
	 * @Fields name: 名称
	 */
    @TableField("name")
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields aliasName: 别称
	 */
    @TableField("alias_name")
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields shortName: 简称
	 */
    @TableField("short_name")
	@ApiModelProperty(value = "简称", name = "shortName", dataType = "String")
    private String shortName;

	/**
	 * @Fields fullName: 全称
	 */
    @TableField("full_name")
	@ApiModelProperty(value = "全称", name = "fullName", dataType = "String")
    private String fullName;

	/**
	 * @Fields type: 类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]
	 */
    @TableField("type")
	@ApiModelProperty(value = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
    @TableField("icon")
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields parentId: 父级主键
	 */
    @TableField("parent_id")
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentNo: 父级编号
	 */
    @TableField("parent_no")
	@ApiModelProperty(value = "父级编号", name = "parentNo", dataType = "String")
    private String parentNo;

	/**
	 * @Fields parentName: 父级名称
	 */
    @TableField("parent_name")
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;

	/**
	 * @Fields directId: 直属主键
	 */
    @TableField("direct_id")
	@ApiModelProperty(value = "直属主键", name = "directId", dataType = "Long")
    private Long directId;

	/**
	 * @Fields directNo: 直属编号
	 */
    @TableField("direct_no")
	@ApiModelProperty(value = "直属编号", name = "directNo", dataType = "String")
    private String directNo;

	/**
	 * @Fields directName: 直属名称
	 */
    @TableField("direct_name")
	@ApiModelProperty(value = "直属名称", name = "directName", dataType = "String")
    private String directName;

	/**
	 * @Fields path: 全路径
	 */
    @TableField("path")
	@ApiModelProperty(value = "全路径", name = "path", dataType = "String")
    private String path;

	/**
	 * @Fields level: 层级
	 */
    @TableField("level")
	@ApiModelProperty(value = "层级", name = "level", dataType = "Integer")
    private Integer level;

	/**
	 * @Fields outline: 层级码
	 */
    @TableField("outline")
	@ApiModelProperty(value = "层级码", name = "outline", dataType = "String")
    private String outline;

	/**
	 * @Fields isLeaf: 是否叶子
	 */
    @TableField("is_leaf")
	@ApiModelProperty(value = "是否叶子", name = "isLeaf", dataType = "Boolean")
    private Boolean isLeaf;

	/**
	 * @Fields isSystem: 是否系统
	 */
    @TableField("is_system")
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
    @TableField("is_enabled")
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;

	/**
	 * @Fields isDeleted: 是否删除
	 */
    @TableField("is_deleted")
    @TableLogic
	@ApiModelProperty(value = "是否删除", name = "isDeleted", dataType = "Boolean")
    private Boolean isDeleted;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
    @TableField("creator_id")
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
    @TableField("creator_name")
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
    @TableField("updator_id")
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
    @TableField("updator_name")
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
    @TableField("update_time")
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;


    public static final String ID = "id";

    public static final String NO = "no";

    public static final String NAME = "name";

    public static final String ALIAS_NAME = "alias_name";

    public static final String SHORT_NAME = "short_name";

    public static final String FULL_NAME = "full_name";

    public static final String TYPE = "type";

    public static final String ICON = "icon";

    public static final String PARENT_ID = "parent_id";

    public static final String PARENT_NO = "parent_no";

    public static final String PARENT_NAME = "parent_name";

    public static final String DIRECT_ID = "direct_id";

    public static final String DIRECT_NO = "direct_no";

    public static final String DIRECT_NAME = "direct_name";

    public static final String PATH = "path";

    public static final String LEVEL = "level";

    public static final String OUTLINE = "outline";

    public static final String IS_LEAF = "is_leaf";

    public static final String IS_SYSTEM = "is_system";

    public static final String IS_ENABLED = "is_enabled";

    public static final String IS_DELETED = "is_deleted";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATOR_ID = "creator_id";

    public static final String CREATOR_NAME = "creator_name";

    public static final String CREATE_TIME = "create_time";

    public static final String UPDATOR_ID = "updator_id";

    public static final String UPDATOR_NAME = "updator_name";

    public static final String UPDATE_TIME = "update_time";

}
