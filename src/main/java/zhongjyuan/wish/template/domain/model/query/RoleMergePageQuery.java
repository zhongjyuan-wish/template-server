package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.PageQuery;

/**
 * @ClassName RoleMergePageQuery
 * @Description 角色合并关联分页查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleMergePageQuery", description = "角色合并关联分页查询条件对象")
public class RoleMergePageQuery extends PageQuery {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields roleId: 角色主键
	 */
	@ApiModelProperty(value = "角色主键", name = "roleId", dataType = "Long")
    private Long roleId;

	/**
	 * @Fields roleCode: 角色编码
	 */
	@ApiModelProperty(value = "角色编码", name = "roleCode", dataType = "String")
    private String roleCode;

	/**
	 * @Fields roleName: 角色名称
	 */
	@ApiModelProperty(value = "角色名称", name = "roleName", dataType = "String")
    private String roleName;

	/**
	 * @Fields mergeRoleId: 合并角色主键
	 */
	@ApiModelProperty(value = "合并角色主键", name = "mergeRoleId", dataType = "Long")
    private Long mergeRoleId;

	/**
	 * @Fields mergeRoleCode: 合并角色编码
	 */
	@ApiModelProperty(value = "合并角色编码", name = "mergeRoleCode", dataType = "String")
    private String mergeRoleCode;

	/**
	 * @Fields mergeRoleName: 合并角色名称
	 */
	@ApiModelProperty(value = "合并角色名称", name = "mergeRoleName", dataType = "String")
    private String mergeRoleName;

	/**
	 * @Fields status: 状态
	 */
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;



}