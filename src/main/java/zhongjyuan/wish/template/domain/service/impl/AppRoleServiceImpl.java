package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.AppRoleMapper;
import zhongjyuan.wish.template.domain.service.IAppRoleService;

import zhongjyuan.wish.template.domain.model.entity.AppRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.AppRoleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName AppRoleServiceImpl
 * @Description 应用角色关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class AppRoleServiceImpl implements IAppRoleService {

	@Autowired
	AppRoleMapper appRoleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppRoleDO create(AppRoleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = appRoleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		appRoleMapper.insert(entity);

		log.info("======AppRoleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(AppRoleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppRoleDO update(AppRoleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		appRoleMapper.updateById(entity);

		log.info("======AppRoleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppRoleDO 实体对象
	 * @Throws
	 */
	private AppRoleDO updateEnabled(AppRoleDO entity) {

		// 获取原始数据
		AppRoleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(AppRoleCode.APPROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public AppRoleDO delete(Long id) {

		// 获取原始数据
		AppRoleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		appRoleMapper.updateById(originEntity);

		log.info("======AppRoleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<AppRoleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<AppRoleDO> entities = new ArrayList<AppRoleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<AppRoleDO> queryWrapper = new QueryWrapper<AppRoleDO>();
		queryWrapper.lambda().in(AppRoleDO::getId, ids);
		entities = appRoleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<AppRoleDO> successEntities = new ArrayList<AppRoleDO>();
		
		// 根据主键更新
		for (AppRoleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			appRoleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======AppRoleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(AppRoleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(AppRoleCode.APPROLE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public AppRoleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return appRoleMapper.selectById(id);
	}
	
	@Override
	public List<AppRoleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return appRoleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<AppRoleVO> selectList(AppRoleQuery query) {
		List<AppRoleVO> vos = new ArrayList<AppRoleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppRoleDO> queryWrapper = new QueryWrapper<AppRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<AppRoleDO> entities = appRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = AppRoleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<AppRoleBriefVO> selectBriefList(AppRoleBriefQuery query) {
		List<AppRoleBriefVO> briefVOs = new ArrayList<AppRoleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppRoleDO> queryWrapper = new QueryWrapper<AppRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppRoleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<AppRoleDO> entities = appRoleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = AppRoleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<AppRolePageVO> selectPage(AppRolePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<AppRoleDO> queryWrapper = new QueryWrapper<AppRoleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(AppRoleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<AppRoleDO> page = appRoleMapper.selectPage(new CustomPage<AppRoleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> AppRoleConverter.entityToPageVos(entities));
	}

}
