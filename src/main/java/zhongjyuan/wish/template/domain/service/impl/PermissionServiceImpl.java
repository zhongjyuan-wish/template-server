package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.PermissionMapper;
import zhongjyuan.wish.template.domain.service.IPermissionService;

import zhongjyuan.wish.template.domain.model.entity.PermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.PermissionCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName PermissionServiceImpl
 * @Description 权限表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class PermissionServiceImpl implements IPermissionService {

	@Autowired
	PermissionMapper permissionMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PermissionDO create(PermissionDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = permissionMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		permissionMapper.insert(entity);

		log.info("======PermissionServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(PermissionDO entity) {

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PermissionDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(PermissionDO::getGroup, entity.getGroup());
		}

		if (permissionMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(PermissionCode.PERMISSION_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(PermissionCode.PERMISSION_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PermissionDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(PermissionDO::getGroup, entity.getGroup());
		}

		if (permissionMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(PermissionCode.PERMISSION_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(PermissionCode.PERMISSION_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PermissionDO update(PermissionDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		permissionMapper.updateById(entity);

		log.info("======PermissionServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return PermissionDO 实体对象
	 * @Throws
	 */
	private PermissionDO updateEnabled(PermissionDO entity) {

		// 获取原始数据
		PermissionDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(PermissionCode.PERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(PermissionCode.PERMISSION_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(PermissionCode.PERMISSION_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在相同有效编码数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PermissionDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(PermissionDO::getGroup, entity.getGroup());
		}

		if (permissionMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(PermissionCode.PERMISSION_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(PermissionCode.PERMISSION_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PermissionDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(PermissionDO::getGroup, entity.getGroup());
		}

		if (permissionMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(PermissionCode.PERMISSION_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			throw new BusinessException(PermissionCode.PERMISSION_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PermissionDO delete(Long id) {

		// 获取原始数据
		PermissionDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		permissionMapper.updateById(originEntity);

		log.info("======PermissionServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public PermissionDO deleteByCode(String code) {

		// 获取原始数据
		PermissionDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		permissionMapper.updateById(originEntity);

		log.info("======PermissionServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<PermissionDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<PermissionDO> entities = new ArrayList<PermissionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsSystem, false);
		queryWrapper.lambda().in(PermissionDO::getId, ids);
		entities = permissionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<PermissionDO> successEntities = new ArrayList<PermissionDO>();
		
		// 根据主键更新
		for (PermissionDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			permissionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======PermissionServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<PermissionDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<PermissionDO> entities = new ArrayList<PermissionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsSystem, false);
		queryWrapper.lambda().in(PermissionDO::getCode, codes);
		entities = permissionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<PermissionDO> successEntities = new ArrayList<PermissionDO>();
		
		// 根据主键更新
		for (PermissionDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			permissionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======PermissionServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(PermissionDO entity) {
	
		if (entity == null) {
			throw new BusinessException(PermissionCode.PERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(PermissionCode.PERMISSION_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(PermissionCode.PERMISSION_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public PermissionDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return permissionMapper.selectById(id);
	}
	
	@Override
	public PermissionDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);
		queryWrapper.lambda().eq(PermissionDO::getCode, code);
		
		return permissionMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<PermissionDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return permissionMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<PermissionDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);
		queryWrapper.lambda().in(PermissionDO::getCode, codes);
		
		return permissionMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<PermissionVO> selectList(PermissionQuery query) {
		List<PermissionVO> vos = new ArrayList<PermissionVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PermissionDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PermissionDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PermissionDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PermissionDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<PermissionDO> entities = permissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = PermissionConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<PermissionBriefVO> selectBriefList(PermissionBriefQuery query) {
		List<PermissionBriefVO> briefVOs = new ArrayList<PermissionBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PermissionDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PermissionDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PermissionDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PermissionDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<PermissionDO> entities = permissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = PermissionConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<PermissionPageVO> selectPage(PermissionPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<PermissionDO> queryWrapper = new QueryWrapper<PermissionDO>();
		queryWrapper.lambda().eq(PermissionDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(PermissionDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(PermissionDO::getName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(PermissionDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(PermissionDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<PermissionDO> page = permissionMapper.selectPage(new CustomPage<PermissionDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> PermissionConverter.entityToPageVos(entities));
	}

}
