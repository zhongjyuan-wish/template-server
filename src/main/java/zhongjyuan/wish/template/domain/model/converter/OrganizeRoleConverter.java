package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.OrganizeRoleDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName OrganizeRoleConverter
 * @Description 组织角色关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class OrganizeRoleConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	public static OrganizeRoleDO createVoToEntity(OrganizeRoleCreateVO createVO) {
		OrganizeRoleDO entity = new OrganizeRoleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return OrganizeRoleDO 实体对象
	 * @Throws
	 */
	public static OrganizeRoleDO updateVoToEntity(OrganizeRoleUpdateVO updateVO) {
		OrganizeRoleDO entity = new OrganizeRoleDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeRoleVO VO对象
	 * @Throws
	 */
	public static OrganizeRoleVO entityToVo(OrganizeRoleDO entity) {

		OrganizeRoleVO vo = new OrganizeRoleVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeRoleBriefVO 简要对象
	 * @Throws
	 */
	public static OrganizeRoleBriefVO entityToBriefVo(OrganizeRoleDO entity) {

		OrganizeRoleBriefVO briefVO = new OrganizeRoleBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizeRoleVO> VO对象集合
	 * @Throws
	 */
	public static List<OrganizeRoleVO> entityToVos(List<OrganizeRoleDO> entities) {

		List<OrganizeRoleVO> vos = new ArrayList<OrganizeRoleVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, OrganizeRoleVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizeRoleBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<OrganizeRoleBriefVO> entityToBriefVos(List<OrganizeRoleDO> entities) {

		List<OrganizeRoleBriefVO> briefVOs = new ArrayList<OrganizeRoleBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, OrganizeRoleBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<OrganizeRolePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<OrganizeRolePageVO> entityToPageVos(List<OrganizeRoleDO> entities) {

		List<OrganizeRolePageVO> pageVOs = new ArrayList<OrganizeRolePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, OrganizeRolePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}