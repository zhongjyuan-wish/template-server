package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.RoleModulePermissionMapper;
import zhongjyuan.wish.template.domain.service.IRoleModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.RoleModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.RoleModulePermissionCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName RoleModulePermissionServiceImpl
 * @Description 角色模块权限关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class RoleModulePermissionServiceImpl implements IRoleModulePermissionService {

	@Autowired
	RoleModulePermissionMapper roleModulePermissionMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleModulePermissionDO create(RoleModulePermissionDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = roleModulePermissionMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		roleModulePermissionMapper.insert(entity);

		log.info("======RoleModulePermissionServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(RoleModulePermissionDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleModulePermissionDO update(RoleModulePermissionDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		roleModulePermissionMapper.updateById(entity);

		log.info("======RoleModulePermissionServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleModulePermissionDO 实体对象
	 * @Throws
	 */
	private RoleModulePermissionDO updateEnabled(RoleModulePermissionDO entity) {

		// 获取原始数据
		RoleModulePermissionDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(RoleModulePermissionCode.ROLEMODULEPERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public RoleModulePermissionDO delete(Long id) {

		// 获取原始数据
		RoleModulePermissionDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		roleModulePermissionMapper.updateById(originEntity);

		log.info("======RoleModulePermissionServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<RoleModulePermissionDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<RoleModulePermissionDO> entities = new ArrayList<RoleModulePermissionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<RoleModulePermissionDO> queryWrapper = new QueryWrapper<RoleModulePermissionDO>();
		queryWrapper.lambda().in(RoleModulePermissionDO::getId, ids);
		entities = roleModulePermissionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<RoleModulePermissionDO> successEntities = new ArrayList<RoleModulePermissionDO>();
		
		// 根据主键更新
		for (RoleModulePermissionDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			roleModulePermissionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======RoleModulePermissionServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(RoleModulePermissionDO entity) {
	
		if (entity == null) {
			throw new BusinessException(RoleModulePermissionCode.ROLEMODULEPERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public RoleModulePermissionDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return roleModulePermissionMapper.selectById(id);
	}
	
	@Override
	public List<RoleModulePermissionDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return roleModulePermissionMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<RoleModulePermissionVO> selectList(RoleModulePermissionQuery query) {
		List<RoleModulePermissionVO> vos = new ArrayList<RoleModulePermissionVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleModulePermissionDO> queryWrapper = new QueryWrapper<RoleModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleModulePermissionDO> entities = roleModulePermissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = RoleModulePermissionConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<RoleModulePermissionBriefVO> selectBriefList(RoleModulePermissionBriefQuery query) {
		List<RoleModulePermissionBriefVO> briefVOs = new ArrayList<RoleModulePermissionBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleModulePermissionDO> queryWrapper = new QueryWrapper<RoleModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<RoleModulePermissionDO> entities = roleModulePermissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = RoleModulePermissionConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<RoleModulePermissionPageVO> selectPage(RoleModulePermissionPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<RoleModulePermissionDO> queryWrapper = new QueryWrapper<RoleModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(RoleModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<RoleModulePermissionDO> page = roleModulePermissionMapper.selectPage(new CustomPage<RoleModulePermissionDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> RoleModulePermissionConverter.entityToPageVos(entities));
	}

}
