package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.ModulePermissionMapper;
import zhongjyuan.wish.template.domain.service.IModulePermissionService;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.ModulePermissionCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName ModulePermissionServiceImpl
 * @Description 模块权限关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class ModulePermissionServiceImpl implements IModulePermissionService {

	@Autowired
	ModulePermissionMapper modulePermissionMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModulePermissionDO create(ModulePermissionDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = modulePermissionMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		modulePermissionMapper.insert(entity);

		log.info("======ModulePermissionServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(ModulePermissionDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModulePermissionDO update(ModulePermissionDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		modulePermissionMapper.updateById(entity);

		log.info("======ModulePermissionServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionDO 实体对象
	 * @Throws
	 */
	private ModulePermissionDO updateEnabled(ModulePermissionDO entity) {

		// 获取原始数据
		ModulePermissionDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(ModulePermissionCode.MODULEPERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModulePermissionDO delete(Long id) {

		// 获取原始数据
		ModulePermissionDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		modulePermissionMapper.updateById(originEntity);

		log.info("======ModulePermissionServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<ModulePermissionDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<ModulePermissionDO> entities = new ArrayList<ModulePermissionDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<ModulePermissionDO> queryWrapper = new QueryWrapper<ModulePermissionDO>();
		queryWrapper.lambda().in(ModulePermissionDO::getId, ids);
		entities = modulePermissionMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<ModulePermissionDO> successEntities = new ArrayList<ModulePermissionDO>();
		
		// 根据主键更新
		for (ModulePermissionDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			modulePermissionMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======ModulePermissionServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(ModulePermissionDO entity) {
	
		if (entity == null) {
			throw new BusinessException(ModulePermissionCode.MODULEPERMISSION_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public ModulePermissionDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return modulePermissionMapper.selectById(id);
	}
	
	@Override
	public List<ModulePermissionDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return modulePermissionMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<ModulePermissionVO> selectList(ModulePermissionQuery query) {
		List<ModulePermissionVO> vos = new ArrayList<ModulePermissionVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModulePermissionDO> queryWrapper = new QueryWrapper<ModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ModulePermissionDO> entities = modulePermissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = ModulePermissionConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<ModulePermissionBriefVO> selectBriefList(ModulePermissionBriefQuery query) {
		List<ModulePermissionBriefVO> briefVOs = new ArrayList<ModulePermissionBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModulePermissionDO> queryWrapper = new QueryWrapper<ModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ModulePermissionDO> entities = modulePermissionMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = ModulePermissionConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<ModulePermissionPageVO> selectPage(ModulePermissionPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModulePermissionDO> queryWrapper = new QueryWrapper<ModulePermissionDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModulePermissionDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<ModulePermissionDO> page = modulePermissionMapper.selectPage(new CustomPage<ModulePermissionDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> ModulePermissionConverter.entityToPageVos(entities));
	}

}
