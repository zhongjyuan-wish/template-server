package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.OrganizeStationMapper;
import zhongjyuan.wish.template.domain.service.IOrganizeStationService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeStationDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.OrganizeStationCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName OrganizeStationServiceImpl
 * @Description 组织岗位关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class OrganizeStationServiceImpl implements IOrganizeStationService {

	@Autowired
	OrganizeStationMapper organizeStationMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeStationDO create(OrganizeStationDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = organizeStationMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		organizeStationMapper.insert(entity);

		log.info("======OrganizeStationServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(OrganizeStationDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeStationDO update(OrganizeStationDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		organizeStationMapper.updateById(entity);

		log.info("======OrganizeStationServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeStationDO 实体对象
	 * @Throws
	 */
	private OrganizeStationDO updateEnabled(OrganizeStationDO entity) {

		// 获取原始数据
		OrganizeStationDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(OrganizeStationCode.ORGANIZESTATION_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeStationDO delete(Long id) {

		// 获取原始数据
		OrganizeStationDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		organizeStationMapper.updateById(originEntity);

		log.info("======OrganizeStationServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<OrganizeStationDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<OrganizeStationDO> entities = new ArrayList<OrganizeStationDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<OrganizeStationDO> queryWrapper = new QueryWrapper<OrganizeStationDO>();
		queryWrapper.lambda().in(OrganizeStationDO::getId, ids);
		entities = organizeStationMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<OrganizeStationDO> successEntities = new ArrayList<OrganizeStationDO>();
		
		// 根据主键更新
		for (OrganizeStationDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			organizeStationMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======OrganizeStationServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(OrganizeStationDO entity) {
	
		if (entity == null) {
			throw new BusinessException(OrganizeStationCode.ORGANIZESTATION_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public OrganizeStationDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return organizeStationMapper.selectById(id);
	}
	
	@Override
	public List<OrganizeStationDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return organizeStationMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<OrganizeStationVO> selectList(OrganizeStationQuery query) {
		List<OrganizeStationVO> vos = new ArrayList<OrganizeStationVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeStationDO> queryWrapper = new QueryWrapper<OrganizeStationDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeStationDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeStationDO> entities = organizeStationMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = OrganizeStationConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<OrganizeStationBriefVO> selectBriefList(OrganizeStationBriefQuery query) {
		List<OrganizeStationBriefVO> briefVOs = new ArrayList<OrganizeStationBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeStationDO> queryWrapper = new QueryWrapper<OrganizeStationDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeStationDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeStationDO> entities = organizeStationMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = OrganizeStationConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<OrganizeStationPageVO> selectPage(OrganizeStationPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeStationDO> queryWrapper = new QueryWrapper<OrganizeStationDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeStationDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<OrganizeStationDO> page = organizeStationMapper.selectPage(new CustomPage<OrganizeStationDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> OrganizeStationConverter.entityToPageVos(entities));
	}

}
