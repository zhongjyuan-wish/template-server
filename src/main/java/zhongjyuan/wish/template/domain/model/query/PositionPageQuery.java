package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.PageQuery;

/**
 * @ClassName PositionPageQuery
 * @Description 职业分页查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PositionPageQuery", description = "职业分页查询条件对象")
public class PositionPageQuery extends PageQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields rankId: 分级主键
	 */
	@ApiModelProperty(value = "分级主键", name = "rankId", dataType = "Long")
    private Long rankId;

	/**
	 * @Fields rankCode: 分级编码
	 */
	@ApiModelProperty(value = "分级编码", name = "rankCode", dataType = "String")
    private String rankCode;

	/**
	 * @Fields rankName: 分级名称
	 */
	@ApiModelProperty(value = "分级名称", name = "rankName", dataType = "String")
    private String rankName;

	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}