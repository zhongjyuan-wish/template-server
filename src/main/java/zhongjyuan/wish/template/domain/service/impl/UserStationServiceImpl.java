package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserStationMapper;
import zhongjyuan.wish.template.domain.service.IUserStationService;

import zhongjyuan.wish.template.domain.model.entity.UserStationDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserStationCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserStationServiceImpl
 * @Description 用户岗位关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserStationServiceImpl implements IUserStationService {

	@Autowired
	UserStationMapper userStationMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserStationDO create(UserStationDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userStationMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userStationMapper.insert(entity);

		log.info("======UserStationServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserStationDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserStationDO update(UserStationDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userStationMapper.updateById(entity);

		log.info("======UserStationServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserStationDO 实体对象
	 * @Throws
	 */
	private UserStationDO updateEnabled(UserStationDO entity) {

		// 获取原始数据
		UserStationDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserStationCode.USERSTATION_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserStationDO delete(Long id) {

		// 获取原始数据
		UserStationDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userStationMapper.updateById(originEntity);

		log.info("======UserStationServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserStationDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserStationDO> entities = new ArrayList<UserStationDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserStationDO> queryWrapper = new QueryWrapper<UserStationDO>();
		queryWrapper.lambda().in(UserStationDO::getId, ids);
		entities = userStationMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserStationDO> successEntities = new ArrayList<UserStationDO>();
		
		// 根据主键更新
		for (UserStationDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userStationMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserStationServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserStationDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserStationCode.USERSTATION_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserStationDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userStationMapper.selectById(id);
	}
	
	@Override
	public List<UserStationDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userStationMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserStationVO> selectList(UserStationQuery query) {
		List<UserStationVO> vos = new ArrayList<UserStationVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserStationDO> queryWrapper = new QueryWrapper<UserStationDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserStationDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserStationDO> entities = userStationMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserStationConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserStationBriefVO> selectBriefList(UserStationBriefQuery query) {
		List<UserStationBriefVO> briefVOs = new ArrayList<UserStationBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserStationDO> queryWrapper = new QueryWrapper<UserStationDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserStationDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserStationDO> entities = userStationMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserStationConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserStationPageVO> selectPage(UserStationPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserStationDO> queryWrapper = new QueryWrapper<UserStationDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserStationDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserStationDO> page = userStationMapper.selectPage(new CustomPage<UserStationDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserStationConverter.entityToPageVos(entities));
	}

}
