package ${package.Parent}.${package_ResponseVO};

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import ${import_Model_AbstractModel};

/**
 * @ClassName ${entityNaming}VO
 * @Description ${table.comment?replace("表","")}对象
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${entityNaming}VO", description = "${table.comment?replace("表","")}对象")
public class ${entityNaming}VO extends AbstractModel {

	private static final long serialVersionUID = 1L;
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>

    <#-- 主键 -->
    <#if field.keyFlag>
    <#-- 普通字段 -->
    <#elseif field.fill??>
    <#elseif field.convert>
    </#if>
    <#if !vo_ignorePropertyName?seq_contains(field.propertyName)>
    <#if field.comment!?length gt 0>
	/**
	 * @Fields ${field.propertyName}: ${field.comment}
	 */
        <#if swagger>
	@ApiModelProperty(value = "${field.comment}", name = "${field.propertyName}", dataType = "${field.propertyType}")
        </#if>
    </#if>
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->
}