package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.UserModulePermissionDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName UserModulePermissionConverter
 * @Description 用户模块权限关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class UserModulePermissionConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return UserModulePermissionDO 实体对象
	 * @Throws
	 */
	public static UserModulePermissionDO createVoToEntity(UserModulePermissionCreateVO createVO) {
		UserModulePermissionDO entity = new UserModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return UserModulePermissionDO 实体对象
	 * @Throws
	 */
	public static UserModulePermissionDO updateVoToEntity(UserModulePermissionUpdateVO updateVO) {
		UserModulePermissionDO entity = new UserModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserModulePermissionVO VO对象
	 * @Throws
	 */
	public static UserModulePermissionVO entityToVo(UserModulePermissionDO entity) {

		UserModulePermissionVO vo = new UserModulePermissionVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserModulePermissionBriefVO 简要对象
	 * @Throws
	 */
	public static UserModulePermissionBriefVO entityToBriefVo(UserModulePermissionDO entity) {

		UserModulePermissionBriefVO briefVO = new UserModulePermissionBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserModulePermissionVO> VO对象集合
	 * @Throws
	 */
	public static List<UserModulePermissionVO> entityToVos(List<UserModulePermissionDO> entities) {

		List<UserModulePermissionVO> vos = new ArrayList<UserModulePermissionVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, UserModulePermissionVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<UserModulePermissionBriefVO> entityToBriefVos(List<UserModulePermissionDO> entities) {

		List<UserModulePermissionBriefVO> briefVOs = new ArrayList<UserModulePermissionBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, UserModulePermissionBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<UserModulePermissionPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<UserModulePermissionPageVO> entityToPageVos(List<UserModulePermissionDO> entities) {

		List<UserModulePermissionPageVO> pageVOs = new ArrayList<UserModulePermissionPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, UserModulePermissionPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}