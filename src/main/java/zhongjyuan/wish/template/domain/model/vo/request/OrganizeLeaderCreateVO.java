package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName OrganizeLeaderCreateVO
 * @Description 组织责任人关联创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizeLeaderCreateVO", description = "组织责任人关联创建对象")
public class OrganizeLeaderCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields organizeId: 组织主键
	 */
	// @NotNull(message = "组织主键不能为空.")
	// @NotBlank(message = "组织主键不能为空.")
	// @Size(message = "组织主键最大长度为50.", max = 50)
	// @Min(message = "组织主键最小为600.", value = 600)
	@ApiModelProperty(value = "组织主键", name = "organizeId", dataType = "Long")
    private Long organizeId;

	/**
	 * @Fields organizeNo: 组织编号
	 */
	// @NotNull(message = "组织编号不能为空.")
	// @NotBlank(message = "组织编号不能为空.")
	// @Size(message = "组织编号最大长度为50.", max = 50)
	// @Min(message = "组织编号最小为600.", value = 600)
	@ApiModelProperty(value = "组织编号", name = "organizeNo", dataType = "String")
    private String organizeNo;

	/**
	 * @Fields organizeName: 组织名称
	 */
	// @NotNull(message = "组织名称不能为空.")
	// @NotBlank(message = "组织名称不能为空.")
	// @Size(message = "组织名称最大长度为50.", max = 50)
	// @Min(message = "组织名称最小为600.", value = 600)
	@ApiModelProperty(value = "组织名称", name = "organizeName", dataType = "String")
    private String organizeName;

	/**
	 * @Fields type: 类型[Leader:责任人;Header:分管领导;Functional:职能上级]
	 */
	// @NotNull(message = "类型[Leader:责任人;Header:分管领导;Functional:职能上级]不能为空.")
	// @NotBlank(message = "类型[Leader:责任人;Header:分管领导;Functional:职能上级]不能为空.")
	// @Size(message = "类型[Leader:责任人;Header:分管领导;Functional:职能上级]最大长度为50.", max = 50)
	// @Min(message = "类型[Leader:责任人;Header:分管领导;Functional:职能上级]最小为600.", value = 600)
	@ApiModelProperty(value = "类型[Leader:责任人;Header:分管领导;Functional:职能上级]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields userId: 用户主键
	 */
	// @NotNull(message = "用户主键不能为空.")
	// @NotBlank(message = "用户主键不能为空.")
	// @Size(message = "用户主键最大长度为50.", max = 50)
	// @Min(message = "用户主键最小为600.", value = 600)
	@ApiModelProperty(value = "用户主键", name = "userId", dataType = "Long")
    private Long userId;

	/**
	 * @Fields userName: 用户名称
	 */
	// @NotNull(message = "用户名称不能为空.")
	// @NotBlank(message = "用户名称不能为空.")
	// @Size(message = "用户名称最大长度为50.", max = 50)
	// @Min(message = "用户名称最小为600.", value = 600)
	@ApiModelProperty(value = "用户名称", name = "userName", dataType = "String")
    private String userName;

	/**
	 * @Fields userAccount: 用户账号
	 */
	// @NotNull(message = "用户账号不能为空.")
	// @NotBlank(message = "用户账号不能为空.")
	// @Size(message = "用户账号最大长度为50.", max = 50)
	// @Min(message = "用户账号最小为600.", value = 600)
	@ApiModelProperty(value = "用户账号", name = "userAccount", dataType = "String")
    private String userAccount;

	/**
	 * @Fields status: 状态
	 */
	// @NotNull(message = "状态不能为空.")
	// @NotBlank(message = "状态不能为空.")
	// @Size(message = "状态最大长度为50.", max = 50)
	// @Min(message = "状态最小为600.", value = 600)
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

}