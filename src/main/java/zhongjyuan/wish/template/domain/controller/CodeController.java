package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.ICodeService;

import zhongjyuan.wish.template.domain.model.entity.CodeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName CodeController
 * @Description 码管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "码管理", tags = "码管理")
@RequestMapping("{version}/codes")
public class CodeController {

	@Autowired
	ICodeService codeService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> create(@RequestBody @Valid CodeCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		CodeDO entity = CodeConverter.createVoToEntity(createVO);
		entity = codeService.create(entity);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> update(@RequestBody @Valid CodeUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		CodeDO entity = CodeConverter.updateVoToEntity(updateVO);
		entity = codeService.update(entity);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<CodeDO> entities = codeService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(CodeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<CodeDO> entities = codeService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(CodeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> delete(@PathVariable("id") Long id) {
		CodeDO entity = codeService.delete(id);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> deleteByCode(@PathVariable("code") String code) {
		CodeDO entity = codeService.deleteByCode(code);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> enable(@PathVariable("id") Long id) {
		CodeDO entity = codeService.select(id);
		entity.setIsEnabled(true);
		entity = codeService.update(entity);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> enableByCode(@PathVariable("code") String code) {
		CodeDO entity = codeService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = codeService.update(entity);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> disable(@PathVariable("id") Long id) {
		CodeDO entity = codeService.select(id);
		entity.setIsEnabled(false);
		entity = codeService.update(entity);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> disableByCode(@PathVariable("code") String code) {
		CodeDO entity = codeService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = codeService.update(entity);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<CodeDO> entities = codeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(CodeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<CodeDO> entities = codeService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(CodeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> select(@PathVariable("id") Long id) {
		CodeDO entity = codeService.select(id);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeVO> selectByCode(@PathVariable("code") String code) {
		CodeDO entity = codeService.selectByCode(code);
		return ResponseResult.success(CodeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeVO>> selectList(@Valid CodeQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(codeService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<CodeDO> entities = codeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(CodeConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeBriefVO> selectBrief(@PathVariable("id") Long id) {
		CodeDO entity = codeService.select(id);
		return ResponseResult.success(CodeConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<CodeBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		CodeDO entity = codeService.selectByCode(code);
		return ResponseResult.success(CodeConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<CodeBriefVO>> selectBriefList(@Valid CodeBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(codeService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页码 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<CodePageVO>> selectPage(@Valid CodePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(codeService.selectPage(query));
	}
	
}
