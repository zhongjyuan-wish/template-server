package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName AppModuleDO
 * @Description 应用模块关联表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_app_module")
@ApiModel(value = "AppModuleDO", description = "应用模块关联表实体对象")
public class AppModuleDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields appId: 应用主键
	 */
    @TableField("app_id")
	@ApiModelProperty(value = "应用主键", name = "appId", dataType = "Long")
    private Long appId;

	/**
	 * @Fields appCode: 应用编码
	 */
    @TableField("app_code")
	@ApiModelProperty(value = "应用编码", name = "appCode", dataType = "String")
    private String appCode;

	/**
	 * @Fields appName: 应用名称
	 */
    @TableField("app_name")
	@ApiModelProperty(value = "应用名称", name = "appName", dataType = "String")
    private String appName;

	/**
	 * @Fields moduleId: 模块主键
	 */
    @TableField("module_id")
	@ApiModelProperty(value = "模块主键", name = "moduleId", dataType = "Long")
    private Long moduleId;

	/**
	 * @Fields moduleCode: 模块编码
	 */
    @TableField("module_code")
	@ApiModelProperty(value = "模块编码", name = "moduleCode", dataType = "String")
    private String moduleCode;

	/**
	 * @Fields moduleName: 模块名称
	 */
    @TableField("module_name")
	@ApiModelProperty(value = "模块名称", name = "moduleName", dataType = "String")
    private String moduleName;

	/**
	 * @Fields status: 状态
	 */
    @TableField("status")
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;


    public static final String ID = "id";

    public static final String APP_ID = "app_id";

    public static final String APP_CODE = "app_code";

    public static final String APP_NAME = "app_name";

    public static final String MODULE_ID = "module_id";

    public static final String MODULE_CODE = "module_code";

    public static final String MODULE_NAME = "module_name";

    public static final String STATUS = "status";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATE_TIME = "create_time";

}
