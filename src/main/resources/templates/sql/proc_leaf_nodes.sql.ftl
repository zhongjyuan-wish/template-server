CREATE PROCEDURE `proc_${table.name}_leaf_nodes`(IN type INT,
	IN hitNodeIds LONGTEXT)
    COMMENT '根据指定节点-获取节点下所有叶子节点'
BEGIN

		<#noparse># 计算节点下是否有节点，count = 0 表示为叶子节点</#noparse>
    DECLARE count INT;
		<#noparse># 返回叶子节点结果集</#noparse>
    DECLARE leafChildIdList LONGTEXT;
		<#noparse># 存放叶子节点</#noparse>
    DECLARE leafChild LONGTEXT;
		<#noparse># 临时存放子节点</#noparse>
    DECLARE tempLeafChildId LONGTEXT;
		<#noparse># 临时存放可能的叶子节点</#noparse>	
    DECLARE tempLeafChildIdList LONGTEXT;
		<#noparse># 临时存放节点</#noparse>
    DECLARE tempIdList LONGTEXT;
    
    SET leafChildIdList = '';
    SET tempLeafChildId = hitNodeIds;

		<#noparse># 外层循环，用于查询节点下所有的子节点</#noparse>
    WHILE tempLeafChildId IS NOT NULL DO
					 
			<#noparse># 临时存放节点，用于内层循环判定是否为叶子节点，避免影响外层循环使用 tempLeafChildId</#noparse>
			SET tempLeafChildIdList = tempLeafChildId;
				
			<#noparse># 内层循环，用于判断外层查询到的子节点是否为叶子节点</#noparse>
			WHILE LENGTH(tempLeafChildIdList) > 0 DO
				
				<#noparse># 假定逗号分隔的第一个为叶子节点</#noparse>
				SET leafChild = SUBSTRING_INDEX(tempLeafChildIdList, ',', 1);
				
				<#noparse># 查询该节点下是否有子节点</#noparse>
				select count(id) INTO count FROM ${table.name} WHERE is_deleted = 0 AND parent_id = leafChild;
				 
				<#noparse># 如果该节点下没有子节点，则认为是叶子节点，存入到返回结果中</#noparse>
				IF count = 0 THEN
					SET leafChildIdList = CONCAT_WS(',', leafChildIdList, leafChild);
				END IF;
				
				<#noparse># 将第一个节点截取掉，继续识别剩余的节点</#noparse>
				SET tempLeafChildIdList = SUBSTRING(tempLeafChildIdList, LENGTH(leafChild) + 2);
			END WHILE;

			<#noparse># 查询节点下所有子节点</#noparse>
			SELECT GROUP_CONCAT(id) INTO tempLeafChildId FROM ${table.name} WHERE is_deleted = 0 AND FIND_IN_SET(parent_id, tempLeafChildId) > 0;
				
    END WHILE;
		
		<#noparse># 返回结果[主键字符串]</#noparse>
		IF type IS NULL OR type = 0 THEN
			SELECT SUBSTRING(leafChildIdList, 2) AS childLeafIds;
		END IF;
		
		<#noparse># 返回结果[数据集]</#noparse>
		IF type IS NOT NULL AND type = 1 THEN
			SELECT * FROM ${table.name} menu
			WHERE menu.is_deleted = 0
			AND FIND_IN_SET(menu.id, SUBSTRING(leafChildIdList, 2))
			ORDER BY menu.outline;
		END IF;
	<#noparse>
	# create by zhongjyuan
	</#noparse>
END