package zhongjyuan.wish.template.domain.model.vo.response;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName UserInfoVO
 * @Description 用户信息对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserInfoVO", description = "用户信息对象")
public class UserInfoVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields name: 名称
	 */
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields account: 账号
	 */
	@ApiModelProperty(value = "账号", name = "account", dataType = "String")
    private String account;

	/**
	 * @Fields aliasName: 别称
	 */
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields face: 头像
	 */
	@ApiModelProperty(value = "头像", name = "face", dataType = "String")
    private String face;

	/**
	 * @Fields email: 电子邮箱
	 */
	@ApiModelProperty(value = "电子邮箱", name = "email", dataType = "String")
    private String email;

	/**
	 * @Fields mobile: 移动号码
	 */
	@ApiModelProperty(value = "移动号码", name = "mobile", dataType = "String")
    private String mobile;

	/**
	 * @Fields telephone: 固定号码
	 */
	@ApiModelProperty(value = "固定号码", name = "telephone", dataType = "String")
    private String telephone;

	/**
	 * @Fields qq: QQ号码
	 */
	@ApiModelProperty(value = "QQ号码", name = "qq", dataType = "String")
    private String qq;

	/**
	 * @Fields weibo: 新浪微博
	 */
	@ApiModelProperty(value = "新浪微博", name = "weibo", dataType = "String")
    private String weibo;

	/**
	 * @Fields wechat: 微信号码
	 */
	@ApiModelProperty(value = "微信号码", name = "wechat", dataType = "String")
    private String wechat;

	/**
	 * @Fields signature: 签名
	 */
	@ApiModelProperty(value = "签名", name = "signature", dataType = "String")
    private String signature;

	/**
	 * @Fields rowIndex: 行号
	 */
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields creatorId: 创建者主键
	 */
	@ApiModelProperty(value = "创建者主键", name = "creatorId", dataType = "Long")
    private Long creatorId;

	/**
	 * @Fields creatorName: 创建者名称
	 */
	@ApiModelProperty(value = "创建者名称", name = "creatorName", dataType = "String")
    private String creatorName;

	/**
	 * @Fields createTime: 创建时间
	 */
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;

	/**
	 * @Fields updatorId: 更新者主键
	 */
	@ApiModelProperty(value = "更新者主键", name = "updatorId", dataType = "Long")
    private Long updatorId;

	/**
	 * @Fields updatorName: 更新者名称
	 */
	@ApiModelProperty(value = "更新者名称", name = "updatorName", dataType = "String")
    private String updatorName;

	/**
	 * @Fields updateTime: 更新时间
	 */
	@ApiModelProperty(value = "更新时间", name = "updateTime", dataType = "LocalDateTime")
    private LocalDateTime updateTime;
}