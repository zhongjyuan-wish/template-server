package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName MenuCreateVO
 * @Description 菜单创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MenuCreateVO", description = "菜单创建对象")
public class MenuCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields code: 编码
	 */
	// @NotNull(message = "编码不能为空.")
	// @NotBlank(message = "编码不能为空.")
	// @Size(message = "编码最大长度为50.", max = 50)
	// @Min(message = "编码最小为600.", value = 600)
	@ApiModelProperty(value = "编码", name = "code", dataType = "String")
    private String code;

	/**
	 * @Fields name: 名称
	 */
	// @NotNull(message = "名称不能为空.")
	// @NotBlank(message = "名称不能为空.")
	// @Size(message = "名称最大长度为50.", max = 50)
	// @Min(message = "名称最小为600.", value = 600)
	@ApiModelProperty(value = "名称", name = "name", dataType = "String")
    private String name;

	/**
	 * @Fields nickName: 别称
	 */
	// @NotNull(message = "别称不能为空.")
	// @NotBlank(message = "别称不能为空.")
	// @Size(message = "别称最大长度为50.", max = 50)
	// @Min(message = "别称最小为600.", value = 600)
	@ApiModelProperty(value = "别称", name = "nickName", dataType = "String")
    private String nickName;

	/**
	 * @Fields shortName: 简称
	 */
	// @NotNull(message = "简称不能为空.")
	// @NotBlank(message = "简称不能为空.")
	// @Size(message = "简称最大长度为50.", max = 50)
	// @Min(message = "简称最小为600.", value = 600)
	@ApiModelProperty(value = "简称", name = "shortName", dataType = "String")
    private String shortName;

	/**
	 * @Fields icon: 图标
	 */
	// @NotNull(message = "图标不能为空.")
	// @NotBlank(message = "图标不能为空.")
	// @Size(message = "图标最大长度为50.", max = 50)
	// @Min(message = "图标最小为600.", value = 600)
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields link: 链接
	 */
	// @NotNull(message = "链接不能为空.")
	// @NotBlank(message = "链接不能为空.")
	// @Size(message = "链接最大长度为50.", max = 50)
	// @Min(message = "链接最小为600.", value = 600)
	@ApiModelProperty(value = "链接", name = "link", dataType = "String")
    private String link;

	/**
	 * @Fields type: 类型
	 */
	// @NotNull(message = "类型不能为空.")
	// @NotBlank(message = "类型不能为空.")
	// @Size(message = "类型最大长度为50.", max = 50)
	// @Min(message = "类型最小为600.", value = 600)
	@ApiModelProperty(value = "类型", name = "type", dataType = "Integer")
    private Integer type;

	/**
	 * @Fields childDisplay: 子级显示[1:树节点;2:选项卡;3:父级嵌套]
	 */
	// @NotNull(message = "子级显示[1:树节点;2:选项卡;3:父级嵌套]不能为空.")
	// @NotBlank(message = "子级显示[1:树节点;2:选项卡;3:父级嵌套]不能为空.")
	// @Size(message = "子级显示[1:树节点;2:选项卡;3:父级嵌套]最大长度为50.", max = 50)
	// @Min(message = "子级显示[1:树节点;2:选项卡;3:父级嵌套]最小为600.", value = 600)
	@ApiModelProperty(value = "子级显示[1:树节点;2:选项卡;3:父级嵌套]", name = "childDisplay", dataType = "Integer")
    private Integer childDisplay;

	/**
	 * @Fields parentId: 父级主键
	 */
	// @NotNull(message = "父级主键不能为空.")
	// @NotBlank(message = "父级主键不能为空.")
	// @Size(message = "父级主键最大长度为50.", max = 50)
	// @Min(message = "父级主键最小为600.", value = 600)
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
	// @NotNull(message = "父级编码不能为空.")
	// @NotBlank(message = "父级编码不能为空.")
	// @Size(message = "父级编码最大长度为50.", max = 50)
	// @Min(message = "父级编码最小为600.", value = 600)
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
	// @NotNull(message = "父级名称不能为空.")
	// @NotBlank(message = "父级名称不能为空.")
	// @Size(message = "父级名称最大长度为50.", max = 50)
	// @Min(message = "父级名称最小为600.", value = 600)
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;








	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;






}