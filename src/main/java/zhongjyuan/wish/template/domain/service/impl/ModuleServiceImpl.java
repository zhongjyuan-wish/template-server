package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.plugin.framework.TreeManage;
import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.mapper.ModuleMapper;
import zhongjyuan.wish.template.domain.service.IModuleService;

import zhongjyuan.wish.template.domain.model.entity.ModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.ModuleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName ModuleServiceImpl
 * @Description 模块表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class ModuleServiceImpl implements IModuleService {

	@Autowired
	ModuleMapper moduleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModuleDO create(ModuleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = moduleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		moduleMapper.insert(entity);

		log.info("======ModuleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(ModuleDO entity) {

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ModuleDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(ModuleDO::getGroup, entity.getGroup());
		}
		if (parentFlag) {
			queryWrapper.lambda().eq(ModuleDO::getParentId, entity.getParentId());
		}

		if (moduleMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_CREATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			if (parentFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ModuleCode.MODULE_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ModuleDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(ModuleDO::getGroup, entity.getGroup());
		}

		if (moduleMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_CREATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			if (!parentFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ModuleCode.MODULE_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModuleDO update(ModuleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		moduleMapper.updateById(entity);

		log.info("======ModuleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModuleDO 实体对象
	 * @Throws
	 */
	private ModuleDO updateEnabled(ModuleDO entity) {

		// 获取原始数据
		ModuleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(ModuleCode.MODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(ModuleCode.MODULE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(ModuleCode.MODULE_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在分组
		Boolean groupFlag = entity.getGroup() != null;

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ModuleDO::getCode, entity.getCode());
		if (groupFlag) {
			queryWrapper.lambda().eq(ModuleDO::getGroup, entity.getGroup());
		}
		if (parentFlag) {
			queryWrapper.lambda().eq(ModuleDO::getParentId, entity.getParentId());
		}

		if (moduleMapper.exists(queryWrapper)) {
			if (groupFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_UPDATE_BY_CODE_AND_GROUP_EXISTS_WARN);
			}
			if (parentFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ModuleCode.MODULE_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ModuleDO::getName, entity.getName());
		if (groupFlag) {
			queryWrapper.lambda().eq(ModuleDO::getGroup, entity.getGroup());
		}

		if (moduleMapper.exists(queryWrapper)) {
			if (!groupFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_UPDATE_BY_NAME_AND_GROUP_EXISTS_WARN);
			}
			if (!parentFlag) {
				throw new BusinessException(ModuleCode.MODULE_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(ModuleCode.MODULE_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModuleDO delete(Long id) {

		// 获取原始数据
		ModuleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		moduleMapper.updateById(originEntity);

		log.info("======ModuleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModuleDO deleteByCode(String code) {

		// 获取原始数据
		ModuleDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		moduleMapper.updateById(originEntity);

		log.info("======ModuleServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<ModuleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<ModuleDO> entities = new ArrayList<ModuleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsSystem, false);
		queryWrapper.lambda().in(ModuleDO::getId, ids);
		entities = moduleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<ModuleDO> successEntities = new ArrayList<ModuleDO>();
		
		// 根据主键更新
		for (ModuleDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			moduleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======ModuleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<ModuleDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<ModuleDO> entities = new ArrayList<ModuleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsSystem, false);
		queryWrapper.lambda().in(ModuleDO::getCode, codes);
		entities = moduleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<ModuleDO> successEntities = new ArrayList<ModuleDO>();
		
		// 根据主键更新
		for (ModuleDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			moduleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======ModuleServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(ModuleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(ModuleCode.MODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(ModuleCode.MODULE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(ModuleCode.MODULE_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public ModuleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return moduleMapper.selectById(id);
	}
	
	@Override
	public ModuleDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);
		queryWrapper.lambda().eq(ModuleDO::getCode, code);
		
		return moduleMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<ModuleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return moduleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<ModuleDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);
		queryWrapper.lambda().in(ModuleDO::getCode, codes);
		
		return moduleMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<ModuleVO> selectList(ModuleQuery query) {
		List<ModuleVO> vos = new ArrayList<ModuleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModuleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ModuleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ModuleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(ModuleDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(ModuleDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = ModuleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<ModuleBriefVO> selectBriefList(ModuleBriefQuery query) {
		List<ModuleBriefVO> briefVOs = new ArrayList<ModuleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModuleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ModuleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ModuleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(ModuleDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(ModuleDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = ModuleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<ModulePageVO> selectPage(ModulePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModuleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ModuleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ModuleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(ModuleDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(ModuleDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<ModuleDO> page = moduleMapper.selectPage(new CustomPage<ModuleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> ModuleConverter.entityToPageVos(entities));
	}

	@Override
	public List<ModuleTreeVO> selectLazy(TreeLazyQuery<Long> query) {

		// 转成复杂查询条件对象
		TreeComplexQuery<Long> complexQuery = new TreeComplexQuery<Long>();
		BeanUtil.copyProperties(query, complexQuery);

		return selectChildrens(complexQuery);
	}

	@Override
	public List<ModuleTreeVO> selectTree(TreeQuery<Long> query) {
		List<ModuleTreeVO> treeVOs = new ArrayList<ModuleTreeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModuleDO> queryWrapper = new QueryWrapper<ModuleDO>();
		queryWrapper.lambda().eq(ModuleDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModuleDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(ModuleDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(ModuleDO::getParentName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			treeVOs = ModuleConverter.entityToTreeVos(entities);
			treeVOs = (List<ModuleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ModuleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ModuleTreeVO> selectParents(TreeComplexQuery<Long> query) {
		List<ModuleTreeVO> treeVOs = new ArrayList<ModuleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectParents(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ModuleConverter.entityToTreeVos(entities);
			treeVOs = (List<ModuleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ModuleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ModuleTreeVO> selectChildrens(TreeComplexQuery<Long> query) {
		List<ModuleTreeVO> treeVOs = new ArrayList<ModuleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectChildrens(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ModuleConverter.entityToTreeVos(entities);
			treeVOs = (List<ModuleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ModuleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ModuleTreeVO> selectLeafs(TreeComplexQuery<Long> query) {
		List<ModuleTreeVO> treeVOs = new ArrayList<ModuleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectLeafs(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ModuleConverter.entityToTreeVos(entities);
			treeVOs = (List<ModuleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ModuleTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<ModuleTreeVO> selectFulls(TreeComplexQuery<Long> query) {
		List<ModuleTreeVO> treeVOs = new ArrayList<ModuleTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<ModuleDO> entities = moduleMapper.selectFulls(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = ModuleConverter.entityToTreeVos(entities);
			treeVOs = (List<ModuleTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, ModuleTreeVO.class);
		}

		return treeVOs;
	}
}
