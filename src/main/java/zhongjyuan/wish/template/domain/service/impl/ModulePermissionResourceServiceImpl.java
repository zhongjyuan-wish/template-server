package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.ModulePermissionResourceMapper;
import zhongjyuan.wish.template.domain.service.IModulePermissionResourceService;

import zhongjyuan.wish.template.domain.model.entity.ModulePermissionResourceDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.ModulePermissionResourceCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName ModulePermissionResourceServiceImpl
 * @Description 模块权限资源关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class ModulePermissionResourceServiceImpl implements IModulePermissionResourceService {

	@Autowired
	ModulePermissionResourceMapper modulePermissionResourceMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModulePermissionResourceDO create(ModulePermissionResourceDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = modulePermissionResourceMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		modulePermissionResourceMapper.insert(entity);

		log.info("======ModulePermissionResourceServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(ModulePermissionResourceDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModulePermissionResourceDO update(ModulePermissionResourceDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		modulePermissionResourceMapper.updateById(entity);

		log.info("======ModulePermissionResourceServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ModulePermissionResourceDO 实体对象
	 * @Throws
	 */
	private ModulePermissionResourceDO updateEnabled(ModulePermissionResourceDO entity) {

		// 获取原始数据
		ModulePermissionResourceDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(ModulePermissionResourceCode.MODULEPERMISSIONRESOURCE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public ModulePermissionResourceDO delete(Long id) {

		// 获取原始数据
		ModulePermissionResourceDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		modulePermissionResourceMapper.updateById(originEntity);

		log.info("======ModulePermissionResourceServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<ModulePermissionResourceDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<ModulePermissionResourceDO> entities = new ArrayList<ModulePermissionResourceDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<ModulePermissionResourceDO> queryWrapper = new QueryWrapper<ModulePermissionResourceDO>();
		queryWrapper.lambda().in(ModulePermissionResourceDO::getId, ids);
		entities = modulePermissionResourceMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<ModulePermissionResourceDO> successEntities = new ArrayList<ModulePermissionResourceDO>();
		
		// 根据主键更新
		for (ModulePermissionResourceDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			modulePermissionResourceMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======ModulePermissionResourceServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(ModulePermissionResourceDO entity) {
	
		if (entity == null) {
			throw new BusinessException(ModulePermissionResourceCode.MODULEPERMISSIONRESOURCE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public ModulePermissionResourceDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return modulePermissionResourceMapper.selectById(id);
	}
	
	@Override
	public List<ModulePermissionResourceDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return modulePermissionResourceMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<ModulePermissionResourceVO> selectList(ModulePermissionResourceQuery query) {
		List<ModulePermissionResourceVO> vos = new ArrayList<ModulePermissionResourceVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModulePermissionResourceDO> queryWrapper = new QueryWrapper<ModulePermissionResourceDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModulePermissionResourceDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ModulePermissionResourceDO> entities = modulePermissionResourceMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = ModulePermissionResourceConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<ModulePermissionResourceBriefVO> selectBriefList(ModulePermissionResourceBriefQuery query) {
		List<ModulePermissionResourceBriefVO> briefVOs = new ArrayList<ModulePermissionResourceBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModulePermissionResourceDO> queryWrapper = new QueryWrapper<ModulePermissionResourceDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModulePermissionResourceDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<ModulePermissionResourceDO> entities = modulePermissionResourceMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = ModulePermissionResourceConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<ModulePermissionResourcePageVO> selectPage(ModulePermissionResourcePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<ModulePermissionResourceDO> queryWrapper = new QueryWrapper<ModulePermissionResourceDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(ModulePermissionResourceDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<ModulePermissionResourceDO> page = modulePermissionResourceMapper.selectPage(new CustomPage<ModulePermissionResourceDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> ModulePermissionResourceConverter.entityToPageVos(entities));
	}

}
