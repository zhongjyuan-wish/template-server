package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IRoleMergeService;

import zhongjyuan.wish.template.domain.model.entity.RoleMergeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName RoleMergeController
 * @Description 角色合并关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "角色合并关联管理", tags = "角色合并关联管理")
@RequestMapping("{version}/roleMerges")
public class RoleMergeController {

	@Autowired
	IRoleMergeService roleMergeService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "角色合并关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMergeVO> create(@RequestBody @Valid RoleMergeCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleMergeDO entity = RoleMergeConverter.createVoToEntity(createVO);
		entity = roleMergeService.create(entity);
		return ResponseResult.success(RoleMergeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "角色合并关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMergeVO> update(@RequestBody @Valid RoleMergeUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		RoleMergeDO entity = RoleMergeConverter.updateVoToEntity(updateVO);
		entity = roleMergeService.update(entity);
		return ResponseResult.success(RoleMergeConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色合并关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMergeVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleMergeDO> entities = roleMergeService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleMergeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "角色合并关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMergeVO> delete(@PathVariable("id") Long id) {
		RoleMergeDO entity = roleMergeService.delete(id);
		return ResponseResult.success(RoleMergeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMergeVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleMergeDO> entities = roleMergeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleMergeConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMergeVO> select(@PathVariable("id") Long id) {
		RoleMergeDO entity = roleMergeService.select(id);
		return ResponseResult.success(RoleMergeConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMergeVO>> selectList(@Valid RoleMergeQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleMergeService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMergeBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<RoleMergeDO> entities = roleMergeService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(RoleMergeConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<RoleMergeBriefVO> selectBrief(@PathVariable("id") Long id) {
		RoleMergeDO entity = roleMergeService.select(id);
		return ResponseResult.success(RoleMergeConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<RoleMergeBriefVO>> selectBriefList(@Valid RoleMergeBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleMergeService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "角色合并关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页角色合并关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<RoleMergePageVO>> selectPage(@Valid RoleMergePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(roleMergeService.selectPage(query));
	}
	
}
