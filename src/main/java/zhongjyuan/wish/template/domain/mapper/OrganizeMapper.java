package zhongjyuan.wish.template.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zhongjyuan.wish.template.domain.model.entity.OrganizeDO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

import zhongjyuan.domain.query.TreeComplexQuery;

/**
 * @ClassName OrganizeMapper
 * @Description 组织表Mapper接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Mapper
public interface OrganizeMapper extends BaseMapper<OrganizeDO> {

	/**
	 * @Title selectMaxRowIndex
	 * @Author zhongjyuan
	 * @Description 查询最大行号
	 * @Param @return
	 * @Return Integer 最大行号
	 * @Throws
	 */
	@Select("SELECT IFNULL(MAX(ROW_INDEX),0) FROM sys_organize;")
	Integer selectMaxRowIndex();
	
	/**
	 * @Title updateOutline
	 * @Author zhongjyuan
	 * @Description 更新层级码
	 * @Param
	 * @Return void
	 * @Throws
	 */
	void updateOutline();

	/**
	 * @Title selectParents
	 * @Author zhongjyuan
	 * @Description 查询所有父级
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<OrganizeDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeDO> selectParents(TreeComplexQuery<Long> complexQuery);

	/**
	 * @Title selectChildrens
	 * @Author zhongjyuan
	 * @Description 查询所有子级
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<OrganizeDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeDO> selectChildrens(TreeComplexQuery<Long> complexQuery);

	/**
	 * @Title selectLeafs
	 * @Author zhongjyuan
	 * @Description 查询所有叶子节点
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<OrganizeDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeDO> selectLeafs(TreeComplexQuery<Long> complexQuery);

	/**
	 * @Title selectFulls
	 * @Author zhongjyuan
	 * @Description 查询全路径节点
	 * @Param @param complexQuery 复杂查询条件对象
	 * @Param @return
	 * @Return List<OrganizeDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeDO> selectFulls(TreeComplexQuery<Long> complexQuery);
}
