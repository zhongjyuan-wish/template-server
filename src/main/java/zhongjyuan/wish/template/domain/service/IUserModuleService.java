package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.UserModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IUserModuleService
 * @Description 用户模块关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IUserModuleService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserModuleDO 实体对象
	 * @Throws
	 */
	UserModuleDO create(UserModuleDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserModuleDO 实体对象
	 * @Throws
	 */
	UserModuleDO update(UserModuleDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return UserModuleDO 实体对象
	 * @Throws
	 */
	UserModuleDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<UserModuleDO> 实体对象集合
	 * @Throws
	 */
	List<UserModuleDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return UserModuleDO 实体对象
	 * @Throws
	 */
	UserModuleDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<UserModuleDO> 实体对象集合
	 * @Throws
	 */
	List<UserModuleDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<UserModuleVO> VO对象集合
	 * @Throws
	 */
	List<UserModuleVO> selectList(UserModuleQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<UserModuleBriefVO> 简要对象集合
	 * @Throws
	 */
	List<UserModuleBriefVO> selectBriefList(UserModuleBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<UserModulePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<UserModulePageVO> selectPage(UserModulePageQuery query);

}
