package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName OrganizeBriefQuery
 * @Description 组织简要查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizeBriefQuery", description = "组织简要查询条件对象")
public class OrganizeBriefQuery extends SortQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields aliasName: 别称
	 */
	@ApiModelProperty(value = "别称", name = "aliasName", dataType = "String")
    private String aliasName;

	/**
	 * @Fields shortName: 简称
	 */
	@ApiModelProperty(value = "简称", name = "shortName", dataType = "String")
    private String shortName;

	/**
	 * @Fields fullName: 全称
	 */
	@ApiModelProperty(value = "全称", name = "fullName", dataType = "String")
    private String fullName;

	/**
	 * @Fields type: 类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]
	 */
	@ApiModelProperty(value = "类型[Group:集团;Corp:公司;Dept:部门;Agent;代理]", name = "type", dataType = "String")
    private String type;

	/**
	 * @Fields icon: 图标
	 */
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields parentId: 父级主键
	 */
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentNo: 父级编号
	 */
	@ApiModelProperty(value = "父级编号", name = "parentNo", dataType = "String")
    private String parentNo;

	/**
	 * @Fields parentName: 父级名称
	 */
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;

	/**
	 * @Fields directId: 直属主键
	 */
	@ApiModelProperty(value = "直属主键", name = "directId", dataType = "Long")
    private Long directId;

	/**
	 * @Fields directNo: 直属编号
	 */
	@ApiModelProperty(value = "直属编号", name = "directNo", dataType = "String")
    private String directNo;

	/**
	 * @Fields directName: 直属名称
	 */
	@ApiModelProperty(value = "直属名称", name = "directName", dataType = "String")
    private String directName;





	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}