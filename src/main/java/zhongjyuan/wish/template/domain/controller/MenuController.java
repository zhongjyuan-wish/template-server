package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IMenuService;

import zhongjyuan.wish.template.domain.model.entity.MenuDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName MenuController
 * @Description 菜单管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "菜单管理", tags = "菜单管理")
@RequestMapping("{version}/menus")
public class MenuController {

	@Autowired
	IMenuService menuService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> create(@RequestBody @Valid MenuCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		MenuDO entity = MenuConverter.createVoToEntity(createVO);
		entity = menuService.create(entity);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> update(@RequestBody @Valid MenuUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		MenuDO entity = MenuConverter.updateVoToEntity(updateVO);
		entity = menuService.update(entity);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuDO> entities = menuService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(MenuConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuDO> entities = menuService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(MenuConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> delete(@PathVariable("id") Long id) {
		MenuDO entity = menuService.delete(id);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> deleteByCode(@PathVariable("code") String code) {
		MenuDO entity = menuService.deleteByCode(code);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> enable(@PathVariable("id") Long id) {
		MenuDO entity = menuService.select(id);
		entity.setIsEnabled(true);
		entity = menuService.update(entity);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> enableByCode(@PathVariable("code") String code) {
		MenuDO entity = menuService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = menuService.update(entity);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> disable(@PathVariable("id") Long id) {
		MenuDO entity = menuService.select(id);
		entity.setIsEnabled(false);
		entity = menuService.update(entity);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "菜单", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> disableByCode(@PathVariable("code") String code) {
		MenuDO entity = menuService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = menuService.update(entity);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuDO> entities = menuService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(MenuConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuDO> entities = menuService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(MenuConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> select(@PathVariable("id") Long id) {
		MenuDO entity = menuService.select(id);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuVO> selectByCode(@PathVariable("code") String code) {
		MenuDO entity = menuService.selectByCode(code);
		return ResponseResult.success(MenuConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuVO>> selectList(@Valid MenuQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuDO> entities = menuService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(MenuConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuBriefVO> selectBrief(@PathVariable("id") Long id) {
		MenuDO entity = menuService.select(id);
		return ResponseResult.success(MenuConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<MenuBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		MenuDO entity = menuService.selectByCode(code);
		return ResponseResult.success(MenuConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuBriefVO>> selectBriefList(@Valid MenuBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<MenuPageVO>> selectPage(@Valid MenuPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectPage(query));
	}
	
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuTreeVO>> selectTree(@Valid TreeQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectTree(query));
	}
	
	@RequestMapping(value = "/tree/lazy", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(懒加载) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuTreeVO>> selectLazy(@Valid TreeLazyQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectLazy(query));
	}

	@RequestMapping(value = "/tree/parents", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuTreeVO>> selectParents(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectParents(query));
	}

	@RequestMapping(value = "/tree/parentIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectParentIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuTreeVO> treeVOs = menuService.selectParents(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/childrens", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(所有子级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuTreeVO>> selectChildrens(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectChildrens(query));
	}

	@RequestMapping(value = "/tree/childrenIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectChildrenIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuTreeVO> treeVOs = menuService.selectChildrens(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/leafs", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuTreeVO>> selectLeafs(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectLeafs(query));
	}

	@RequestMapping(value = "/tree/leafIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectLeafIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuTreeVO> treeVOs = menuService.selectLeafs(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/fulls", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<MenuTreeVO>> selectFulls(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(menuService.selectFulls(query));
	}

	@RequestMapping(value = "/tree/fullIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "菜单", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形菜单(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectFullIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<MenuTreeVO> treeVOs = menuService.selectFulls(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}
}
