package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IStationService;

import zhongjyuan.wish.template.domain.model.entity.StationDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName StationController
 * @Description 岗位管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "岗位管理", tags = "岗位管理")
@RequestMapping("{version}/stations")
public class StationController {

	@Autowired
	IStationService stationService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> create(@RequestBody @Valid StationCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		StationDO entity = StationConverter.createVoToEntity(createVO);
		entity = stationService.create(entity);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> update(@RequestBody @Valid StationUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		StationDO entity = StationConverter.updateVoToEntity(updateVO);
		entity = stationService.update(entity);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationDO> entities = stationService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationDO> entities = stationService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> delete(@PathVariable("id") Long id) {
		StationDO entity = stationService.delete(id);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> deleteByCode(@PathVariable("code") String code) {
		StationDO entity = stationService.deleteByCode(code);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> enable(@PathVariable("id") Long id) {
		StationDO entity = stationService.select(id);
		entity.setIsEnabled(true);
		entity = stationService.update(entity);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> enableByCode(@PathVariable("code") String code) {
		StationDO entity = stationService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = stationService.update(entity);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> disable(@PathVariable("id") Long id) {
		StationDO entity = stationService.select(id);
		entity.setIsEnabled(false);
		entity = stationService.update(entity);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "岗位", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> disableByCode(@PathVariable("code") String code) {
		StationDO entity = stationService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = stationService.update(entity);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationDO> entities = stationService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationDO> entities = stationService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> select(@PathVariable("id") Long id) {
		StationDO entity = stationService.select(id);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationVO> selectByCode(@PathVariable("code") String code) {
		StationDO entity = stationService.selectByCode(code);
		return ResponseResult.success(StationConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationVO>> selectList(@Valid StationQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<StationDO> entities = stationService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(StationConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationBriefVO> selectBrief(@PathVariable("id") Long id) {
		StationDO entity = stationService.select(id);
		return ResponseResult.success(StationConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<StationBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		StationDO entity = stationService.selectByCode(code);
		return ResponseResult.success(StationConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<StationBriefVO>> selectBriefList(@Valid StationBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "岗位", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页岗位 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<StationPageVO>> selectPage(@Valid StationPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(stationService.selectPage(query));
	}
	
}
