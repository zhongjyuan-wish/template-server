package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.UserModuleMapper;
import zhongjyuan.wish.template.domain.service.IUserModuleService;

import zhongjyuan.wish.template.domain.model.entity.UserModuleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.UserModuleCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName UserModuleServiceImpl
 * @Description 用户模块关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class UserModuleServiceImpl implements IUserModuleService {

	@Autowired
	UserModuleMapper userModuleMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserModuleDO create(UserModuleDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = userModuleMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		userModuleMapper.insert(entity);

		log.info("======UserModuleServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(UserModuleDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserModuleDO update(UserModuleDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		userModuleMapper.updateById(entity);

		log.info("======UserModuleServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserModuleDO 实体对象
	 * @Throws
	 */
	private UserModuleDO updateEnabled(UserModuleDO entity) {

		// 获取原始数据
		UserModuleDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(UserModuleCode.USERMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public UserModuleDO delete(Long id) {

		// 获取原始数据
		UserModuleDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		userModuleMapper.updateById(originEntity);

		log.info("======UserModuleServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<UserModuleDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<UserModuleDO> entities = new ArrayList<UserModuleDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<UserModuleDO> queryWrapper = new QueryWrapper<UserModuleDO>();
		queryWrapper.lambda().in(UserModuleDO::getId, ids);
		entities = userModuleMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<UserModuleDO> successEntities = new ArrayList<UserModuleDO>();
		
		// 根据主键更新
		for (UserModuleDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			userModuleMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======UserModuleServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(UserModuleDO entity) {
	
		if (entity == null) {
			throw new BusinessException(UserModuleCode.USERMODULE_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public UserModuleDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return userModuleMapper.selectById(id);
	}
	
	@Override
	public List<UserModuleDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return userModuleMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<UserModuleVO> selectList(UserModuleQuery query) {
		List<UserModuleVO> vos = new ArrayList<UserModuleVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserModuleDO> queryWrapper = new QueryWrapper<UserModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserModuleDO> entities = userModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = UserModuleConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<UserModuleBriefVO> selectBriefList(UserModuleBriefQuery query) {
		List<UserModuleBriefVO> briefVOs = new ArrayList<UserModuleBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserModuleDO> queryWrapper = new QueryWrapper<UserModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserModuleDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<UserModuleDO> entities = userModuleMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = UserModuleConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<UserModulePageVO> selectPage(UserModulePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<UserModuleDO> queryWrapper = new QueryWrapper<UserModuleDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(UserModuleDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<UserModuleDO> page = userModuleMapper.selectPage(new CustomPage<UserModuleDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> UserModuleConverter.entityToPageVos(entities));
	}

}
