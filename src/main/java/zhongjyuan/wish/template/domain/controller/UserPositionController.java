package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IUserPositionService;

import zhongjyuan.wish.template.domain.model.entity.UserPositionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName UserPositionController
 * @Description 用户职位关联管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "用户职位关联管理", tags = "用户职位关联管理")
@RequestMapping("{version}/userPositions")
public class UserPositionController {

	@Autowired
	IUserPositionService userPositionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "用户职位关联", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserPositionVO> create(@RequestBody @Valid UserPositionCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserPositionDO entity = UserPositionConverter.createVoToEntity(createVO);
		entity = userPositionService.create(entity);
		return ResponseResult.success(UserPositionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "用户职位关联", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserPositionVO> update(@RequestBody @Valid UserPositionUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		UserPositionDO entity = UserPositionConverter.updateVoToEntity(updateVO);
		entity = userPositionService.update(entity);
		return ResponseResult.success(UserPositionConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户职位关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserPositionVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserPositionDO> entities = userPositionService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserPositionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "用户职位关联", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserPositionVO> delete(@PathVariable("id") Long id) {
		UserPositionDO entity = userPositionService.delete(id);
		return ResponseResult.success(UserPositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserPositionVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserPositionDO> entities = userPositionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserPositionConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserPositionVO> select(@PathVariable("id") Long id) {
		UserPositionDO entity = userPositionService.select(id);
		return ResponseResult.success(UserPositionConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserPositionVO>> selectList(@Valid UserPositionQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userPositionService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserPositionBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<UserPositionDO> entities = userPositionService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(UserPositionConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<UserPositionBriefVO> selectBrief(@PathVariable("id") Long id) {
		UserPositionDO entity = userPositionService.select(id);
		return ResponseResult.success(UserPositionConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<UserPositionBriefVO>> selectBriefList(@Valid UserPositionBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userPositionService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "用户职位关联", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页用户职位关联 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<UserPositionPageVO>> selectPage(@Valid UserPositionPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(userPositionService.selectPage(query));
	}
	
}
