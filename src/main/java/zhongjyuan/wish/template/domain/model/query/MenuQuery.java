package zhongjyuan.wish.template.domain.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.query.SortQuery;

/**
 * @ClassName MenuQuery
 * @Description 菜单查询条件对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MenuQuery", description = "菜单查询条件对象")
public class MenuQuery extends SortQuery {

	private static final long serialVersionUID = 1L;




	/**
	 * @Fields nickName: 别称
	 */
	@ApiModelProperty(value = "别称", name = "nickName", dataType = "String")
    private String nickName;

	/**
	 * @Fields shortName: 简称
	 */
	@ApiModelProperty(value = "简称", name = "shortName", dataType = "String")
    private String shortName;

	/**
	 * @Fields icon: 图标
	 */
	@ApiModelProperty(value = "图标", name = "icon", dataType = "String")
    private String icon;

	/**
	 * @Fields link: 链接
	 */
	@ApiModelProperty(value = "链接", name = "link", dataType = "String")
    private String link;

	/**
	 * @Fields type: 类型
	 */
	@ApiModelProperty(value = "类型", name = "type", dataType = "Integer")
    private Integer type;

	/**
	 * @Fields childDisplay: 子级显示[1:树节点;2:选项卡;3:父级嵌套]
	 */
	@ApiModelProperty(value = "子级显示[1:树节点;2:选项卡;3:父级嵌套]", name = "childDisplay", dataType = "Integer")
    private Integer childDisplay;

	/**
	 * @Fields parentId: 父级主键
	 */
	@ApiModelProperty(value = "父级主键", name = "parentId", dataType = "Long")
    private Long parentId;

	/**
	 * @Fields parentCode: 父级编码
	 */
	@ApiModelProperty(value = "父级编码", name = "parentCode", dataType = "String")
    private String parentCode;

	/**
	 * @Fields parentName: 父级名称
	 */
	@ApiModelProperty(value = "父级名称", name = "parentName", dataType = "String")
    private String parentName;





	/**
	 * @Fields isSystem: 是否系统
	 */
	@ApiModelProperty(value = "是否系统", name = "isSystem", dataType = "Boolean")
    private Boolean isSystem;

	/**
	 * @Fields isEnabled: 是否有效
	 */
	@ApiModelProperty(value = "是否有效", name = "isEnabled", dataType = "Boolean")
    private Boolean isEnabled;









}