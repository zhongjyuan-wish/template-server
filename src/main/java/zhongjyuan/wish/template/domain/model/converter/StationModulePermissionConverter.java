package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.StationModulePermissionDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName StationModulePermissionConverter
 * @Description 岗位模块权限关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class StationModulePermissionConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return StationModulePermissionDO 实体对象
	 * @Throws
	 */
	public static StationModulePermissionDO createVoToEntity(StationModulePermissionCreateVO createVO) {
		StationModulePermissionDO entity = new StationModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return StationModulePermissionDO 实体对象
	 * @Throws
	 */
	public static StationModulePermissionDO updateVoToEntity(StationModulePermissionUpdateVO updateVO) {
		StationModulePermissionDO entity = new StationModulePermissionDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return StationModulePermissionVO VO对象
	 * @Throws
	 */
	public static StationModulePermissionVO entityToVo(StationModulePermissionDO entity) {

		StationModulePermissionVO vo = new StationModulePermissionVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return StationModulePermissionBriefVO 简要对象
	 * @Throws
	 */
	public static StationModulePermissionBriefVO entityToBriefVo(StationModulePermissionDO entity) {

		StationModulePermissionBriefVO briefVO = new StationModulePermissionBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<StationModulePermissionVO> VO对象集合
	 * @Throws
	 */
	public static List<StationModulePermissionVO> entityToVos(List<StationModulePermissionDO> entities) {

		List<StationModulePermissionVO> vos = new ArrayList<StationModulePermissionVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, StationModulePermissionVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<StationModulePermissionBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<StationModulePermissionBriefVO> entityToBriefVos(List<StationModulePermissionDO> entities) {

		List<StationModulePermissionBriefVO> briefVOs = new ArrayList<StationModulePermissionBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, StationModulePermissionBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<StationModulePermissionPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<StationModulePermissionPageVO> entityToPageVos(List<StationModulePermissionDO> entities) {

		List<StationModulePermissionPageVO> pageVOs = new ArrayList<StationModulePermissionPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, StationModulePermissionPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}