package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.UserPositionDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IUserPositionService
 * @Description 用户职位关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IUserPositionService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserPositionDO 实体对象
	 * @Throws
	 */
	UserPositionDO create(UserPositionDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return UserPositionDO 实体对象
	 * @Throws
	 */
	UserPositionDO update(UserPositionDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return UserPositionDO 实体对象
	 * @Throws
	 */
	UserPositionDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<UserPositionDO> 实体对象集合
	 * @Throws
	 */
	List<UserPositionDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return UserPositionDO 实体对象
	 * @Throws
	 */
	UserPositionDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<UserPositionDO> 实体对象集合
	 * @Throws
	 */
	List<UserPositionDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<UserPositionVO> VO对象集合
	 * @Throws
	 */
	List<UserPositionVO> selectList(UserPositionQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<UserPositionBriefVO> 简要对象集合
	 * @Throws
	 */
	List<UserPositionBriefVO> selectBriefList(UserPositionBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<UserPositionPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<UserPositionPageVO> selectPage(UserPositionPageQuery query);

}
