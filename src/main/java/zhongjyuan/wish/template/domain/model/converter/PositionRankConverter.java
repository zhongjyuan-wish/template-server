package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.PositionRankDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName PositionRankConverter
 * @Description 职业分级转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class PositionRankConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return PositionRankDO 实体对象
	 * @Throws
	 */
	public static PositionRankDO createVoToEntity(PositionRankCreateVO createVO) {
		PositionRankDO entity = new PositionRankDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return PositionRankDO 实体对象
	 * @Throws
	 */
	public static PositionRankDO updateVoToEntity(PositionRankUpdateVO updateVO) {
		PositionRankDO entity = new PositionRankDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return PositionRankVO VO对象
	 * @Throws
	 */
	public static PositionRankVO entityToVo(PositionRankDO entity) {

		PositionRankVO vo = new PositionRankVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return PositionRankBriefVO 简要对象
	 * @Throws
	 */
	public static PositionRankBriefVO entityToBriefVo(PositionRankDO entity) {

		PositionRankBriefVO briefVO = new PositionRankBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<PositionRankVO> VO对象集合
	 * @Throws
	 */
	public static List<PositionRankVO> entityToVos(List<PositionRankDO> entities) {

		List<PositionRankVO> vos = new ArrayList<PositionRankVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, PositionRankVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<PositionRankBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<PositionRankBriefVO> entityToBriefVos(List<PositionRankDO> entities) {

		List<PositionRankBriefVO> briefVOs = new ArrayList<PositionRankBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, PositionRankBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<PositionRankPageVO> 分页对象集合
	 * @Throws
	 */
	public static List<PositionRankPageVO> entityToPageVos(List<PositionRankDO> entities) {

		List<PositionRankPageVO> pageVOs = new ArrayList<PositionRankPageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, PositionRankPageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}