package ${package.Parent}.${package_Converter};

import java.util.ArrayList;
import java.util.List;

import ${import_Commons_BeanUtil};
import ${import_Commons_EntityUtil};

import ${package.Entity}.${entity};
import ${package.Parent}.${package_RequestVO}.*;
import ${package.Parent}.${package_ResponseVO}.*;

/**
 * @ClassName ${entityNaming}Converter
 * @Description ${table.comment?replace("表","")}转换器
 * @Author ${author}
 * @Date ${date}
 * @Copyright ${copyright}
 */
public class ${entityNaming}Converter {

	/**
	 * @Title createVoToEntity
	 * @Author ${author}
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	public static ${entity} createVoToEntity(${entityNaming}CreateVO createVO) {
		${entity} entity = new ${entity}();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author ${author}
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return ${entity} 实体对象
	 * @Throws
	 */
	public static ${entity} updateVoToEntity(${entityNaming}UpdateVO updateVO) {
		${entity} entity = new ${entity}();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author ${author}
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ${entityNaming}VO VO对象
	 * @Throws
	 */
	public static ${entityNaming}VO entityToVo(${entity} entity) {

		${entityNaming}VO vo = new ${entityNaming}VO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author ${author}
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ${entityNaming}BriefVO 简要对象
	 * @Throws
	 */
	public static ${entityNaming}BriefVO entityToBriefVo(${entity} entity) {

		${entityNaming}BriefVO briefVO = new ${entityNaming}BriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author ${author}
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<${entityNaming}VO> VO对象集合
	 * @Throws
	 */
	public static List<${entityNaming}VO> entityToVos(List<${entity}> entities) {

		List<${entityNaming}VO> vos = new ArrayList<${entityNaming}VO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, ${entityNaming}VO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author ${author}
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<${entityNaming}BriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<${entityNaming}BriefVO> entityToBriefVos(List<${entity}> entities) {

		List<${entityNaming}BriefVO> briefVOs = new ArrayList<${entityNaming}BriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, ${entityNaming}BriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author ${author}
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<${entityNaming}PageVO> 分页对象集合
	 * @Throws
	 */
	public static List<${entityNaming}PageVO> entityToPageVos(List<${entity}> entities) {

		List<${entityNaming}PageVO> pageVOs = new ArrayList<${entityNaming}PageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, ${entityNaming}PageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
<#if treeFlag>
	/**
	 * @Title entityToTreeVos
	 * @Author ${author}
	 * @Description 实体对象集合转树形对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<${entityNaming}TreeVO> 树形对象集合
	 * @Throws
	 */
	public static List<${entityNaming}TreeVO> entityToTreeVos(List<${entity}> entities) {

		List<${entityNaming}TreeVO> treeVOs = new ArrayList<${entityNaming}TreeVO>();

		/*
		 * 对象转换:
		 */
		treeVOs = BeanUtil.copyListProperties(entities, ${entityNaming}TreeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return treeVOs;
	}
</#if>
}