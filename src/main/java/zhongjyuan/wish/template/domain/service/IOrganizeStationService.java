package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.OrganizeStationDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IOrganizeStationService
 * @Description 组织岗位关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IOrganizeStationService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeStationDO 实体对象
	 * @Throws
	 */
	OrganizeStationDO create(OrganizeStationDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeStationDO 实体对象
	 * @Throws
	 */
	OrganizeStationDO update(OrganizeStationDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return OrganizeStationDO 实体对象
	 * @Throws
	 */
	OrganizeStationDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<OrganizeStationDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeStationDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return OrganizeStationDO 实体对象
	 * @Throws
	 */
	OrganizeStationDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<OrganizeStationDO> 实体对象集合
	 * @Throws
	 */
	List<OrganizeStationDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<OrganizeStationVO> VO对象集合
	 * @Throws
	 */
	List<OrganizeStationVO> selectList(OrganizeStationQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<OrganizeStationBriefVO> 简要对象集合
	 * @Throws
	 */
	List<OrganizeStationBriefVO> selectBriefList(OrganizeStationBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<OrganizeStationPageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<OrganizeStationPageVO> selectPage(OrganizeStationPageQuery query);

}
