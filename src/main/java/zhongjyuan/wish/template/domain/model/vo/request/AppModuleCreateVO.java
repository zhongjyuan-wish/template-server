package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName AppModuleCreateVO
 * @Description 应用模块关联创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppModuleCreateVO", description = "应用模块关联创建对象")
public class AppModuleCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields appId: 应用主键
	 */
	// @NotNull(message = "应用主键不能为空.")
	// @NotBlank(message = "应用主键不能为空.")
	// @Size(message = "应用主键最大长度为50.", max = 50)
	// @Min(message = "应用主键最小为600.", value = 600)
	@ApiModelProperty(value = "应用主键", name = "appId", dataType = "Long")
    private Long appId;

	/**
	 * @Fields appCode: 应用编码
	 */
	// @NotNull(message = "应用编码不能为空.")
	// @NotBlank(message = "应用编码不能为空.")
	// @Size(message = "应用编码最大长度为50.", max = 50)
	// @Min(message = "应用编码最小为600.", value = 600)
	@ApiModelProperty(value = "应用编码", name = "appCode", dataType = "String")
    private String appCode;

	/**
	 * @Fields appName: 应用名称
	 */
	// @NotNull(message = "应用名称不能为空.")
	// @NotBlank(message = "应用名称不能为空.")
	// @Size(message = "应用名称最大长度为50.", max = 50)
	// @Min(message = "应用名称最小为600.", value = 600)
	@ApiModelProperty(value = "应用名称", name = "appName", dataType = "String")
    private String appName;

	/**
	 * @Fields moduleId: 模块主键
	 */
	// @NotNull(message = "模块主键不能为空.")
	// @NotBlank(message = "模块主键不能为空.")
	// @Size(message = "模块主键最大长度为50.", max = 50)
	// @Min(message = "模块主键最小为600.", value = 600)
	@ApiModelProperty(value = "模块主键", name = "moduleId", dataType = "Long")
    private Long moduleId;

	/**
	 * @Fields moduleCode: 模块编码
	 */
	// @NotNull(message = "模块编码不能为空.")
	// @NotBlank(message = "模块编码不能为空.")
	// @Size(message = "模块编码最大长度为50.", max = 50)
	// @Min(message = "模块编码最小为600.", value = 600)
	@ApiModelProperty(value = "模块编码", name = "moduleCode", dataType = "String")
    private String moduleCode;

	/**
	 * @Fields moduleName: 模块名称
	 */
	// @NotNull(message = "模块名称不能为空.")
	// @NotBlank(message = "模块名称不能为空.")
	// @Size(message = "模块名称最大长度为50.", max = 50)
	// @Min(message = "模块名称最小为600.", value = 600)
	@ApiModelProperty(value = "模块名称", name = "moduleName", dataType = "String")
    private String moduleName;

	/**
	 * @Fields status: 状态
	 */
	// @NotNull(message = "状态不能为空.")
	// @NotBlank(message = "状态不能为空.")
	// @Size(message = "状态最大长度为50.", max = 50)
	// @Min(message = "状态最小为600.", value = 600)
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

}