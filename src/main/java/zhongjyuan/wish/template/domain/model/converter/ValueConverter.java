package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.ValueDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName ValueConverter
 * @Description 码值转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class ValueConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	public static ValueDO createVoToEntity(ValueCreateVO createVO) {
		ValueDO entity = new ValueDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return ValueDO 实体对象
	 * @Throws
	 */
	public static ValueDO updateVoToEntity(ValueUpdateVO updateVO) {
		ValueDO entity = new ValueDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ValueVO VO对象
	 * @Throws
	 */
	public static ValueVO entityToVo(ValueDO entity) {

		ValueVO vo = new ValueVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return ValueBriefVO 简要对象
	 * @Throws
	 */
	public static ValueBriefVO entityToBriefVo(ValueDO entity) {

		ValueBriefVO briefVO = new ValueBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ValueVO> VO对象集合
	 * @Throws
	 */
	public static List<ValueVO> entityToVos(List<ValueDO> entities) {

		List<ValueVO> vos = new ArrayList<ValueVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, ValueVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ValueBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<ValueBriefVO> entityToBriefVos(List<ValueDO> entities) {

		List<ValueBriefVO> briefVOs = new ArrayList<ValueBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, ValueBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ValuePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<ValuePageVO> entityToPageVos(List<ValueDO> entities) {

		List<ValuePageVO> pageVOs = new ArrayList<ValuePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, ValuePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
	/**
	 * @Title entityToTreeVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转树形对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<ValueTreeVO> 树形对象集合
	 * @Throws
	 */
	public static List<ValueTreeVO> entityToTreeVos(List<ValueDO> entities) {

		List<ValueTreeVO> treeVOs = new ArrayList<ValueTreeVO>();

		/*
		 * 对象转换:
		 */
		treeVOs = BeanUtil.copyListProperties(entities, ValueTreeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return treeVOs;
	}
}