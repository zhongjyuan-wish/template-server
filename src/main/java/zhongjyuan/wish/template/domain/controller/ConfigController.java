package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IConfigService;

import zhongjyuan.wish.template.domain.model.entity.ConfigDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName ConfigController
 * @Description 配置管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "配置管理", tags = "配置管理")
@RequestMapping("{version}/configs")
public class ConfigController {

	@Autowired
	IConfigService configService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> create(@RequestBody @Valid ConfigCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ConfigDO entity = ConfigConverter.createVoToEntity(createVO);
		entity = configService.create(entity);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> update(@RequestBody @Valid ConfigUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ConfigDO entity = ConfigConverter.updateVoToEntity(updateVO);
		entity = configService.update(entity);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ConfigDO> entities = configService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ConfigConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ConfigDO> entities = configService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ConfigConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> delete(@PathVariable("id") Long id) {
		ConfigDO entity = configService.delete(id);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> deleteByCode(@PathVariable("code") String code) {
		ConfigDO entity = configService.deleteByCode(code);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> enable(@PathVariable("id") Long id) {
		ConfigDO entity = configService.select(id);
		entity.setIsEnabled(true);
		entity = configService.update(entity);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> enableByCode(@PathVariable("code") String code) {
		ConfigDO entity = configService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = configService.update(entity);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> disable(@PathVariable("id") Long id) {
		ConfigDO entity = configService.select(id);
		entity.setIsEnabled(false);
		entity = configService.update(entity);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "配置", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> disableByCode(@PathVariable("code") String code) {
		ConfigDO entity = configService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = configService.update(entity);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ConfigDO> entities = configService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ConfigConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ConfigDO> entities = configService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ConfigConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> select(@PathVariable("id") Long id) {
		ConfigDO entity = configService.select(id);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigVO> selectByCode(@PathVariable("code") String code) {
		ConfigDO entity = configService.selectByCode(code);
		return ResponseResult.success(ConfigConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigVO>> selectList(@Valid ConfigQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(configService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ConfigDO> entities = configService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ConfigConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigBriefVO> selectBrief(@PathVariable("id") Long id) {
		ConfigDO entity = configService.select(id);
		return ResponseResult.success(ConfigConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<ConfigBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		ConfigDO entity = configService.selectByCode(code);
		return ResponseResult.success(ConfigConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<List<ConfigBriefVO>> selectBriefList(@Valid ConfigBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(configService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "配置", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页配置 作者:zhongjyuan 時間:2023年11月15日 15:09:55")
	public ResponseResult<PageResult<ConfigPageVO>> selectPage(@Valid ConfigPageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(configService.selectPage(query));
	}
	
}
