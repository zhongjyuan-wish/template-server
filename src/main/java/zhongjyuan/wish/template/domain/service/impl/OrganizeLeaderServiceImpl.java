package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.mapper.OrganizeLeaderMapper;
import zhongjyuan.wish.template.domain.service.IOrganizeLeaderService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeLeaderDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.OrganizeLeaderCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName OrganizeLeaderServiceImpl
 * @Description 组织责任人关联表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class OrganizeLeaderServiceImpl implements IOrganizeLeaderService {

	@Autowired
	OrganizeLeaderMapper organizeLeaderMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeLeaderDO create(OrganizeLeaderDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = organizeLeaderMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		organizeLeaderMapper.insert(entity);

		log.info("======OrganizeLeaderServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(OrganizeLeaderDO entity) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeLeaderDO update(OrganizeLeaderDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		organizeLeaderMapper.updateById(entity);

		log.info("======OrganizeLeaderServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeLeaderDO 实体对象
	 * @Throws
	 */
	private OrganizeLeaderDO updateEnabled(OrganizeLeaderDO entity) {

		// 获取原始数据
		OrganizeLeaderDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(OrganizeLeaderCode.ORGANIZELEADER_FOR_SELECT_NOT_EXISTS_WARN);
		}
		
		// region 相关业务校验


		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeLeaderDO delete(Long id) {

		// 获取原始数据
		OrganizeLeaderDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		organizeLeaderMapper.updateById(originEntity);

		log.info("======OrganizeLeaderServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<OrganizeLeaderDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<OrganizeLeaderDO> entities = new ArrayList<OrganizeLeaderDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<OrganizeLeaderDO> queryWrapper = new QueryWrapper<OrganizeLeaderDO>();
		queryWrapper.lambda().in(OrganizeLeaderDO::getId, ids);
		entities = organizeLeaderMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<OrganizeLeaderDO> successEntities = new ArrayList<OrganizeLeaderDO>();
		
		// 根据主键更新
		for (OrganizeLeaderDO entity : entities) {
			EntityUtil.setDeletion(entity);
			
			organizeLeaderMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======OrganizeLeaderServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(OrganizeLeaderDO entity) {
	
		if (entity == null) {
			throw new BusinessException(OrganizeLeaderCode.ORGANIZELEADER_FOR_SELECT_NOT_EXISTS_WARN);
		}
	}

	@Override
	public OrganizeLeaderDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return organizeLeaderMapper.selectById(id);
	}
	
	@Override
	public List<OrganizeLeaderDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return organizeLeaderMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<OrganizeLeaderVO> selectList(OrganizeLeaderQuery query) {
		List<OrganizeLeaderVO> vos = new ArrayList<OrganizeLeaderVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeLeaderDO> queryWrapper = new QueryWrapper<OrganizeLeaderDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeLeaderDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeLeaderDO> entities = organizeLeaderMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = OrganizeLeaderConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<OrganizeLeaderBriefVO> selectBriefList(OrganizeLeaderBriefQuery query) {
		List<OrganizeLeaderBriefVO> briefVOs = new ArrayList<OrganizeLeaderBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeLeaderDO> queryWrapper = new QueryWrapper<OrganizeLeaderDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeLeaderDO::getDescription, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeLeaderDO> entities = organizeLeaderMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = OrganizeLeaderConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<OrganizeLeaderPageVO> selectPage(OrganizeLeaderPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeLeaderDO> queryWrapper = new QueryWrapper<OrganizeLeaderDO>();

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeLeaderDO::getDescription, query.getSearchValue())
;
				});
			}

		}

		// 分页查询
		CustomPage<OrganizeLeaderDO> page = organizeLeaderMapper.selectPage(new CustomPage<OrganizeLeaderDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> OrganizeLeaderConverter.entityToPageVos(entities));
	}

}
