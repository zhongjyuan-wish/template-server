package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.plugin.framework.TreeManage;
import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.mapper.DistrictMapper;
import zhongjyuan.wish.template.domain.service.IDistrictService;

import zhongjyuan.wish.template.domain.model.entity.DistrictDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.DistrictCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName DistrictServiceImpl
 * @Description 行政区划表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class DistrictServiceImpl implements IDistrictService {

	@Autowired
	DistrictMapper districtMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public DistrictDO create(DistrictDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = districtMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		districtMapper.insert(entity);

		log.info("======DistrictServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(DistrictDO entity) {

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);
		queryWrapper.lambda().eq(DistrictDO::getCode, entity.getCode());
		if (parentFlag) {
			queryWrapper.lambda().eq(DistrictDO::getParentId, entity.getParentId());
		}

		if (districtMapper.exists(queryWrapper)) {
			if (parentFlag) {
				throw new BusinessException(DistrictCode.DISTRICT_FOR_CREATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(DistrictCode.DISTRICT_FOR_CREATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);
		queryWrapper.lambda().eq(DistrictDO::getName, entity.getName());

		if (districtMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(DistrictCode.DISTRICT_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(DistrictCode.DISTRICT_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public DistrictDO update(DistrictDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		districtMapper.updateById(entity);

		log.info("======DistrictServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return DistrictDO 实体对象
	 * @Throws
	 */
	private DistrictDO updateEnabled(DistrictDO entity) {

		// 获取原始数据
		DistrictDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(DistrictCode.DISTRICT_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(DistrictCode.DISTRICT_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(DistrictCode.DISTRICT_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效编码数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);
		queryWrapper.lambda().eq(DistrictDO::getCode, entity.getCode());
		if (parentFlag) {
			queryWrapper.lambda().eq(DistrictDO::getParentId, entity.getParentId());
		}

		if (districtMapper.exists(queryWrapper)) {
			if (parentFlag) {
				throw new BusinessException(DistrictCode.DISTRICT_FOR_UPDATE_BY_CODE_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(DistrictCode.DISTRICT_FOR_UPDATE_BY_CODE_EXISTS_WARN);
		}

		// 是否存在相同有效名称数据
		queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);
		queryWrapper.lambda().eq(DistrictDO::getName, entity.getName());

		if (districtMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(DistrictCode.DISTRICT_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(DistrictCode.DISTRICT_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public DistrictDO delete(Long id) {

		// 获取原始数据
		DistrictDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		districtMapper.updateById(originEntity);

		log.info("======DistrictServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public DistrictDO deleteByCode(String code) {

		// 获取原始数据
		DistrictDO originEntity = selectByCode(code);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		districtMapper.updateById(originEntity);

		log.info("======DistrictServiceImpl:deleteByCode({})======", code);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<DistrictDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<DistrictDO> entities = new ArrayList<DistrictDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsSystem, false);
		queryWrapper.lambda().in(DistrictDO::getId, ids);
		entities = districtMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<DistrictDO> successEntities = new ArrayList<DistrictDO>();
		
		// 根据主键更新
		for (DistrictDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			districtMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======DistrictServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<DistrictDO> deleteByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		// 定义集合
		List<DistrictDO> entities = new ArrayList<DistrictDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsSystem, false);
		queryWrapper.lambda().in(DistrictDO::getCode, codes);
		entities = districtMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<DistrictDO> successEntities = new ArrayList<DistrictDO>();
		
		// 根据主键更新
		for (DistrictDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			districtMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======DistrictServiceImpl:deleteByCode({})======", codes);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(DistrictDO entity) {
	
		if (entity == null) {
			throw new BusinessException(DistrictCode.DISTRICT_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(DistrictCode.DISTRICT_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(DistrictCode.DISTRICT_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public DistrictDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return districtMapper.selectById(id);
	}
	
	@Override
	public DistrictDO selectByCode(String code) {
		if (StringUtils.isEmpty(code))
			return null;

		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);
		queryWrapper.lambda().eq(DistrictDO::getCode, code);
		
		return districtMapper.selectOne(queryWrapper);
	}
	
	@Override
	public List<DistrictDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return districtMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<DistrictDO> selectByCode(List<String> codes) {
		if (CollectionUtils.isEmpty(codes))
			return null;

		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);
		queryWrapper.lambda().in(DistrictDO::getCode, codes);
		
		return districtMapper.selectList(queryWrapper);
	}
	
	@Override
	public List<DistrictVO> selectList(DistrictQuery query) {
		List<DistrictVO> vos = new ArrayList<DistrictVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(DistrictDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(DistrictDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(DistrictDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(DistrictDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(DistrictDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = DistrictConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<DistrictBriefVO> selectBriefList(DistrictBriefQuery query) {
		List<DistrictBriefVO> briefVOs = new ArrayList<DistrictBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(DistrictDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(DistrictDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(DistrictDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(DistrictDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(DistrictDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = DistrictConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<DistrictPageVO> selectPage(DistrictPageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(DistrictDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(DistrictDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(DistrictDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(DistrictDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(DistrictDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<DistrictDO> page = districtMapper.selectPage(new CustomPage<DistrictDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> DistrictConverter.entityToPageVos(entities));
	}

	@Override
	public List<DistrictTreeVO> selectLazy(TreeLazyQuery<Long> query) {

		// 转成复杂查询条件对象
		TreeComplexQuery<Long> complexQuery = new TreeComplexQuery<Long>();
		BeanUtil.copyProperties(query, complexQuery);

		return selectChildrens(complexQuery);
	}

	@Override
	public List<DistrictTreeVO> selectTree(TreeQuery<Long> query) {
		List<DistrictTreeVO> treeVOs = new ArrayList<DistrictTreeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<DistrictDO> queryWrapper = new QueryWrapper<DistrictDO>();
		queryWrapper.lambda().eq(DistrictDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(DistrictDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(DistrictDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(DistrictDO::getParentName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			treeVOs = DistrictConverter.entityToTreeVos(entities);
			treeVOs = (List<DistrictTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, DistrictTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<DistrictTreeVO> selectParents(TreeComplexQuery<Long> query) {
		List<DistrictTreeVO> treeVOs = new ArrayList<DistrictTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectParents(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = DistrictConverter.entityToTreeVos(entities);
			treeVOs = (List<DistrictTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, DistrictTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<DistrictTreeVO> selectChildrens(TreeComplexQuery<Long> query) {
		List<DistrictTreeVO> treeVOs = new ArrayList<DistrictTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectChildrens(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = DistrictConverter.entityToTreeVos(entities);
			treeVOs = (List<DistrictTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, DistrictTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<DistrictTreeVO> selectLeafs(TreeComplexQuery<Long> query) {
		List<DistrictTreeVO> treeVOs = new ArrayList<DistrictTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectLeafs(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = DistrictConverter.entityToTreeVos(entities);
			treeVOs = (List<DistrictTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, DistrictTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<DistrictTreeVO> selectFulls(TreeComplexQuery<Long> query) {
		List<DistrictTreeVO> treeVOs = new ArrayList<DistrictTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<DistrictDO> entities = districtMapper.selectFulls(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = DistrictConverter.entityToTreeVos(entities);
			treeVOs = (List<DistrictTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, DistrictTreeVO.class);
		}

		return treeVOs;
	}
}
