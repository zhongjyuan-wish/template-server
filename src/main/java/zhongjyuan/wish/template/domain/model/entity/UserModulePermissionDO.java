package zhongjyuan.wish.template.domain.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractEmptyEntity;

/**
 * @ClassName UserModulePermissionDO
 * @Description 用户模块权限关联表实体对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_user_module_permission")
@ApiModel(value = "UserModulePermissionDO", description = "用户模块权限关联表实体对象")
public class UserModulePermissionDO extends AbstractEmptyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * @Fields id: 主键
	 */
    @TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键", name = "id", dataType = "Long")
    private Long id;

	/**
	 * @Fields userModuleId: 用户模块主键
	 */
    @TableField("user_module_id")
	@ApiModelProperty(value = "用户模块主键", name = "userModuleId", dataType = "Long")
    private Long userModuleId;

	/**
	 * @Fields userModuleCode: 用户模块编码
	 */
    @TableField("user_module_code")
	@ApiModelProperty(value = "用户模块编码", name = "userModuleCode", dataType = "String")
    private String userModuleCode;

	/**
	 * @Fields userModuleName: 用户模块名称
	 */
    @TableField("user_module_name")
	@ApiModelProperty(value = "用户模块名称", name = "userModuleName", dataType = "String")
    private String userModuleName;

	/**
	 * @Fields modulePermissionId: 模块权限主键
	 */
    @TableField("module_permission_id")
	@ApiModelProperty(value = "模块权限主键", name = "modulePermissionId", dataType = "Long")
    private Long modulePermissionId;

	/**
	 * @Fields modulePermissionCode: 模块权限编码
	 */
    @TableField("module_permission_code")
	@ApiModelProperty(value = "模块权限编码", name = "modulePermissionCode", dataType = "String")
    private String modulePermissionCode;

	/**
	 * @Fields modulePermissionName: 模块权限名称
	 */
    @TableField("module_permission_name")
	@ApiModelProperty(value = "模块权限名称", name = "modulePermissionName", dataType = "String")
    private String modulePermissionName;

	/**
	 * @Fields status: 状态
	 */
    @TableField("status")
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
    @TableField("row_index")
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
    @TableField("description")
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

	/**
	 * @Fields createTime: 创建时间
	 */
    @TableField("create_time")
	@ApiModelProperty(value = "创建时间", name = "createTime", dataType = "LocalDateTime")
    private LocalDateTime createTime;


    public static final String ID = "id";

    public static final String USER_MODULE_ID = "user_module_id";

    public static final String USER_MODULE_CODE = "user_module_code";

    public static final String USER_MODULE_NAME = "user_module_name";

    public static final String MODULE_PERMISSION_ID = "module_permission_id";

    public static final String MODULE_PERMISSION_CODE = "module_permission_code";

    public static final String MODULE_PERMISSION_NAME = "module_permission_name";

    public static final String STATUS = "status";

    public static final String ROW_INDEX = "row_index";

    public static final String DESCRIPTION = "description";

    public static final String CREATE_TIME = "create_time";

}
