package zhongjyuan.wish.template.domain.model.converter;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.template.domain.model.entity.RoleMergeDO;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName RoleMergeConverter
 * @Description 角色合并关联转换器
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public class RoleMergeConverter {

	/**
	 * @Title createVoToEntity
	 * @Author zhongjyuan
	 * @Description 创建对象转实体对象
	 * @Param @param createVO 创建对象
	 * @Param @return
	 * @Return RoleMergeDO 实体对象
	 * @Throws
	 */
	public static RoleMergeDO createVoToEntity(RoleMergeCreateVO createVO) {
		RoleMergeDO entity = new RoleMergeDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setCreation(entity);
		BeanUtil.copyProperties(createVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title updateVoToEntity
	 * @Author zhongjyuan
	 * @Description 更新对象转实体对象
	 * @Param @param updateVO 更新对象
	 * @Param @return
	 * @Return RoleMergeDO 实体对象
	 * @Throws
	 */
	public static RoleMergeDO updateVoToEntity(RoleMergeUpdateVO updateVO) {
		RoleMergeDO entity = new RoleMergeDO();

		/*
		 * 对象转换: [注意]实体处理放在第一位
		 */
		EntityUtil.setEdition(entity);
		BeanUtil.copyProperties(updateVO, entity);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return entity;
	}

	/**
	 * @Title entityToVo
	 * @Author zhongjyuan
	 * @Description 实体对象转VO对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMergeVO VO对象
	 * @Throws
	 */
	public static RoleMergeVO entityToVo(RoleMergeDO entity) {

		RoleMergeVO vo = new RoleMergeVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, vo);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vo;
	}

	/**
	 * @Title entityToBriefVo
	 * @Author zhongjyuan
	 * @Description 实体对象转简要对象
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return RoleMergeBriefVO 简要对象
	 * @Throws
	 */
	public static RoleMergeBriefVO entityToBriefVo(RoleMergeDO entity) {

		RoleMergeBriefVO briefVO = new RoleMergeBriefVO();

		/*
		 * 对象转换:
		 */
		BeanUtil.copyProperties(entity, briefVO);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVO;
	}

	/**
	 * @Title entityToVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转VO对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleMergeVO> VO对象集合
	 * @Throws
	 */
	public static List<RoleMergeVO> entityToVos(List<RoleMergeDO> entities) {

		List<RoleMergeVO> vos = new ArrayList<RoleMergeVO>();

		/*
		 * 对象转换:
		 */
		vos = BeanUtil.copyListProperties(entities, RoleMergeVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return vos;
	}

	/**
	 * @Title entityToBriefVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转简要对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleMergeBriefVO> 简要对象集合
	 * @Throws
	 */
	public static List<RoleMergeBriefVO> entityToBriefVos(List<RoleMergeDO> entities) {

		List<RoleMergeBriefVO> briefVOs = new ArrayList<RoleMergeBriefVO>();

		/*
		 * 对象转换:
		 */
		briefVOs = BeanUtil.copyListProperties(entities, RoleMergeBriefVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return briefVOs;
	}

	/**
	 * @Title entityToPageVos
	 * @Author zhongjyuan
	 * @Description 实体对象集合转分页对象集合
	 * @Param @param entities 实体对象集合
	 * @Param @return
	 * @Return List<RoleMergePageVO> 分页对象集合
	 * @Throws
	 */
	public static List<RoleMergePageVO> entityToPageVos(List<RoleMergeDO> entities) {

		List<RoleMergePageVO> pageVOs = new ArrayList<RoleMergePageVO>();

		/*
		 * 对象转换:
		 */
		pageVOs = BeanUtil.copyListProperties(entities, RoleMergePageVO::new);

		/*
		 * 特殊属性处理: 1、子级 2、关联 3、......
		 */

		return pageVOs;
	}
	
}