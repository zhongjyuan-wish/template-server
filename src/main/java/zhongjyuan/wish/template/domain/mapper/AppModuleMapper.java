package zhongjyuan.wish.template.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zhongjyuan.wish.template.domain.model.entity.AppModuleDO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * @ClassName AppModuleMapper
 * @Description 应用模块关联表Mapper接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Mapper
public interface AppModuleMapper extends BaseMapper<AppModuleDO> {

	/**
	 * @Title selectMaxRowIndex
	 * @Author zhongjyuan
	 * @Description 查询最大行号
	 * @Param @return
	 * @Return Integer 最大行号
	 * @Throws
	 */
	@Select("SELECT IFNULL(MAX(ROW_INDEX),0) FROM sys_app_module;")
	Integer selectMaxRowIndex();
	
}
