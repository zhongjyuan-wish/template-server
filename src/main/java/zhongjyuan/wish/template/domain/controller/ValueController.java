package zhongjyuan.wish.template.domain.controller;

import java.util.ArrayList;
import java.util.List;	
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.wish.annotation.ApiVersion;
import zhongjyuan.wish.annotation.Log;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.LogType;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.service.IValueService;

import zhongjyuan.wish.template.domain.model.entity.ValueDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.request.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName ValueController
 * @Description 码值管理
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@ApiVersion(1)
@RestController
@Api(value = "码值管理", tags = "码值管理")
@RequestMapping("{version}/values")
public class ValueController {

	@Autowired
	IValueService valueService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "新增") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "新增码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> create(@RequestBody @Valid ValueCreateVO createVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ValueDO entity = ValueConverter.createVoToEntity(createVO);
		entity = valueService.create(entity);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "修改") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "修改码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> update(@RequestBody @Valid ValueUpdateVO updateVO, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		ValueDO entity = ValueConverter.updateVoToEntity(updateVO);
		entity = valueService.update(entity);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueVO>> delete(@RequestBody @Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueDO> entities = valueService.delete(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ValueConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueVO>> deleteByCode(@RequestBody @Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueDO> entities = valueService.deleteByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ValueConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> delete(@PathVariable("id") Long id) {
		ValueDO entity = valueService.delete(id);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.DELETE)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "删除") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "删除码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> deleteByCode(@PathVariable("code") String code) {
		ValueDO entity = valueService.deleteByCode(code);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> enable(@PathVariable("id") Long id) {
		ValueDO entity = valueService.select(id);
		entity.setIsEnabled(true);
		entity = valueService.update(entity);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/enable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "启用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "启用码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> enableByCode(@PathVariable("code") String code) {
		ValueDO entity = valueService.selectByCode(code);
		entity.setIsEnabled(true);
		entity = valueService.update(entity);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> disable(@PathVariable("id") Long id) {
		ValueDO entity = valueService.select(id);
		entity.setIsEnabled(false);
		entity = valueService.update(entity);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}/disable", method = RequestMethod.PUT)
	@Log(type = LogType.OPERATE, title = "码值", module = "system", action = "禁用") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "禁用码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> disableByCode(@PathVariable("code") String code) {
		ValueDO entity = valueService.selectByCode(code);
		entity.setIsEnabled(false);
		entity = valueService.update(entity);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueVO>> select(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueDO> entities = valueService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ValueConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/bycode", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueVO>> selectByCode(@Valid BatchQuery<String> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueDO> entities = valueService.selectByCode(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ValueConverter.entityToVos(entities));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> select(@PathVariable("id") Long id) {
		ValueDO entity = valueService.select(id);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/bycode/{code}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueVO> selectByCode(@PathVariable("code") String code) {
		ValueDO entity = valueService.selectByCode(code);
		return ResponseResult.success(ValueConverter.entityToVo(entity));
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueVO>> selectList(@Valid ValueQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectList(query));
	}

	@RequestMapping(value = "/brief", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueBriefVO>> selectBrief(@Valid BatchQuery<Long> batchQuery, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueDO> entities = valueService.select(new ArrayList<>(batchQuery.getAssemble()));
		return ResponseResult.success(ValueConverter.entityToBriefVos(entities));
	}

	@RequestMapping(value = "/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueBriefVO> selectBrief(@PathVariable("id") Long id) {
		ValueDO entity = valueService.select(id);
		return ResponseResult.success(ValueConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/bycode/brief/{id}", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<ValueBriefVO> selectBriefByCode(@PathVariable("code") String code) {
		ValueDO entity = valueService.selectByCode(code);
		return ResponseResult.success(ValueConverter.entityToBriefVo(entity));
	}
	
	@RequestMapping(value = "/brief/list", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询简要码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueBriefVO>> selectBriefList(@Valid ValueBriefQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectBriefList(query));
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询分页码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<PageResult<ValuePageVO>> selectPage(@Valid ValuePageQuery query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectPage(query));
	}
	
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueTreeVO>> selectTree(@Valid TreeQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectTree(query));
	}
	
	@RequestMapping(value = "/tree/lazy", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(懒加载) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueTreeVO>> selectLazy(@Valid TreeLazyQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectLazy(query));
	}

	@RequestMapping(value = "/tree/parents", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueTreeVO>> selectParents(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectParents(query));
	}

	@RequestMapping(value = "/tree/parentIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectParentIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueTreeVO> treeVOs = valueService.selectParents(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/childrens", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(所有子级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueTreeVO>> selectChildrens(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectChildrens(query));
	}

	@RequestMapping(value = "/tree/childrenIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(所有父级) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectChildrenIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueTreeVO> treeVOs = valueService.selectChildrens(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/leafs", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueTreeVO>> selectLeafs(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectLeafs(query));
	}

	@RequestMapping(value = "/tree/leafIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(所有叶子) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectLeafIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueTreeVO> treeVOs = valueService.selectLeafs(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/tree/fulls", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<ValueTreeVO>> selectFulls(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		return ResponseResult.success(valueService.selectFulls(query));
	}

	@RequestMapping(value = "/tree/fullIds", method = RequestMethod.GET)
	@Log(type = LogType.ACCESS, title = "码值", module = "system", action = "查询") /* [可选]日志捕捉:type[日志类型];title[日志标题(名词)];module[日志模块(业务)];action[日志行为(动作)] */
	@ApiOperation(value = "查询树形码值(全路径) 作者:zhongjyuan 時間:2023年11月15日 15:09:56")
	public ResponseResult<List<Long>> selectFullIds(@Valid TreeComplexQuery<Long> query, BindingResult bindingResult) { /* [可选]BindingResult:VO包含属性校验时需要增加该参数 */
		List<ValueTreeVO> treeVOs = valueService.selectFulls(query);
		return ResponseResult.success(treeVOs.stream().map(tree -> tree.getId()).collect(Collectors.toList()));
	}
}
