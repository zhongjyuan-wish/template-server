package zhongjyuan.wish.template.domain.model.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import zhongjyuan.domain.AbstractModel;

/**
 * @ClassName RoleModulePermissionCreateVO
 * @Description 角色模块权限关联创建对象
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleModulePermissionCreateVO", description = "角色模块权限关联创建对象")
public class RoleModulePermissionCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;


	/**
	 * @Fields roleModuleId: 角色模块主键
	 */
	// @NotNull(message = "角色模块主键不能为空.")
	// @NotBlank(message = "角色模块主键不能为空.")
	// @Size(message = "角色模块主键最大长度为50.", max = 50)
	// @Min(message = "角色模块主键最小为600.", value = 600)
	@ApiModelProperty(value = "角色模块主键", name = "roleModuleId", dataType = "Long")
    private Long roleModuleId;

	/**
	 * @Fields roleModuleCode: 角色模块编码
	 */
	// @NotNull(message = "角色模块编码不能为空.")
	// @NotBlank(message = "角色模块编码不能为空.")
	// @Size(message = "角色模块编码最大长度为50.", max = 50)
	// @Min(message = "角色模块编码最小为600.", value = 600)
	@ApiModelProperty(value = "角色模块编码", name = "roleModuleCode", dataType = "String")
    private String roleModuleCode;

	/**
	 * @Fields roleModuleName: 角色模块名称
	 */
	// @NotNull(message = "角色模块名称不能为空.")
	// @NotBlank(message = "角色模块名称不能为空.")
	// @Size(message = "角色模块名称最大长度为50.", max = 50)
	// @Min(message = "角色模块名称最小为600.", value = 600)
	@ApiModelProperty(value = "角色模块名称", name = "roleModuleName", dataType = "String")
    private String roleModuleName;

	/**
	 * @Fields modulePermissionId: 模块权限主键
	 */
	// @NotNull(message = "模块权限主键不能为空.")
	// @NotBlank(message = "模块权限主键不能为空.")
	// @Size(message = "模块权限主键最大长度为50.", max = 50)
	// @Min(message = "模块权限主键最小为600.", value = 600)
	@ApiModelProperty(value = "模块权限主键", name = "modulePermissionId", dataType = "Long")
    private Long modulePermissionId;

	/**
	 * @Fields modulePermissionCode: 模块权限编码
	 */
	// @NotNull(message = "模块权限编码不能为空.")
	// @NotBlank(message = "模块权限编码不能为空.")
	// @Size(message = "模块权限编码最大长度为50.", max = 50)
	// @Min(message = "模块权限编码最小为600.", value = 600)
	@ApiModelProperty(value = "模块权限编码", name = "modulePermissionCode", dataType = "String")
    private String modulePermissionCode;

	/**
	 * @Fields modulePermissionName: 模块权限名称
	 */
	// @NotNull(message = "模块权限名称不能为空.")
	// @NotBlank(message = "模块权限名称不能为空.")
	// @Size(message = "模块权限名称最大长度为50.", max = 50)
	// @Min(message = "模块权限名称最小为600.", value = 600)
	@ApiModelProperty(value = "模块权限名称", name = "modulePermissionName", dataType = "String")
    private String modulePermissionName;

	/**
	 * @Fields status: 状态
	 */
	// @NotNull(message = "状态不能为空.")
	// @NotBlank(message = "状态不能为空.")
	// @Size(message = "状态最大长度为50.", max = 50)
	// @Min(message = "状态最小为600.", value = 600)
	@ApiModelProperty(value = "状态", name = "status", dataType = "String")
    private String status;

	/**
	 * @Fields rowIndex: 行号
	 */
	// @NotNull(message = "行号不能为空.")
	// @NotBlank(message = "行号不能为空.")
	// @Size(message = "行号最大长度为50.", max = 50)
	// @Min(message = "行号最小为600.", value = 600)
	@ApiModelProperty(value = "行号", name = "rowIndex", dataType = "Integer")
    private Integer rowIndex;

	/**
	 * @Fields description: 描述
	 */
	// @NotNull(message = "描述不能为空.")
	// @NotBlank(message = "描述不能为空.")
	// @Size(message = "描述最大长度为50.", max = 50)
	// @Min(message = "描述最小为600.", value = 600)
	@ApiModelProperty(value = "描述", name = "description", dataType = "String")
    private String description;

}