package zhongjyuan.wish.template.domain.service;

import java.util.List;

import zhongjyuan.domain.response.PageResult;

import zhongjyuan.wish.template.domain.model.entity.AppRoleDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

/**
 * @ClassName IAppRoleService
 * @Description 应用角色关联表服务接口
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:55
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
public interface IAppRoleService {

	/**
	 * @Title create
	 * @Author zhongjyuan
	 * @Description 创建
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppRoleDO 实体对象
	 * @Throws
	 */
	AppRoleDO create(AppRoleDO entity);

	/**
	 * @Title update
	 * @Author zhongjyuan
	 * @Description 更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return AppRoleDO 实体对象
	 * @Throws
	 */
	AppRoleDO update(AppRoleDO entity);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param id 主键
	 * @Param @return
	 * @Return AppRoleDO 实体对象
	 * @Throws
	 */
	AppRoleDO delete(Long id);

	/**
	 * @Title delete
	 * @Author zhongjyuan
	 * @Description 删除
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<AppRoleDO> 实体对象集合
	 * @Throws
	 */
	List<AppRoleDO> delete(List<Long> ids);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param id 主键
	 * @Param @return
	 * @Return AppRoleDO 实体对象
	 * @Throws
	 */
	AppRoleDO select(Long id);

	/**
	 * @Title select
	 * @Author zhongjyuan
	 * @Description 查询
	 * @Param @param ids 主键集合
	 * @Param @return
	 * @Return List<AppRoleDO> 实体对象集合
	 * @Throws
	 */
	List<AppRoleDO> select(List<Long> ids);

	/**
	 * @Title selectList
	 * @Author zhongjyuan
	 * @Description 查询VO对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<AppRoleVO> VO对象集合
	 * @Throws
	 */
	List<AppRoleVO> selectList(AppRoleQuery query);

	/**
	 * @Title selectBriefList
	 * @Author zhongjyuan
	 * @Description 查询简要对象集合
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return List<AppRoleBriefVO> 简要对象集合
	 * @Throws
	 */
	List<AppRoleBriefVO> selectBriefList(AppRoleBriefQuery query);

	/**
	 * @Title selectPage
	 * @Author zhongjyuan
	 * @Description 查询分页
	 * @Param @param query 查询条件对象
	 * @Param @return
	 * @Return PageResult<AppRolePageVO> 分页结果对象
	 * @Throws
	 */
	PageResult<AppRolePageVO> selectPage(AppRolePageQuery query);

}
