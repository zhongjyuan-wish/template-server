package zhongjyuan.wish.template.domain.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.wish.batis.pagehelper.CustomPage;
import zhongjyuan.wish.batis.utils.PageUtil;

import zhongjyuan.wish.configurate.global.GlobalConfigurate;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.utils.BeanUtil;
import zhongjyuan.domain.utils.EntityUtil;

import zhongjyuan.wish.plugin.framework.TreeManage;
import zhongjyuan.domain.*;
import zhongjyuan.domain.Constant;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.query.*;

import zhongjyuan.wish.template.domain.mapper.OrganizeMapper;
import zhongjyuan.wish.template.domain.service.IOrganizeService;

import zhongjyuan.wish.template.domain.model.entity.OrganizeDO;
import zhongjyuan.wish.template.domain.model.query.*;
import zhongjyuan.wish.template.domain.model.converter.*;
import zhongjyuan.wish.template.domain.model.vo.response.*;

import zhongjyuan.wish.template.domain.model.OrganizeCode;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName OrganizeServiceImpl
 * @Description 组织表服务实现类
 * @Author zhongjyuan
 * @Date 2023年11月15日 15:09:56
 * @Copyright Copyright (c) 2023 zhongjyuan.com
 */
@Slf4j
@Service
public class OrganizeServiceImpl implements IOrganizeService {

	@Autowired
	OrganizeMapper organizeMapper;

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeDO create(OrganizeDO entity) {

		// 校验是否支持创建
		createEnabled(entity);

		// 设置行号
		if (ObjectUtils.isEmpty(entity.getRowIndex())) {
			int maxRowIndex = organizeMapper.selectMaxRowIndex();
			entity.setRowIndex(maxRowIndex + GlobalConfigurate.wishCommonPageSpan());
		}

		// 创建
		organizeMapper.insert(entity);

		log.info("======OrganizeServiceImpl:create({})======", entity);
		return entity;
	}

	/**
	 * @Title createEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持创建
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void createEnabled(OrganizeDO entity) {

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效名称数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(OrganizeDO::getName, entity.getName());

		if (organizeMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(OrganizeCode.ORGANIZE_FOR_CREATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_CREATE_BY_NAME_EXISTS_WARN);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeDO update(OrganizeDO entity) {

		// 校验是否支持更新
		entity = updateEnabled(entity);

		// 根据主键更新
		organizeMapper.updateById(entity);

		log.info("======OrganizeServiceImpl:update({})======", entity);
		return entity;
	}

	/**
	 * @Title updateEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持更新
	 * @Param @param entity 实体对象
	 * @Param @return
	 * @Return OrganizeDO 实体对象
	 * @Throws
	 */
	private OrganizeDO updateEnabled(OrganizeDO entity) {

		// 获取原始数据
		OrganizeDO originEntity = select(entity.getId());
		if (originEntity == null) {
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (originEntity.getIsSystem()) {
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (originEntity.getIsDeleted()) {
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_IS_DELETED_TURE_WARN);
		}
		
		// region 相关业务校验

		// 是否存在父级
		Boolean parentFlag = ObjectUtils.isNotEmpty(entity.getParentId());		

		// 是否存在相同有效名称数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsDeleted, false);
		queryWrapper.lambda().eq(OrganizeDO::getName, entity.getName());

		if (organizeMapper.exists(queryWrapper)) {
			if (!parentFlag) {
				throw new BusinessException(OrganizeCode.ORGANIZE_FOR_UPDATE_BY_NAME_AND_PARENT_EXISTS_WARN);
			}
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_UPDATE_BY_NAME_EXISTS_WARN);
		}

		// endregion

		BeanUtil.copyPropertiesIgnoreNull(entity, originEntity);
		return originEntity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public OrganizeDO delete(Long id) {

		// 获取原始数据
		OrganizeDO originEntity = select(id);

		// 校验是否支持删除
		deleteEnabled(originEntity);

		// 逻辑删除处理
		EntityUtil.setDeletion(originEntity);

		// 根据主键更新
		organizeMapper.updateById(originEntity);

		log.info("======OrganizeServiceImpl:delete({})======", id);
		return originEntity;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class) /* [注意]方法事务 */
	public List<OrganizeDO> delete(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;

		// 定义集合
		List<OrganizeDO> entities = new ArrayList<OrganizeDO>();

		// 查询原始数据: 排除系统数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsSystem, false);
		queryWrapper.lambda().in(OrganizeDO::getId, ids);
		entities = organizeMapper.selectList(queryWrapper);
		if (CollectionUtils.isEmpty(entities)) {
			throw new BusinessException(ResponseCode.FAILURE);
		}

		// 定义集合
		List<OrganizeDO> successEntities = new ArrayList<OrganizeDO>();
		
		// 根据主键更新
		for (OrganizeDO entity : entities) {

			if (entity.getIsSystem()) {
				continue;
			}

			if (entity.getIsDeleted()) {
				continue;
			}
			EntityUtil.setDeletion(entity);
			
			organizeMapper.updateById(entity);
			
			successEntities.add(entity);
		}

		log.info("======OrganizeServiceImpl:delete({})======", ids);
		return successEntities;
	}
	
	/**
	 * @Title deleteEnabled
	 * @Author zhongjyuan
	 * @Description 是否支持删除
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	private void deleteEnabled(OrganizeDO entity) {
	
		if (entity == null) {
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_SELECT_NOT_EXISTS_WARN);
		}

		if (entity.getIsSystem()) {
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_IS_SYSTEM_TURE_WARN);
		}

		if (entity.getIsDeleted()) {
			throw new BusinessException(OrganizeCode.ORGANIZE_FOR_IS_DELETED_TURE_WARN);
		}
	}

	@Override
	public OrganizeDO select(Long id) {
		if (ObjectUtils.isEmpty(id))
			return null;
		
		return organizeMapper.selectById(id);
	}
	
	@Override
	public List<OrganizeDO> select(List<Long> ids) {
		if (CollectionUtils.isEmpty(ids))
			return null;
		
		return organizeMapper.selectBatchIds(ids);
	}
	
	@Override
	public List<OrganizeVO> selectList(OrganizeQuery query) {
		List<OrganizeVO> vos = new ArrayList<OrganizeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(OrganizeDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(OrganizeDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(OrganizeDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(OrganizeDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			vos = OrganizeConverter.entityToVos(entities);
		}

		return vos;
	}

	@Override
	public List<OrganizeBriefVO> selectBriefList(OrganizeBriefQuery query) {
		List<OrganizeBriefVO> briefVOs = new ArrayList<OrganizeBriefVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(OrganizeDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(OrganizeDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(OrganizeDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(OrganizeDO::getIsEnabled, query.getIsEnabled());
			}
			
			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			briefVOs = OrganizeConverter.entityToBriefVos(entities);
		}

		return briefVOs;
	}

	@Override
	public PageResult<OrganizePageVO> selectPage(OrganizePageQuery query) {

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(OrganizeDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(OrganizeDO::getParentName, query.getSearchValue())
;
				});
			}

			// 系统数据查询
			if (ObjectUtils.isNotEmpty(query.getIsSystem())) {
				queryWrapper.lambda().eq(OrganizeDO::getIsSystem, query.getIsSystem());
			}
			
			// 有效数据查询
			if (ObjectUtils.isNotEmpty(query.getIsEnabled())) {
				queryWrapper.lambda().eq(OrganizeDO::getIsEnabled, query.getIsEnabled());
			}
			
		}

		// 分页查询
		CustomPage<OrganizeDO> page = organizeMapper.selectPage(new CustomPage<OrganizeDO>(query, Constant.ROW_INDEX_L_A), queryWrapper);

		// 分页数据转换
		return PageUtil.convert(page, entities -> OrganizeConverter.entityToPageVos(entities));
	}

	@Override
	public List<OrganizeTreeVO> selectLazy(TreeLazyQuery<Long> query) {

		// 转成复杂查询条件对象
		TreeComplexQuery<Long> complexQuery = new TreeComplexQuery<Long>();
		BeanUtil.copyProperties(query, complexQuery);

		return selectChildrens(complexQuery);
	}

	@Override
	public List<OrganizeTreeVO> selectTree(TreeQuery<Long> query) {
		List<OrganizeTreeVO> treeVOs = new ArrayList<OrganizeTreeVO>();

		// 原始数据查询: 排除删除数据
		QueryWrapper<OrganizeDO> queryWrapper = new QueryWrapper<OrganizeDO>();
		queryWrapper.lambda().eq(OrganizeDO::getIsDeleted, false);

		// 查询条件
		if (query != null) {
			// 关键字搜索
			if (StringUtils.isNotBlank(query.getSearchValue())) {
				queryWrapper.lambda().and(wrapper -> {
					// 描述
					wrapper.like(OrganizeDO::getDescription, query.getSearchValue())
							// 名称
							.or().like(OrganizeDO::getName, query.getSearchValue())
							// 父级名称
							.or().like(OrganizeDO::getParentName, query.getSearchValue())
;
				});
			}

			// 数据排序
			PageUtil.wrapperSort(queryWrapper, query);
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectList(queryWrapper);
		if (!CollectionUtils.isEmpty(entities)) {
			treeVOs = OrganizeConverter.entityToTreeVos(entities);
			treeVOs = (List<OrganizeTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, OrganizeTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<OrganizeTreeVO> selectParents(TreeComplexQuery<Long> query) {
		List<OrganizeTreeVO> treeVOs = new ArrayList<OrganizeTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectParents(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = OrganizeConverter.entityToTreeVos(entities);
			treeVOs = (List<OrganizeTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, OrganizeTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<OrganizeTreeVO> selectChildrens(TreeComplexQuery<Long> query) {
		List<OrganizeTreeVO> treeVOs = new ArrayList<OrganizeTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectChildrens(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = OrganizeConverter.entityToTreeVos(entities);
			treeVOs = (List<OrganizeTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, OrganizeTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<OrganizeTreeVO> selectLeafs(TreeComplexQuery<Long> query) {
		List<OrganizeTreeVO> treeVOs = new ArrayList<OrganizeTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectLeafs(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = OrganizeConverter.entityToTreeVos(entities);
			treeVOs = (List<OrganizeTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, OrganizeTreeVO.class);
		}

		return treeVOs;
	}

	@Override
	public List<OrganizeTreeVO> selectFulls(TreeComplexQuery<Long> query) {
		List<OrganizeTreeVO> treeVOs = new ArrayList<OrganizeTreeVO>();

		// 处理显示层级条件
		if (ObjectUtils.isEmpty(query.getShowLevel() == null ? null : query.getShowLevel().intValue())) {
			query.setShowLevel(1L);
		}

		// 处理显示兄弟节点条件
		if (ObjectUtils.isEmpty(query.getShowSiblings())) {
			query.setShowSiblings(false);
		}

		// 处理显示命中节点条件
		if (ObjectUtils.isEmpty(query.getShowHitNodes())) {
			query.setShowHitNodes(false);
		}

		// 处理命中节点条件
		if (CollectionUtils.isNotEmpty(query.getHitNodeIds())) {
			query.setHitNodeIdString(query.getHitNodeIds());
		}

		Long rootId = query.getRootId();
		Integer showLevel = query.getShowLevel() == null ? null : query.getShowLevel().intValue();

		// 查询数据
		List<OrganizeDO> entities = organizeMapper.selectFulls(query);
		if (CollectionUtils.isNotEmpty(entities)) {
			treeVOs = OrganizeConverter.entityToTreeVos(entities);
			treeVOs = (List<OrganizeTreeVO>) TreeManage.pack(treeVOs, rootId, showLevel, OrganizeTreeVO.class);
		}

		return treeVOs;
	}
}
